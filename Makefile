C = $(IBASE)/bin/c

PERL_FLAGS = $(shell perl -MConfig -e 'print "-I$$Config{archlib}/CORE $$Config{ccflags}"')

all: check src/amq_helper lib/cutils.so lib/wireapi.so

check:
	@if [[ -z "$$IBASE" ]]; then \
	echo 'The IBASE environment variable is not defined.'; \
	exit 1; \
	fi

src/amq_helper: src/amq_helper.c
	cd $(dir $<) && $(C) -l $(notdir $<)

src/%_wrap.c: src/%.i
	swig -perl5 -outdir lib $<

lib/%.so: src/%_wrap.c
	cd $(dir $<) && $(C) -l -shared $(PERL_FLAGS) $(notdir $<)
	mv $(basename $<) $@

util/drop_caches: src/drop_caches.c
	gcc -Wall $< -o $@

clean:
	rm -f src/*.o src/*_wrap.c lib/*.so src/amq_helper

.PHONY: check clean
