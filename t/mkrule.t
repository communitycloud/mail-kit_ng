#!/usr/bin/perl

use strict;
use warnings;

use Util::String qw(unescape_chars);
use Test::More tests => 40;
use MIME::Parser;
use Scalar::Util qw(reftype);
use FindBin;
use Encode;

BEGIN {
	use_ok('MailKit::Rule', 'mk_match_rule')
		or exit 1;
}

isa_ok(MailKit::Rule->new, 'MailKit::Rule')
	or exit 1;

my $parser = MIME::Parser->new;
$parser->output_to_core(1);
$parser->extract_nested_messages(0);

sub test_rule($$;$)
{
	my ($obj, $pattern, $negate) = @_;
	my $r = MailKit::Rule->new();
	my $p = MailKit::CommandParser->new($r);
	$p->input_line($pattern);
	my $name = ref($obj) && reftype($obj) eq 'HASH' && $obj->{test_name}
		? $obj->{test_name} : $obj;
	$obj = unescape_chars($obj, 'nrt') unless ref $obj;
	my $matches = $r->match($obj);
	unless ($negate) {
		ok($matches, "$name MATCHES $pattern");
	} else {
		ok(!$matches, "$name DOESN'T MATCH $pattern");
	}
}

sub test_parts($$$)
{
	my ($obj, $rule, $count) = @_;
	my $n = grep { mk_match_rule($rule, $_) } $obj->parts_DFS;
	is($n, $count, "$obj->{test_name} PARTS MATCH rule $rule $count TIME(S)");
}

sub open_mime($)
{
	my $name = shift;
	my $mime = $parser->parse_open("$FindBin::Bin/mkrule/$name");
	$mime->{test_name} = "MIME($name)";
	$mime;
}

test_rule('Hello, World!', "match 'Hello, World!'");
test_rule('Hello, World!', 'match "hello, world!"i');
test_rule('Hello, World!', 'match /^hello, world!$/i');
test_rule('Hello, World!', 'match ?Hello, world?C', 'negate');
test_rule('Hello,\n World!\n', 'match ~ hello world');
test_rule('Hello,\n World!\n', 'match ?hello world?la');
test_rule('example.pdf', 'match glob(*.txt *.pdf *.doc)');
test_rule('example.pdf.zip', 'match ?*.txt *.pdf *.doc?s', 'negate');

my $m1 = open_mime('aYm49cVO');
test_rule($m1, 'header from ~ Damir Simunic');
test_rule($m1, 'header from ~ Damir Simuni', 'negate');
test_rule($m1, 'mail from damir.simunic@gmail.com');
test_rule($m1, 'mail to *@knowledge-gateway.org');
test_rule($m1, 'header Subject ~a few attachments');
test_rule($m1, 'mime attachment *.eml');
test_rule($m1, 'mime attachment pronouncible.html');
test_rule($m1, 'mime type multipart/mixed');
test_rule($m1, 'rule spam', 'negate');
test_parts($m1, 'attachment', 2);

my $m2 = open_mime('scrVnw6j');
test_rule($m2, 'header from ~ joe blow');
test_rule($m2, 'header subject ~ one with pictures pdf and a few other');
test_rule($m2, 'mail from joeblow68@gmail.com');
test_rule($m2, 'mime attachment 6t5hy3li.eml');
test_rule($m2, 'rule authentication-verified');
test_rule($m2, 'rule spam', 'negate');
test_parts($m2, 'attachment', 2);

my $m3 = open_mime('vXyt9KKT');
test_rule($m3, 'header from ~ jugrenno@gmail.com');
test_rule($m3, decode('utf8', 'header Subject ~ reunión en bogotá'));
test_rule($m3, 'mime type multipart/alternative');
test_rule($m3, 'rule authentication-verified');
test_rule($m3, 'rule spam', 'negate');
test_parts($m3, 'attachment', 0);

my $m4 = open_mime('dCfVn4nw');
test_rule($m4, 'body line "This is a test mailing"');
test_rule($m4, 'body text ~ this is a test mailing');
test_rule($m4, 'rule authentication-forged');
test_rule($m4, 'rule spam');
test_parts($m4, 'attachment', 0);

my $m5 = open_mime('95jkuzUh');
test_rule($m5, 'rule autoresponse');

my $m6 = open_mime('GohOdI9q.meta');
test_rule($m6, 'rule sendmail-skiplist');
