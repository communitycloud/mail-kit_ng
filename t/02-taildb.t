#!/usr/bin/perl

use strict;
use warnings;

use Test::More tests => 4;
use File::Temp qw/tempfile/;

my $sep = "\0";
my ($fh, $file) = tempfile(CLEANUP => 1);
my ($nrecords, $gaps, $nread, $last) = (10_000, 0, 0);

require_ok('Tail::DB') or exit 1;

my $r = Tail::DB->new($file, sep => $sep, timeout => 1, type => 'tail');
isa_ok($r, 'Tail::DB');

if (fork == 0) {
	open my $db, '>', $file
		or die "Couldn't open $file: $!";
	for (my $i = 1; $i <= $nrecords; $i ++) {
		if ($i % 1000 == 0) {
			select(undef, undef, undef, 0.1);
			unlink $file
				or die "Couldn't unlink $file: $!";
			open $db, '>', $file
				or die "Couldn't open $file: $!";
		}
		syswrite $db, "$i$sep";
	}
	exit;
}

while (defined (my $rec = $r->read)) {
	$nread ++;
	$gaps ++ if defined $last && $last != $rec - 1;
	$last = $rec;
	last if $rec == $nrecords;
}

is($gaps, 0, 'No gaps');
is($nread, $nrecords, 'All read');
