#!/usr/bin/perl

use strict;
use warnings;

BEGIN {
	use Test::More tests => 2;
	use_ok('App::Proc', qw(app_fork app_wait register_signal));
}

register_signal(CHLD => 'AUTO');

my $kids = 0;

for (1 .. 1000) {
	app_fork(
		reap => sub { $kids-- },
		job => sub { 0 });
	$kids++;
	app_wait(-1);
}

while ($kids) {
	sleep;
}

ok(1, 'no race');
