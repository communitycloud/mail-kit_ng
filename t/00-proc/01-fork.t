#!/usr/bin/perl

use strict;
use warnings;

BEGIN {
	use Test::More tests => 3;
	use_ok('App::Proc', qw(app_fork app_wait $parent_pipe));
}

my $factor = $ENV{TEST_PROC_FACTOR} || 1;

my $forks = 0;
my $reaps = 0;
my $runs = 5;
my $reads = 0;

my $job = sub {
	print $parent_pipe "ok $_\n";
};

my $reap = sub { $reaps ++ };
my @pipes = ();

for (1 .. 10 * $factor) {
	app_fork(
		job => $job,
		runs => $runs,
		reap => $reap,
		pipe => \my $pipe,
	) && $forks ++;
	push @pipes, $pipe;
}

for my $pipe (@pipes) {
	while (<$pipe>) {
		$reads ++ if /ok/;
	}
}

1 while (app_wait > 0);

is($forks, $reaps, "reaped all $forks forks");
is($reads, $forks * $runs, "read all $reads states");
