#!/usr/bin/perl

use strict;
use warnings;

my $factor = $ENV{TEST_PROC_FACTOR} || 1;
my $sock = "\0$0.$$";
my %prefork = (
	min_kids => 2 * $factor,
	min_idle => 1 * $factor,
	max_idle => 4 * $factor,
	max_kids => 8 * $factor,
);

pipe my ($read_stats, $write_stats);

my $pid = fork();
if ($pid == 0) {
	goto SERVER;
} else {
	goto TEST;
}

TEST:

use Test::More tests => 12;

$read_stats->blocking(0);
close $write_stats;

my ($n, $i) = (-1, -1);

sub settle()
{
	select undef, undef, undef, 0.250;
}

sub read_stats()
{
	while (my $stats = readline $read_stats) {
		chomp $stats;
		($n, $i) = split ' ', $stats;
	}
}

sub check
{
	settle();
	read_stats();
	cmp_ok($n, '>=', $prefork{min_kids}, "$n vs $prefork{min_kids} min kids (@_)");
	cmp_ok($i, '>=', $prefork{min_idle}, "$i vs $prefork{min_idle} min idle (@_)");
	cmp_ok($i, '<=', $prefork{max_idle}, "$i vs $prefork{max_idle} max idle (@_)");
}

check("init");

my @c;
for (1.. 6 * $factor) {
	my $c = IO::Socket::UNIX->new($sock)
		or die "connect: $!";
	push @c, $c;
	$c->print("busy\n");
}

check(scalar @c, 'busy');

for my $c (@c) {
	$c->print("idle\n");
}

check(scalar @c, 'idle');

my $closed = 0;
for my $c (@c) {
	local $SIG{PIPE} = sub { $closed++ };
	$c->print("quit\n");
}

check($closed, 'killed');

kill TERM => $pid;
wait;
exit;

SERVER:

use IO::Socket;
use App::Proc qw(:prefork);
use App::Yield qw(yield_hook);

my $server = IO::Socket::UNIX->new(
	Local => $sock,
	Listen => 65536)
or die "bind: $!";

$write_stats->autoflush(1);
close $read_stats;

my $current_stats;
my $print_stats = sub {
	my $n = scalar keys %App::Proc::kids;
	my $i = grep $_->{state} eq 'idle', values %App::Proc::kids;
	my $stats = "$n $i";
	unless ($current_stats && $current_stats eq $stats) {
		print $write_stats $stats, "\n";
	}
};

my $job = sub {
	my $c = $server->accept();
	while (my $cmd = readline $c) {
		chomp $cmd;
		if ($cmd eq 'busy') {
			child_proc_busy();
		} elsif ($cmd eq 'idle') {
			child_proc_idle();
		} elsif ($cmd eq 'quit') {
			last;
		}
	}
	0;
};

$SIG{TERM} = sub {
	kill HUP => keys %App::Proc::kids;
	exit;
};

app_prefork(%prefork,
	job => $job,
	post_fork_hook => $print_stats,
);
