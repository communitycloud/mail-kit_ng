#!/usr/bin/perl

use strict;
use warnings;

use Test::More tests => 5;
use App::Daemon::Control qw(daemon_control);

use FindBin;
my $dummyd = "$FindBin::Bin/daemon/dummyd";
my $control = "$FindBin::Bin/daemon/control";

require_ok('App::Daemon_ng') or exit 1;
ok(system($dummyd, "--daemon-start", "--control=$control") == 0, 'starting daemon');
ok(daemon_control("running", $control) == 1, 'daemon running');
ok(system($dummyd, "--daemon-stop", "--control=$control") == 0, 'stopping daemon');
ok(daemon_control("running", $control) == 0, 'daemon stopped');
