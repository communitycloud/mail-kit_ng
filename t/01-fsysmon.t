#!/usr/bin/perl

use strict;
use warnings;

use Test::More tests => 24;

require_ok('Filesystem::Monitor') or exit 1;
isa_ok(my $m = new Filesystem::Monitor, 'Filesystem::Monitor');

use File::Temp qw/tempdir/;
my $dir = tempdir(CLEANUP => 1);

open my $h, ">$dir/a" or die "open \$dir/a: $!";
ok(my $w = $m->watch("$dir/a"), 'Watch existsing file');

syswrite $h, "XXXX";
$h->flush();

my @ev = $m->wait(.1);
is(scalar @ev, 1, 'Got one event after writing');
is($ev[0]->{type}, 'modify', 'Event is of correct type');
is($ev[0]->{path}, "$dir/a", 'Event has correct path');

$w->cancel();
syswrite $h, "XXXX";
$h->flush();
@ev = $m->wait(.1);
is(scalar @ev, 0, 'Watcher cancelled');

ok($m->watch("$dir/b"), 'Watch missing file');
open $h, ">$dir/b";
@ev = $m->wait(.1);
is(scalar @ev, 1, 'Got one event after creation');
is($ev[0]->{type}, 'create', 'Event is of correct type');
is($ev[0]->{path}, "$dir/b", 'Event has correct path');

ok($m->watch("$dir/x/y"), 'Watch missing directory');
mkdir "$dir/x";
@ev = $m->wait(.1);
is(scalar @ev, 0, 'No bogus event after directory creation');

open $h, ">$dir/x/y";
@ev = $m->wait(.1);
is(scalar @ev, 1, 'Got one event after creation');
is($ev[0]->{type}, 'create', 'Event is of correct type');
is($ev[0]->{path}, "$dir/x/y", 'Event has correct path');

close $h;
unlink "$dir/x/y";
rmdir "$dir/x";
@ev = $m->wait(.1);
is(scalar @ev, 1, 'Got one event after deletion');
is($ev[0]->{type}, 'delete', 'Event is of correct type');
is($ev[0]->{path}, "$dir/x/y", 'Event has correct path');

ok($m->watch("$dir/1/2/3/f"), 'Watch in depth file');
mkdir "$dir/1";
mkdir "$dir/1/2";
mkdir "$dir/1/2/3";
@ev = $m->wait(.1);
is(scalar @ev, 0, 'No bogus event after directory creation');
open $h, ">$dir/1/2/3/f";
@ev = $m->wait(.1);
is(scalar @ev, 1, 'Got one event after creation');
is($ev[0]->{type}, 'create', 'Event is of correct type');
is($ev[0]->{path}, "$dir/1/2/3/f", 'Event has correct path');
