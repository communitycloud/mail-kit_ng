#!/usr/bin/perl

use strict;
use warnings;

use Time::HiRes qw(time);

{ $| ++ }

while (<STDIN>) {
	print time, " ", $_;
}
