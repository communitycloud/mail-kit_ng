#!/usr/bin/perl

use strict;
use warnings;

use FindBin;
use lib "$FindBin::Bin/lib";

use WireAPI;
use AMQ::Relay;
use MailEv::Message;
use MailEv::Message::Log;
use Getopt::Long qw(:config no_ignore_case bundling);

my $version = "0.02";

sub parse_connection($)
{
	my $s = shift;

	if (my ($user, $pass, $host) = $s =~ /^(.+):(.+)@(.+)$/) {
		return ($host, $user, $pass);
	}

	return ($s);
}

sub main()
{
	my ($help, $log_file, $log_level);

	GetOptions(
		"log-file=s"        => \$log_file,
		"log-level=s"       => \$log_level,
		"help|h"            => \$help,
	) or usage_error();

	if (!@ARGV || $help) {
		help();
		exit();
	}

	unless (@ARGV == 5) {
		usage_error("invalid number of arguments");
	}

	my ($src, $dst, $exchange, $exchange_type, $routing_key) = @ARGV;

	my ($host_src, $user_src, $pass_src) = parse_connection($src);
	my ($host_dst, $user_dst, $pass_dst) = parse_connection($dst);

	$0 = "$program_name $host_src > $host_dst";

	if ($log_file) {
		log_open($log_file, $log_level);
	} elsif ($log_level) {
		add_message_sink(undef, $log_level);
	}

	AMQ::Relay::configure_src($host_src, $user_src, $pass_src);
	AMQ::Relay::configure_dst($host_dst, $user_dst, $pass_dst);
	AMQ::Relay::configure_binding($exchange, $exchange_type, $routing_key);
	AMQ::Relay::run();
}

sub help()
{
	print <<"HELP"
Usage: $program_name [OPTIONS] SRC DST EXCHANGE TYPE ROUTING-KEY

Relay AMQ messages

	SRC specifies source connection. DST specifies destination connection.
	EXCHANGE specifies exchange name. TYPE specifies exchange type.
	ROUTING-KEY specifies routing key to bind to.

	Connection strings are specified as USERNAME:PASSWORD\@HOST

Options are:

	--log-file=FILE       Log file
	--log-level=LEVEL     Log level
	--help                This message

HELP
}

main();
