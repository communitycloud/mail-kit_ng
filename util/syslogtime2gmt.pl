#!/usr/bin/perl

use strict;
use warnings;

use Time::Local;

my %syslog_months = (
	'Jan' => 0, 'Feb' =>  1, 'Mar' =>  2,
	'Apr' => 3, 'May' =>  4, 'Jun' =>  5,
	'Jul' => 6, 'Aug' =>  7, 'Sep' =>  8,
	'Oct' => 9, 'Nov' => 10, 'Dec' => 11,
);
my %syslog_months_map = reverse %syslog_months;

my $current_year = $ENV{CURRENT_YEAR} || (localtime)[5];
my $previous_time;

while (<>) {
	chomp;
	my ($month, $day, $hh, $mm, $ss, $line) = /^(\S{3})\s+(\d+)\s(\d+):(\d+):(\d+) (.*)/ or die "Bad line: $_\n";
	my $time = timelocal($ss, $mm, $hh, $day, $syslog_months{$month}, $current_year);
	if ($previous_time && $previous_time > $time) {
		$time = timelocal($ss, $mm, $hh, $day, $syslog_months{$month}, ++$current_year);
	}
	$previous_time = $time;
	my @time = gmtime $time;
	printf "%s %2d %02d:%02d:%02d %s\n", $syslog_months_map{$time[4]}, @time[3,2,1,0], $line;
}
