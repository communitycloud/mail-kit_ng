#!/usr/bin/perl

use strict;
use warnings;

use Time::HiRes qw(sleep);

my $last;
my $speed = shift || 1;

{ $| ++ }

while (<STDIN>) {
	my ($time, $line) = split(' ', $_, 2);

	sleep (($time - $last) * $speed)
		if $last;
	print $line;
	$last = $time;
}
