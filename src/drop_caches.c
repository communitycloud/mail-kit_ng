/*
 * filename: util/drop_caches.c
 * created:  Sun 31 Jan 2010 12:47:21 AM EET
 */

#define _XOPEN_SOURCE 600
#define _FILE_OFFSET_BITS 64
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

#define PROGRAM_NAME "drop_caches"

int main(int argc, char **argv)
{
	int fd;

	if (argc != 2) {
		fprintf(stderr, "Usage: %s <filename>\n", PROGRAM_NAME);
		exit(1);
	}

	fd = open(argv[1], O_RDONLY);
	if (fd < 0) {
		perror(PROGRAM_NAME ": open");
		exit(1);
	}

	if (fdatasync(fd) != 0) {
		perror(PROGRAM_NAME ": fdatasync");
		exit(1);
	}

	if (posix_fadvise(fd, 0, 0, POSIX_FADV_DONTNEED)) {
		perror(PROGRAM_NAME ": fadvise");
		exit(1);
	}

	close(fd);
	exit(0);
}
