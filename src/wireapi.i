%module wireapi
%include cpointer.i
%include stdint.i

%rename(amq_client_connection_destroy) amq_client_connection_destroy_wrap;
%rename(amq_client_session_destroy) amq_client_session_destroy_wrap;
%rename(amq_content_basic_unlink) amq_content_basic_unlink_wrap;
%rename(asl_field_list_next) asl_field_list_next_wrap;
%rename(asl_field_list_destroy) asl_field_list_destroy_wrap;
%rename(icl_longstr_destroy) icl_longstr_destroy_wrap;

%{
#include "wireapi.h"
void amq_client_connection_destroy_wrap(amq_client_connection_t *connection)
{
    amq_client_connection_destroy(&connection);
}

void amq_client_session_destroy_wrap(amq_client_session_t *session)
{
    amq_client_session_destroy(&session);
}

void amq_content_basic_unlink_wrap(amq_content_basic_t *content)
{
    amq_content_basic_unlink(&content);
}

asl_field_t *asl_field_list_next_wrap(asl_field_t *field)
{
    return asl_field_list_next(&field);
}

void asl_field_list_destroy_wrap(asl_field_list_t *list)
{
    asl_field_list_destroy(&list);
}

void icl_longstr_destroy_wrap(icl_longstr_t *str)
{
    icl_longstr_destroy(&str);
}

%}

%immutable;
typedef struct {
    int alive, silent, interrupt, shutdown;
    int trace, timeout;
    char *host;
    char *error_text;
    int direct;
    unsigned char reply_code;
    char *reply_text;
} amq_client_connection_t;

typedef struct {
    int64_t body_size;
    unsigned char *body_data;
    char *exchange, *routing_key, *consumer_tag, *producer_id;
    char *content_type, *content_encoding, *correlation_id, *reply_to;
    char *expiration, *message_id, *type, *user_id, *app_id, *sender_id;
    icl_longstr_t *headers;
    unsigned char priority, delivery_mode;
    int64_t timestamp;
} amq_content_basic_t;

typedef struct {
    char *name;
    unsigned char type;
} asl_field_t;

typedef enum {
    ICL_CONSOLE_DATE,
    ICL_CONSOLE_TIME,
    ICL_CONSOLE_MILLI,
    ICL_CONSOLE_MICRO,
    ICL_CONSOLE_THREAD,
    ICL_CONSOLE_FLUSH,
    ICL_CONSOLE_QUIET
} icl_console_option_t;

typedef struct {
    unsigned char channel_nbr;
    int alive, active;
    char *error_text, *chrono_block;
    unsigned char reply_code;
    char *reply_text;
    char *consumer_tag, *exchange, *routing_key;
    int64_t delivery_tag;
    int redelivered;
    int32_t message_count, consumer_count;
    char *sender_id, *queue;
} amq_client_session_t;
%mutable;

void icl_console_mode(icl_console_option_t attr, int value);
void icl_system_initialise(int argc, char **argv);
void icl_system_terminate(void);

icl_longstr_t *amq_client_connection_auth_plain(char *login, char *password);

amq_client_connection_t *amq_client_connection_new(char *host, char *virtual_host, icl_longstr_t *auth_data, char *instance, int trace, int timeout);
void amq_client_connection_destroy_wrap(amq_client_connection_t *connection);
amq_client_session_t *amq_client_session_new(amq_client_connection_t *);
void amq_client_session_destroy_wrap(amq_client_session_t *);

int amq_client_session_exchange_declare(amq_client_session_t *session, int ticket, char *exchange, char *type, int passive, int durable, int auto_delete, int internal, icl_longstr_t *arguments);
int amq_client_session_queue_declare(amq_client_session_t *session, int ticket, char *queue, int passive, int durable, int exclusive, int auto_delete, icl_longstr_t *arguments);
int amq_client_session_queue_bind(amq_client_session_t *session, int ticket, char *queue, char *exchange, char *routing_key, icl_longstr_t *arguments);
int amq_client_session_basic_consume(amq_client_session_t *session, int ticket, char *queue, char *consumer_tag, int no_local, int no_ack, int exclusive, icl_longstr_t *arguments);
int amq_client_session_basic_ack(amq_client_session_t *session, int64_t delivery_tag, int multiple);

amq_content_basic_t *amq_client_session_basic_arrived(amq_client_session_t *session);
int amq_client_session_wait(amq_client_session_t *session, int timeout);

int amq_content_basic_get_body(amq_content_basic_t *content, unsigned char *buffer, size_t maxsize);

asl_field_list_t *asl_field_list_new(icl_longstr_t *field_table);
asl_field_t *asl_field_list_first(asl_field_list_t *list);
asl_field_t *asl_field_list_next_wrap(asl_field_t *item);
char *asl_field_string(asl_field_t *field);
void asl_field_list_destroy_wrap(asl_field_list_t *list);
void icl_longstr_destroy_wrap(icl_longstr_t *str);
icl_longstr_t *asl_field_list_flatten(asl_field_list_t *list);
void asl_field_new_string(asl_field_list_t *list, char *name, char *value);

int amq_client_session_basic_publish(amq_client_session_t *session, amq_content_basic_t* content, int ticket, char *exchange, char *routing_key, int mandatory, int immediate);

amq_content_basic_t* amq_content_basic_new(void);
int amq_content_basic_set_body(amq_content_basic_t* content, char *data, size_t size, void *freefn);
int amq_content_basic_set_headers(amq_content_basic_t* content, icl_longstr_t *headers);
int amq_content_basic_set_message_id(amq_content_basic_t *content, char *value);
int amq_content_basic_set_content_type(amq_content_basic_t *content, char *value);
int amq_content_basic_set_content_encoding(amq_content_basic_t *content, char *value);
int amq_content_basic_set_app_id(amq_content_basic_t *content, char *value);
int amq_content_basic_set_correlation_id(amq_content_basic_t *content, char *value);
int amq_content_basic_set_reply_to(amq_content_basic_t *content, char *value);
int amq_content_basic_set_priority(amq_content_basic_t *content, int value);
int amq_content_basic_set_expiration(amq_content_basic_t *content, char *value);
int amq_content_basic_set_delivery_mode(amq_content_basic_t *content, int value);
int amq_content_basic_set_sender_id(amq_content_basic_t *content, char *value);
int amq_content_basic_set_timestamp(amq_content_basic_t *content, int64_t value);
int amq_content_basic_set_type(amq_content_basic_t *content, char *value);
int amq_content_basic_set_user_id(amq_content_basic_t *content, char *user_id);
void amq_content_basic_unlink_wrap(amq_content_basic_t *content);
