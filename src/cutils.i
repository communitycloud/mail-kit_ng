%module cutils
%include carrays.i
%include cmalloc.i

%{
char *unsigned2char(unsigned char *byte)
{
    return (char *)byte;
}
%}

%array_functions(char *, charp_array);
%array_functions(unsigned char, byte_array);
%malloc(unsigned char, byte);
%free(unsigned char, byte);
char *unsigned2char(unsigned char *byte);
