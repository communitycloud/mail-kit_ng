/*
 * filename: amq_helper.c
 * created:  Sun 09 Aug 2009
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "wireapi.h"

#define PROGRAM "amq-helper"
#define VERSION "0.06"
#define HELP                                                                 \
	"Usage: " PROGRAM " [OPTIONS] COMMAND [ARGUMENTS]\n"                 \
	"\n"                                                                 \
	"Commands:\n"                                                        \
	"  send <PROP>=<VAL>  Send the message to the server\n"              \
	"  dump               Dump the messages to stdout\n"                 \
	"  test               Test connection and exchange\n"                \
	"Integration commands:\n"                                            \
	"  .identify           Identify the program and version\n"           \
	"  .ecodes             Print the exit codes table\n"                 \
	"\n"                                                                 \
	"Options:\n"                                                         \
	"  -s server[:port]   Name or address, port of server (localhost)\n" \
	"  -u username        Username to login (guest)\n"                   \
	"  -p password        Password to login (guest)\n"                   \
	"  -e exchange        Exchange name (amq.direct)\n"                  \
	"  -E exchange        Create the exchange unless already exists\n"   \
	"  -k key             Routing key (.)\n"                             \
	"  -t type            Exchange type (direct)\n"                      \
	"  -h                 Show summary of command-line options\n"        \
	"\n"

enum {
	AMQ_CODE_BAD_USAGE = 10,
	AMQ_CODE_BAD_ARGUMENTS,
	AMQ_CODE_UNKNOWN_PROPERTY,
	AMQ_CODE_EXCHANGE_FAILURE,
	AMQ_CODE_CONNECTION_FAILURE,
	AMQ_CODE_CONNECTION_BROKEN,
	AMQ_CODE_INTERRUPT,
};

// Current connection
amq_client_connection_t *connection = NULL;
// Current session
amq_client_session_t *session = NULL;

/* Exit gracefully by calling the appropriate cleanup functions */
static void terminate(int status)
{
	amq_client_connection_destroy(&connection);
	amq_client_session_destroy(&session);
	icl_system_terminate();
	exit(status);
}

/* Dumps the content to stream in the same format understood by pipe_messages() */
static void dump_content(amq_content_basic_t *content, FILE *stream)
{
	asl_field_list_t *field_list;
	asl_field_t *field;
	size_t size;
	byte *message;

	size = content->body_size;
	message = malloc(size + 1);
	assert(message);
	amq_content_basic_get_body(content, message, size);
	message[size] = '\0';

	if (content->message_id && *content->message_id)
		fprintf(stream, "Message-ID: %s\n", content->message_id);
	if (content->exchange && *content->exchange)
		fprintf(stream, "Exchange: %s\n", content->exchange);
	if (content->routing_key && *content->routing_key)
		fprintf(stream, "Routing-Key: %s\n", content->routing_key);
	if (content->app_id && *content->app_id)
		fprintf(stream, "App-ID: %s\n", content->app_id);
	if (content->user_id && *content->user_id)
		fprintf(stream, "User-ID: %s\n", content->user_id);
	if (content->sender_id && *content->sender_id)
		fprintf(stream, "Sender-ID: %s\n", content->sender_id);
	if (content->correlation_id && *content->correlation_id)
		fprintf(stream, "Correlation-ID: %s\n", content->correlation_id);
	if (content->reply_to && *content->reply_to)
		fprintf(stream, "Reply-To: %s\n", content->reply_to);
	if (content->content_type && *content->content_type)
		fprintf(stream, "Content-Type: %s\n", content->content_type);
	if (content->content_encoding && *content->content_encoding)
		fprintf(stream, "Content-Encoding: %s\n", content->content_encoding);
	if (content->timestamp)
		fprintf(stream, "Timestamp: %jd\n", content->timestamp);
	if (content->headers) {
		field_list = asl_field_list_new(content->headers);
		field = asl_field_list_first(field_list);
		while (field) {
			fprintf(stream, "X-%s: %s\n", field->name, asl_field_string(field));
			field = asl_field_list_next(&field);
		}
		asl_field_list_destroy(&field_list);

	}

	if (size)
	{
		fprintf(stream, "Content-Length: %d\n", size);
		fprintf(stream, "\n");
		fprintf(stream, "%s\n", message);
		fprintf(stream, "\n");
	} else
		fprintf(stream, "\n");

	fflush(stream);
	free(message);
}

/* Loops forever and dumps to stdout the messages matching exchange and routing_key */
static void dump_messages(char *exchange, char *routing_key)
{
	amq_content_basic_t *content;

	//  Create a private queue
	amq_client_session_queue_declare(session, 0, NULL, FALSE, FALSE, TRUE, TRUE, NULL);

	//  Bind the queue to the exchange
	amq_client_session_queue_bind(session, 0, NULL, exchange, routing_key, NULL);

	// Consume from the queue
	amq_client_session_basic_consume(session, 0, NULL, NULL, TRUE, TRUE, TRUE, NULL);

	while (connection->alive) {
		// Dump all available content
		while ((content = amq_client_session_basic_arrived(session)))
		{
			dump_content(content, stdout);
			amq_content_basic_unlink(&content);
		}

		// Wait forever until a message arrives
		amq_client_session_wait (session, 0);
	}

	if (connection->interrupt)
		terminate(AMQ_CODE_INTERRUPT);
	else
		terminate(AMQ_CODE_CONNECTION_BROKEN);
}

/* Utility to build message content from property/value strings */
static Bool set_content_property(amq_content_basic_t *content, char *property, char *value)
{
	if (strcasecmp(property, "body") == 0)
		amq_content_basic_set_body(content, value, strlen(value), NULL);
	else if (strcasecmp(property, "content-type") == 0)
		amq_content_basic_set_content_type(content, value);
	else if (strcasecmp(property, "content-encoding") == 0)
		amq_content_basic_set_content_encoding(content, value);
	else if (strcasecmp(property, "app-id") == 0)
		amq_content_basic_set_app_id(content, value);
	else if (strcasecmp(property, "correlation-id") == 0)
		amq_content_basic_set_correlation_id(content, value);
	else if (strcasecmp(property, "reply-to") == 0)
		amq_content_basic_set_reply_to(content, value);
	else if (strcasecmp(property, "message-id") == 0)
		amq_content_basic_set_message_id(content, value);
	else if (strcasecmp(property, "priority") == 0)
		amq_content_basic_set_priority(content, atoi(value));
	else
		return FALSE;
	return TRUE;
}

/* Send a message to the specified exchange built from the properties supplied
 * at command line */
static void send_message(char *exchange, char *routing_key, char **argv, int argc)
{
	int i;
	char *arg, *sep, *value;
	amq_content_basic_t *content;
	asl_field_list_t *field_list = NULL;
	icl_longstr_t *headers;
	
	content = amq_content_basic_new();
	assert(content);

	amq_content_basic_set_app_id(content, PROGRAM);

	for (i = 0; i < argc; i++) {
		arg = argv[i];
		sep = strchr(arg, '=');

		if (!sep) {
			fprintf(stderr, "%s: Argument not in KEY=VALUE format: %s\n",
					PROGRAM, arg);
			terminate(AMQ_CODE_BAD_ARGUMENTS);
		}

		value = sep + 1;
		*sep = '\0';

		if (strncasecmp(arg, "X-", 2) == 0) {
			if (!field_list)
				field_list = asl_field_list_new(NULL);
			asl_field_new_string(field_list, arg + 2, value);
		}
		else if (strcasecmp(arg, "exchange") == 0)
			exchange = value;
		else if (strcasecmp(arg, "routing-key") == 0)
			routing_key = value;
		else if (!set_content_property(content, arg, value)) {
			fprintf(stderr, "%s: Unknown property: %s\n",
					PROGRAM, arg);
			terminate(AMQ_CODE_UNKNOWN_PROPERTY);
		}
	}

	if (field_list)
	{
		headers = asl_field_list_flatten(field_list);
		amq_content_basic_set_headers(content, headers);
		asl_field_list_destroy(&field_list);
		icl_longstr_destroy(&headers);
	}

	amq_client_session_basic_publish(session, content, 0, exchange, routing_key, FALSE, FALSE);
	amq_content_basic_unlink(&content);
}

/* Read standard input looking for MIME-like message format
 *
 * Property ": " Value "\n"
 * Property ": " Value "\n"
 * ...
 * "Content-Length: " content-length "\n"
 * "\n"
 * content "\n"
 * "\n"
 * */
static void pipe_messages(char *exchange, char *routing_key)
{
	amq_content_basic_t *content = NULL;
	char *line = NULL;
	size_t alloc, len;
	int content_length = 0;
	char *sep, *value;
	byte *message = NULL;
	asl_field_list_t *field_list = NULL;
	icl_longstr_t *headers;
	char *msg_exchange = NULL, *msg_routing_key = NULL;

	while ((len = getline(&line, &alloc, stdin)) != -1)
	{
		/* Check if the connection is still alive*/
		if (!connection->alive)
		{
			if (connection->interrupt)
				terminate(AMQ_CODE_INTERRUPT);
			else
				terminate(AMQ_CODE_CONNECTION_BROKEN);
		}

		/* Initialize new content */
		if (content == NULL) {
			content = amq_content_basic_new();
			assert(content);
			content_length = 0;
			if (msg_exchange && msg_exchange != exchange)
				free(msg_exchange);
			if (msg_routing_key && msg_routing_key != routing_key)
				free(msg_routing_key);
			msg_exchange = exchange;
			msg_routing_key = routing_key;
		}

		/* An empty line: content follows */
		if (len == 1) {
			if (content_length > 0)
			{
				message = malloc(content_length + 1);
				assert(message);
				fread(message, content_length, 1, stdin);

				/* Consume the message trailing line */
				getc(stdin);
				/* .. and the aesthetic trailing line */
				getc(stdin);

				message[content_length] = '\0';
				amq_content_basic_set_body(content, message, strlen((char *)message), free);
			}

			if (field_list)
			{
				headers = asl_field_list_flatten(field_list);
				amq_content_basic_set_headers(content, headers);
				asl_field_list_destroy(&field_list);
				icl_longstr_destroy(&headers);
			}

			/* The message is complete; publish it and repeat */
			amq_client_session_basic_publish(session, content, 0, msg_exchange, msg_routing_key, FALSE, TRUE);
			amq_content_basic_unlink(&content);
			continue;
		}


		/* We're expecting a header line in "Name: Value" format */
		line[len-1] = '\0';
		sep = strstr(line, ": ");

		if (!sep) {
			/* Erroneous input: reset the cycle */
			fprintf(stderr, "%s: Warning: Expecting header line but got: `%s'\n",
					PROGRAM, line);
			amq_content_basic_unlink(&content);
			continue;
		}

		/* Form up the key/value strings inplace */
		*sep = '\0';
		value = sep + 2;

		/* Handle the special content-length header ourselves,
		 * otherwise try to match a field list header or use
		 * set_content_property() to build the content */
		if (strcasecmp(line, "content-length") == 0)
			content_length = atoi(value);
		else if (strncasecmp(line, "X-", 2) == 0) {
			if (!field_list)
				field_list = asl_field_list_new(NULL);
			asl_field_new_string(field_list, line + 2, value);
		}
		/* Allow overwriting exchange and routing_key per message basis */
		else if (strcasecmp(line, "exchange") == 0)
			msg_exchange = strdup(value);
		else if (strcasecmp(line, "routing-key") == 0)
			msg_routing_key = strdup(value);
		else if (!set_content_property(content, line, value))
			fprintf(stderr, "%s: Warning: Unknown header property: %s\n",
					PROGRAM, line);
	}
}

/* Open the connection to server and authenticate */
static void open_connection(char *server, char *username, char *password)
{
	icl_longstr_t *auth_data;
	auth_data = amq_client_connection_auth_plain(username, password);
	connection = amq_client_connection_new(server, "/", auth_data, PROGRAM, 0, 30000);
	if (connection)
		session = amq_client_session_new(connection);
	if (!session) {
		fprintf(stderr, "%s: could not connect to %s\n", PROGRAM, server);
		terminate(AMQ_CODE_CONNECTION_FAILURE);
	}
}

/* Integrate amq_helper by using meaningful exit codes */
static void print_exit_codes(void)
{
	printf("%d BAD_USAGE Bad usage\n",
			AMQ_CODE_BAD_USAGE);
	printf("%d BAD_ARGUMENTS Malformed arguments\n",
			AMQ_CODE_BAD_ARGUMENTS);
	printf("%d UNKNOWN_PROPERTY An unknown message property was specified\n",
			AMQ_CODE_UNKNOWN_PROPERTY);
	printf("%d EXCHANGE_FAILURE Failed to declare the exchange\n",
			AMQ_CODE_EXCHANGE_FAILURE);
	printf("%d CONNECTION_FAILURE Failed to connect to server\n",
			AMQ_CODE_CONNECTION_FAILURE);
	printf("%d CONNECTION_BROKEN Connection has broken\n",
			AMQ_CODE_CONNECTION_BROKEN);
	printf("%d INTERRUPT Signal interrupt\n",
			AMQ_CODE_INTERRUPT);
}

/* Identify to wrapper programs */
static void identify(void)
{
	printf("PROGRAM %s\n", PROGRAM);
	printf("VERSION %s\n", VERSION);
}

int main(int argc, char **argv)
{
	// Default values
	char *server = "localhost";
	char *exchange = "amq.direct";
	char *username = "guest";
	char *password = "guest";
	char *exchange_type = "direct";
	char *routing_key = ".";
	int exchange_passive = 1;

	//  Initialise system in order to use console.
	icl_system_initialise (argc, argv);

	// Read command line options
	int opt;
	while ((opt = getopt(argc, argv, "e:E:t:k:u:p:s:h")) != -1) {
		switch (opt) {
			case 'e':
				exchange = optarg;
				break;
			case 'E':
				exchange = optarg;
				exchange_passive = 0;
				break;
			case 't':
				exchange_type = optarg;
				break;
			case 'k':
				routing_key = optarg;
				break;
			case 'u':
				username = optarg;
				break;
			case 'p':
				password = optarg;
				break;
			case 's':
				server = optarg;
				break;
			case 'h':
				printf(HELP);
				exit(EXIT_SUCCESS);
				break;
			default:
				fprintf(stderr, "Usage error; try `%s -h' for help\n", PROGRAM);
				exit(EXIT_FAILURE);
				break;
		}
	}

	// Check the remaining command and arguments
	if (optind >= argc) {
		fprintf(stderr, "No command given; try `%s -h' for help\n", PROGRAM);
		exit(EXIT_FAILURE);
	}
	char *command = argv[optind++];

	if (strcmp(command, ".identify") == 0)
	{
		identify();
		exit(EXIT_SUCCESS);
	}

	if (strcmp(command, ".ecodes") == 0)
	{
		print_exit_codes();
		exit(EXIT_SUCCESS);
	}

	if (
			strcmp(command, "send") &&
			strcmp(command, "dump") &&
			strcmp(command, "test") &&
			strcmp(command, "pipe"))
	{
		fprintf(stderr, "Invalid command `%s'; try `%s -h' for help\n",
				command, PROGRAM);
		exit(EXIT_FAILURE);
	}

	// Connect
	open_connection(server, username, password);

	// Declare the exchange
	if (amq_client_session_exchange_declare(
		session, 0, exchange, exchange_type, exchange_passive, 0, 0, 0, NULL))
	{
		fprintf(stderr, "%s: failed to check the exchange\n", PROGRAM);
		terminate(AMQ_CODE_EXCHANGE_FAILURE);
	}

	// Dispatch
	if (strcmp(command, "dump") == 0) {
		dump_messages(exchange, routing_key);
	} else if (strcmp(command, "send") == 0) {
		if (argc - optind == 0) {
			fprintf(stderr, "No message properties given; see `%s -h' for help\n", PROGRAM);
			terminate(AMQ_CODE_BAD_USAGE);
		}
		send_message(exchange, routing_key, argv + optind, argc - optind);
	} else if (strcmp(command, "pipe") == 0) {
		pipe_messages(exchange, routing_key);
	}

	terminate(EXIT_SUCCESS);
	return 0;
}

/* vim.exec: ! $IBASE/bin/c -l % && ./%:r */
