#!/usr/bin/perl

# Copyright (C) 2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Filesystem::Monitor::Watcher::KQueue;

use strict;
use warnings;

use base qw(Filesystem::Monitor::Watcher);
use Filesystem::Monitor::Stat;
use Util::File qw(dirname);
use App::Message "fsysmon(backend):";

sub open
{
	my ($w, $path) = @_;
	$w->open_path($path)
		unless $ENV{FSMON_KQUEUE_PARENTONLY};
	$w->open_parent(dirname($path))
		or return 0;
	$w->{stat} = Filesystem::Monitor::Stat->new($path);
	return 1;
}

sub cancel
{
	my $w = shift;
	$w->cancel_path;
	$w->cancel_parent;
}

sub handle
{
	my ($w, $ev) = @_;

	if ($ev->{path} eq $w->{path} && $ev->{type} eq "modify") {
		$w->{monitor}->emit($ev->{type}, $w->{path}, $ev->{source}, $ev->{time});
	}

	elsif ($ev->{path} eq $w->{parent}->{path}) {
		if ($ev->{type} eq "modify") {
			for my $diff ($w->{stat}->diff) {
				$w->{monitor}->emit($diff->{type}, $diff->{path}, "stat");
				if ($diff->{type} eq "create") {
					debug("reload watcher parent=%s reason=create",
						$w->{parent}->{path});
					$w->reload;
				}
			}

			if ($w->parent_distance > 1) {
				debug("reload watcher path=%s parent=%s reason=distance",
					$w->{path}, $w->{parent}->{path});
				$w->reload();
			}
		}

		if ($ev->{type} eq "delete") {
			debug("reload watcher parent=%s reason=delete",
				$w->{parent}->{path});
			$w->reload;
		}
	}
}

1;
