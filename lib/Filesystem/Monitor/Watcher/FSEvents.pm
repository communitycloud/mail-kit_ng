#!/usr/bin/perl

# Copyright (C) 2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Filesystem::Monitor::Watcher::FSEvents;

use strict;
use warnings;

use base qw(Filesystem::Monitor::Watcher);
use Filesystem::Monitor::Stat;
use Util::File qw(dirname);
use App::Message "fsysmon(backend):";

sub open
{
	my ($w, $path) = @_;
	$w->open_parent(dirname($path))
		or return 0;
	$w->{stat} = Filesystem::Monitor::Stat->new($path);
	return 1;
}

sub cancel
{
	my $w = shift;
	$w->cancel_parent;
}

sub handle
{
	my ($w, $ev) = @_;
	for my $diff ($w->{stat}->diff) {
		$w->{monitor}->emit($diff->{type}, $diff->{path}, "stat");
	}
}

1;
