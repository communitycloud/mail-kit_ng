#!/usr/bin/perl

# Copyright (C) 2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Filesystem::Monitor::Watcher::Inotify;

use strict;
use warnings;

use base qw(Filesystem::Monitor::Watcher);
use Util::File qw(dirname);

sub open
{
	my ($w, $path) = @_;
	$w->open_parent(dirname($path));
}

sub handle
{
	my ($w, $ev) = @_;

	if ($ev->{name} eq $w->{path}) {
		$w->{monitor}->emit($ev->{type}, $w->{path});
	}

	if ($ev->{path} eq $w->{parent}->{path}) {
		if ($ev->{type} eq 'create' || $ev->{type} eq 'delete') {
			$w->reload();
		}
	}
}

sub cancel
{
	my $w = shift;
	$w->cancel_parent;
}

1;
