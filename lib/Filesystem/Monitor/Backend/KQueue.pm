#!/usr/bin/perl

# Copyright (C) 2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Filesystem::Monitor::Backend::KQueue;

use strict;
use warnings;

use IO::KQueue;
use base qw(Filesystem::Monitor::Backend);

sub new
{
	my $class = shift;
	return bless { events => [] }, $class;
}

sub kqueue
{
	my $b = shift;
	return $b->{kqueue} ||= do {
		my $kqueue = new IO::KQueue
			or die "unable to create new kqueue object: $!";
		$kqueue;
	};
}

sub i_open
{
	my ($b, $path) = @_;

	open my $handle, $path
		or return undef;

	my $fileno = fileno($handle);

	$b->kqueue->EV_SET($fileno, EVFILT_VNODE, EV_ADD | EV_CLEAR,
		NOTE_DELETE | NOTE_WRITE | NOTE_RENAME | NOTE_REVOKE, 0, $path);

	return $handle;
}

sub i_close
{
	my ($b, $h) = @_;
	close $h if $h;
}

sub wait
{
	my ($b, $timeout) = @_;
	$timeout = -1 unless defined $timeout;
	my @kev = $b->kqueue->kevent($timeout);
	push @{$b->{events}}, @kev;
	return @kev > 0;
}

sub read
{
	my $b = shift;
	my @events;

	while (my $ev = pop @{$b->{events}}) {

		my ($fd, $filter, $flags, $fflags, $data, $path) = @{$ev};

		my $type = $fflags & NOTE_WRITE ? 'modify'
			: $fflags & (NOTE_DELETE|NOTE_REVOKE) ? 'delete'
			: $fflags & NOTE_RENAME ? 'delete'
			: next;

		push @events, $b->event($type, $path);
	}

	return @events;
}

1;
