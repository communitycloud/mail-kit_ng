#!/usr/bin/perl

# Copyright (C) 2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Filesystem::Monitor::Backend::Inotify;

use strict;
use warnings;

use Linux::Inotify2;
use base qw(Filesystem::Monitor::Backend);

sub inotify
{
	my $b = shift;
	return $b->{inotify} ||= do {
		my $inotify = new Linux::Inotify2
			or die "unable to create new inotify object: $!";
		$inotify->blocking(0);
		$inotify;
	};
}

sub select
{
	my $b = shift;
	return $b->{select} ||= do {
		require IO::Select;
		my $select = IO::Select->new();
		$select->add($b->inotify->fileno);
		$select;
	};
}

sub i_open
{
	my ($b, $path) = @_;
	my $mask = IN_MODIFY | IN_ATTRIB | IN_MOVED_FROM | IN_MOVED_TO
		| IN_CREATE | IN_DELETE | IN_DELETE_SELF | IN_MOVE_SELF;
	return $b->inotify->watch($path, $mask),
}

sub i_close
{
	my ($b, $handle) = @_;
	$handle->cancel() if $handle;
}

sub wait
{
	my ($b, $timeout) = @_;
	return $b->select->can_read($timeout) || 0 > 0;
}

sub read
{
	my $b = shift;
	my @events;

	for my $e ($b->inotify->read) {
		my $type = $e->IN_CREATE || $e->IN_MOVED_TO ? 'create'
			: $e->IN_MODIFY ? 'modify'
			: $e->IN_DELETE || $e->IN_DELETE_SELF
				|| $e->IN_MOVED_FROM || $e->IN_MOVE_SELF ? 'delete'
			: next;
		push @events, $b->event($type, $e->w->name, name => $e->fullname);
	}

	return @events;
}

1;
