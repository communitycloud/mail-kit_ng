#!/usr/bin/perl

# Copyright (C) 2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Filesystem::Monitor::Backend::FSEvents;

use strict;
use warnings;

use Mac::FSEvents;
use base qw(Filesystem::Monitor::Backend);
use Util::File qw(resolve_path);

sub new
{
	my $class = shift;
	return bless {
		map => {},
		ready => {},
	}, $class;
}

sub select
{
	my $b = shift;
	return $b->{select} ||= do {
		require IO::Select;
		my $select = IO::Select->new();
		$select->add($_) for keys %{$b->{map}};
		$select;
	};
}

sub open
{
	my ($b, $path) = @_;
	$path = resolve_path($path) . '/';
	$b->SUPER::open($path);
}

sub i_open
{
	my ($b, $path) = @_;
	my $fs = Mac::FSEvents->new({path => $path, latency => 0.01});
	my $fh = fileno($fs->watch);
	$b->{map}->{$fh} = $fs;
	$b->{select}->add($fh) if $b->{select};
	return { fs => $fs, fh => $fh };
}

sub i_close
{
	my ($b, $h) = @_;
	$b->{select}->remove($h->{fh}) if $b->{select};
	delete $b->{map}->{$h->{fh}};
	$h->{fs}->stop();
}

sub wait
{
	my ($b, $timeout) = @_;
	my @ready = $b->select->can_read($timeout);
	for my $fh (@ready) {
		$b->{ready}->{$fh}++;
	}
	return @ready > 0;
}

sub read
{
	my $b = shift;
	my @events;

	for my $fh (keys %{$b->{ready}}) {
		for my $ev ($b->{map}->{$fh}->read_events) {
			push @events, $b->event("modify", $ev->path);
		}

		delete $b->{ready}->{$fh};
	}

	return @events;
}

1;
