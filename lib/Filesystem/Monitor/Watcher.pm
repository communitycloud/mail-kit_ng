#!/usr/bin/perl

# Copyright (C) 2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Filesystem::Monitor::Watcher;

use strict;
use warnings;

use Util::File qw(dirname);

sub new
{
	my ($class, $monitor, $path) = @_;

	my $w = bless {
		monitor => $monitor,
		path => $path,
	}, $class;

	if ($w->open($path)) {
		return $w;
	} else {
		return undef;
	}
}

sub bind
{
	my ($w, $path) = @_;
	$w->{monitor}->add_watcher_path($path, $w);
}

sub unbind
{
	my ($w, $path) = @_;
	$w->{monitor}->remove_watcher_path($path, $w);
}

sub backend
{
	$_[0]->{monitor}->{backend};
}

sub open
{
	my ($w, $path) = @_;
	$w->open_path($path);
}

sub reload
{
	my $w = shift;
	$w->cancel();
	$w->open($w->{path});
}

sub open_path
{
	my ($w, $path) = @_;

	if ($w->{handle} = $w->backend->open($path)) {
		$w->bind($w->{handle}->{path});
		return 1;
	} else {
		return 0;
	}
}

sub open_parent
{
	my ($w, $path) = @_;

	until ($w->{parent} = $w->backend->open($path)) {
		return 0 unless $!{ENOENT};
		return 0 if $path eq '/';
		$path = dirname($path);
	}

	$w->bind($w->{parent}->{path});

	return 1;
}

sub parent_distance
{
	my $w = shift;
	my $a = $w->{path} =~ y#/##;
	my $b = $w->{parent}->{path} =~ y#/##;
	return $a - $b;
}

sub cancel
{
	my $w = shift;
	$w->cancel_path;
}

sub cancel_path
{
	my $w = shift;
	if ($w->{handle}) {
		$w->unbind($w->{handle}->{path});
		$w->backend->close($w->{handle});
	}
}

sub cancel_parent
{
	my $w = shift;
	if ($w->{parent}) {
		$w->unbind($w->{parent}->{path});
		$w->backend->close($w->{parent});
	}
}

sub handle
{
	my ($class, $ev) = @_;
	return $ev;
}

1;
