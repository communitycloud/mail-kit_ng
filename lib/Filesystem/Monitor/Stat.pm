#!/usr/bin/perl

# Copyright (C) 2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Filesystem::Monitor::Stat;

use strict;
use warnings;

sub new
{
	my ($class, @paths) = @_;
	my $d = bless { fs => {} }, $class;
	$d->add(@paths);
	return $d;
}

sub add
{
	my ($d, @paths) = @_;

	$d->{fs} ||= {};
	for my $p (@paths) {
		if (my @stat = stat($p)) {
			$d->{fs}->{$p} = {
				mtime => $stat[9] || 0,
				size => $stat[7] || 0,
			}
		} else {
			$d->{fs}->{$p} = undef;
		}
	}

	return $d->{fs};
}

sub del
{
	my ($d, @paths) = @_;

	for my $p (@paths) {
		delete $d->{fs}->{$p};
	}
}

sub diff
{
	my $d = shift;

	my $old = delete $d->{fs};
	my @paths = keys %{$old};
	my $new = $d->add(@paths);

	my @diff;

	for my $e (@paths) {
		my $n = $new->{$e};
		my $o = $old->{$e};

		if ($n) {
			my $o = $old->{$e};
			unless (defined $o) {
				push @diff, { type => 'create', path => $e }
			} elsif ($n->{mtime} != $o->{mtime} || $n->{size} != $o->{size}) {
				push @diff, { type => 'modify', path => $e }
			}
		} elsif ($o) {
			push @diff, { type => 'delete', path => $e };
		}
	}

	return @diff;
}

1;
