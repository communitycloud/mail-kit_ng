#!/usr/bin/perl

# Copyright (C) 2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Filesystem::Monitor::Backend;

use strict;
use warnings;

use Util::Package qw(load_object);
use App::Message "fsysmon(backend):";
use Time::HiRes;

sub new
{
	my $class = shift;
	if ($class eq __PACKAGE__) {
		return $class->load();
	} else {
		return bless {}, $class;
	}
}

sub get_backend_class()
{
	if (my $class = $ENV{FSMON_BACKEND}) {
		return $class;
	} elsif ($^O eq "linux") {
		return "Inotify";
	} elsif ($^O eq "darwin") {
		return "KQueue";
	} else {
		error("$^O OS not supported");
	}
}

sub load
{
	my $class = shift;
	return load_object(get_backend_class(), __PACKAGE__);
}

sub new_watcher
{
	my $class = shift;
	return load_object(get_backend_class(), "Filesystem::Monitor::Watcher", @_);
}

sub name
{
	$_[0]->{_name} ||= do {
		my $ref = ref $_[0];
		my $pkg = __PACKAGE__;
		$ref =~ s/^$pkg\:://;
		return lc $ref;
	};
}

sub open
{
	my ($b, $path) = @_;

	if (my $h = $b->{_handles}->{$path}) {
		$h->{refcnt} ++;
		debug("open %s handle path=%s refcnt=%d",
			$b->name, $h->{path}, $h->{refcnt});
		return $h;
	}

	my $h = $b->i_open($path);

	if ($h) {
		debug("open %s handle path=%s refcnt=%d",
			$b->name, $path, 1);
		return $b->{_handles}->{$path} = { path => $path, handle => $h, refcnt => 1 };
	} else {
		debug("open %s handle path=%s error=%s",
			$b->name, $path, $!);
		return undef;
	}
}

sub close
{
	my ($b, $h) = @_;

	if (ref $h ne 'HASH') {
		internal_warning("close %s handle: invalid handle %s", $b->name, $h);
		return 0;
	}

	debug("close %s handle path=%s refcnt=%s",
		$b->name, $h->{path}, $h->{refcnt});

	if (-- $h->{refcnt} <= 0) {
		$b->i_close($h->{handle});
		delete $b->{_handles}->{$h->{path}};
	}

	return 1;
}

sub event
{
	my ($b, $type, $path, %prop) = @_;

	my $ev = {
		type => $type,
		path => $path,
		source => $b->name,
		time => Time::HiRes::time,
		%prop };

	debug("event source=%s type=%s path=%s time=%s",
		$ev->{source}, $ev->{type}, $ev->{path}, $ev->{time} );

	return $ev;
}

sub i_open
{
	internal_error "$_[0]: Virtual i_open";
}

sub i_close
{
	internal_error "$_[0]: Virtual i_close";
}

1;
