#!/usr/bin/perl

# Copyright (C) 2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Filesystem::Monitor;

use strict;
use warnings;

use App::Message "fsysmon:";
use Filesystem::Monitor::Backend;
use Time::HiRes;

sub new
{
	my $class = shift;
	return bless {
		watcher => {},
		backend => Filesystem::Monitor::Backend->new,
		events => [],
	}, $class;
}

sub watch
{
	my ($m, $path) = @_;
	$m->{backend}->new_watcher($m, $path);
}

sub add_watcher_path
{
	my ($m, $path, $w) = @_;
	$m->{watcher}->{$path}->{$w} = $w;
}

sub remove_watcher_path
{
	my ($m, $path, $w) = @_;
	delete $m->{watcher}->{$path}->{$w};
}

sub wait
{
	my ($m, $timeout) = @_;

	if ($m->{backend}->wait($timeout)) {
		$m->read();
	}

	if (!$timeout && !@{$m->{events}}) {
		goto &wait;
	}

	return $m->events;
}

sub read
{
	my $m = shift;

	for my $ev ($m->{backend}->read()) {
		if (my $h = $m->{watcher}->{$ev->{path}}) {
			for my $w (values %{$h}) {
				$w->handle($ev);
			}
		}
	}
}

sub events
{
	my $m = shift;

	if (my @events = @{$m->{events}}) {
		$m->{events} = [];
		return @events;
	}

	return ();
}

sub emit
{
	my ($m, $type, $path, $source, $time) = @_;
	$source ||= "unspecified";
	$time ||= Time::HiRes::time;
	debug("event type=%s path=%s source=%s time=%s",
		$type, $path, $source, $time);
	push @{$m->{events}}, { type => $type, path => $path,
		source => $source, time => $time };
}

1;
