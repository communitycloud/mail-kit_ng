#!/usr/bin/perl

package MailKit::Sendmail;

use strict;
use warnings;

use App::Message qw(sendmail:);
use Util::File qw(scalar2io is_handle);
use Util::String qw(parse_uri);
use Email::Simple;
use Email::Address;
use Util::Export qw(sendmail_local sendmail_smtp sendmail_uri);
use Sys::Hostname;

sub to_mail_handle($)
{
	my $mail = shift;
	return $mail if is_handle($mail);
	if (ref $mail eq "MailStream") {
		return @{$mail};
	} elsif (ref $mail eq "SCALAR") {
		$mail = $$mail;
	} elsif (ref $mail eq "Email::Simple") {
		$mail = $mail->as_string;
	}
	return scalar2io($mail);
}

sub make_mail_stream($)
{
	my $mail = shift;
	my $stream = [];

	if (ref $mail eq "ARRAY") {
		for my $m (@{$mail}) {
			push @{$stream}, to_mail_handle($m)
		}
	} else {
		push @{$stream}, to_mail_handle($mail);
	}

	return bless $stream, "MailStream";
}

sub read_mail_line($)
{
	my $stream = shift;
	shift @{$stream} while @{$stream} and eof $stream->[0];
	return undef unless scalar @{$stream};
	return readline $stream->[0];
}

sub read_mail_header($)
{
	my $stream = shift;
	my $header = "";

	while (defined (my $line = read_mail_line($stream)))
	{
		last if $line eq "\n\r" or $line eq "\n";
		$header .= $line;
	}

	return Email::Simple->new($header);
}

sub header_checks($%)
{
	my ($header, %arg) = @_;

	unless ($header->header_names) {
		error("invalid mail header");
	}

	if (my $headers = $arg{required_headers}) {
		for my $name (@{$headers}) {
			unless ($header->header($name)) {
				error("the message is missing the required %s header", $name);
			}
		}
	}
}

sub sendmail_common($%)
{
	my ($mail, %arg) = @_;

	my $stream = make_mail_stream($mail);
	my $header = read_mail_header($stream);

	header_checks($header, %arg);

	my $sender = $arg{sender};
	my $recipients = $arg{recipients};

	if ($arg{auto}) {
		$sender ||= sprintf "%s@%s",
			scalar getpwuid($<), hostname();
		$recipients = $header->header('To');
	}

	unless ($sender) {
		error('no sender specified');
	}

	unless ($recipients) {
		error('no recipients specified');
	}

	unless (ref $recipients eq 'ARRAY') {
		my @addresses = Email::Address->parse($recipients)
			or error('failed to parse SMTP recipients=%s', $recipients);
		$recipients = [map { $_->address } @addresses];
	}

	return ($stream, $header, $sender, $recipients);
}

sub sendmail_local($%)
{
	my ($mail, %arg) = @_;

	my ($stream, $header, $sender, $recipients) = sendmail_common($mail, %arg);

	my $sendmail = $arg{sendmail} || "/usr/sbin/sendmail";

	no warnings "exec";
	open my $pipe, '|-', $sendmail, '-i', '-f', $sender, '--', @{$recipients}
		or error("couldn't open pipe to sendmail ($sendmail): $!");

	print $pipe $header->as_string
		or error("failed to write headers to sendmail: $!");

	while (defined (my $line = read_mail_line($stream))) {
		print $pipe $line
			or error("failed to write message to sendmail: $!");
	}

	close $pipe
		or error("close sendmail failed: $!");
}

sub sendmail_smtp($%)
{
	my ($mail, %arg) = @_;

	my ($stream, $header, $sender, $recipients) = sendmail_common($mail, %arg);

	unless ($arg{host}) {
		error("no host specified");
	}

	if ($arg{host} =~ /(.+):(\d+)$/) {
		$arg{host} = $1;
		$arg{port} = $2;
	} else {
		$arg{port} ||= $arg{ssl} ? 465 : 25;
	}

	$arg{hello} ||= hostname();

	goto TLS if $arg{tls};

	require Net::SMTP;
	require Net::SMTP::SSL if $arg{ssl};

	my $class = $arg{ssl} ? "Net::SMTP::SSL" : "Net::SMTP";
	my $smtp = $class->new($arg{host}, Port => $arg{port}, Hello => $arg{hello}, Debug => $arg{debug})
		or error("connection to $arg{host} failed: $!");

	if (defined $arg{user} && defined $arg{pass}) {
		$smtp->auth($arg{user}, $arg{pass})
			or error("error in SASL auth: %s", $smtp->message);
	}

	$smtp->mail($sender)
		or error("error in MAIL FROM: %s", $smtp->message);

	for my $to (@{$recipients}) {
		$smtp->to($to)
			or error("error in RCPT for %s: %s", $to, $smtp->message);
	}

	$smtp->data
		or error("error in DATA start: %s", $smtp->message);
	$smtp->datasend($header->as_string, "\n")
		or error("error sending header DATA: %s", $smtp->message);

	while (defined (my $line = read_mail_line($stream))) {
		$smtp->datasend($line)
			or error("error sending body DATA: %s", $smtp->message);
	}

	$smtp->dataend
		or error("error in DATA end: %s", $smtp->message);

	$smtp->quit
		or error("error in QUIT: %s", $smtp->message);
	
	return;

TLS:

	require Net::SMTP::TLS;

	eval {
		my $smtp = Net::SMTP::TLS->new($arg{host},
			Port => $arg{port},
			Hello => $arg{hello},
			User => $arg{user},
			Password => $arg{pass});

		$smtp->mail($sender);
		for my $to (@{$recipients}) {
			$smtp->to($to);
		}

		$smtp->data;
		$smtp->datasend($header->as_string);
		while (defined (my $line = read_mail_line($stream))) {
			$smtp->datasend($line);
		}
		$smtp->dataend;
		$smtp->quit;
	};

	if (my $err = error_string()) {
		error($err);
	}
}

sub sendmail_uri($$%)
{
	my ($mail, $uri, %arg) = @_;

	my $u = parse_uri($uri) || {};
	$u->{scheme} ||= 'local';

	# gmail support
	if ($u->{scheme} eq 'gmail') {
		$u->{scheme} = 'ssmtp';
		$arg{host} ||= 'smtp.gmail.com';
		if ($u->{path} =~ /^(.+?):(.+)$/) {
			$arg{user} ||= $1;
			$arg{pass} ||= $2;
		}
	}

	# ssl support
	if ($u->{scheme} eq 'ssmtp') {
		$u->{scheme} = 'smtp';
		$arg{port} ||= 465;
		$arg{ssl} ||= 1;
	}

	# URI arguments
	for my $a (qw(user pass ssl port)) {
		$arg{$a} ||= $u->{query}->{$a};
	}

	for my $key (qw(sender recipients user pass host port)) {
		if (!defined $arg{$key} && defined $u->{$key}) {
			$arg{$key} = $u->{$key};
		}
	}

	if ($u->{scheme} eq 'local' || $u->{scheme} eq 'sendmail') {
		sendmail_local($mail, %arg);
	} elsif ($u->{scheme} eq 'smtp') {
		sendmail_smtp($mail, %arg);
	} else {
		error('invalid URI scheme: %s', $uri);
	}
}

1;
