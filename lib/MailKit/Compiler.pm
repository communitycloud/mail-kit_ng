#!/usr/bin/perl

package MailKit::Compiler;

use strict;
use warnings;

use App::Message "compiler:";
use Util::File qw(slurp scalar2io dirname cleanup_filename);
use Util::List qw(in_list list_intersect);
use Util::String qw(un_camel_case parse_kvp);
use Hash::Stable;
use MailKit::CommandParser qw(encode_multiline);
use MailKit::Compiler::Context;
use MailKit::Compiler::Path;

my @compilers = qw(as ehtml html text headers attachments subject recipient sender mkit);
my @filters = qw(tt tmpl lang);
my @directives = qw(manifest context decl);
my @basenames = qw(CONTEXT AS HEADERS SUBJECT RECIPIENT SENDER ATTACHMENTS);

sub new
{
	my ($class, %arg) = @_;

	my $context = MailKit::Compiler::Context->new;
	my $path = MailKit::Compiler::Path->new;

	my $p = bless {
		path => $path,
		context => $context,
		as => "",
		capture => $arg{capture},
	}, $class;

	$path->append(grep length, split ':', ($ENV{MKIT_PATH} || ''));
	$path->append(cleanup_filename(dirname(__FILE__) . '/../../templates'));

	$context->add($ENV{MKIT_CONTEXT});
	$context->set("compiler" => $p);

	$path->append(@{$arg{path}}) if $arg{path};
	$context->add(%{$arg{context}}) if $arg{context};

	return $p;
}

sub context
{
	$_[0]->{context};
}

sub path
{
	$_[0]->{path};
}

sub capture
{
	$_[0]->{capture} = [];
}

sub get_pipeline
{
	my ($p, $input) = @_;

	my @pipeline = ();

	my ($filename, $extension) = $input =~ m#([^/]+?)(?:\.([^.]+))?$#;

	my ($name) = $filename =~ m#^(?:\d+-)?(.+)#;
	my @exts = split /[+,:]/, ($extension || "");

	if (in_list($name, @basenames)) {
		unshift @pipeline, lc $name;
	}

	my @any = (@compilers, @directives, @filters);
	map { unshift @pipeline, $_ if in_list($_, @any) } @exts;

	my @c = list_intersect(\@pipeline, \@compilers);
	my @d = list_intersect(\@pipeline, \@directives);

	unless (@c || @d) {
		return ();
	}

	return @pipeline;
}

sub dispatch
{
	my ($p, $op, @a) = @_;
	my $prefix = in_list($op, @compilers)
		? "compile_" : in_list($op, @filters)
		? "filter_" : in_list($op, @directives)
		? "import_" : error("(internal) unknown operation: %s", $op);
	my $call = $prefix . $op;
	internal_error("uncallable operation: %s", $op) unless $p->can($call);
	$p->$call(@a);
}

sub run_pipeline
{
	my ($p, $input, @pipeline) = @_;

	my @c = list_intersect(\@pipeline, \@compilers);
	my @d = list_intersect(\@pipeline, \@directives);
	my @p = list_intersect(\@pipeline, \@filters);

	if (@c + @d > 1) {
		error("input %q maps to multiple compilers or directives: %{pa:t}",
			$input, [@c, @d]);
	}

	my $buff = slurp($input);

	for my $op (@p) {
		$buff = $p->dispatch($op, $buff);
	}

	my ($op) = (@c, @d);
	$p->as(qq(# From "%s"\n), $input) if @c;
	$p->dispatch($op, $buff);
}

sub get_lang_pot
{
	my $p = shift;

	my $lang = $p->{context}->get('language');
	return undef unless defined $lang;

	my $dir = $p->{path}->find('lang')
		or return undef;
	return undef unless -d $dir;

	#$lang = lc $lang;
	$lang =~ s#[-_.]+#_#g;

	do {
		my $pot = sprintf('%s/%s.po', $dir, $lang);
		return $pot if -f $pot;
	} while ($lang =~ s/_[^_]+$//);

	return undef;
}

sub as
{
	my ($p, $as, @arg) = @_;
	$as = sprintf($as, @arg) if @arg;
	$p->{as} .= $as;
}

sub op
{
	my ($p, $op, @arg) = @_;
	push @{$p->{capture}}, [$op, @arg] if ref $p->{capture};
	$arg[-1] = encode_multiline($arg[-1]) if @arg;
	$p->as("%s %s\n", $op, "@arg");
}

sub resolve_path
{
	my ($p, $name) = @_;
	my $path = $p->{path}->find($name)
		or error("cannot find %q in path (%{pa:t})", $name, $p->{path}->{list});
	return $path;
}

sub compile
{
	my ($p, $input) = @_;
	my $path = $p->resolve_path($input);
	$p->as("# From top-level\n");
	$p->op(':', 'set', 'cwd', 
		-d $path ? $path : dirname($path));
	my $len = length $p->{as};
	$p->compile_any($input);
	error("input %q is empty", $input) if length $p->{as} == $len;
	return $p->{as};
}

sub compile_any
{
	my ($p, $input) = @_;
	my $path = $p->resolve_path($input);
	if (-d $path) {
		return $p->compile_directory($input);
	} else {
		return $p->compile_file($input);
	}
}

sub compile_file
{
	my ($p, $input, @pipeline) = @_;

	$input = $p->resolve_path($input);
	@pipeline = $p->get_pipeline($input)
		unless @pipeline;

	$p->{path}->enter(dirname($input));
	eval { $p->run_pipeline($input, @pipeline) };
	$p->{path}->leave();

	if ($@) {
		error("compilation of %q failed:\n%s", $input, error_string);
	}
}

sub compile_directory
{
	my ($p, $dir) = @_;

	my @files;

	$dir = $p->resolve_path($dir);
	$dir =~ s#/+$##;

	if (-f $dir . '/MANIFEST') {
		@files = grep length, slurp($dir . '/MANIFEST');
	} elsif (-f $dir . '/index.mkit') {
		return $p->compile_file($dir . '/index.mkit');
	} else {
		opendir my $handle, $dir
			or error("cannot open directory %q: $!");
		@files = sort grep {
			$p->get_pipeline("$dir/$_")
		} readdir $handle;
		closedir $handle;
	}

	$p->{path}->enter($dir);
	$p->compile_files(@files);
	$p->{path}->leave();
}

sub compile_files
{
	my ($p, @files) = @_;
	for my $file (@files) {
		$file = $p->resolve_path($file);
		$p->compile_any($file);
	}
}

sub compile_as
{
	my ($p, $as) = @_;
	chomp($as);
	$p->as("%s\n", $as);
}

sub compile_mkit
{
	require MailKit::Compiler::Mkit;
	my ($p, $mkit) = @_;
	MailKit::Compiler::Mkit->new($p)->compile(scalar2io($mkit));
}

sub compile_html
{
	my ($p, $html) = @_;
	chomp($html);
	$p->op('+', 'html', $html);
}

sub compile_ehtml
{
	use Sys::Hostname;
	my ($p, $html) = @_;
	my %cid;
	my $ehtml_links = qr/\bsrc\s*=\s*"embed:(.+?)"/;
	while (my ($src) = $html =~ $ehtml_links) {
		unless ($cid{$src}) {
			$cid{$src} = sprintf("%d.%s@%s",
				1000 + rand(8999), substr(time, -5), hostname());
			$p->op('+', 'embed', "$src\->$cid{$src}");
		}
		$html =~ s#$ehtml_links#src="cid:$cid{$src}"#;
	}
	$p->compile_html($html);
}

sub compile_text
{
	my ($p, $text) = @_;
	chomp($text);
	$p->op('+', 'text', $text);
}

sub compile_headers
{
	my ($p, $headers) = @_;

	my $kv = parse_kvp($headers, '\n+', '\s*:\s*');
	while (my ($header, $value) = each %{$kv})
	{
		$p->op('+', 'header', $header, $value)
	}
}

sub compile_attachments
{
	my ($p, $attachments) = @_;

	for my $file (split "\n+", $attachments)
	{
		$p->op('+', 'attach', $file);
	}
}

sub compile_subject
{
	my ($p, $subject) = @_;
	chomp($subject);
	$p->op('+', 'subject', $subject);
}

sub compile_recipient
{
	my ($p, $recipient) = @_;
	chomp($recipient);
	$p->op('+', 'recipient', $recipient);
}

sub compile_sender
{
	my ($p, $sender) = @_;
	chomp($sender);
	$p->op('+', 'sender', $sender);
}

sub filter
{
	my ($p, $template, @filters) = @_;

	for my $f (@filters) {
		my $filter = "filter_$f";
		$p->can($filter) or internal_error("invalid filter %q", $f);
		$template = $p->$filter($template);
	}

	return $template;
}

sub hash_filters
{
	my $p = shift;
	my $h = {};

	for my $f (@filters) {
		$h->{$f} = sub { $p->filter($_[0], $f) };
	}

	return $h;
}

sub filter_lang
{
	require Util::Pot;
	require Util::Langtext;

	my ($p, $text) = @_;

	if (my $lang_pot = $p->get_lang_pot) {
		my $pot = &Util::Pot::read_pot($lang_pot);
		my $langtext = &Util::Langtext::langtext($text, $pot);
		return $langtext if defined $langtext;
	}

	return &Util::Langtext::langtext($text, undef, 1);
}

sub filter_tt
{
	require Template;
	my ($p, $template) = @_;
	$p->{tt} ||= new Template({
			STRICT => 1,
			INCLUDE_PATH => $p->{path}->{list},
			FILTERS => $p->hash_filters});
	$p->{tt}->process(\$template, $p->{context}->get, \my $out)
		or error("tt filter: %s", $p->{tt}->error);
	return $out;
}

sub filter_tmpl
{
	require Text::Template;
	my ($p, $template) = @_;
	my $tmpl = Text::Template->new(TYPE => 'STRING', SOURCE => $template);
	return $tmpl->fill_in(HASH => $p->{context}->get);
}

sub import_context
{
	my ($p, $context) = @_;
	$p->{context}->add($context);
}

sub import_pl
{
	my ($p, $code) = @_;
	local $MailKit::Compiler::compiler;
	$MailKit::Compiler::compiler = $p;
	eval $code;
	error("perl code failed: $@") if $@;
}

sub import_manifest
{
	my ($p, $manifest) = @_;
	my @files = split "\n+", $manifest;
	return $p->compile_files(@files);
}

sub import_decl
{
	my ($p, $declarations) = @_;
	$p->{context}->import_declarations(\$declarations);
}

1;
