#!/usr/bin/perl

package MailKit::Compiler::Path;

use strict;
use warnings;

use Util::List qw(list_index in_list list_remove);

sub new
{
	my $class = shift;
	return bless {
		list => [],
		stack => [],
		current => undef,
	};
}

sub append
{
	my ($p, @path) = @_;
	push @{$p->{list}}, grep !in_list($_, @{$p->{list}}), @path;
	$p->update_current();
}

sub prepend
{
	my ($p, @path) = @_;
	list_remove($_, @{$p->{list}}) for @path;
	unshift @{$p->{list}}, @path;
	$p->update_current();
}

sub update_current
{
	my $p = shift;
	if (defined (my $i = list_index(".", @{$p->{list}}))) {
		$p->{current} = \$p->{list}->[$i];
		${$p->{current}} = $p->current_directory;
	}
}

sub enter
{
	my ($p, $dir) = @_;
	push @{$p->{stack}}, $dir;
	${$p->{current}} = $dir
		if $p->{current};
}

sub leave
{
	my $p = shift;
	my $dir = pop @{$p->{stack}};
	${$p->{current}} = $p->current_directory
		if $p->{current};
}

sub current_directory
{
	my $p = shift;
	return $p->{stack}->[-1] || ".";
}

sub find
{
	my ($p, $name, @ext) = @_;

	if ($name =~ m#^/#) {
		return $name if -e $name;
		return undef;
	}

	for my $path (@{$p->{list}}) {
		my $f = "$path/$name";
		return $f if -f $f;
		for my $c (@ext) {
			my $f = "$f.$c";
			return $f if -f $f;
		}
		return $f if -d $f;
	}

	return undef;
}

1;
