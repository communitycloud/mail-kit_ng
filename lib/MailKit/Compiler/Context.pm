#!/usr/bin/perl

package MailKit::Compiler::Context;

use strict;
use warnings;

use App::Message "context:";
use Util::File;
use Util::String qw(un_camel_case parse_kvp boolean_value looks_like_number);
use Util::List qw(in_list);

sub new
{
	my $class = shift;
	bless {
		stash => {},
		alias => {},
		decl  => {},
	}, $class;
}

sub set
{
	my ($c, $name, $value) = @_;
	$name = $c->resolve_name($name);
	return $c->set_array($name, $value)
		if looks_like_array($name);
	$value = $c->typecheck($name, $value);
	$c->{stash}->{$name} = $value;
	for my $alias (@{$c->{decl}{$name}{alias}}) {
		$c->{stash}->{$alias} = $value;
	}
}

sub looks_like_array($)
{
	$_[0] =~ /\[\d*\]$/;
}

sub set_array
{
	my ($c, $name, $value) = @_;

	$name =~ s/\[(\d*)\]$//;
	my $idx = $1;

	unless (ref $c->get($name)) {
		$c->set($name, []);
	}
	my $array = $c->get($name);
	if (defined $idx && length $idx) {
		$array->[$idx] = $value;
	} else {
		push @{$array}, $value;
	}
}

sub typecheck
{
	my ($c, $name, $value) = @_;

	my $decl = $c->{decl}->{$name}
		or return $value;
	my $type = $decl->{type} || "Any";

	unless (defined $value) {
		error("mandatory variable %q is not set", $name)
			unless $decl->{qual}->{wonder};
		return undef;
	}

	if ($type =~ /^Num(?:eric)?$/) {
		unless (looks_like_number($value)) {
			error("invalid <Numeric> value of %q: %q", $name, $value);
		}
	} elsif ($type eq "Enum") {
		unless (in_list($value, @{$decl->{args}})) {
			error("invalid <Enum> value of %q: %q", $name, $value);
		}
	} elsif ($type eq "Email") {
		require Email::Address;
		if (ref $value eq "Email::Address") {
			return $value;
		}
		my @address = Email::Address->parse($value)
			or error("invalid address for Email %q variable: %q", $name, $value);
		return $address[0];
	} elsif ($type eq "Bool") {
		return boolean_value($value);
	}

	return $value;
}

sub resolve_name
{
	my ($c, $name) = @_;
	$name = un_camel_case($name);
	$name = $c->{alias}->{$name} while defined $c->{alias}->{$name};
	return $name;
}

sub get
{
	my ($c, $name) = @_;
	return $c->{stash} unless defined $name;
	return $c->{stash}->{$name};
}

sub delete
{
	my ($c, $name) = @_;
	$name = $c->resolve_name($name);
	my $value = delete $c->{stash}->{$name};
	for my $alias (@{$c->{decl}{$name}{alias}}) {
		delete $c->{stash}->{$alias};
	}
	return $value;
}

sub add
{
	my $c = shift;

	if (@_ == 1) {
		my $input = shift;
		if (ref $input eq "HASH") {
			$c->add(%{$input});
		} elsif (!defined $input) {
			return;
		} elsif ($input =~ /^\s*\</) {
			$c->add_xml($input);
		} elsif ($input =~ /^\s*\{/) {
			$c->add_json($input);
		} elsif ($input =~ /[=:]/) {
			$c->add_kvp($input);
		} else {
			error("invalid input: %s", $input);
		}
	} else {
		my %context = @_;
		while (my ($name, $value) = each %context) {
			$c->set($name, $value);
		}
	}
}

sub add_xml
{
	my ($c, $input) = @_;
	require XML::Light;

	my $parser = XML::Light->parser;
	my $xml = eval { $parser->parse($input) };
	my $error = $@;

	if ($error) {
		my $fix = sprintf('<root>%s</root>', $input);
		$xml = eval { $parser->parse($fix) };
	}

	unless ($xml) {
		error("invalid (pseudo) xml input: %s",
			error_string($error))
	}

	for my $e ($xml->root->content) {
		if (my $name = $e->{tag}) {
			$c->set($name, $e->text);
		}
	}
}

sub add_json
{
	my ($c, $input) = @_;
	my $json = eval "use JSON v2; JSON->new->latin1"
		or error("couldn't load JSON v2 object");
	my $context = $json->decode($input);
	$c->add($context);
}

sub add_kvp
{
	my ($c, $input) = @_;
	my %c1 = parse_kvp($input, ';', '=');
	my %c2 = parse_kvp($input, '\n+', '\s*:\s*');
	$c->add(keys %c2 > keys %c1 ? %c2 : %c1);
}

sub declare
{
	my ($c, $decl) = @_;

	my $name = un_camel_case($decl->{name});

	if (exists $c->{decl}->{$name}->{type} && $decl->{type}) {
		error("cannot redeclare %q", $name);
	} else {
		$c->{decl}->{$name} = $decl;
	}

	if (defined $decl->{value}) {
		$c->set($name, $decl->{value});
	} elsif (defined $decl->{default}) {
		$c->set($name, defined $c->{stash}->{$name}
			? $c->{stash}->{$name}
			: $decl->{default});
	} elsif (defined $c->{stash}->{$name}) {
		$c->set($name, $c->{stash}->{$name});
	}

	for my $alias (@{$decl->{alias}}) {
		$alias = un_camel_case($alias);
		if ($alias eq $name) {
			error("recursive alias for %s", $name);
		}
		$c->{alias}->{$alias} = $name;
		if (exists $c->{stash}->{$alias}) {
			$c->set($name, $c->{stash}->{$alias});
		}
	}
}

sub import_declarations
{
	require MailKit::DeclarationParser;
	my ($c, $input) = @_;

	my $parser = $c->{parser} ||= MailKit::DeclarationParser->new;
	my $declarations = $parser->parse($input) or return;

	for my $decl (@{$declarations}) {
		$c->declare($decl);
	}
}

1;
