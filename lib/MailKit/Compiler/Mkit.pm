#!/usr/bin/perl

package MailKit::Compiler::Mkit;

use strict;
use warnings;

use Util::File qw(fopen scalar2io);
use MailKit::CommandParser qw(encode_multiline);
use MailKit::Compiler;
use MailKit::Assembler::Syntax;

sub new
{
	my ($class, $compiler) = @_;

	my $kc = bless {
		output => "",
		buffer => "",
		compiler => $compiler || MailKit::Compiler->new(),
		filters => [],
	}, $class;

	$kc->{parser} = MailKit::CommandParser->new($kc);

	return $kc;
}

sub syntax
{
	return (
		": filter +",
		": buffer",
		": flush",
		": declare +",
		": let . +",
		": include +",
		": path append +",
		": path prepend +",
		"< text +",
		"< html +",
		"< include +",
		@MailKit::Assembler::Syntax::syntax,
	);
}

sub buffer
{
	my ($k, $buffer) = @_;
	$k->{buffer} .= $buffer;
}

sub flush
{
	my ($k) = @_;
	if (length $k->{buffer}) {
		if (my @filters = @{$k->{filters}}) {
			$k->{buffer} = $k->{compiler}->filter($k->{buffer}, @filters);
		}

		$k->{parser}->input(scalar2io($k->{buffer}));

		$k->{buffer} = "";
	}
}

sub compile
{
	my ($k, $input) = @_;

	my $io = fopen($input);

	while (defined (my $line = $k->{parser}->read_line($io)))
	{
		if (my ($processor, @tok) = eval { $k->{parser}->parse_line($line) }) {
			if ($tok[0] eq ":" && ($tok[1] eq "buffer" || $tok[1] eq "flush")) {
				$k->{parser}->input_tokens($processor, @tok);
				next;
			}
		}

		if ($k->{buffering}) {
			chomp $line;
			$k->buffer(encode_multiline($line) . "\n");
		} else {
			$k->{parser}->input_line($line);
		}
	}

	$k->flush();

	return $k->{compiler}->{as};
}

sub emit
{
	my ($kc, $op, $command, @arg) = @_;

	if ($op eq ":" && $command eq "filter") {
		$kc->{buffering} = 1;
		$kc->flush();
		$kc->{filters} = [ split /\s*,\s*/, $arg[0] ];
	} elsif ($op eq ":" && $command eq "buffer") {
		$kc->{buffering} = 1;
		$kc->flush();
	} elsif ($op eq ":" && $command eq "flush") {
		$kc->{buffering} = 0;
		$kc->flush();
	} elsif ($op eq ":" && $command eq "let") {
		$kc->{compiler}->context->set($arg[0], $arg[1]);
	} elsif ($op eq ":" && $command eq "declare") {
		$kc->{compiler}->import_decl($arg[0]);
	} elsif ($op eq "<" && ($command eq "text" || $command eq "html")) {
		$kc->{compiler}->compile_file(split('\s*[|,]\s*', $arg[0]), $command);
	} elsif (($op eq "<" || $op eq ":") && $command eq "include") {
		$kc->{compiler}->compile_any($arg[0]);
	} elsif ($op eq ":" && $command eq "path" && $arg[0] eq "append") {
		$kc->{compiler}->{path}->append(split ":", $arg[1]);
	} elsif ($op eq ":" && $command eq "path" && $arg[0] eq "prepend") {
		$kc->{compiler}->{path}->prepend(split ":", $arg[1]);
	} else {
		$kc->{compiler}->op($op, $command, @arg);
	}
}

1;
