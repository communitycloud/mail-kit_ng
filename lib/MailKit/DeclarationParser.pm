#!/usr/bin/perl

package MailKit::DeclarationParser;

use strict;
use warnings;

use base qw(Hand::Parser);

sub configure
{
	my $p = shift;
	$p->{word_chars} .= '_';
	$p->{punct_chars} .= ':=';
}

sub start
{
	goto &parse_declarations;
}

sub parse_declarations
{
	my $p = shift;

	my $declarations = [];

	$p->skip_spaces();

	while (defined $p->peek_char) {
		if (my $decl = $p->parse_declaration) {
			push @{$declarations}, $decl;
		}
	}

	return $declarations;
}

sub parse_declaration
{
	my $p = shift;

	my $decl = {};
	my @toks = ();

	while (1) {
		my $tok = $p->next_token();
		if (!defined $tok || $tok eq ';' || $tok eq ',') {
			last;
		} elsif ($tok eq '?') {
			$decl->{qual}->{wonder}++;
		} elsif ($tok eq '*') {
			$decl->{qual}->{star}++;
		} elsif ($tok eq '!') {
			$decl->{qual}->{exclamation}++;
		} elsif ($tok eq '(') {
			$decl->{args} = $p->parse_args;
		} elsif ($tok eq '{') {
			$decl->{flags} = $p->parse_flags;
		} elsif ($tok eq 'or') {
			$decl->{alias} = $p->parse_alias;
		} elsif ($tok eq '=') {
			$decl->{value} = $p->expect("value", "word string");
		} elsif ($tok eq ':') {
			$p->expect("default sign", "punct", "=");
			$decl->{default} = $p->expect("default", "word string");
		} else {
			push @toks, $tok;
		}
	}

	return undef unless @toks;

	$decl->{name} = pop @toks;
	$decl->{type} = pop @toks if @toks;
	map { $decl->{qual} = 1 } @toks;

	return $decl;
}

sub parse_args
{
	my $p = shift;

	my @args;

	while (1) {
		my $tok = $p->expect("args");
		last if $tok eq ')';
		next if $tok eq ',';
		$p->push_back($tok);
		push @args, $p->expect("args value", "word string");
	}

	return \@args;
}

sub parse_flags
{
	my $p = shift;

	my %flags;

	while (1) {
		my $tok = $p->expect("flags");
		last if $tok eq '}';
		next if $tok eq ',';
		my $sep = $p->expect("flag sep", "punct", ":");
		my $val = $p->expect("flag value", "word string");
		$flags{$tok} = $val;
	}

	return \%flags;
}

sub parse_alias
{
	my $p = shift;
	my @alias = ($p->expect(alias => "word"));

	while (1) {
		my $tok = $p->next_token();
		last unless defined $tok;
		if ($tok ne "or") {
			$p->push_back($tok);
			last;
		}
		push @alias, $p->expect(alias => "word");
	}

	return \@alias;
}

1;
