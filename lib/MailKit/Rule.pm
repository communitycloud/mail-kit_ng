#!/usr/bin/perl

package MailKit::Rule;

use strict;
use warnings;

use App::Message qw(mkrule:);
use Util::String qw(trim compile_glob);
use MailKit::CommandParser;
use Util::File qw(dirname cleanup_filename
	find_in_path path_stat_changed slurp);
use Util::Email qw(get_mime_header get_mail_address);
use Util::Email::Render qw(render_mime_text);
use Util::Export qw(mk_match_rule mk_list_rules);

our @path = (split (':', $ENV{MKIT_RULE_PATH} || ''),
	cleanup_filename(dirname(__FILE__) . '/../../rules'));
my %cache;

sub mk_match_rule($;$)
{
	my ($name, $ctx) = @_;

	my $rule = $cache{rule}{$name} ||= __PACKAGE__->parse(
		find_in_path($name, \@path)
	or error("couldn't find %q in path (%s)", $name, join(', ', @path)));

	if (defined $ctx) {
		$rule->match($ctx);
	}
}

sub mk_list_rules()
{
	my @rules;
	my %seen;

	for my $dir (@path) {
		opendir my $handle, $dir
			or next;
		for my $e (readdir $handle) {
			next unless -f "$dir/$e";
			next if $seen{$e}++;
			push @rules, $e;
		}
	}

	@rules;
}

sub new
{
	my ($class, $logic) = @_;
	bless {
		logic => uc($logic || 'OR'),
		rules => [],
	}, $class;
}

sub parse
{
	my ($class, $file) = @_;
	my $r = $class->new();
	my $p = MailKit::CommandParser->new($r);
	$p->parse($file, 'utf8');
	$r;
}

sub syntax
{
	my @syntax = (
		"header . *",
		"smtp . *",
		"mail . *",
		"mime type +",
		"mime disposition +",
		"mime attachment +",
		"body line +",
		"body text +",
		"body grep +",
		"set logic AND",
		"set logic OR",
		"match *",
		"true",
		"false",
		"rule +",
		"begin",
		"end");
	@syntax, map { "!$_" } @syntax;
}

sub emit
{
	my ($r, $op, @arg) = @_;

	my $e = $r->{stack}->[0] || $r;

	if ($op eq 'begin') {
		my $rule = __PACKAGE__->new($arg[0]);
		$e->add(rule => $rule);
		push @{$r->{stack}}, $rule;
	} elsif ($op eq 'end') {
		pop @{$r->{stack}}
			or error('unexpected end');
	} elsif ($op eq 'set') {
		$e->{$arg[0]} = $arg[1];
	} elsif ($op eq 'mail' && $arg[0] eq 'header') {
		$e->add('header', $arg[1], $arg[2]);
	} else {
		$e->add($op, @arg);
	}
}

sub compile_pattern($)
{
	my $p = shift;

	trim($p);

	return qr// unless length $p;

	if (my ($re, $flags) = $p =~ m#^/(.*)/(\w*)$#) {
		error('invalid regexp flags %q', $flags)
			if $flags =~ /[^ismx]/;
		return qr/(?$flags:$re)/;
	} elsif (my ($q, $s, $i) = $p =~ m/^(["'])(.*)\1([i]?)$/) {
		return qr/\A(?$i:\Q$s\E)\Z/;
	} elsif (my ($g, $f) = $p =~ m/^\?(.*)\?(\w*)$/) {
		return compile_glob($g, $f);
	} elsif ((($g) = $p =~ m/^glob\(\s*(.*?)\s*\)$/) ||
			 (($g) = $p =~ m/^glob:\s*(.*?)\s*$/)) {
		return compile_glob($g, "sC");
	} elsif ((($g) = $p =~ m/^loose\(\s*(.*?)\s*\)$/) ||
			 (($g) = $p =~ m/^loose:\s*(.*?)\s*$/)) {
		return compile_glob($g, "l");
	} elsif (($g) = $p =~ m/~\s*(.*)/) {
		return compile_glob($g, "l");
	} elsif (($g) = $p =~ m/#\s*(.*)/) {
		return compile_glob($g, "sC");
	} else {
		return compile_glob($p, "aC");
	}
}

sub match_pattern($$)
{
	my ($value, $pattern) = @_;
	return 0 unless defined $value;
	return 1 unless defined $pattern;

	if (ref $value eq 'ARRAY') {
		for my $v (@{$value}) {
			return 1 if $v =~ $pattern;
		}
		return 0;
	}

	return $value =~ $pattern;
}

sub add
{
	my ($r, $type, $a1, $a2) = @_;

	my $not = $type =~ s/^!// ? 1 : 0;

	if ($type eq 'header') {
		push @{$r->{rules}}, [$not, $type => $a1,
			compile_pattern($a2)];
	} elsif ($type eq 'smtp') {
		push @{$r->{rules}}, [$not, $type => $a1,
			compile_pattern($a2)];
	} elsif ($type eq 'match') {
		push @{$r->{rules}}, [$not, $type =>
			compile_pattern($a1)];
	} elsif ($type eq 'rule') {
		push @{$r->{rules}}, [$not, $type => $a1];
	} elsif ($type eq 'true' || $type eq 'false') {
		push @{$r->{rules}}, [$not, $type];
	} elsif ($type =~ /^(mail|mime|body)$/) {
		push @{$r->{rules}}, [$not, $type => $a1,
			compile_pattern($a2)];
	} else {
		error('invalid rule type %q', $type);
	}
}

sub match_header
{
	my ($r, $ctx, $header, $pattern) = @_;
	if (my @headers = get_mime_header($ctx, $header)) {
		match_pattern(\@headers, $pattern);
	} else {
		return 0;
	}
}

sub match_smtp
{
	my ($r, $ctx, $field, $pattern) = @_;
	return 0 unless ref $ctx eq 'HASH';
	$field .= 's' if $field eq 'recipient';
	return match_pattern($ctx->{$field}, $pattern);
}

sub match_rule
{
	my ($r, $ctx, $rule) = @_;

	if (ref($rule)) {
		$rule->match($ctx);
	} else {
		mk_match_rule($rule, $ctx);
	}
}

sub match_body
{
	my ($r, $ctx, $name, $pattern) = @_;
	return 0 unless ref $ctx && UNIVERSAL::isa($ctx, 'MIME::Entity');

	if ($name eq 'text') {
		$cache{text}{$ctx} = render_mime_text($ctx)
			unless defined $cache{text}{$ctx};
		my $text = $cache{text}{$ctx};
		return $text =~ $pattern;
	} elsif ($name eq 'line') {
		my $body = $ctx->bodyhandle
			or return 0;
		my $io = $body->open('r')
			or return 0;
		while (my $line = <$io>) {
			$line =~ s/\n\r?$//;
			return 1 if $line =~ $pattern;
		}
		return 0;
	}

	return 0;
}

sub match_mime
{
	my ($r, $ctx, $name, $pattern) = @_;
	return 0 unless ref $ctx && UNIVERSAL::isa($ctx, 'MIME::Entity');
	if ($name eq 'attachment') {
		for my $part ($ctx->parts_DFS) {
			my $disposition = $part->head->get('Content-Disposition') or next;
			my $filename = $part->head->recommended_filename or next;
			if ($disposition =~ /attachment/i && $filename =~ $pattern) {
				return 1;
			}
		}
	} elsif ($name eq 'type') {
		return 1 if $ctx->mime_type =~ $pattern;
	} elsif ($name eq 'disposition') {
		my $disposition = $ctx->head->mime_attr('content-disposition');
		return 1 if defined $disposition && $disposition =~ $pattern;
	} else {
		return 0;
	}
}

sub match_mail
{
	my ($r, $ctx, $name, $pattern) = @_;
	return 0 unless ref $ctx && UNIVERSAL::isa($ctx, 'MIME::Entity');
	if ($name =~ /^(from|to|sender|cc|bcc)$/) {
		for my $header (get_mime_header($ctx, $name)) {
			my @addresses = get_mail_address($header);
			return 1 if match_pattern(\@addresses, $pattern);
		}
	} elsif ($name =~ /^(subject)$/) {
		my $header = get_mime_header($ctx, $name);
		$header = '' unless defined $header;
		return match_pattern($header, $pattern);
	} else {
		return 0;
	}
}

sub match
{
	my ($r, $ctx) = @_;

	my $ret = 0;

	for my $rule (@{$r->{rules}}) {
		my ($not, $type, @args) = @{$rule};
		my $match = "match_$type";
		if ($type eq 'true') {
			$ret = 1;
		} elsif ($type eq 'false') {
			$ret = 0;
		} elsif ($type eq 'match') {
			$ret = match_pattern($ctx, $args[0]);
		} elsif ($r->can($match)) {
			$ret = $r->$match($ctx, @args);
		}

		$ret = !$ret if $not;

		return 0 if $r->{logic} eq 'AND' && !$ret;
		return 1 if $r->{logic} eq 'OR' && $ret;
	}

	return $ret;
}

sub dump
{
	my ($r, $d) = @_;

	my $i = "  " x ($d || 0);

	if (my $op = $r->{logic}) {
		printf STDERR "%slogic %s\n",
			$i, $op;
	}

	for my $rule (@{$r->{rules}}) {
		my ($type, @arg) = @{$rule};
		if ($type eq 'rule') {
			if (ref $arg[0]) {
				printf STDERR "%sbegin\n", $i;
				$arg[0]->dump($d || 0 + 1);
				printf STDERR "%send\n", $i;
			} else {
				printf STDERR "%srule %s\n", $i, $arg[0];
			}
		} else {
			printf STDERR "%s%s\n",
				$i, join (' ', $type, @arg);
		}
	}
}

1;
