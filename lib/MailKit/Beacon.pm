#!/usr/bin/perl

package MailKit::Beacon;

use strict;
use warnings;

use App::Message qw(beacon:);
use MailKit::Sendmail qw(sendmail_uri);
use MailKit::Compiler;
use MailKit::Assembler;
use POSIX qw(strftime);
use Sys::Hostname;
use Util::String qw(short_id parse_uri);
use Util::Time qw(make_duration);
use Net::IMAP::Client;
use MIME::Parser;

my $parser = get_parser();

sub get_parser
{
	my $parser = MIME::Parser->new;
	$parser->extract_nested_messages(0);
	$parser->output_under($ENV{TEMP} || '/tmp');
	return $parser;
}

sub new
{
	my ($class, %config) = @_;
	bless \%config, $class;
}

sub send
{
	my $b = shift;
	my $data = $b->compile();

	sendmail_uri($data, $b->{smtp},
		sender => $b->{sender},
		recipients => $b->{recipient});

	info('sent beacon %s:%s', $b->{name}, $b->{id});
}

sub compile
{
	my $b = shift;

	my $c = MailKit::Compiler->new();
	my $a = MailKit::Assembler->new();
	my $p = MailKit::CommandParser->new($a);

	$c->context->add($b->context);

	my $as = $c->compile($b->{template});
	$p->parse(\$as);
	$a->assemble();
}

sub recv
{
	my $b = shift;
	my $c = parse_uri($b->{imap})
		or error('invalid imap URI');

	if ($c->{scheme} eq 'imaps') {
		$c->{port} ||= 993;
		$c->{query}->{ssl} = 1;
	}

	my $imap = Net::IMAP::Client->new(
		server => $c->{host},
		user   => $c->{user},
		pass   => $c->{pass},
		port   => $c->{port},
		ssl    => $c->{query}->{ssl},
	) or error('cannot connect to imap server (%s): %s',
		$b->{name}, error_string);

	$imap->login or
		error('imap login failed (%s): %s', $b->{name}, $imap->last_error);

	$imap->select($c->{inbox} || 'INBOX');

	my $criteria = "UNSEEN HEADER X-MailKit-Beacon-Name \"$b->{name}\"";
	my $ids = $imap->search($criteria);
	for my $id (@{$ids}) {
		my $data = $imap->get_rfc822_body($id);
		my $mime = $parser->parse_data($data);

		my $id = $mime->head->get('X-MailKit-Beacon-Name');
		my $name = $mime->head->get('X-MailKit-Beacon-Id');
		my $timestamp = $mime->head->get('X-MailKit-Beacon-Timestamp');

		chomp ($id, $name, $timestamp);

		info('received beacon %s:%s sent %s ago',
			$name, $id, make_duration(time - $timestamp));
	}
}

sub context
{
	my $b = shift;


	my $date = strftime("%e %b %Y %R %Z", localtime);
	$date =~ s/^ //;

	my $id = $b->{id} ||= short_id(undef, 4);

	return {
		id => $id,
		name => $b->{name},
		sender => $b->{sender},
		recipient => $b->{recipient},
		hostname => hostname(),
		date => $date,
		timestamp => time,
	};
}

1;
