#!/usr/bin/perl
#
# This module is adapted from smtpprox
# at http://bent.latency.net/smtpprox/
#
package MailKit::Proxy;

use strict;
use warnings;

use MSDW::SMTP::Server;
use MSDW::SMTP::Client;
use Socket qw(inet_ntoa sockaddr_in);

use App::Message qw(proxy:);
use App::Proc qw(afork);
use Util::Package qw(load_package);

sub new
{
	my ($class, $listen, $talk, %arg) = @_;

	my ($server_addr, $server_port,
		$client_addr, $client_port);

	unless (defined $listen) {
		error("usage error: no listen address specified");
	}

	($server_addr, $server_port) = split ':', $listen, 2;

	defined $server_port
		or error("usage error: invalid listen address specified: %s", $listen);

	if (defined $talk) {
		($client_addr, $client_port) = split ':', $talk, 2;
		defined $client_port
			or error("usage error: invalid talk address specified: %s", $talk);
	}

	return bless {
		children => {},
		server_addr => $server_addr,
		server_port => $server_port,
		client_addr => $client_addr,
		client_port => $client_port,
		servers => $arg{servers} || 16,
		min_requests_per_child => $arg{min_requests_per_child} || 20,
		max_requests_per_child => $arg{max_requests_per_child} || 60,
	}, $class;
}

sub kill
{
	kill $_[1], keys %{$_[0]->{children}};
}

sub nop
{
	return $_[0] if ref $_[0];
	return undef;
}

sub bind
{
	my ($p) = @_;

	$p->{server} = MSDW::SMTP::Server->new(
		interface => $p->{server_addr},
		port => $p->{server_port});

	if ($p->{client_addr}) {
		info("listen=%s:%s talk=%s:%s",
			$p->{server_addr}, $p->{server_port},
			$p->{client_addr}, $p->{client_port});
	} else {
		info("listen=%s:%s",
			$p->{server_addr}, $p->{server_port});
	}
}

sub prefork
{
	my $p = shift;
	if (scalar keys %{$p->{children}} >= $p->{servers}) {
		my $pid = wait;
		delete $p->{children}->{$pid};
	} else {
		my $pid = afork()
			or return 0;
		$p->{children}->{$pid}++;
		return $pid;
	}
}

sub run
{
	my ($p, $cb, @arg) = @_;

	my $lives = $p->{min_requests_per_child}
		+ int(rand($p->{max_requests_per_child} - $p->{min_requests_per_child}));

	while ($lives-- > 0)
	{
		$p->{server}->accept();

		my ($port, $addr) = sockaddr_in($p->{server}->{peeraddr});
		note("new connection from %s", inet_ntoa($addr));

		my $client;
		if (defined $p->{client_addr}) {
			$client = MSDW::SMTP::Client->new(
				interface => $p->{client_addr},
				port => $p->{client_port});
		}

		my $banner = $client ? $client->hear() : "220 MailKit Proxy";
		$p->{server}->ok($banner);

		while (my $what = $p->{server}->chat) {
			if ($what eq '.') {
				my $data = $cb
					? $cb->($p->{server}, @arg)
					: $p->{server}->{data};
				$client && $client->yammer2($data);
			} else {
				$client && $client->say($what);
			}

			if ($client) {
				$p->{server}->ok($client->hear());
			} else {
				if ($what =~ /^data/i) {
					$p->{server}->ok('354 Go');
				} elsif ($what =~ /^quit/i) {
					$p->{server}->ok('221 Bye');
				} else {
					$p->{server}->ok();
				}
			}
		}

		$client = undef;
		delete $p->{server}->{s};
	}

	exit 0;
}

1;
