#!/usr/bin/perl

# Copyright (C) 2011 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package MailKit::SMTP::Server;

use strict;
use warnings;

use App::Message qw(smtpd:);
use App::Proc qw(app_prefork child_proc_busy child_proc_idle %kids);
use IO::Socket::INET;
use Socket qw(inet_ntoa sockaddr_in);
use MailKit::SMTP::Connection;
use Util::String qw(strneq);

sub new
{
	my ($class, %server) = @_;
	bless {
		host => undef,
		port => 1025,
		handle => \&handle,
		%server,
	}, $class;
}

sub bind
{
	my $s = shift;
	$s->{sock} = IO::Socket::INET->new(
		LocalAddr => $s->{host},
		LocalPort => $s->{port},
		ReuseAddr => 1,
		Listen    => 65536)
		or error("can't bind: %s", $!);

	info("accepting connections host=%s port=%s",
		$s->{sock}->sockhost,
		$s->{sock}->sockport);
}

sub handle
{
	my $c = shift;

	$c->{debug}++;

	$c->banner();
	while (my $cmd = $c->talk) {
		$c->reply;
	}
}

sub run
{
	my $s = shift;

	$s->{sock} || $s->bind;

	my $job = sub {
		my ($client, $peeraddr) = $s->{sock}->accept()
			or error("accept failed: %s", $!);
		child_proc_busy();

		my ($port, $addr) = sockaddr_in($peeraddr);
		my $ip = inet_ntoa($addr);

		if ($s->acl_check($ip)) {
			note("new connection from %s", $ip);
			$s->{handle}->(
				MailKit::SMTP::Connection->new($client));
		} else {
			info("connection from %s denied", $ip);
		}

		child_proc_idle();
	};

	app_prefork(
		job => $job,
		runs => 4,
		min_kids => 2,
		max_kids => 12,
		min_spare => 2,
	);
}

sub kill
{
	my $s = shift;
	kill 15 => keys %kids;
}

sub acl_check
{
	my ($s, $ip) = @_;

	if (!ref $s->{acl}) {
		return 1;
	}

	my @allow = ref $s->{acl}->{allow} ? @{$s->{acl}->{allow}} : ($s->{acl}->{allow});
	my @deny = ref $s->{acl}->{deny} ? @{$s->{acl}->{deny}} : ($s->{acl}->{deny});

	for my $m (@allow) {
		return 1 if acl_match($m, $ip);
	}

	for my $m (@deny) {
		return 0 if acl_match($m, $ip);
	}

	return 1;
}

sub acl_match($$)
{
	my ($m, $i) = @_;

	return 0 if !defined $m;

	if ($m eq '*') {
		return 1;
	}

	if (substr($m, -1) eq '.') {
		return strneq($m, $i);
	}

	return $m eq $i;
}

1;
