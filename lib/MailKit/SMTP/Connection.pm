#!/usr/bin/perl

# Copyright (C) 2011 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package MailKit::SMTP::Connection;

use strict;
use warnings;

use App::Message qw(smtpd:);
use Sys::Hostname;
use Util::String qw(trim);

sub new
{
	my ($class, $sock) = @_;
	bless {
		helo => undef,
		sender => undef,
		recipients => [],
		replicate => undef,
		cmd => '',
		arg => undef,
		data => undef,
		indata => 0,
		debug => $ENV{MK_SMTP_DEBUG},
		sock => $sock
	}, $class;
}

sub reset
{
	my $c = shift;
	$c->{sender} = undef;
	$c->{recipients} = [];
	$c->{replicate} = undef;
}

sub banner
{
	my ($c, $name) = @_;

	if ($c->{debug}) {
		printf STDERR "=== Connection from %s:%s.\n",
			$c->{sock}->peerhost, $c->{sock}->peerport();
	}

	$c->reply(220, hostname, $name || 'Mail-Kit');
}

sub parse_command($)
{
	my $line = shift;

	trim($line);

	$line =~ /^(helo|ehlo)\s+(.+)/i or
	$line =~ /^(mail\s+from|rcpt\s+to):(.*)/i or
	$line =~ /^(data|rset|help|noop|quit)$/i or
	$line =~ /^(replicate)\s+(.+)$/i or # ext
		return ('error', $line);

	my ($cmd, $arg) = ($1, $2);
	$cmd = lc $cmd;
	$cmd =~ s/\s+/ /;

	if ($cmd eq 'mail from' || $cmd eq 'rcpt to') {
		trim($arg);
		$arg =~ s/^<>.+/<>/;
		$arg =~ s/^<(.+)>.*/$1/;
	} elsif ($cmd eq 'ehlo') {
		$cmd = 'helo';
	}

	return ($cmd, $arg);
}

sub capture
{
	require IO::File;
	$_[0]->{data} = IO::File->new_tmpfile;
}

sub talk
{
	my $c = shift;

	my $line = $c->readline;
	return () unless defined $line;

	if ($c->{indata} == 0) {
		($c->{cmd}, $c->{arg}) = parse_command($line);

		if ($c->{cmd} eq 'helo') {
			$c->{helo} = $c->{arg};
		} elsif ($c->{cmd} eq 'rset') {
			$c->reset();
		} elsif ($c->{cmd} eq 'mail from') {
			$c->{sender} = $c->{arg};
		} elsif ($c->{cmd} eq 'rcpt to') {
			push @{$c->{recipients}}, $c->{arg};
		} elsif ($c->{cmd} eq 'replicate') {
			$c->{replicate} = $c->{arg};
		}
	} else {
		if ($line eq "\r\n" && ($c->peekline || "") eq ".\r\n") {
			$c->readline();
			$c->{cmd} = '.';
			$c->{indata} = 0;
		} elsif ($line eq ".\r\n") {
			$c->{cmd} = '.';
			$c->{indata} = 0;
		} else {
			$line =~ s/^\.\./\./;
			$c->{cmd} = 'data line';
			$c->{arg} = $line;
		}
	}

	if ($c->{data}) {
		if ($c->{cmd} eq 'data line') {
			$c->{data}->print($line)
				or error("write error: %s", $!);
		} elsif ($c->{cmd} eq '.') {
			seek $c->{data}, 0, 0;
		} elsif ($c->{cmd} eq 'data') {
			truncate $c->{data}, 0;
			seek $c->{data}, 0, 0;
		}
	}

	return wantarray ? ($c->{cmd}, $c->{arg}) : $c->{cmd};
}

sub readline
{
	my $c = shift;
	if (exists $c->{peekline}) {
		return delete $c->{peekline};
	}
	return undef unless $c->{sock}->opened;
	local $/ = "\r\n";
	my $line = scalar readline $c->{sock};
	if ($c->{debug} && defined $line) {
		chomp(my $line = $line);
		print STDERR "<-  $line\n";
	}
	return $line;
}

sub peekline
{
	my $c = shift;
	delete $c->{peekline};
	$c->{peekline} = $c->readline();
}

sub reply
{
	my ($c, @msg) = @_;

	if (@msg) {
		if ($c->{cmd} eq 'data') {
			if ($msg[0] =~ /^354\b/) {
				$c->{indata} = 1;
			} else {
				$c->{indata} = 0;
			}
		}
		$c->{sock}->print("@msg\r\n")
			or error("write error: %s", $!);
		print STDERR " -> @msg\n"
			if $c->{debug};
		if ($c->{cmd} eq 'quit') {
			if ($msg[0] =~ /^221\b/) {
				$c->close();
			}
		}
		return 1;
	}

	@msg = (250, 'Ok');

	if ($c->{cmd} eq 'data') {
		if (!defined $c->{sender}) {
			@msg = (503, "No sender");
		} elsif (!@{$c->{recipients}}) {
			@msg = (554, "No recipients");
		} else {
			@msg = (354, 'End data with <CR><LF>.<CR><LF>');
		}
	} elsif ($c->{cmd} eq 'mail from' || $c->{cmd} eq 'rcpt to') {
		if (!defined $c->{helo}) {
			@msg = (503, 'Greet first');
		}
	} elsif ($c->{cmd} eq 'data line') {
		return 1;
	} elsif ($c->{cmd} eq '.') {
		@msg = (250, 'Got it');
	} elsif ($c->{cmd} eq 'quit') {
		@msg = (221, 'Bye');
	} elsif ($c->{cmd} eq 'help') {
		@msg = (214, 'No help available');
	} elsif ($c->{cmd} eq 'noop') {
		@msg = (252, 'Ok');
	} elsif ($c->{cmd} eq 'error') {
		@msg = (500, 'Command unrecognized')
	}

	$c->reply(@msg);
}

sub close
{
	my $c = shift;

	if ($c->{sock}->opened) {
		close $c->{sock};
		if ($c->{debug}) {
			print STDERR "=== Connection closed.\n";
		}
	}
}

sub DESTROY
{
	$_[0]->close();
}

1;
