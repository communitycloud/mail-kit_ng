#!/usr/bin/perl

# Copyright (C) 2011 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package MailKit::SMTP::Client;

use strict;
use warnings;

use App::Message qw(smtpc:);
use IO::Socket::INET;
use Util::File qw(fopen);
use Sys::Hostname;

sub new
{
	my ($class, $server, %config) = @_;

	my $c = bless {
		debug => $ENV{MK_SMTP_DEBUG} || $config{debug},
	}, $class;

	$c->{sock} = IO::Socket::INET->new(
		PeerAddr => $server,
		Timeout => $config{timeout},
	) or error("connecting to %s failed: %s", $server, $!);

	if ($c->{debug}) {
		printf STDERR "=== Connected to %s:%s\n",
			$c->{sock}->peerhost, $c->{sock}->peerport;
	}

	return $c;
}

sub say
{
	my ($s, @msg) = @_;
	$s->send("@msg\r\n");
}

sub send
{
	my ($s, $data) = @_;
	if ($s->{debug}) {
		CORE::print STDERR " -> $data\n";
	}
	$s->{sock}->print($data)
		or error("write error: %s", $!);
}

# MIME::Entity print() compatibility
sub print
{
	my ($s, $data) = @_;

	while ($data =~ m/^(.*)\r?(\n|\Z)/mg) {
		$s->data_line($1);
	}
}

sub hear
{
	my $c = shift;

	local $/ = "\r\n";
	my $reply = $c->{sock}->getline()
		or return undef;

	while ($reply =~ /^\d{3}-.*\Z/m) {
		my $cont = $c->{sock}->getline()
			or return undef;
		$reply .= $cont;
	}

	if ($c->{debug}) {
		for my $line (split "\r\n", $reply) {
			print STDERR "<- $line\n";
		}
	}

	chomp $reply;
	return $reply;
}

sub talk
{
	my ($c, $cmd, $arg) = @_;

	my ($line, $code);

	$c->{banner} ||= $c->expect("banner");

	if ($cmd eq 'rcpt to' || $cmd eq 'mail from') {
		$line = sprintf("%s:<%s>", uc $cmd, $arg);
	} elsif (defined $arg) {
		$line = sprintf("%s %s", uc $cmd, $arg);
	} else {
		$line = uc $cmd;
	}

	if ($cmd eq 'data') {
		$code = 354;
	} elsif ($cmd eq 'data line') {
		$c->send($arg);
		return;
	} elsif ($cmd eq '.') {
		$line = "\r\n\.";
	} elsif ($cmd eq 'quit') {
		$code = 221;
	}

	$c->say($line);
	$c->expect($cmd, $code);
}

sub expect
{
	my ($s, $expect, $code) = @_;

	my $reply = $s->hear()
		or error("in smtp %s: connection closed", $expect);

	$code ||= 2;

	unless ($reply =~ /^$code/) {
		error("in smtp %s: %s", $expect, $reply);
	} else {
		return $reply;
	}
}

sub helo
{
	shift->talk('helo', join ' ', hostname(), @_);
}

sub ehlo
{
	$_[0]->talk('ehlo', $_[1] || hostname());
}

sub from
{
	$_[0]->talk('mail from', $_[1]);
}

sub to
{
	my $c = shift;
	for my $r (@_) {
		$c->talk('rcpt to', $r);
	}
}

sub replicate
{
	my ($c, $id) = @_;
	$c->talk('replicate', $id);
}

sub data
{
	my ($c, $data) = @_;

	if (@_ == 1) {
		$c->talk('data');
	} else {
		$c->data();
		my $fh = fopen($data);
		while (defined (my $line = readline $fh)) {
			chomp $line;
			$c->data_line($line);
		}
		$c->data_end();
	}
}

sub data_line
{
	my ($c, $line) = @_;
	$line =~ s/^\./../;
	$c->send("$line\r\n");
}

sub data_end
{
	my $c = shift;
		$c->send("\r\n.\r\n");
		$c->expect('data');
}

sub rdata
{
	my ($c, $data) = @_;
	local $/ = "\r\n";
	$c->data($data);
}

sub quit
{
	$_[0]->talk('quit');
}

sub close
{
	my $c = shift;

	if ($c->{sock} && $c->{sock}->opened) {
		close $c->{sock};
		if ($c->{debug}) {
			print STDERR "=== Connection closed.\n";
		}
	}
}

sub DESTROY
{
	$_[0]->close();
}

1;
