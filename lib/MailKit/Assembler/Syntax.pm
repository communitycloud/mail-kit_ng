#!/usr/bin/perl

package MailKit::Assembler::Syntax;

use strict;
use warnings;

our @syntax = (
	"+ rfc822date *",
	"+ rfc822id *",
	"+ subject +",
	"+ recipient +",
	"+ sender +",
	"+ header . +",
	"+ verp +",
	"+ attach +",
	"+ embed +",
	"+ prepend",
	"+ text +",
	"+ html +",
	": set cwd +",
	": set format +",
);

1;
