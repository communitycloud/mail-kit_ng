#!/usr/bin/perl

package MailKit::Store;

use App::Message qw(mkstore:);
use Util::File qw(mkhier rmtree fopen fcopy slurp spew is_handle);
use Util::String qw(html2text utf8_encode parse_uri);
use Util::Email qw(slurp_mime_part get_mime_header);
use Util::Email::Render qw(render_mime_text render_mime_html list_mime_structure);
use MailKit::Rule qw(mk_match_rule mk_list_rules);
use Hash::Config;
use Date::Parse;
use Util::Time qw(parse_duration make_duration);
use JSON v2;
use S3::Lite;
use JSON::Events;

# A new kind of spam uses charset IBM037 to encode content.
# This makes perl recognise the charset appropriately.
use Encode;
use Encode::Alias;
define_alias(qr/^cp0+(\d+)$/i => '"cp$1"');

use strict;
use warnings;

sub new
{
	my ($class, $path) = @_;

	bless {
		path => $path,
		id_charset => ['a'..'z', 'A'..'Z', '0'..'9'],
		id_length => 9,
		id_hash_length => 2,
		s3_client => {},
	}, $class;
}

sub mkdirs
{
	my $s = shift;
	mkhier($s->path());
	mkhier($s->path('etc'));
	mkhier($s->path('run'));
	mkhier($s->path('tmp'));
}

sub preload
{
	require MIME::Parser;
	require MIME::Head;
	require MIME::WordDecoder;
	require MailKit::SMTP::Client;
	require ECS::API;
	require IO::File;
	require Digest::SHA;
	require Convert::TNEF;
	require MIME::Types;
	mk_match_rule($_)
		for mk_list_rules();
	require IO::Compress::Gzip;
	require IO::Uncompress::Gunzip;
}

sub path
{
	my ($s, $id, @path) = @_;

	if (@_ == 1) {
		return $s->{path};
	}

	if (length $id == $s->{id_length}) {
		unshift @path,
		substr($id, 0, $s->{id_hash_length}),
		substr($id, $s->{id_hash_length});
	} else {
		unshift @path, $id;
	}

	error("store path not defined") unless $s->{path};
	return join '/', $s->{path}, @path;
}

sub parser
{
	require MIME::Parser;
	$_[1] ||= 'ondisk';
	$_[0]->{parser}->{$_[1]} ||= do {
		my $parser = MIME::Parser->new;

		if ($_[1] eq 'ondisk') {
			unless (-d $_[0]->path('tmp')) {
				mkhier($_[0]->path('tmp'));
			}

			$parser->output_under($_[0]->path('tmp'));
		} elsif ($_[1] eq 'incore') {
			$parser->output_to_core(1);
		} else {
			internal_error("invalid parser type %q", $_[1]);
		}

		$parser->extract_nested_messages(0);
		$parser;
	};
}

sub ecs
{
	require ECS::API;
	$_[0]->{ecs} ||= ECS::API->new;
}

sub generate_id
{
	my $s = shift;

	while (1) {
		my $id = "";
		for (1 .. $s->{id_length}) {
			$id .= $s->{id_charset}
				[int rand(@{$s->{id_charset}})]
		}

		unless (-d $s->path($id)) {
			return $id;
		}
	}
}

sub index
{
	$_[0]->{index} ||= fopen($_[0]->path('index'), mode => '>>');
}

sub config
{
	my $s = shift;

	my $config = $s->path('/etc/store.cfg');
	if (-e $config) {
		$s->{config} ||= Hash::Config->parse($config)
			or error("failed to parse store.cfg: %s", error_string());
	} else {
		return {};
	}
}

sub exists
{
	my $s = shift;
	my $id = shift;
	-d $s->path($id) &&
		(-f $s->path($id, 'smtp'));
}

sub delete
{
	my ($s, $id) = @_;
	unless (defined $id) {
		warning('undefined id in delete()');
		return 0;
	}

	unless ($s->exists($id)) {
		debug('trying to delete nonexistent message id=%s', $id);
		return 1;
	}

	rmtree($s->path($id), 1)
		or error('failed to delete message id=%s: %s',
		$id, $!);
	note('message deleted id=%s', $id);
	1;
}

sub add
{
	require MIME::WordDecoder;

	my ($s, $data, $sender, $recipients, $id, $replicate, $import) = @_;

	# Index flags to keep track of message properties
	my $flags = '';

	# Allocate a new id for the message and save the data
	if ($id) {
		$id =~ s#/##g;
		error("invalid id=%e supplied", $id)
			unless length $id eq $s->{id_length};
		# this message is being replicated by another store
		$flags .= 'R' if $replicate;
	} else {
		$id = $s->generate_id;
	}

	# Normalise sender and recipients value
	if (defined $sender) {
		$sender = remove_batv_signature($sender);
	} else {
		error("cannot add message without sender");
	}

	unless (ref($recipients) && @$recipients) {
		error("cannot add message without recipients");
	}

	# Store SMTP data and properties
	$s->add_smtp($id, $data, $sender, $recipients);

	# Generate META properties
	my $meta = eval { $s->add_meta($id, $import) };

	# Flag the entry as error to prevent further processing
	if ($@) {
		warning("meta generation failed: %s", $@);
		$flags .= 'E';
	} else {
		$flags .= $s->get_flags($meta);
	}

	# S3 handling
	if ($s->s3_check($id)) {
		# catch any unaccounted s3 errors
        eval {
            if ($s->s3_upload($id, $meta)) {
                $flags .= '3';
            }
        };

		if ($@) {
			warning("s3 handling failed: %s", error_string());
		}
	}

	# purge parser's files
	$s->parser->filer->purge;

	# Log essential
	note("new message id=%s sender=%s recipients=%{pa:t} flags=%s",
		$id, $sender, $recipients, $flags);

	# Add the message to the index
	$s->add_index($id, $flags);

	# Generate store event
	if ($s->events) {
		$s->events_store($id, $sender, $recipients, $flags, $meta);
	}

	# Return the store ID
	return $id;
}

sub add_smtp
{
	my ($s, $id, $data, $sender, $recipients) = @_;

	unless (-d $s->path($id)) {
		mkhier($s->path($id));
	}

	require MIME::Head;
	my $smtp = MIME::Head->new;

	$smtp->add('Sender', $sender);
	for my $r (@{$recipients}) {
		$smtp->add('Recipient', $r, -1);
	}

	$s->_write_compress($id, 'smtp', $smtp);
	$s->_write_compress($id, 'data', $data);

	# XXX return smtp structure to save a trip to $s->smtp
}

sub add_meta
{
	my ($s, $id, $import) = @_;

	# Load SMTP properties
	my $smtp = $s->smtp($id);

	require MIME::Head;
	# Create a MIME Head to store meta properties
	my $meta = MIME::Head->new;

	if (defined $smtp->{sender}) {
		$meta->add('Sender', $smtp->{sender});
	}

	for my $r (@{$smtp->{recipients}}) {
		$meta->add('Recipient', $r, -1);
	}

	if ($import) {
		$meta->add('Import', 'true');
	}

	# Data properties
	$meta->add('ID', $id);
	$meta->add('Size', -s $s->path($id, 'data'));

	# Parse the MIME message and extract its properties
	my $mime = $s->mime($id);

	# Common properties
	for my $header (qw(Subject From To CC In-Reply-To References Message-ID)) {
		for my $value (get_mime_header($mime, $header)) {
			$meta->add($header, $value);
		}
	}

	if (my $date = get_mime_header($mime, 'Date')) {
		eval { $meta->add('Date', str2time($date)) };
		if ($@) {
			warning('error parsing date %s: %s', $date, error_string);
			$meta->add('Date', time);
		}
	}

	$meta->add('Is-Spam', mk_match_rule('spam', $mime) ? 1 : 0);

	if (mk_match_rule('authentication-forged', $mime)) {
		$meta->add('Authentication', 'forged');
	} elsif (mk_match_rule('authentication-verified', $mime)) {
		$meta->add('Authentication', 'verified');
	} else {
		$meta->add('Authentication', 'unknown');
	}

	# Attachments
	$s->add_attachments($id, $mime, $meta);

	# Message type
	my $message_type = 'message';

	if ($mime->mime_type eq 'multipart/report' &&
		(my $report = extract_delivery_report($mime))) {
		$message_type = 'report';
		while (my ($key, $value) = each %$report) {
			$meta->add($key, $value, -1);
		}
	} elsif (mk_match_rule('autoresponse', $mime) ||
		mk_match_rule('autoresponse', $smtp)) {
		$message_type = 'autoresponse';
	}

	$meta->add('Message-Type', $message_type);

	# Text body and excerpt
	my $text = $s->add_text($id, $mime);
	my $html = $s->add_html($id, $mime);
	my $excerpt = get_excerpt($text);

	# Message hash
	my $subject = get_mime_header($mime, 'subject');
	for my $recipient (@{$smtp->{recipients}}) {
		$meta->add('Hash',
			hash_message($smtp->{sender}, $recipient, $subject, $excerpt), -1);
	}

	# Write meta
	$s->_write_meta($id, 'meta', $meta);

	# Structure
	if (($s->config->{process_structure}||'') eq 'on') {
		$s->add_structure($id, $mime);
	}

	return $meta;
}

sub add_text
{
	my ($s, $id, $mime) = @_;

	$mime ||= $s->mime($id);

	my $text = render_mime_text($mime);
	$s->_write_compress($id, 'text', \$text);

	return $text;
}

sub add_html
{
	my ($s, $id, $mime) = @_;

	$mime ||= $s->mime($id);

	my $html = render_mime_html($mime);
	$s->_write_compress($id, 'html', \$html);

	return $html;
}

sub add_tnef
{
	my ($s, $id, $meta, $io, $nattach) = @_;

	require Convert::TNEF;
	require MIME::Types;
	no warnings qw(once);

	my $tnef = Convert::TNEF->read($io, { output_dir => $s->path('tmp') });
	if (!$tnef) {
		warning("tnef convert error id=%s: %s", $id, $Convert::TNEF::errstr);
		return;
	}

	my $mimetypes = MIME::Types->new;
	for my $a ($tnef->attachments) {
		my $data = $a->datahandle() || $a->datahandle('Attachment');
		unless ($data) {
			warning("cannot get tnef datahandle id=%s name=%s: %s",
				$id, $a->longname, $Convert::TNEF::errstr);
			next;
		}

		my $io = $data->open("r");
		unless ($io) {
			warning("cannot open tnef bodyhandle id=%s name=%s", $id, $a->longname);
			next;
		}

		my $mimetype = $mimetypes->mimeTypeOf($a->longname) ||
			(-T $io ? "text/plain" : "application/octet-stream");
		unless (-s $io) {
			warning('corrupt tnef attachment detected: %s', $a->longname);
			next;
		}

		fcopy($io, $s->path($id, 'attachments', $$nattach++));
		$meta->add('Attachment', join('|', $a->longname, $mimetype, -s $io) , -1);
	}

	$tnef->purge();
}

sub add_attachments
{
	my ($s, $id, $mime, $meta) = @_;

	$mime ||= $s->mime($id);

	if (@_ == 3) {
		$meta = $s->meta($id);
	} else {
		$meta->delete('Attachment');
	}

	my $nattachment = 0;
	for my $part ($mime->parts_DFS) {
		if (mk_match_rule('attachment', $part))
		{
			my $filename = $part->head->recommended_filename || "";

			unless (-d $s->path($id, 'attachments')) {
				mkdir($s->path($id, 'attachments'))
					or error("mkdir attachments: %s", $!);
			}

			my $body = $part->bodyhandle
				or next;
			my $io = $body->open("r")
				or error("cannot open body handle id=%s filename=%s", $id, $filename);

			if ($part->mime_type eq 'application/ms-tnef') {
				$s->add_tnef($id, $meta, $io, \$nattachment);
			} else {
				fcopy($io, $s->path($id, 'attachments', $nattachment++));
				$meta->add('Attachment', join('|', $filename, $part->mime_type, -s $io) , -1);
			}
		}
	}

	if (@_ == 3) {
		$s->_write_meta($id, 'meta', $meta);
	}
}

sub add_index
{
	my ($s, $id, $flags) = @_;
	# Add the message to the index
	printf { $s->index } "%d %s %s\n",
		time, $id, $flags || 0;
	$s->index->flush();
}

sub add_structure
{
	my ($s, $id, $mime) = @_;

	my $headers = {};
	my $head = $mime->head;

	for my $name ($head->tags) {
		my @value = get_mime_header($mime, $name);
		if (@value > 1) {
			$headers->{$name} = [@value];
		} else {
			$headers->{$name} = $value[0];
		}
	}

	my $structure = {
		headers => $headers,
		body => list_mime_structure($mime),
	};

	my $json = JSON->new->utf8->pretty;
	my $data = $json->encode($structure);

	$s->_write_compress($id, 'structure', $data);
}

sub get_flags
{
	my ($s, $meta) = @_;
	
	my @flags = ();

	if (get_mime_header($meta, 'is-spam')) {
		push @flags, 's'
	}

	if (my $i = get_mime_header($meta, 'import')) {
		push @flags, 'I'
			if $i eq 'true';
	}

	if (my $a = get_mime_header($meta, 'authentication')) {
		if ($a eq 'forged') {
			push @flags, 'f'
		} elsif ($a eq 'verified') {
			push @flags, 'v'
		}
	}

	if (my $t = get_mime_header($meta, 'message-type')) {
		if ($t eq 'report') {
			push @flags, 'r'
		} elsif ($t eq 'autoresponse') {
			push @flags, 'a'
		} elsif ($t eq 'message') {
			push @flags, 'm'
		}
	}

	if (get_mime_header($meta, 'attachment')) {
		push @flags, 'A'
	}

	return join '', sort { lc $a cmp lc $b } @flags;
}

sub reload
{
	my ($s, $id, $what) = @_;

	return 0
		unless $s->exists($id);

	if (($what || 'meta') eq 'meta') {
		$s->add_meta($id);
	} elsif ($what eq 'text') {
		$s->add_text($id);
	} elsif ($what eq 'html') {
		$s->add_html($id);
	} elsif ($what eq 'attachments') {
		$s->add_attachments($id);
	} elsif ($what eq 'message') {
		my $smtp = $s->smtp($id);
		my $data = fopen($s->path($id, 'data'));
		$s->add($data, $smtp->{sender}, $smtp->{recipients});
	} else {
		error("invalid reload spec %q", $what);
	}
}

sub hash_message($$$$)
{
	require Digest::SHA;
	my ($sender, $recipient, $subject, $excerpt) = @_;
	my $string = join (';', $sender, $recipient, $subject, $excerpt);
	utf8::encode($string);
	return Digest::SHA::sha1_hex(join ';', $string);
}

sub get_excerpt($;$)
{
	my ($text, $length) = @_;

	$length ||= 1000;
	my $excerpt = '';

	for (my $i = 0; $i < length $text; $i++) {
		my $c = substr($text, $i, 1);
		next if $c =~ /[[:space:]]/;
		$excerpt .= $c;
		last if length $excerpt == $length;
	}

	return $excerpt;
}

sub extract_delivery_report($)
{
	my $mime = shift;

	my ($part) = grep { $_->effective_type eq 'message/delivery-status' }
		$mime->parts;
	return undef unless $part;

	my @lines = grep { !/^\s*$/ } slurp_mime_part($part);
	my $head = MIME::Head->new(\@lines);

	$head->unfold();

	my $report = {};

	for my $h (qw(
			Final-Recipient Original-Recipient Action
			Diagnostic-Code Status
		))
	{
		my $value = $head->get($h) or next;
		$value =~ s/[\n\r]+$//;
		$report->{$h} = $value;
	}

	unless (defined $report->{'Diagnostic-Code'}) {
		return undef;
	}

	return $report;
}

sub remove_batv_signature($)
{
	my $sender = shift;

	if ($sender =~ /^prvs=\d[0-9a-fA-F]+=([^@]+)(@.*)$/i) {
		return "$1$2";
	} elsif ($sender =~ /^prvs=([^=@]+)=\d[0-9a-fA-F]+(@.*)/i) {
		return "$1$2";
	} elsif ($sender =~ /^SRS0[+-=].+[=+](.+)=(\S+)\@\S+$/i) {
		return "$2\@$1";
	} elsif ($sender =~ /^SRS1[+-=]\S+=\S+==\S+=\S{2}=(\S+)=(\S+)\@\S+/i) {
		return "$2\@$1";
	} elsif ($sender =~ /^btv1==\d[0-9a-fA-F]+==([^@]+)(@.*)$/i) {
		return "$1$2";
	} else {
		return $sender;
	}
}

sub post
{
	my ($s, $id, $domain) = @_;

	my $meta = $s->meta($id);
	my $flags = $s->get_flags($meta);

	# pre cleanup hook
	if ($s->cleanup('pre_post', $id, undef, $flags)) {
		return undef;
	}

	if (mk_match_rule('post-skiplist', $meta)) {
		return undef;
	}

	unless (defined $domain) {
		if (my $recipient = $meta->get('Recipient')) {
			chomp $recipient;
			(undef, $domain) = split '@', $recipient, 2;
		}
	}

	unless (defined $domain) {
		error("post id=%s could not determine domain", $id);
	}

	my $param = [];
	my $message_type;

	my $head = $meta->header_hashref;
	while (my ($name, $values) = each %{$head}) {
		for my $value (@{$values}) {
			$value =~ s/[\n\r]+$//;
			push @{$param}, $name => $value;
			$message_type = $value if $name eq 'Message-Type';
		}
	}

	my $text_h = $s->_read_compress($id, 'text');
	push @{$param}, Text => scalar slurp($text_h);

	eval {
		my $html_h = $s->_read_compress($id, 'html');
		push @{$param}, HTML => scalar slurp($html_h);
	};

	if (($s->config->{process_structure}||'') eq 'on') {
		if (my $path = $s->_get_path($id, 'structure')) {
			my $structure_h = $s->_read_compress($id, 'structure');
			push @{$param}, Structure => scalar slurp($structure_h);
		}
	}

	my $operation = $message_type eq 'report' ? 'bounce' : 'message';
	$s->ecs->request($operation, $domain, $param)
		or error("post id=%s: %s", $id, error_string);

	# generate post event
	if ($s->events) {
		$s->events_post($id, $domain);
	}

	note("post id=%s domain=%s", $id, $domain);

	# post cleanup hook
	$s->cleanup('post_post', $id, undef, $flags);
}

sub _read_meta
{
	my ($s, $id, $entry) = @_;
	my $parser = $s->parser('incore');
	my $handle = $s->_read_compress($id, $entry);
	my $entity = eval { $parser->parse($handle) }
		or error("parse %s id=%s error: %s", $entry, $id, $@);
	$entity->head->unfold();
	return $entity->head();
}

sub _write_meta
{
	my ($s, $id, $entry, $mime) = @_;
	$s->_write_compress($id, $entry, $mime);
}

sub _get_path
{
	my ($s, $id, $entry) = @_;

	my $path = $s->path($id, $entry);
	my $compress = -e $path . '.gz';

	if ($compress) {
		$path .= '.gz';
	}

	return -e $path ? $path : undef;
}

sub _read_compress
{
	my ($s, $id, $entry) = @_;

	my $path = $s->path($id, $entry);
	my $compress = -e $path . '.gz';

	if ($compress) {
		$path .= '.gz';
	}

	my $handle = fopen($path, error => 0)
		or error("failed to open %q %s id=%s: %s",
			$entry, $compress ? 'compress entry' : 'entry', $id, $!);
	
	unless ($compress) {
		return $handle;
	}

	{
		no warnings qw(once);
		require IO::Uncompress::Gunzip;
		my $z = IO::Uncompress::Gunzip->new($handle)
			or error("failed to gunzip entry %s id=%s: %s",
			$entry, $id, $IO::Uncompress::Gunzip::GunzipError);
		return $z;
	}
}

sub _write_compress
{
	my ($s, $id, $entry, $data, $compress) = @_;

	my $path = $s->path($id, $entry);

	unless (defined $compress) {
		$compress = ($s->config->{compress}||'') eq 'on';
	}

	# check per-domain settings
	if ($compress && (my $domains = $s->config->{compress_domains})) {
		my $smtp = $s->smtp($id, $entry eq 'smtp' && ref($data) eq 'MIME::Head' ? $data : undef);
		$domains = [$domains] unless ref $domains;
		$compress = 0
			unless grep { $smtp->{domain} eq $_ } @{$domains};
	}

	# Normalise data value
	if (ref($data) eq 'MIME::Head') {
		$data->unfold();
		$data = $data->as_string();
	} elsif (ref($data) eq 'MIME::Entity') {
		$data = $data->as_string();
	} elsif (ref($data) eq 'SCALAR') {
		$data = (my $copy = $$data);
	} elsif (!defined $data) {
		$data = '';
	}

	if (utf8::is_utf8($data)) {
		utf8::encode($data);
	}

	if ($compress && (my $t = $s->config->{compress_threshold})) {
		my $size = 0;
		if (!ref($data)) {
			use bytes;
			$size = length($data);
		} elsif (is_handle($data) || -f $data) {
			$size = -s $data;
		}
		if ($size < $t) {
			$compress = 0;
		}
	}

	if ($compress) {
		$path .= '.gz';
	}

	my $handle = fopen($path, mode => '>', error => 0)
		or error("failed to open %q %s id=%s for writing: %s",
			$entry, $compress ? 'compress entry' : 'entry', $id, $!);

	if ($compress) {
		no warnings qw(once);
		require IO::Compress::Gzip;
		$handle = IO::Compress::Gzip->new($handle)
			or error("failed to gzip entry %s id=%s: %s",
			$entry, $id, $IO::Compress::Gunzip::GzipError);
	}

	unless (ref($data)) {
		$handle->print($data);
	} else {
		fcopy($data, $handle);
	}

	close($handle)
		or error("failed to close %q %s id=%s: %s",
			$entry, $compress ? 'compress entry' : 'entry', $id, $!);
}

sub mime
{
	my ($s, $id) = @_;
	$s->parser->parse(
		$s->_read_compress($id, 'data'));
}

sub meta
{
	$_[0]->_read_meta($_[1], 'meta');
}

# server implementation

sub smtp
{
	my ($s, $id, $smtp) = @_;
	my $h = $smtp ? $smtp : $s->_read_meta($id, 'smtp');
	my $sender = $h->get('sender');
	my @recipients = $h->get('recipient');
	map chomp, $sender, @recipients;
	my $domain = (split('@', $recipients[0]), 2)[1];
	return { sender => $sender, recipients => \@recipients, domain => $domain };
}

sub smtp_server
{
	my ($s, $c) = @_;

	my $data = $c->capture();
	$c->banner("Mail-Kit Store");

	while (my $cmd = $c->talk) {
		if ($cmd eq '.') {
			my $id = $s->add($data, $c->{sender}, $c->{recipients},
				$c->{replicate}, !!$c->{replicate});
			$c->reply(250, "mail-kit stored as $id");
		} else {
			$c->reply();
		}
	}
}

sub sendmail
{
	my ($s, $id, $server, $replicate) = @_;

	unless ($s->exists($id)) {
		debug('trying to send unexisting message id=%s', $id);
		return 0;
	}

	my $meta = $s->meta($id);
	my $flags = $s->get_flags($meta);
	my $sender = $meta->get('Sender');
	my @recipients = $meta->get('Recipient');

	my $hook = $replicate ? 'replicate' : 'sendmail';

	# pre cleanup hook
	if ($s->cleanup("pre_$hook", $id, undef, $flags)) {
		return undef;
	}

	if (mk_match_rule('sendmail-skiplist', $meta)) {
		return undef;
	}

	unless (@recipients) {
		error("cannot send mail id=%s with no recipients", $id);
	}

	map chomp, $sender, @recipients;

	note("%s message id=%s server=%s",
		$replicate ? "replicate" : "send", $id, $server);

	require MailKit::SMTP::Client;
	my $client = MailKit::SMTP::Client->new($server);

	$client->helo();
	$client->from($sender);

	if ($replicate) {
		$client->replicate($id);
	}

	for my $r (@recipients) {
		$client->to($r);
	}

	my $h = $s->_read_compress($id, 'data');
	my $r = $client->rdata($h);

	$client->quit();

	# post cleanup hook
	$s->cleanup("post_$hook", $id, undef, $flags);

	return $r;
}

sub replicate
{
	$_[0]->sendmail($_[1], $_[2], 1);
}

# cleanup implementation

sub looks_like_bounce
{
	my ($s, $id) = @_;
	return 0 unless $s->exists($id);
	my $meta = eval { $s->meta($id) }
		or return undef;
	my $recipient = $meta->get('Recipient');
	chomp $recipient;
	return $recipient =~ /^(.+)=(.+)=(.+)@.*$/;
}

sub parse_age_spec($)
{
	my $spec = shift;

	$spec =~ s/_old$//;
	$spec =~ s/_//g;

	my $age = parse_duration($spec);

	if (!$age || $spec eq $age) {
		warning('error parsing age based rule: %s', $spec);
		return undef;
	}

	return $age;
}

sub match_rule
{
	my ($s, $hook, $rule, $id, $timestamp, $flags) = @_;

	# age based rule
	if ($hook =~ /^(.*)_old$/) {
		my $age = parse_age_spec($1)
			or return 0;

		if (time - $age < $timestamp) {
			debug('age based rule mismatch: message too recent id=%s hook=%s timestamp=%s',
				$id, $hook, $timestamp);
			return 0;
		}
	}

	if ($rule eq 'report') {
		return $flags =~ /r/;
	} elsif ($rule eq 'autoresponse') {
		return $flags =~ /a/;
	} elsif ($rule eq 'spam') {
		return $flags =~ /s/;
	} elsif ($rule eq 'looks_like_bounce') {
		return $s->looks_like_bounce($id);
	} elsif ($rule eq '*') {
		return 1;
	} else {
		warning('unknown rule=%s hook=%s', $rule, $hook);
		return 0;
	}

	return 0;
}

sub cleanup_match
{
	my ($s, $hook, $rules, $id, $timestamp, $flags) = @_;

	unless (ref($rules)) {
		$rules = [$rules];
	}

	for my $rule (@{$rules}) {
		if ($s->match_rule($hook, $rule, $id, $timestamp, $flags)) {
			return $rule;
		}
	}

	return undef;
}

sub cleanup
{
	my ($s, $hook, $id, $timestamp, $flags) = @_;

	my $config = $s->config->{cleanup};

	unless ($config && $config->{$hook}) {
		# debug('no rules defined for cleanup hook %s', $hook);
		return 0;
	}

	my $rules = $config->{$hook};

	if (my $rule = $s->cleanup_match($hook, $rules, $id, $timestamp, $flags)) {
		my $mode;

		if ($hook =~ /^pre_/) {
			$mode = 'action ignored';
			return 1;
		} elsif ($s->{cleanup_dryrun}) {
			$mode = 'dryrun';
		}

		note('message cleanup id=%s hook=%s rule=%s age=%s timestamp=%s%s',
			$id, $hook, $rule, make_duration(time - $timestamp), $timestamp, $mode ? " ($mode)" : '');

		unless ($mode) {
			$s->delete($id);
		}

		return 1;
	}

	return 0;
}

# s3 implementation

sub s3_client
{
	my ($s, $uri) = @_;

	if (my $c = $s->{s3_client}->{$uri}) {
		return $c;
	}

	my $u = parse_uri($uri);
	my $account = $u->{scheme};
	my $credentials = $s->path('etc/s3', "$account.credentials");

	unless (-f $credentials) {
		warning("cannot find credentials for s3 uri=%s", $uri);
		return undef;
	}

	my $s3 = S3::Lite->new();
	$s3->parse_credentials($credentials);

	$s->{s3_client}->{$uri} = $s3;
}

sub s3_path
{
	my ($s, $id, @path) = @_;
	return join('/',
		substr($id, 0, $s->{id_hash_length}),
		substr($id, $s->{id_hash_length}),
		@path);
}

sub s3_check
{
	my ($s, $id) = @_;
	my $smtp = $s->smtp($id);

	my $c = $s->config->{s3}
		or return undef;
	my $d = $smtp->{domain};

	if (exists $c->{map}->{$d}) {
		return $c->{map}->{$d};
	} elsif (exists $c->{map}->{'*'}) {
		return $c->{map}->{'*'};
	}

	undef;
}

sub s3_upload
{
	my ($s, $id, $meta) = @_;

	my $uri = $s->s3_check($id)
		or return undef;

	# configuration uri is parsed as following:
	# scheme part stands for account name
	# host part stand for the bucket name
	my $u = parse_uri($uri);
	my $bucket = $u->{path};

	my $s3 = $s->s3_client($uri)
		or return undef;

	my $key = $s->s3_path($id);
	my $handle = $s->_read_compress($id, 'data');
	my $data = slurp($handle);

	unless ($s3->put($bucket, $key, $data)) {
		warning("s3 data upload failed id=%s bucket=%s key=%s", $id, $bucket, $key);
		return undef;
	}

	# keep track which files have been uploaded to delete later
	my @uploaded = ($s->_get_path($id, 'data'));

	$meta ||= $s->meta($id);

	my $nattachment = 0;
	for my $a ($meta->get('Attachment')) {
		my $file = $s->path($id, 'attachments', $nattachment);
		$data = slurp($file);
		unless ($s3->put($bucket, "$key/attachments/$nattachment", $data)) {
			warning("s3 attachment upload failed id=%s bucket=%s key=%s nattachment=%d",
				$id, $bucket, $key, $nattachment);
			return undef;
		}
		push @uploaded, $file;
		$nattachment++;
	}

	# delete uploaded files as per keep_after_upload setting
	unless ($s->config->{s3}->{keep_after_upload}) {
		for my $file (@uploaded) {
			unlink $file
				or warning('failed to unlink file after s3 upload: %s', $!);
		}
	}

	# update meta with the S3 header
	$meta->add('S3', $uri);
	$s->_write_meta($id, 'meta', $meta);

	return 1;
}

# events

sub events
{
	my $s = shift;

	my $json_events = $s->{config}->{json_events};

	if (!$json_events) {
		return undef;
	} elsif ($s->{events}) {
		return $s->{events}
	} else {
		$s->{events} = JSON::Events->new(
			($json_events eq 'true' || $json_events eq 1) ? undef : $json_events);
	}
}

sub events_store
{
	my ($s, $id, $sender, $recipients, $flags, $meta) = @_;

	my %event = (
		mkstore_id => $id,
		sender => $sender,
		recipients => $recipients,
		flags => $flags,
	);

	for my $header (qw(Subject From To CC Date Message-ID)) {
		(my $prop = lc($header)) =~ tr/-/_/;
		if (defined(my $value = $meta->get($header))) {
			chomp $value;
			$event{$prop} = $value;
		}
	}

	$s->events->put("inbound.mkstore.store", %event);
}

sub events_post
{
	my ($s, $id, $domain) = @_;

	$s->events->put("inbound.mkstore.post",
		mkstore_id => $id, domain => $domain);
}

1;
