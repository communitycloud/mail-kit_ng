#!/usr/bin/perl

package MailKit::CommandParser;

use strict;
use warnings;

use App::Message "parser:";
use Util::Export qw(encode_multiline);
use Util::File qw(fopen);

sub new
{
	my ($class, @processors) = @_;

	my $parser = bless {
		syntax => [ ],
		processors => [ ],
		lineno_start => 0,
		lineno_end => 0,
	}, $class;

	map { $parser->add_processor($_) }
		@processors;

	return $parser;
}

sub add_processor
{
	my ($parser, $processor) = @_;
	internal_error("bad processor: %s", $processor)
		unless UNIVERSAL::can($processor, 'syntax');
	push @{$parser->{processors}}, $processor;
	for my $spec ($processor->syntax)
	{
		push @{$parser->{syntax}}, {
			processor => $#{$parser->{processors}},
			regexp => $parser->compile_syntax($spec),
			spec => $spec,
		}
	}
}

sub compile_syntax
{
	my ($p, $spec) = @_;

	my $isk = '(?:[a-zA-Z][a-zA-Z0-9_-]*)';
	my ($op, @path) = split(' +', $spec);
	my $regexp = "\\s*(\Q$op\E)(?:\\s+|\\b)";
	for my $p (@path) {
		if ($p eq ".") {
			$regexp .= "($isk)\\b\\s*";
		} elsif ($p eq "+") {
			$regexp .= "((?:.|\\n)+)";
		} elsif ($p eq "*") {
			$regexp .= "((?:.|\\n)*)";
		} elsif ($p =~ /^$isk$/) {
			$regexp .= "(\Q$p\E)\\b\\s*";
		} else {
			internal_error("invalid syntax spec: $spec");
		}
	}

	return qr/^$regexp$/;
}

sub read_line
{
	my ($p, $io) = @_;

	my $line;

	if (defined $p->{line}) {
		$line = delete $p->{line};
	} else {
		$line = readline($io);
	}

	return undef unless defined $line;

	$p->{lineno_start} = ++$p->{lineno_end};
	while (my $nextline = readline($io)) {
		if ($nextline =~ s#^\s*\\[\t ]?##) {
			$line .= $nextline;
		} else {
			$p->{line} = $nextline;
			last;
		}
		$p->{lineno_end}++;
	}

	return $line;
}

sub parse
{
	my ($p, $filename, $layer) = @_;

	$p->{filename} = $filename;
	$p->{lineno_start} = 0;
	$p->{lineno_end} = 0;

	my $handle = fopen($filename, layer => $layer);
	$p->input($handle);
}

sub parser_error
{
	my ($p, $message, @args) = @_;
	my $lineno = $p->{lineno_start} == $p->{lineno_end}
		? $p->{lineno_start} : sprintf "%d-%d", @$p{qw(lineno_start lineno_end)};
	my $location = $p->{filename}
		? sprintf("at %s line %s", $p->{filename}, $lineno)
		: sprintf("at line %s", $lineno);
	error("$message (%s)", @args, $location);
}

sub input
{
	my ($p, $io) = @_;

	while (defined (my $line = $p->read_line($io)))
	{
		$p->input_line($line);
	}
}

sub parse_line
{
	my ($p, $line) = @_;

	$line =~ s#^\s+##;
	chomp $line;

	return unless length $line;
	return if $line =~ /^#/;

	for my $syntax (@{$p->{syntax}}) {
		if (my @tok = $line =~ /$syntax->{regexp}/s) {
			return $syntax->{processor}, @tok;
		}
	}

	$p->parser_error("invalid line %q", $line);
}

sub input_line
{
	my ($p, $line) = @_;

	if (my ($processor, @tok) = $p->parse_line($line)) {
		$p->input_tokens($processor, @tok);
	}
}

sub input_tokens
{
	my ($p, $processor, @tok) = @_;
	return $p->{processors}->[$processor]->emit(@tok);
}

sub encode_multiline($)
{
	my $string = shift;
	if ($string =~ m#\n#) {
		$string = "\n" . $string;
		$string =~ s#\n#\n \\ #g;
	}
	return $string;
}

1;
