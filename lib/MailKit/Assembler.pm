#!/usr/bin/perl

package MailKit::Assembler;

use strict;
use warnings;

use App::Message "assembler:";
use Util::File qw(slurp basename);

use Encode;
use Email::MIME v1.903;
use Email::MessageID;
use Email::Date::Format;
use Util::String qw(looks_like_utf8);
use MIME::Words qw(encode_mimewords);

sub new_mime_part()
{
	return Email::MIME->create();
}

sub normalise_headername($)
{
	my $h = shift;
	return undef unless defined $h;
	$h = ucfirst $h;
	$h =~ s/[-_](.)/-\u$1/g;
	return $h;
}

sub new
{
	my $class = shift;
	return bless {
		mime => new_mime_part,
		text => "",
		html => "",
		attachments => [ ],
		embedded => [ ],
		options => { },
	}, $class;
}

sub syntax
{
	use MailKit::Assembler::Syntax;
	return @MailKit::Assembler::Syntax::syntax;
}

sub create_attachment($)
{
	my $file = shift;
	my $part = new_mime_part();

	require MIME::Types;
	my $mimetypes = MIME::Types->new;

	my $body = slurp($file);
	my $basename = basename($file);
	my $content_type = $mimetypes->mimeTypeOf($file) ||
		(-T $file ? "text/plain" : "application/octet-stream");
	my $encoding = "base64";
	if ($content_type =~ "text/plain") {
		(my $charset, $encoding) = guess_content_charset($body);
		$content_type .= "; charset=\"$charset\"";
	}

	$part->header_set("Content-Type", "$content_type; name=\"$basename\"");
	$part->header_set("Content-Disposition", "attachment; filename=\"$basename\"");
	$part->header_set("Content-Transfer-Encoding", $encoding);
	$part->body_set($body);

	return $part;
}

sub create_related($)
{
	my ($file, $content_id) = split('->', $_[0], 2);
	$content_id ||= basename($file);
	my $part = create_attachment($file);
	$part->header_set('Content-ID' => '<' . $content_id . '>');
	$part->header_set('Content-Disposition');
	return $part;
}

sub guess_content_charset($)
{
	my ($charset, $encoding);

	use bytes;
	if ($_[0] !~ /[^[:ascii:]]/) {
		$charset = "us-ascii";
		if (length $_[0] > 998) {
			$encoding = "quoted-printable";
		} else {
			$encoding = "7bit";
		}
	} else {
		eval { Encode::decode("utf-8", my $copy = $_[0], Encode::FB_CROAK) };
		$charset = $@ ? "binary" : "utf-8";
		$encoding = "base64";
	}

	return ($charset, $encoding);
}

sub setup_text_part($$)
{
	my ($part, $body) = @_;
	my ($charset, $encoding) = guess_content_charset($body);
	$part->header_set("Content-Type", "text/plain; charset=\"$charset\"");
	$part->header_set("Content-Transfer-Encoding", $encoding);
	$part->body_set($body);
}

sub setup_html_part($$)
{
	my ($part, $body) = @_;
	my ($charset, $encoding) = guess_content_charset($body);
	$part->header_set("Content-Type", "text/html; charset=\"$charset\"");
	$part->header_set("Content-Transfer-Encoding", $encoding);
	$part->body_set($body);
}

sub get_file_path
{
	my ($as, $filename) = @_;
	$filename = $as->{options}->{cwd} . '/' . $filename
		if $filename !~ m#^/# && $as->{options}->{cwd};
	return $filename;
}

sub emit
{
	my ($as, $op, @arg) = @_;

	if ($op eq "+") {
		return $as->op_add(@arg);
	} elsif ($op eq ":") {
		return $as->op_cmd(@arg);
	} else {
		error("invalid op %q", $op);
	}
}

sub op_add
{
	my ($as, $name, @arg) = @_;

	if ($name eq "subject") {
		$as->op_add("header", "subject", @arg);
	}

	elsif ($name eq "recipient") {
		return $as->op_add("header", "to", @arg);
	}

	elsif ($name eq "sender") {
		$as->op_add("header", "from", @arg);
	}

	elsif ($name eq "rfc822date") {
		$as->op_add("header", "date",
			Email::Date::Format::email_date(
				length $arg[0] ? $arg[0] : time));
	}

	elsif ($name eq "rfc822id") {
		$as->op_add("header", "message-ID",
			sprintf("<%s>", Email::MessageID->new(host => $arg[0])));
	}

	elsif ($name eq "verp") {
		if ($arg[0] =~ " ") {
			$arg[0] =~ tr"@"=";
			my @comp = split " +", $arg[0];
			my $host = pop @comp;
			my $verp = sprintf "<%s@%s>",
			join("=", @comp), $host;
			$as->op_add("header", "return-path", $verp);
		} else {
			$arg[0] =~ s/@/=/ while $arg[0] =~ tr/@// > 1;
			$as->op_add("header", "return-path", $arg[0]);
		}
	}

	elsif ($name eq "header") {
		my ($header, $value) = @arg;
		my $encoded = $value;
		if (looks_like_utf8($encoded)) {
			# $encoded = decode('utf-8', $value);
			$encoded = encode_mimewords($encoded, Charset => 'utf-8');
		}
		$as->{mime}->header_set(normalise_headername($header), $encoded);
	} elsif ($name eq "text" || $name eq "html") {
		$as->{$name} .= $arg[0];
	} elsif ($name eq "attach") {
		push @{$as->{attachments}}, $as->get_file_path($arg[0]);
	} elsif ($name eq "embed") {
		push @{$as->{embedded}}, $as->get_file_path($arg[0]);
	} else {
		error("invalid + command %q", $name);
	}
}

sub op_cmd
{
	my ($as, $cmd, @arg) = @_;

	if ($cmd eq "set") {
		$as->{options}->{$arg[0]} = $arg[1];
	} else {
		error("invalid : command %q", $cmd);
	}
}

sub assemble
{
	my $as = shift;

	my $format = $as->{options}->{format};

	if (!$format || $format eq "mime") {
		return $as->assemble_mime()->as_string();
	} elsif ($format eq "mbox") {
		my $mime = $as->assemble_mime();
		return sprintf("From %s %s\n%s\n\n",
			$mime->header('From') || "anonymous",
			$mime->header('Date') || scalar localtime,
			$mime->as_string);
	} elsif ($format eq "eml") {
		require Email::Address;
		my $mime = $as->assemble_mime();
		my $sender = $mime->header('x-sender') ||
			$mime->header('Return-Path') ||
			$mime->header('From');
		my $recipient = $mime->header('x-receiver') ||
			$mime->header('To');
		$mime->header_set('x-sender');
		$mime->header_set('x-receiver');
		return sprintf("x-sender: %s\nx-receiver: %s\n%s",
			((Email::Address->parse($sender))[0]->address),
			((Email::Address->parse($recipient))[0]->address),
			$mime->as_string());
	} elsif ($format eq "header") {
		return $as->{mime}->as_string;
	} else {
		error("unknown format `%s'", $format);
	}
}

sub assemble_mime
{
	my $as = shift;

	my $mime = $as->{mime};

	my ($text, $html, $content, @embedded, @attachments);

	if (@{$as->{attachments}}) {
		$content = new_mime_part();
		@attachments = map create_attachment($_), @{$as->{attachments}};
	} elsif (@{$as->{embedded}}) {
		$content = new_mime_part();
		@embedded = map create_related($_), @{$as->{embedded}};
	} else {
		$content = $mime;
	}

	if (length $as->{text} && !length $as->{html}) {
		$text = $content;
	} elsif (length $as->{html} && !length $as->{text}) {
		$html = $content;
	} elsif (length $as->{text} && length $as->{html}) {
		$text = new_mime_part();
		$html = new_mime_part();
	}

	if ($text) {
		setup_text_part($text, $as->{text});
	}

	if ($html) {
		setup_html_part($html, $as->{html});
	}

	if (@attachments || @embedded || ($text && $html)) {
		$mime->header_set("MIME-Version" => "1.0");
	}

	if ($text && $html) {
		$content->header_set("Content-Type", "multipart/alternative");
		$content->parts_set([$text, $html]);
	}

	if (@embedded && @attachments) {
		my $related = new_mime_part();
		$related->header_set("Content-Type", "multipart/related");
		$related->parts_set([$content, @embedded]);
		$content = $related;
	}

	if (@attachments) {
		$mime->header_set("Content-Type", "multipart/mixed");
		$mime->parts_set([$content, @attachments]);
	} elsif (@embedded) {
		$mime->header_set("Content-Type", "multipart/related");
		$mime->parts_set([$content, @embedded]);
	}

	return $mime;
}

1;
