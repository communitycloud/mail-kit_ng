#!/usr/bin/perl

# Copyright (C) 2011 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package MailKit::Filter;

use strict;
use warnings;

use App::Message qw(filter:);

use MIME::Parser;
use MailKit::Compiler;
use Util::String qw(trim boolean_value uri_unescape text2html parse_kvp);
use Util::File qw(slurp dirname cleanup_filename);
use Util::List qw(in_list update_hash list_unique);
use Util::Email qw(slurp_mime_part);
use Encode qw(:DEFAULT :fallbacks);
use HTML::Entities qw(encode_entities_numeric);
use MailKit::Rule qw(mk_match_rule);
use MIME::Words qw(decode_mimewords encode_mimewords);
use MIME::WordDecoder;
use Email::Address;

use Net::DNS;
my $resolver;

sub new
{
	my ($class, %arg) = @_;
	bless { filters => [], options => {}, events => undef, %arg }, $class;
}

sub parser
{
	$_[0]->{parser} ||= do {
		my $parser = new MIME::Parser;
		$parser->output_under($_[0]->{tmp} || '/tmp');
		$parser->extract_nested_messages(0);
		$parser;
	};
}

sub append_template
{
	my ($f, $template, $ctx) = @_;
	push @{$f->{filters}}, ['insert_template', 0, $template, $ctx];
}

sub prepend_template
{
	my ($f, $template, $ctx) = @_;
	push @{$f->{filters}}, ['insert_template', 1, $template, $ctx];
}

sub cleanup_part($)
{
	my $mime = shift;
	$mime->head->delete('X-Mailer');
	$mime->head->delete('Content-Disposition');
	$mime->head->set('Content-Transfer-Encoding', $mime->suggest_encoding);
}

sub make_alt_part($$)
{
	my ($text, $html) = @_;

	my $part;

	if ($text) {
		$text = MIME::Entity->build(
			Type => 'text/plain',
			Data => $text);
		cleanup_part($text);
	}

	if ($html) {
		$html = MIME::Entity->build(
			Type => 'text/html',
			Data => $html);
		cleanup_part($html);
	}

	if ($text && $html) {
		$part = MIME::Entity->build(Type => 'multipart/alternative');
		$part->parts([$text, $html]);
		cleanup_part($part);
	} elsif ($html) {
		$part = $html;
	} else {
		$part = $text;
	}

	return $part;
}

sub mix_part($$$;$)
{
	my ($mime, $prepend, $text, $html) = @_;

	my $part = make_alt_part($text, $html);

	if ($mime->parts == 0) {
		$mime->make_multipart();
	} elsif ($mime->mime_type =~ 'multipart/(alternative|related)') {
		my $move = MIME::Entity->build(
			Type => $mime->mime_type);
		$move->parts([$mime->parts]);
		cleanup_part($move);
		$mime->parts([$move]);
		$mime->head->mime_attr('Content-Type', 'multipart/mixed');
	} elsif ($mime->mime_type !~ 'multipart/mixed') {
		# ?
	}

	my $offset = $prepend ? 0 : undef;
	$mime->add_part($part, $offset);
}

sub write_part($$)
{
	my ($mime, $data) = @_;
	my $body = $mime->bodyhandle()
		or return undef;
	my $io = $body->open("w")
		or return undef;
	$io->print($data);
	$io->close();
}

sub find_part($$;$)
{
	my ($mime, $type, $prepend) = @_;

	my $found;

	for my $part ($mime->parts_DFS) {
		if ($part->mime_type eq $type) {
			next if ($part->head->mime_attr('Content-Disposition') || '') eq 'attachment';
			return $part if $prepend;
			$found = $part;
		}
	}

	return $found;
}

sub get_part_charset($$)
{
	my ($part, $data_r) = @_;
	my $charset = $part->head->mime_attr('content-type.charset');

	unless ($charset) {
		if ($$data_r =~ /[^\x00-\x7f]/) {
			decode('utf-8', (my $scratch = $$data_r), FB_QUIET);
			$charset = 'utf-8' if $scratch eq '';
		} else {
			$charset = 'ascii';
		}
	}

	if (find_encoding($charset)) {
		return $charset;
	} else {
		return undef;
	}
}

sub encode_text_part($$$)
{
	my ($text_r, $data_r, $part) = @_;

	return unless defined $$text_r && defined $$data_r
		&& length $$text_r;

	my $charset = get_part_charset($part, $data_r)
		or return;

	my $encoding = $$text_r =~ /[^\x00-\x7f]/ ? 'utf-8' : 'ascii';
	return if lc($charset) eq $encoding;

	eval {
		my $string = decode($encoding, (my $octets = $$text_r), FB_CROAK);
		my $text = encode($charset, $string, FB_CROAK);
		$$text_r = $text;
	};

	unless ($@) {
		debug("text encoding downgraded to %s", $charset);
		return;
	}

	my $e1 = $@;

	eval {
		my $string = decode($charset, (my $octets = $$data_r), FB_CROAK);
		my $data = encode($encoding, $string, FB_CROAK);
		$part->head->mime_attr('Content-Type', $part->mime_type)
			unless $part->head->mime_attr('Content-Type');
		$part->head->mime_attr('Content-Type.charset', $encoding);
		$$data_r = $data;
	};

	unless ($@) {
		debug("data encoding upgraded to %s", $encoding);
		return;
	}

	my $e2 = $@;

	debug("encoding failed charset=%s encoding=%s text_error=%s data_error=%s",
		$charset, $encoding, $e1, $e2);
}

sub mangle_part($$$;$)
{
	my ($mime, $prepend, $text, $html) = @_;

	if ($text) {
		if (my $part = find_part($mime, "text/plain", $prepend)) {
			my $data = $part->bodyhandle->data;
			encode_text_part(\$text, \$data, $part);
			write_part($part,
				$prepend ? $text . $data : $data . $text);
		}
	}

	if ($html) {
		eval {
			my $string = decode('UTF-8', (my $octets = $html), FB_CROAK);
			$html = encode_entities_numeric($string, '^\n\x20-\x25\x27-\x7e');
		};
		
		if ($@) {
			debug("failed to decode html chunk: %s", $@);
		}

		if (my $part = find_part($mime, "text/html", $prepend)) {
			my $data = $part->bodyhandle->data;
			if ($prepend) {
				$data =~ s/<body(.*?)>/<body$1>$html/ or
				$data =~ s/<html>/<html>$html/ or
				$data =~ s/\A/$html/;
			} else {
				$data =~ s/<\/body>/$html<\/body>/ or
				$data =~ s/<\/html>/$html<\/html>/ or
				$data =~ s/\Z/$html/;
			}
			write_part($part, $data);
		}
	}
}

sub guess_method($)
{
	return "mangle";
	my $mime = shift;
	if ($mime->mime_type eq "multipart/mixed") {
		return "mix";
	} else {
		return "mangle";
	}
}

sub insert_template
{
	my ($f, $mime,
		$prepend, $template, $ctx) = @_;

	my $method = $f->{insert_method} ||
		$ctx->{insert_method} || "compat";
	$method = guess_method($mime) if $method eq "compat";

	note("%s (%s) template=%s %{ph:t}",
		$prepend ? "prepend" : "append", $method, $template, $ctx);

	my $c = MailKit::Compiler->new();
	my $ops = $c->capture();

	$c->context->add($ctx);
	$c->compile($template);

	my ($text, $html) = ('', '');

	for my $op (@{$ops}) {
		if ($op->[0] eq '+' && $op->[1] eq 'text') {
			$text .= $op->[2];
		} elsif ($op->[0] eq '+' && $op->[1] eq 'html') {
			$html .= $op->[2];
		} elsif ($op->[0] eq '+' && $op->[1] eq 'prepend') {
			$prepend = 1;
		}
	}

	if ($text || $html) {
		if ($method eq "mangle") {
			mangle_part($mime, $prepend, $text, $html);
		} elsif ($method eq "mix") {
			mix_part($mime, $prepend, $text, $html);
		} else {
			error("invalid insert method %q", $method);
		}
	} else {
		warning("no text or html was generated by the %q template",
			$template);
	}

	return $ops;
}

sub strip_attachments_legacy
{
	my ($f, $mime, $attachments, $prepend) = @_;

	my @parts = $mime->parts;
	my @strip = grep { mk_match_rule('attachment', $_) } @parts;

	if (@strip) {
		my @keep = grep { !in_list($_, @strip) } @parts;
		$mime->parts([@keep]);
		note("stripped attachment(s) legacy: %{pa}",
			[ map { $_->head->recommended_filename } @strip ]);
	}

	if ($attachments && @{$attachments}) {
		my $ctx = { attachments => $attachments, prepend => $prepend };
		my $ops = $f->insert_template($mime, $prepend, 'attachment-stripping_old', $ctx);
		my ($note) = grep { $_->[0] eq '+' && $_->[1] eq 'embed' } @{$ops};
		if ($note) {
			my ($name, $text) = split ' ', $note->[2], 2;
			$name =~ s/\+/ /g;
			# upgrade alternative to mixed
			if ($mime->effective_type eq 'multipart/alternative') {
				$mime->make_multipart('mixed', Force => 1);
			}
			$mime->attach(
				Type => 'text/plain',
				Charset => 'utf8',
				Disposition => 'attachment',
				Filename => $name,
				Data => $text);
		}
	}
}

sub strip_attachments_ng
{
	my ($f, $mime, $attachments, $prepend) = @_;

	my $strip;
	$strip = sub {
		my $m = shift;
		my @parts = $m->parts();
		my @strip = grep { mk_match_rule('attachment', $_) } @parts;

		if (@strip) {
			my @keep = grep { !in_list($_, @strip) } @parts;
			$m->parts([@keep]);
			note("stripped attachment(s) ng: %{pa}",
				[ map { $_->head->recommended_filename } @strip ]);
		}

		if ($m->is_multipart) {
			for my $p ($m->parts) {
				$strip->($p);
			}
		}
	};

	$strip->($mime);

	if ($attachments && @{$attachments}) {
		my $ctx = { attachments => $attachments, prepend => $prepend };
		my $ops = $f->insert_template($mime, $prepend, 'attachment-stripping', $ctx);
		my ($note) = grep { $_->[0] eq '+' && $_->[1] eq 'embed' } @{$ops};
		if ($note) {
			my ($name, $text) = split ' ', $note->[2], 2;
			$name =~ s/\+/ /g;
			# upgrade alternative to mixed
			if ($mime->effective_type eq 'multipart/alternative') {
				$mime->make_multipart('mixed', Force => 1);
			}
			$mime->attach(
				Type => 'text/plain',
				Charset => 'utf8',
				Disposition => 'attachment',
				Filename => $name,
				Data => $text);
		}
	}
}

sub strip_attachments
{
	my ($f, $mime, $attachments, $prepend) = @_;

	# XXX this should be replaced with the filter overrides map
	if ($f->{options}->{strip_attachments_legacy}) {
		$f->strip_attachments_legacy($mime, $attachments, $prepend);
	} else {
		$f->strip_attachments_ng($mime, $attachments, 1);
	}
}

sub dmarc_rewrite
{
	my ($f, $mime) = @_;

	my $sender = $mime->head->get('x-sender');
	my $from_head = $mime->head->get('From');

	$sender =~ s/\r?\n$//;
	$from_head =~ s/\r?\n$//;

	my $ev;

	if ($f->{events}) {
		$ev = JSON::Event->new('pipeline.mail-kit_filter.dmarc_rewrite');
		$ev->push(from => $from_head);
		push @{$f->{events}}, $ev;
	}

	unless ($from_head) {
		warning('dmarc_rewrite: no From address');
		$ev->warning('no From address') if $ev;
		return 0;
	}

	unless ($sender) {
		warning('dmarc_rewrite: no x-sender header');
		$ev->warning('no x-sender header') if $ev;
		return 0;
	}

	my ($from) = Email::Address->parse($from_head);

	unless ($from) {
		warning('dmarc_rewrite: coudln\'t parse from header: %s', $from_head);
		$ev->warning('coudln\'t parse from header: %s', $from_head) if $ev;
		return 0;
	}

	my $host = $from->host;

	unless ($host) {
		warning('dmarc_rewrite: could\'t find host in %s', $from_head);
		$ev->warning('could\'t find host in %s', $from_head) if $ev;
		return 0;
	}

	$resolver ||= Net::DNS::Resolver->new;
	my $r = $resolver->search("_dmarc.$host", "TXT");

	unless ($r) {
		warning('dmarc_rewrite: dns lookup of %s failed: %s', $host, $resolver->errorstring);
		$ev->warning('dns lookup of %s failed: %s', $host, $resolver->errorstring);
		$ev->push(status => 'nsfailure');
		return 0;
	}

	my $strict = 0;

	for my $rr ($r->answer) {
		my $record = $rr->txtdata;
		my $spec = parse_kvp($record, qr/;\s*/, qr/\s*=\s*/);
		if ($spec->{p} =~ /^(reject|quarantine)$/i) {
			note('dmarc_rewrite: strict domain=%s found in record=%s; rewriting to %s',
				$host, $record, $sender);
			if ($ev) {
				$ev->info('strict domain=%s found in record=%s; rewriting to %s',
					$host, $record, $sender);
			}
			$strict++;
			last;
		}
	}

	if ($strict) {
		$from->address($sender);
		my $rewritten = $from->format;
		$mime->head->set("From", $rewritten);

		if ($ev) {
			$ev->push(status => 'rewritten');
			$ev->push(rewritten => $rewritten);
		}
	} elsif ($ev) {
		$ev->push(status => 'unmodified');
	}
}

sub normalize_charset
{
	my ($f, $mime) = @_;

	my @elements = $f->scan_encoded_elements($mime);
	my @charsets = $f->scan_charsets($mime, @elements);

	if (@charsets > 1) {
		note('multiple charsets detected: %{pa}, normalising to utf-8', \@charsets);
		for my $e (@elements) {
			if (ref $e) {
				$f->normalize_part($e, 'utf-8');
			} else {
				$f->normalize_header($mime, $e, 'utf-8');
			}
		}
	}
}

sub scan_encoded_elements
{
	my ($f, $mime) = @_;

	my @e;

	# header scan
	TAG:
	for my $t ($mime->head->tags) {
		for my $h ($mime->head->get_all($t)) {
			for my $decoded (decode_mimewords($h)) {
				if (@$decoded == 2) {
					push @e, $t;
					next TAG;
				}
			}
		}
	}

	# part scan
	for my $p ($mime->parts_DFS) {
		if ($p->effective_type eq 'text/plain' || $p->effective_type eq 'text/html') {
			push @e, $p;
		}
	}

	return @e;
}

sub scan_charsets
{
	my ($f, $mime, @elements) = @_;

	my @charsets;
	my @normalized;

	for my $e (@elements) {
		# part charset
		if (ref $e) {
			push @charsets, $e->head->mime_attr('content-type.charset') || 'ASCII';
			next;
		}

		# header charset
		for my $h ($mime->head->get_all($e)) {
			for my $decoded (decode_mimewords($h)) {
				if (@$decoded == 2) {
					push @charsets, $decoded->[1];
				}
			}
		}
	}

	for my $charset (@charsets) {
		if (my $e = find_encoding($charset)) {
			push @normalized, $e->name;
		} else {
			 warning("unable to find encoding for character set %q", $charset);
			 return ();
		}
	}

	return list_unique @normalized;
}

sub normalize_header
{
	my ($f, $mime, $header, $encoding) = @_;

	my @value = map {
		my $encoded = encode($encoding, scalar mime_to_perl_string($_));
		encode_mimewords($encoded, Charset => $encoding) 
	} $mime->head->get_all($header);

	$mime->head->delete($header);
	for my $line (@value) {
		$mime->head->add($header, $line);
	}
}

sub normalize_part
{
	my ($f, $part, $encoding) = @_;

	my $data = $part->bodyhandle->data;
	my $charset = get_part_charset($part, \$data)
		or return;

	eval {
		my $string = decode($charset, $data, FB_CROAK);
		my $recoded = encode($encoding, $string, FB_CROAK);
		write_part($part, $recoded);
	};

	if ($@) {
		warning('failed to normalize part charset: %s', error_string);
		return;
	}

	$part->head->mime_attr('Content-Type', $part->mime_type)
		unless $part->head->mime_attr('Content-Type');
	$part->head->mime_attr('Content-Type.charset', $encoding);
}

sub supply_html
{
	my ($f, $mime) = @_;

	if ($mime->mime_type ne 'text/plain') {
		return;
	}

	my $data = slurp_mime_part($mime);

	my $text = MIME::Entity->build(
		Type => 'text/plain',
		Data => $data);
	my $html = MIME::Entity->build(
		Type => 'text/html',
		Data => text2html($data));

	cleanup_part($text);
	cleanup_part($html);

	if (my $charset = $mime->head->mime_attr('content-type.charset')) {
		$text->head->mime_attr('content-type.charset', $charset);
		$html->head->mime_attr('content-type.charset', $charset);
	}

	$mime->make_multipart();
	$mime->head->mime_attr('Content-Type', 'multipart/alternative');
	$mime->parts([$text, $html]);
}

sub update_vars($$$)
{
	my ($template, $ctx, $vars) = @_;

	while (my ($key, $val) = each %{$vars}) {
		my ($scope, $var, $remove);
		if ($key =~ s/^-//) {
			$remove++;
		}
		if ($key =~ /^(.+)\.(.+)$/) {
			$scope = $1;
			$var = $2;
		} else {
			$var = $key
		}

		if (defined $scope && $scope ne $template) {
			next;
		}

		if ($remove) {
			delete $ctx->{$var};
		} else {
			$ctx->{$var} = $val;
		}
	}
}

sub get_options
{
	my ($f, $mime) = @_;

	my $options = {};

	if (defined (my $spec = $mime->head->get('X-MailKit-Filter'))) {
		$mime->head->delete('X-MailKit-Filter');
		my ($enabled, @options) = split /\s+/, $spec;
		$options->{enabled} = boolean_value($enabled);
		for my $opt (@options) {
			my ($k, $v) = split '=', $opt, 2;
			$options->{$k} = defined $v ? $v : 1;
		}
	}

	my $map = $f->recipient_options($mime);

	while (my ($k, $v) = each %{$map}) {
		if ($v eq '+') {
			$options->{$k}++;
		} elsif ($v eq '-') {
			delete $options->{$k};
		}
	}

	while (my ($k, $v) = each %{$f->{options}}) {
		if (!exists $options->{$k}) {
			$options->{$k} = $v;
		}
	}

	return $options;
}

sub recipient_options
{
	my ($f, $mime) = @_;

	my $map = {};

	my $recipient = $mime->head->get('x-receiver')
		or return {};
	chomp $recipient;

	my @paths = grep { -f } (split (':', $ENV{MKF_RECIPIENT_OPTIONS} || ''),
		cleanup_filename(dirname(__FILE__) . '/../../config/mkf_recipient_options'));

	for my $file (@paths) {
		my $contents = slurp($file);
		parse_option_map($map, $contents, $recipient);
		note('match recipient options: file=%s address=%s %{pa:t}', $file,
			$recipient, [map "$map->{$_}$_", keys %$map]) if scalar %{$map};

	}

	return $map;
}

sub parse_option_map($$$)
{
	my ($map, $contents, $match) = @_;

	my %kvp = parse_kvp($contents, "\n", ":");

	while (my ($pat, $opts) = each %kvp) {
		for my $r (split /\s+/, $pat) {
			if ($r eq $match || $match =~ /\@$r$/) {
				for my $o (split /\s+/, $opts) {
					$map->{$1} = '+' if $o =~ /^\+(.+)/;
					$map->{$1} = '-' if $o =~ /^\-(.+)/;
				}
			}
		}
	}
}

sub get_filters
{
	my ($f, $mime, $options) = @_;

	my (@filters, %vars);

	if (my $spec = ($mime->head->get('X-Strip-Attachments') || $options->{strip_attachments})) {
		chomp $spec;
		if ($spec ne 'false') {
			my $prepend = $spec eq 'prepend' ? 1 : 0;
			chomp(my @attachments = $mime->head->get_all('X-Attachment'));
			push @filters, ['strip_attachments', \@attachments, $prepend];
		}
	}

	if (boolean_value($mime->head->get('X-Supply-HTML')) ||
		$options->{supply_html}) {
		push @filters, ['supply_html'];
	}

	if (boolean_value($mime->head->get('X-Normalize-Charset')) ||
		$options->{normalize_charset}) {
		push @filters, ['normalize_charset'];
	}

	if (boolean_value($mime->head->get('X-Dmarc-Rewrite')) ||
		$options->{dmarc_rewrite}) {
		push @filters, ['dmarc_rewrite'];
	}

	for my $h ($mime->head->get_all('X-Template-Vars')) {
		my $ctx = MIME::Field::ParamVal->parse_params($h);
		map { $ctx->{$_} = uri_unescape($ctx->{$_}) } keys %{$ctx};
		delete $ctx->{_};
		update_hash(\%vars, $ctx);
	}

	for my $template ($mime->head->get_all('X-Append-Template')) {
		my $ctx = MIME::Field::ParamVal->parse_params($template);
		map { $ctx->{$_} = uri_unescape($ctx->{$_}) } keys %{$ctx};
		my $template = delete $ctx->{_};
		update_vars($template, $ctx, \%vars) if %vars;
		push @filters, ['insert_template', 0, $template, $ctx];
	}

	for my $template ($mime->head->get_all('X-Prepend-Template')) {
		my $ctx = MIME::Field::ParamVal->parse_params($template);
		map { $ctx->{$_} = uri_unescape($ctx->{$_}) } keys %{$ctx};
		my $template = delete $ctx->{_};
		update_vars($template, $ctx, \%vars);
		push @filters, ['insert_template', 1, $template, $ctx];
	}

	for my $cleanup (qw(X-Append-Template X-Prepend-Template
		X-Strip-Attachments X-Attachment X-Normalize-Charset X-Dmarc-Rewrite)) {
		$mime->head->delete($cleanup);
	}

	return @filters, @{$f->{filters}};
}

sub process
{
	my ($f, $data) = @_;
	my ($output, $modified);

	eval {
		my $mime = $f->parser->parse($data)
			or error("MIME parser failure");

		my $options = $f->get_options($mime);
		my @filters = $f->get_filters($mime, $options);

		if (($options->{enabled} || !exists $options->{enabled}) && @filters) {
			local $f->{insert_method} = "mix" if $options->{mix};
			local $f->{insert_method} = "mangle" if $options->{mangle};
			local $f->{insert_method} = "compat" if $options->{compat};

			for my $call (@filters) {
				my ($sub, @args) = @{$call};
				$f->$sub($mime, @args);
			}

			$output = IO::File->new_tmpfile;
			$mime->print($output);
		} elsif (exists $options->{enabled}) {
			note('the message had no active filters');
			$output = recover($data);
		} else {
			note('the message was left unmodified');
			$output = $data;
		}
	};

	my $error = $@;
	$f->parser->filer->purge;

	if ($error) {
		if ($f->{recover}) {
			warning("a filter error occurred, recovering: %s",
				error_string());
			if ($f->{events}) {
				my $ev = JSON::Event->new('pipeline.mail-kit_filter');
				$ev->warning("filter stage failed: %s", error_string());
				push @{$f->{events}}, $ev;
			}
			$output = recover($data);
		} else {
			error($error);
		}
	}

	seek $output, 0, 0;
	return $output;
}

sub recover($)
{
	my $data = shift;
	my $output = IO::File->new_tmpfile;

	seek $data, 0, 0;
	while (my $line = readline $data) {
		if ($line =~ /^X-MailKit-Filter:/i) {
			next;
		}
		print $output $line;
	}

	return $output;
}

1;
