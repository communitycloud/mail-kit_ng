#!/usr/bin/perl

# Copyright (C) 2009-2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Hand::Parser;

use strict;
use warnings;

sub new
{
	my ($class, %arg) = @_;

	my $p = bless {
		push_back => "",
		word_chars => join('', 'a'..'z', 'A' .. 'Z', '0' .. '9'),
		punct_chars => '/:(){}*?!;,',
		comment_style => '/',
		quote_chars => q['"],
	}, $class;

	$p->configure(%arg);

	return $p;
}

sub configure
{
}

sub open
{
	my ($p, $in) = @_;

	$p->{file} = $p->{offset} = undef;
	$p->{line} = $p->{column} = 1;

	if (ref $in eq "SCALAR") {
		$p->{input} = $in;
		$p->{offset} = 0;
	} else {
		open $p->{input}, $in
			or $p->error("failed to open %s: %s", $in, $!);
		$p->{file} = $in;
	}
}

sub parse
{
	my ($p, $in) = @_;

	unless (ref($p)) {
		$p = $p->new();
	}

	eval {
		$p->open($in);
		$p->start();
	}
}

sub start
{
	1;
}

sub error
{
	my ($p, $fmt, @args) = @_;
	
	my $msg = @args ? sprintf($fmt, @args) : $fmt;
	my $location = "";

	if ($p->{file} && $p->{line}) {
		$location = sprintf(" at %s line %d, column %d", $p->{file}, $p->{line}, $p->{column});
	} elsif ($p->{line}) {
		$location = sprintf(" at line %d, column %d", $p->{line}, $p->{column});
	}

	die($msg, $location, "\n");
}

sub is_word_char
{
	my ($p, $c) = @_;
	return index ($p->{word_chars}, $c) != -1;
}

sub is_punct_char
{
	my ($p, $c) = @_;
	return index($p->{punct_chars}, $c) != -1;
}

sub is_quote_char
{
	my ($p, $c) = @_;
	return index($p->{quote_chars}, $c) != -1;
}

sub push_back
{
	my ($p, $chars) = @_;
	for my $c (reverse split //, $chars) {
		if ($c eq "\n") {
			$p->{line} --;
			$p->{column} = 1;
		} else {
			$p->{column} --;
		}
		$p->{push_back} .= $c;
	}
}

sub next_char
{
	my $p = shift;

	my $c;

	if (length $p->{push_back})
	{
		$c = substr($p->{push_back}, -1);
		substr($p->{push_back}, -1) = "";
	} elsif (defined $p->{offset}) {
		$c = $p->{offset} < length ${$p->{input}}
			? substr(${$p->{input}}, $p->{offset}++, 1)
			: undef;
	} else {
		$c = getc $p->{input};
	}

	return undef unless defined $c;

	if ($c eq "\n")
	{
		$p->{column} = 1;
		$p->{line} ++;
	} else {
		$p->{column} ++;
	}

	return $c;
}

sub peek_char
{
	my $p = shift;
	my $c = $p->next_char;

	return undef unless defined $c;

	$p->push_back($c);
	return $c;
}

sub next_token
{
	my $p = shift;

	my $token = '';
	my $type = 'void';

	$p->skip_spaces;

	while (1)
	{
		my $c = $p->next_char;
		unless (defined $c)
		{
			unless (length $token) {
				$token = undef;
				$type = 'eos';
			}
			last;
		}

		if ($p->skip_comments($c))
		{
			return $p->next_token;
		}

		if ($p->is_word_char($c)) {
			$token .= $c;
			$type = 'word';
			next;
		} elsif (length $token) {
			$p->push_back($c);
		} elsif ($p->is_quote_char($c)) {
			$token = $p->get_string($c);
			$type = 'string';
		} elsif ($p->is_punct_char($c)) {
			$token = $c;
			$type = 'punct';
		} else {
			$p->error("read next token: garbage character <$c>");
		}

		last;
	}

	return wantarray ? ($token, $type) : $token;
}

sub get_string
{
	my ($p, $quote) = @_;
	my $string = "";
	while (1)
	{
		my $c = $p->next_char;

		if (defined ($c) && $c eq '\\') {
			if (($p->peek_char || '') eq $quote) {
				$c = $p->next_char;
			}
		} elsif ($c eq $quote) {
			return $string;
		}

		$p->error("unclosed string quote <$quote>")
			unless defined $c;
		$string .= $c;
	}

	return $string;
}

sub skip_comments
{
	my ($p, $c) = @_;

	return unless $p->{comment_style} eq $c;

	if ($p->{comment_style} eq '#') {
		while (1)
		{
			$c = $p->next_char;
			last if !defined $c || $c eq "\n";
		}
		return 1;
	}

	$c = $p->peek_char();
	return 0 unless $c eq '/' || $c eq '*';
	$p->next_char;

	if ($c eq '*')
	{
		my $close_maybe = 0;
		while (1)
		{
			$c = $p->next_char;
			$p->error("unclosed multiline comment")
				unless defined $c;

			if ($close_maybe && $c eq '/')
			{
				return 1;
			}

			$close_maybe = $c eq '*';
		}
	}

	if ($c eq '/')
	{
		while (1)
		{
			$c = $p->next_char;
			return 1 unless defined $c;
			return 1 if $c eq "\n";
		}
	}

	return 0;
}

sub skip_chars
{
	my ($p, $chars) = @_;

	my $c;

	while (defined ($c = $p->next_char))
	{
		last if index ($chars, $c) == -1;
	}

	$p->push_back($c) if defined $c;
}

sub skip_spaces
{
	my $p = shift;
	$p->skip_chars(" \t\n\r");
}

sub expect
{
	my ($p, $name, $type, @values) = @_;

	my ($tok, $tt) = $p->next_token;

	my $expectation = $name && $type && @values
		? sprintf("expecting %s <%s> token as <%s>", $name, $type, @values)
		: $name && $type
		? sprintf("expecting %s <%s> token", $name, $type)
		: $name
		? sprintf("expecting %s token", $name)
		: "expecting a token";

	unless (defined $tok) {
		$p->error("$expectation but reached end of stream");
	}

	if ($type && !grep $_ eq $tt, split "[, ]", $type) {
		$p->push_back($tok);
		$p->error("$expectation but got <$tt> token as <$tok> instead")
	}

	if (@values && !grep $_ eq $tok, @values) {
		$p->push_back($tok);
		$p->error("$expectation but got <$type> as <$tok> instead")
	}

	return $tok;
}

1;
