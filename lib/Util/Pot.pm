#!/usr/bin/perl

package Util::Pot;

use strict;
use warnings;

use App::Message;
use Util::File qw(fopen);
use Util::String qw(trim escape_chars unescape_chars);
use Util::List qw(list_unique);
use Util::Export qw(read_pot make_pot index_pot);

sub unescape($)
{
	my $s = shift;
	if (defined $s and my ($q) = $s =~ /^"(.*)"$/) {
		return unescape_chars($q, q/"\ntrabef/);
	}
	return $s;
}

sub escape($)
{
	my $s = shift;

	my $index = index($s, "\n");

	if ($index + 1 == length $s || $index == -1) {
		return sprintf('"%s"', escape_chars($s, q/"\ntrabef/));
	} else {
		return join "\n", map &escape($_), '', split "(?<=\n)", $s;
	}
}

sub read_pot($)
{

	my $fname = shift;
	my $handle = fopen($fname);

	my ($pot, $po, $entry);

	my $add = sub {
		my ($key, $value, $idx) = @_;
		push @{$pot}, $po = {} unless $po;
		my $r = defined $idx
			? \$po->{$key}->[$idx < 0 ? $#{$po->{$key}} - $idx : $idx]
			: \$po->{$key};
		defined $$r ? ($$r .= $value) : ($$r = $value);
		return $r;
	};

	while (my $line = readline $handle) {

		trim $line;

		unless (length $line) {
			$entry = $po = undef;
			next;
		}

		if (my ($comment) = $line =~ /^#(.*)/) {
			if ($comment =~ /^\:(.*)/) {
				&$add(references => trim($1), -1);
			} elsif ($comment =~ /^\,(.*)/) {
				&$add(flags => $_, -1)
					for grep length, split /\s*,\s*/, trim($1);
			} elsif ($comment =~ /^\|(.*)/) {
				&$add(previous => trim($1) . "\n")
			} elsif ($comment =~ /^\.(.*)/) {
				&$add(extracted => trim($1) . "\n")
			} else {
				&$add(comment => trim($comment) . "\n");
			}
		} elsif (my ($msgid) = $line =~ /^msgid\s+(.*)/) {
			$entry = &$add(msgid => unescape($msgid));
		} elsif (my ($idx, $msgstr) = $line =~ /^msgstr\[(\d+)\]\s+(.*)/) {
			$entry = &$add(msgstr => unescape($msgstr), $idx);
		} elsif (($msgstr) = $line =~ /^msgstr\s+(.*)/) {
			$entry = &$add(msgstr => unescape($msgstr), 0);
		} elsif (my ($msgid_plural) = $line =~ /^msgid_plural\s+(.*)/) {
			$entry = &$add(msgid_plural => unescape($msgid_plural));
		} elsif ($entry && $line =~ /^"/) {
			$$entry .= unescape($line);
		} else {
			error("invalid pot line in %s at line %s\n>>> %s", $fname, $., $line);
		}
	}

	return $pot;
}

sub make_pot($)
{
	my $pot = shift;
	my $out = "";

	for my $po (@{$pot})
	{
		if (defined(my $comment = $po->{comment})) {
			$comment =~ s/^/# /gm;
			$out .= $comment;
		}

		if (defined(my $comment = $po->{extracted})) {
			$comment =~ s/^/#. /gm;
			$out .= $comment;
		}

		if (my $references = $po->{references}) {
			$out .= "#: $_\n" for list_unique(@{$references});
		}

		if (my $flags = $po->{flags}) {
			$out .= sprintf("#, %s\n", join(', ', @{$flags}));
		}

		if (defined(my $comment = $po->{previous})) {
			$comment =~ s/^/#| /gm;
			$out .= $comment;
		}

		if (exists $po->{msgid}) {
			$out .= sprintf "msgid %s\n", escape($po->{msgid});
		}

		if (exists $po->{msgid_plural}) {
			$out .= sprintf "msgid %s\n", escape($po->{msgid_plural});
			for (my $i = 0; $i < @{$po->{msgstr}}; $i++) {
				$out .= sprintf "msgstr[%d] %s\n", $i, escape($po->{msgstr}->[$i]);
			}
		} elsif (exists $po->{msgstr}) {
			$out .= sprintf "msgstr %s\n", escape($po->{msgstr}->[0]);
		}

		$out .= "\n";
	}

	chomp $out;
	return $out;
}

sub index_pot($)
{
	my $pot = shift;
	return +{ map { $_->{msgid} => $_ } @{$pot} };
}

1;
