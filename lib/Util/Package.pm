#!/usr/bin/perl

# Copyright (C) 2009-2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Util::Package;

use strict;
use warnings;

use App::Message "package:";
use Util::String qw(deprefix strneq);
use Util::Export qw(load_package package_loaded load_object package2name name2package
	get_symbol put_symbol delete_package);
use Scalar::Util qw(blessed);

sub package_loaded($)
{
	(my $file = $_[0] . '.pm') =~ s#::#/#g;
	return exists $INC{$file};
}

sub load_package($;$)
{
	my $pkg = name2package(@_);

	if (package_loaded($pkg) || eval "require $pkg") {
		return $pkg;
	}

	if (my ($name) = $@ =~ /^Can't locate (.*?) in \@INC/) {
		$name =~ s,/,::,g, $name =~ s,\.pm$,,;
		$name = "package" if $name eq $pkg;
		error("cannot load %s: %s not found", $pkg, $name);
	} else {
		error("cannot load %s: %s", $pkg, $@);
	}
}

sub package2name($;$)
{
	my ($name, $base) = @_;
	return undef if defined $base && !deprefix($name, $base . '::');
	$name =~ s#(^.|::.)#\L$1#g;
	$name =~ s#([A-Z])#-$1#g;
	$name =~ s#::#_#g;
	return lc $name;
}

sub name2package($;$)
{
	my ($name, $base) = @_;
	$name =~ s#(?:^|-)([a-z])#\U$1#g;
	$name =~ s#_([a-z])#::\U$1#g;
	if (defined $base && !strneq($name, $base . '::')) {
		return $base . '::' . $name;
	} else {
		return $name;
	}
}

sub load_object($;$@)
{
	my ($name, $base, @args) = @_;
	my $pkg = name2package($name, $base);
	load_package($pkg);
	return $pkg->new(@args);
}

sub get_symbol($$;$)
{
	my ($pkg, $name, $type) = @_;

	unless (defined $type) {
		# no type given, try all slots
		for my $type (qw(scalar code array hash)) {
			my $val = &get_symbol($pkg, $name, $type);
			return $val if defined $val;
		}
		return undef;
	}

	no strict qw(refs);
	return *{"$pkg\::$name"}{uc $type};
}

sub put_symbol($$$)
{
	my ($pkg, $name, $ref) = @_;
	no strict qw(refs);
	unless (defined $ref) {
		delete ${"$pkg\::"}{$name};
	} else {
		no warnings qw(redefine);
		*{"$pkg\::$name"} =
			!ref $ref || blessed $ref ? \$ref : $ref;
	}
}

sub delete_package($)
{
	my $pkg = shift;
	my $tbl = get_symbol($pkg, "", "hash");
	for my $sym (keys %{$tbl}) {
		put_symbol($pkg, $sym, undef);
	}
}

1;
