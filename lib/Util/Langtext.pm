#!/usr/bin/perl

package Util::Langtext;

use strict;
use warnings;

use App::Message;
use Util::String qw(escape_chars);
use Util::Pot qw(index_pot);
use Text::Balanced qw(extract_delimited extract_bracketed);
use Util::Export qw(langtext xlangtext);

sub langtext($$;$)
{
	my ($text, $pot, $partial) = @_;

	my $idx = index_pot($pot);
	my $xtext = xlangtext($text);

	my $offset = 0;

	for my $x (@{$xtext})
	{
		my $msgstr = $idx->{$x->{text}}->{msgstr}->[0];

		unless (defined $msgstr && length $msgstr) {
			if ($partial) {
				$msgstr = $x->{text};
			} else {
				return undef;
			}
		}

		if ($x->{type} eq "quoted") {
			$msgstr = sprintf('%s%s%s',
				$x->{delim}, escape_chars($msgstr, $x->{delim}),
				$x->{delim});
		}

		substr($text, $offset + $x->{start}, $x->{length}) = $msgstr;
		$offset += length ($msgstr) - $x->{length};
	}

	return $text;
}

sub xlangtext($)
{
	my $text = shift;

	my $q = "'\"";
	my $b = "({";

	my @xtext;

	while ($text =~ m#_(?=([$q$b]))#g) {
		my $delim = $1;
		my $start = pos($text) - 1;
		my $xtext;
		my $type;

		if (index ($q, $delim) >= 0) {
			$xtext = (extract_delimited($text, $delim))[0];
			$type = "quoted";
		} elsif ((my $n = index ($b, $delim)) >= 0) {
			$xtext = (extract_bracketed($text, $delim))[0];
			$type = "bracketed";
		}

		push @xtext, {
			text => substr($xtext, 1, -1),
			start => $start,
			length => length($xtext) + 1,
			type => $type,
			delim => $delim,
			line => (substr($text, 0, $start) =~ tr/\n//) + 1,
		} if defined $xtext;
	}

	return \@xtext;
}

1;
