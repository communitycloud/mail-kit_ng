#!/usr/bin/perl

# Copyright (C) 2009-2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Util::String;

use strict;
use warnings;

use Hash::Stable;
use Scalar::Util qw(looks_like_number);
use Encode qw(:DEFAULT :fallbacks);
use Util::Export qw(
	uri_escape uri_unescape html_escape trim str_replace
	escape_chars unescape_chars equndef looks_like_number
	custom_sprintf define parse_kvp deprefix strneq
	to_camel_case un_camel_case ordinate is_integer can_strfy capture
	shell_expand compile_glob parse_uri sysgetc sysreadline
	html2text text2html unique_enough_id short_id boolean_value
	utf8_encode looks_like_utf8 redact_uri);

my %mem;

sub equndef($$)
{
	return defined $_[0] && defined $_[1]
		? $_[0] eq $_[1]
		: defined $_[0] eq defined $_[1];
}

sub trim(;$)
{
	@_ = @_ ? @_ : $_ if defined wantarray;
	for (@_ ? @_ : $_) {
		s/\A\s+//;
		s/\s+\z//;
		return $_ if defined wantarray;
	}
	return;
}

sub str_replace($$)
{
	my ($str, $subs) = @_;

	return $str unless defined $str and length $str;

	my $get_pairs = ref $subs eq 'ARRAY'
		? sub { @{$subs} ? (shift @{$subs}, shift @{$subs}) : () }
		: sub { each %{$subs} };

	while (my ($p, $r) = $get_pairs->())
	{
		my $pos = 0;
		while (($pos = index($str, $p, $pos)) >= 0) {
			substr($str, $pos, length $p) = $r;
			$pos += length $r;
		}
	}

	return $str;
}

sub escape_chars($$)
{
	my ($str, $chars) = @_;

	my $escape = $mem{escape}->{$chars} ||=
		{ map { eval qq("\\$_") => qq(\\$_) } split //, $chars };

	return str_replace($str, $escape);
}

sub unescape_chars($$)
{
	my ($str, $chars) = @_;

	my $unescape = $mem{unescape}->{$chars} ||=
		{ map { '\\' . $_ => eval qq("\\$_") } split //, $chars };

	return str_replace($str, $unescape);
}

sub parse_uri($)
{
	my $u = shift;
	return undef unless defined $u;

	my $s = { uri => $u };
	my $c = qr/[\@a-zA-Z0-9%.+ _-]/o;

	@{$s}{qw(scheme opaque query_string fragment)} = $u =~ m{
		^
		(?:(\w+):)? # optional scheme
		(.+?) # opaque part
		(?:\?(.*?))? # query
		(?:\#(.*))? # fragment
		$
	}xo or return undef;

	if (defined $s->{opaque} && $s->{opaque} =~ m#^//#) {
		@{$s}{qw(authority path)} = $s->{opaque} =~ m{
			^
			//
			(.+?) # authority
			(?:/(.*))? # path
			$
		}xo or return undef;
	} else {
		$s->{path} = $s->{opaque};
	}

	if (defined $s->{authority}) {
		@{$s}{qw(user pass address)} = $s->{authority} =~ m{
			^
			(?:
				($c+?) # username
				(?::($c+))? # password
				@
			)?
			([\w.-]+(?::\d+)?) # address
			$
		}xo or return undef;

		@{$s}{qw(host port)} = split(':', $s->{address}, 2);
	}

	while (defined $s->{path} && $s->{path} =~ s#;(\w+)=([^;]*)$##) {
		$s->{args}->{$1} = $2;
	}

	if (defined $s->{query_string}) {
		for my $arg (split /[;&]/, $s->{query_string}) {
			my ($n, $v) = split /=/, $arg, 2;
			$s->{query}->{$n} = $v;
		}
	}

	return $s;
}

sub uri_escape($;$)
{
	my ($u, $chr) = @_;
	return undef unless defined($u);
	$chr = "" unless defined $chr;
	$u =~ s(([^A-Za-z0-9\-_.!~*'()$chr]))
	       (sprintf("%%%02X", ord($1)))eg;
	return $u;
}

sub uri_unescape($)
{
	my $u = shift;
	return undef unless defined($u);
	$u =~ s/%([[:xdigit:]]{2})/chr(hex($1))/eg;
	return $u;
}

sub html_escape {
	str_replace $_[0], [
        '&' => '&amp;',
        '<' => '&lt;',
        '>' => '&gt;',
        '"' => '&quot;',
		"'" => '&#39;',
		# '=' => '&#61;',
	];
}

sub define(;\$$)
{
	return &define($_) if @_ == 0;
	${$_[0]} = @_ == 2 ? $_[1] : ""
		unless defined ${$_[0]};
	${$_[0]};
}

sub custom_sprintf(%)
{
	my %map = @_;

	$map{$_} ||= sub { sprintf "%$_[0]$_[1]", $_[2] }
		for qw(s d c u o x e f g b p %);

	my $short = join '', grep length == 1, keys %map;
	my $short_re = "([ 0#+-]?[\\d.]*)([$short])";

	my $long = join '|', grep length > 1, keys %map;
	my $long_re = "\\{($long)(?::(.*?))?}";

	my $regexp = $long
		? qr/%(?:$short_re|$long_re)/
		: qr/%$short_re/;

	return sub ($;@) {

		unless (@_) {
			warn "Not enough arguments for custom sprintf";
			return undef;
		}

		my ($string, @values) = @_;
		return undef unless defined $string;

		my $i = 0;
		$string =~ s<$regexp><
			my ($flag, $class) = defined $2 ? ($1, $2) : ($4, $3);
			define $flag;
			if (ref (my $sub = $map{$class}) eq "CODE") {
				&$sub($flag, $class, $values[$i], \@values, ++$i);
			} else {
				warn "Invalid conversion in custom sprintf: $class";
			}
		>eg;

		return $string;
	};
}

sub parse_kvp($$$)
{
	my ($str, $sep, $eq) = @_;

	return () unless defined $str;

	my $kvp = wantarray ? {} : Hash::Stable->new;

	for my $kv (split $sep, $str) {
		my ($k, $v) = split $eq, $kv, 2;
		$kvp->{$k} = $v;
	}

	return wantarray ? %{$kvp} : $kvp;
}

sub deprefix(\$$)
{
	my ($s, $prefix) = @_;
	return 0 unless defined $prefix && defined ${$s};
	return 0 if length $prefix > length ${$s};
	if (substr(${$s}, 0, length $prefix) eq $prefix) {
		substr(${$s}, 0, length $prefix) = "";
		return 1;
	}
	return 0;
}

sub strneq($$;$)
{
	if (@_ == 2) {
		$_[2] = length $_[0] > length $_[1]
			? length $_[1] : length $_[0];
	}

	return substr($_[0], 0, $_[2]) eq substr($_[1], 0, $_[2]);
}

sub to_camel_case($)
{
	my $name = shift;
	$name =~ s#(?:^|[_-])([a-z])#\U$1#g;
	return $name;
}

sub un_camel_case($)
{
	my $name = shift;
	return $name unless $name =~ /[A-Z]/;
	$name =~ s#^([A-Z]+)#\L$1#g;
	$name =~ s#_*([A-Z]+)#_\L$1#g;
	return $name;
}

sub is_integer($)
{
	return 0 unless defined $_[0];
	return looks_like_number($_[0]) && $_[0] == int($_[0]);
}

sub ordinate($)
{
	return $_[0] unless is_integer($_[0]);
	my $n = abs($_[0]) % 100;
	my $s = $n >= 11 && $n <= 13 ? 'th'
		: ($n %= 10) == 1 ? 'st'
		: $n == 2 ? 'nd'
		: $n == 3 ? 'rd'
		: 'th';
	return $_[0] . $s;
}

sub can_strfy($)
{
	return defined $_[0] && ref $_[0] &&
		defined $overload::VERSION &&
		overload::Overloaded($_[0]) &&
		overload::Method($_[0], '""') &&
		1;
}

sub capture(&)
{
	my $block = shift;
	open my $handle, '>', \my $capture;
	my $selected = select $handle;
	$block->();
	select $selected;
	return $capture;
}

sub shell_expand($%)
{
	my ($str, %vars) = @_;
	return undef unless defined $str;

	$str =~ s#\$([a-z_][a-z_0-9]*)#defined $vars{$1} ? $vars{$1} : ""#egi;
	$str =~ s#\$\{([a-z_][a-z_0-9]*(?::(.*?))?)}#
		defined $vars{$1} ? $vars{$1} : defined $2 ? $2 : ""#egi;

	return $str;
}

sub compile_glob($;$)
{
	my ($glob, $flags) = @_;

	# flags are:
	#
	# a - anchored
	# C - smart-case
	# b - blank-delimited
	# f - fill space
	# F - fill non-words
	# w - word bounds
	# l - loose
	# s - shell
	# imSx - perl flags

	$flags = 's' unless defined $flags;
	$flags .= 'ab' if $flags =~ /s/;
	$flags .= 'CFw' if $flags =~ /l/;

	my $re = '';
	my $re_flags = '';
	my $fill = $flags =~ /f/ ? '\s'
		: $flags =~ /F/ ? '\W' : '';

	my @chars = split //, $glob;
	my ($escape, $caps, $space, $break, $class);

	for (my $i = 0; $i < @chars; $i++) {
		my $c = $chars[$i];
		my $first = $i == 0;
		my $last = $i == $#chars;
		my $inside = !$first && !$last;
		if ($break) {
			$re .= $flags =~ /a/ ? '$|^'
				: $flags =~ /w/ ? '\b|\b'
				: '|';
			undef $break;
		} elsif ($space) {
			if ($flags =~ /f/) {
				$re .= $flags =~ /w/ ? '\b\s*\b' : '\s+';
			} elsif ($flags =~ /F/) {
				$re .= $flags =~ /w/ ? '\W+' : '\s+';
			}
			undef $space;
		}
		if ($escape) {
			$re .= '\\' . $c;
			$caps += $c =~ /A-Z/;
			undef $escape;
		} elsif ($c eq '\\') {
			$escape++;
		} elsif ($c eq '*') {
			$re .= '.*';
		} elsif ($c eq '?') {
			$re .= '.';
		} elsif ($flags !~ /a/ && $first && $c eq '^') {
			$re .= '^' . $fill . '*';
 		} elsif ($flags !~ /a/ && $last && $c eq '$') {
			$re .= $fill . '*' . '$';
		} elsif ($c =~ /[\^\$.|(){}]/) {
			$re .= '\\' . $c;
		} elsif ($c eq ' ' &&
			$flags =~ /[bfF]/ && !$class) {
			$flags =~ /b/ ? $break ++ : $space ++;
			$i ++ while ($chars[$i+1]||'') eq ' ';
		} elsif ($c eq "\n") {
			$re .= '\\n';
		} else {
			$re .= $c;
			$caps ++ if $c =~ /[[:upper:]]/;
			$class ++ if $c eq '[';
			undef $class if $c eq ']';
		}
	}

	if ($flags =~ /a/) {
		my $r = $flags =~ /f/ ? '\s*'
			: $flags =~ /F/ ? '\W*'
			: '';
		$re = "^$r$re$r\$";
	} elsif ($flags =~ /w/) {
		$re = "\\b$re"
			unless $re =~ /^\^/;
		$re .= "\\b"
			unless $re =~ /\$$/;
	}

	if ($flags =~ /C/ && !$caps) {
		$re_flags .= 'i';
	}

	$re_flags .= 's' unless $flags =~ /S/;
	for my $flag (qw(i m x)) {
		if ($flags =~ /$flag/) {
			$re_flags .= $flag;
		}
	}

	return $re_flags ? qr/(?$re_flags:$re)/ : qr/$re/;
}

sub sysgetc(;$)
{
	my $handle = $_[0] || *STDIN;
	sysread $handle, my $c, 1;
	return $c;
}

sub sysreadline(;$)
{
	my $handle = $_[0] || *STDIN;

	my $line = '';

	while (1) {
		my $c;

		unless (sysread $handle, $c, 1) {
			return length $line ? $line : undef;
		}

		$line .= $c;

		if (substr ($line, -(length $/)) eq $/) {
			return $line;
		}
	}
}

sub text2html($)
{
	my $text = shift;
	return undef unless defined $text;

	$text =~ s!(\A|^\n+)(.*?)(\n+$|\Z)!
		my ($pre, $text, $post) = ($1, $2, $3);
		$text = html_escape($text);
		$text =~ s{\b([a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4})\b}
			{<a href="mailto:$1">$1</a>}ig;
		$text =~ s#((?:https?|ftp)://\S+)#<a href="$1">$1</a>#;
		$text =~ s#\r?\n#<br>\n#g;
		"$pre<p>\n$text\n<\/p>$post";
	!msge;

	return $text;
}

sub html2text($;$$$)
{
	my ($html, $lm, $rm, $utf_check) = @_;
	return undef unless defined $html;
	my $utf;
	if ($utf_check && !utf8::is_utf8($html) && $html =~ /[^\x00-\x7f]/) {
		my $string = decode('utf-8', (my $scratch = $html), FB_QUIET);
		if ($scratch eq '') {
			$html = $string;
			$utf ++;
		}
	}
	require HTML::TreeBuilder;
	my $builder = HTML::TreeBuilder->new;
	$builder->parse($html);
	my $tree = $builder->eof;
	require HTML::FormatText;
	my $formatter = HTML::FormatText->new(
		leftmargin => $lm || 0,
		rightmargin => $rm || 72);
	my $text = $formatter->format($tree);
	$text = encode('utf8', $text) if $utf;
	$text;
}

sub unique_enough_id()
{
	require Digest::MD5;
	substr(unpack('H*', Digest::MD5::md5(time(). {}. rand(). $$. 'blah')), 0, 16);
}

sub short_id(;$$$)
{
	my ($charset, $length, $exists) = @_;

	$charset ||= ['a'..'z', 'A'..'Z', '0'..'9'];
	$length ||= 8;

	while (1) {
		my $id = "";
		for (1 .. $length) {
			$id .= $charset->[int rand(@{$charset})];
		}
		unless ($exists && $exists->{$id}) {
			return $id;
		}
	}
}

sub boolean_value($;$)
{
	my ($value, $strict) = @_;
	unless (defined $value) {
		return 0 unless $strict;
		return undef;
	}
	if ($value =~ /^(?:0|off|f|false|n|no|disabled)$/i) {
		return 0;
	}

	if ($value =~ /^(?:1|on|t|true|y|yes|enabled)$/) {
		return 1;
	}

	if ($strict) {
		return undef;
	} else {
		return 1;
	}
}

sub utf8_encode($)
{
	my $s = shift;
	return undef unless defined $s;
	utf8::encode($s) if utf8::is_utf8($s);
	$s;
}

sub looks_like_utf8($)
{
	my $b = shift;

	if ($b =~ /[\xC0-\xFF][\x80-\xBF]+/) {
		return eval { decode_utf8($b, FB_QUIET) } ? 1 : 0;
	} else {
		return 0;
	}
}

sub redact_uri($)
{
	my $uri = shift;
	my $c = qr/[\@a-zA-Z0-9%.+ -]/o;

	# XXX: use parse_uri()
	$uri =~ s#((?:\w+:/?/?)?$c+:)$c+(\@$c)#$1**REDACTED**$2#;

	return $uri;
}

1;
