#!/usr/bin/perl

# Copyright (C) 2009-2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Util::Export;

use strict;
use warnings;

use Exporter;

sub import
{
	my $class = shift;
	my $caller = caller;

	no strict "refs";

	my $export = *{$caller . '::EXPORT'} = [];
	my $export_ok = *{$caller . '::EXPORT_OK'} = [];
	my $export_tags = *{$caller . '::EXPORT_TAGS'} = {};

	my @target = ($export_ok);

	for my $name (@_) {
		if ($name =~ /:$/) {
			@target = ();
			my $tags = substr($name, 0, -1);
			for my $tag (split "/", $tags) {
				if ($tag eq "default") {
					push @target, $export;
				} elsif ($tag eq "ok") {
					push @target, $export_ok;
				} else {
					push @target, \@{$export_tags->{$tag}};
				}
			}
		} else {
			push @{$_}, $name for @target, \@{$export_tags->{all}};
		}
	}

	push @{*{$caller . '::ISA'}}, 'Exporter';
}

1;
