#!/usr/bin/perl

# Copyright (C) 2011 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Util::Email;

use strict;
use warnings;

use Util::String qw(sysreadline);
use Util::Export qw(make_verp_address parse_verp_address
	read_mail_header seek_eml_header normalise_smtp_address
	get_mime_header get_mail_address slurp_mime_part);
use Util::File qw(slurp);
use Encode;

our $verp_delimiter = '=';

sub make_verp_address($$$$;$)
{
	my ($domain, $list_id, $msg_id, $address, $user_id) = @_;

	if (defined $user_id && (!defined $address || length $address > 64)) {
		$address = $user_id;
	}

	$address =~ s/@/$verp_delimiter/g;

	return sprintf '%s=%s=%s@%s',
		$list_id, $msg_id, $address, $domain;
}

sub parse_verp_address($)
{
	my $verp = shift;

	my ($user, $domain) = split('@', $verp, 2);
	$domain =~ s#/$##; # remove buggy trailing slash

	my ($list_id, $msg_id, $address) = split($verp_delimiter, $user, 3);

	my $user_id;
	unless ($address =~ s/$verp_delimiter/@/) {
		$user_id = $address;
		$address = undef;
	}

	return ($domain, $list_id, $msg_id, $address, $user_id);
}

sub read_mail_header(;$)
{
	require Email::Simple;
	my $header = '';

	while (defined (my $line = sysreadline($_[0])))
	{
		last if $line =~ /^\r?\n$/;
		$header .= $line;
	}

	return Email::Simple->new($header);
}

sub seek_eml_header($)
{
	my $handle = shift;

	my ($sender, $sendmail, @recipients);
	my $offset = tell $handle;

	local $/ = "\n";
	while (my $line = readline $handle) {
		if ($line =~ /^x-sender\s*:\s*(.*?)\s*$/i) {
			$sender = $1;
		} elsif ($line =~ /^x-(?:receiver|recipient)\s*:\s*(.*?)\s*$/i) {
			push @recipients, $1;
		} elsif ($line =~ /^x-sendmail\s*:\s*(.*?)\s*$/i) {
			$sendmail = $1;
		} else {
			last;
		}
		$offset = tell $handle;
	}

	seek $handle, $offset, 0;
	return ($sender, \@recipients, $sendmail);
}

sub normalise_smtp_address($)
{
	my $spec = shift;
	if ($spec =~ /^<>/) {
		return '<>';
	} elsif ($spec =~ /^<(.+?)>/) {
		return $1;
	} else {
		return $spec;
	}
}

sub get_mime_header($$)
{
	my ($mime, $name) = @_;
	return () unless ref $mime;

	my @header;

	if (UNIVERSAL::isa($mime, 'MIME::Entity')) {
		$mime = $mime->head;
	} elsif (UNIVERSAL::isa($mime, 'Email::Simple')) {
		$mime = $mime->header_obj;
	}

	if (UNIVERSAL::isa($mime, 'MIME::Head')) {
		require MIME::Words;
		@header = map {
			chomp;
			m/=\?.*\?.*\?=/s
				? Encode::decode('MIME-Header', $_)
				: $_;
		} $mime->get_all($name);
	} elsif (UNIVERSAL::isa($mime, 'Mail::Header')) {
		@header = $mime->get($name);
	} elsif (UNIVERSAL::isa($mime, 'Email::Simple::Header')) {
		@header = ($mime->header($name));
	} else {
		return ();
	}

	return wantarray ? @header : $header[0];
}

sub get_mail_address($)
{
	require Email::Address;
	my $line = shift;
	return unless defined $line;
	my @addresses = Email::Address->parse($line);
	return unless @addresses;
	return wantarray ? map { $_->address } @addresses
		: $addresses[0]->address;
}

sub slurp_mime_part($)
{
	my $mime = shift or return undef;
	my $body = $mime->bodyhandle()
		or return undef;
	my $io = $body->open("r")
		or return undef;
	slurp($io);
}

1;
