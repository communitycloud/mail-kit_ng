#!/usr/bin/perl

# Copyright (C) 2009-2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Util::Strfy;

use strict;
use warnings;

use Util::String qw(escape_chars looks_like_number);
use Util::Export qw($strfy);

our $strfy = __PACKAGE__->new;

sub new
{
	my ($class, %arg) = @_;

	bless {
		string_limit => 100,
		string_quote => 1,
		escape_chars => q("'\ntrabef),
		array_limit  => 100,
		hash_limit   => 100,
		array_delim  => ', ',
		hash_delim   => ' => ',
		ellipsis     => '...',
		unwind       => 0,
		undefined    => 1,
		%arg
	}, $class;
}

sub scalar
{
	my ($s, $value) = @_;

	unless (defined $value) {
		return 'undef';
	}

	if (my $ref = ref ($value)) {
		return sprintf("\\%s", $s->scalar($$value))
			if $ref eq "SCALAR";
		return sprintf("/%s/", $1)
			if $ref eq "Regexp" && $value =~ /^\(\?.*?:(.*)\)$/;
		return "sub {}"
			if $ref eq "CODE";
		if ($s->{unwind}) {
			return sprintf("[%s]", $s->array($value))
				if $ref eq "ARRAY";
			return sprintf("{%s}", $s->hash($value))
				if $ref eq "HASH";
		}
		return ref $value;
	} elsif (ref (\$value) eq "GLOB") {
		return "$value";
	}

	$value = escape_chars("$value", $s->{escape_chars});

	unless(utf8::is_utf8($value)) {
		$value =~ s/([[:cntrl:]]|[[:^ascii:]])/sprintf("\\x{%02x}",ord($1))/eg;
	}

	if ($s->{string_limit} && length($value) > $s->{string_limit}) {
		substr($value, $s->{string_limit} - 3) = $s->{ellipsis};
	}

	if ($s->{string_quote} > 1 ||
		$s->{string_quote} > 0 && !looks_like_number($value)) {
		$value = sprintf('"%s"', $value);
	}

	return $value;
}

sub array
{
	my ($s, $a) = @_;
	my @elems = map { $s->scalar($_) }
		$s->{array_limit} && @{$a} > $s->{array_limit}
			? (@$a[0 .. $s->{array_limit} - 1], $s->{ellipsis})
			: @{$a};
	@elems = grep defined, @elems unless $s->{undefined};
	return join $s->{array_delim}, @elems;
}

sub hash
{
	my ($s, $h) = @_;

	my @h = ref($h) eq "ARRAY"
		? @{$h} : %{$h};

	my @pairs = ();
	for (my $i = 0; $i < @h; $i += 2)
	{
		my ($key, $value) = @h[$i, $i+1];
		unless ($s->{undefined}) {
			next unless defined $value;
		}
		if ($s->{hash_limit} && @pairs > $s->{hash_limit}) {
			push @pairs, $s->{ellipsis};
			last;
		}
		if ($key =~ /\W/) {
			local $s->{string_quote} = 1;
			$key = $s->scalar($key);
		}
		push @pairs, $key . $s->{hash_delim} . $s->scalar($value);
	}
	return join $s->{array_delim}, @pairs;
}

sub caller
{
	my ($s, $step) = @_;
	my ($package, $filename, $line) = caller($step + 1);
	return sprintf "at %s line %d", $filename, $line;
}

sub trace
{
	my ($s, $depth) = @_;

	my %step;

	{
		package DB;
		@step{qw(package filename line subroutine
		has_args wantarray evaltext is_require)} = caller($depth + 1);
	}

	return undef unless defined $step{package};

	if (defined $step{evaltext}) {
		if ($step{is_require}) {
			$step{context} = "require $step{evaltext}";
		} else {
			$step{context} = sprintf("eval %s",
				$s->scalar($step{evaltext}));
		}
	} elsif ($step{subroutine} eq '(eval)') {
		$step{context} = 'eval {...}';
	} elsif ($step{has_args}) {
		$step{context} = sprintf('%s(%s)',
			$step{subroutine}, $s->array(\@DB::args));
	} else {
		$step{context} = sprintf('%s()', $step{subroutine});
	}

	return sprintf("%s called at %s line %d",
		$step{context}, $step{filename}, $step{line});
}

1;
