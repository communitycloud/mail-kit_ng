#!/usr/bin/perl

# Copyright (C) 2009-2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Util::List;

use strict;
use warnings;

use List::Util qw(min max reduce);
use Util::String qw(equndef);
use Util::Export qw(
	update_hash crop_hash
	list_unique list_index list_remove in_list list_intersect list_mesh
	min max reduce);

sub update_hash($$)
{
	my ($hash, $update) = @_;
	return $hash unless $update;
	for my $k (keys %{$update}) {
		$hash->{$k} = $update->{$k}
			unless exists $hash->{$k};
	}
	return $hash;
}

sub crop_hash($$)
{
	my ($hash, $model) = @_;
	return $hash unless $model;
	my $cropped = 0;
	for my $k (keys %{$hash}) {
		unless (exists $model->{$k}) {
			delete $hash->{$k};
			$cropped ++;
		}
	}
	return $cropped;
}

sub list_intersect($$)
{
	my ($a, $b) = @_;
	return () unless $a && $b;
	grep in_list($_, @{$b}), @{$a};
}

sub list_unique(@)
{
	my %seen;
	grep { !$seen{$_}++ } @_;
}

sub list_index($@)
{
	my $search = shift;

	map { return $_ if equndef($search, $_[$_]) } (0 .. $#_);

	return undef;
}

sub list_remove($\@)
{
	my ($search, $list) = @_;
	my $found = 0;

	while (defined (my $idx = list_index($search, @$list))) {
		splice @$list, $idx, 1;
		$found++;
	}

	return $found;
}

sub in_list($@)
{
	my $search = shift;

	map { return 1 if equndef($_, $search) } @_;

	return 0;
}

sub list_mesh(\@\@;\@\@)
{
	my $max = max map { $#$_ } @_;
	map { my $i = $_; map $_->[$i], @_ } 0 .. $max;
}

1;
