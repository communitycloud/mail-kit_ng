#!/usr/bin/perl

package Util::Email::Render;

use strict;
use warnings;

use Util::Export qw(render_mime_text render_mime_html list_mime_structure);
use Util::Email qw(slurp_mime_part);
use Util::String qw(html2text text2html);
use Encode qw(:DEFAULT :fallbacks);
use App::Message;

sub list_mime_structure($);
sub list_structure_type($$);

sub render_mime_text($)
{
	my $mime = shift;
	my $s = list_mime_structure($mime);
	my @text = list_structure_type($s, 'text');
	return join("\n", @text);
}

sub render_mime_html($)
{
	my $mime = shift;
	my $s = list_mime_structure($mime);
	my @text = list_structure_type($s, 'html');
	return '<p>' . join("</p><p>", @text) . '</p>';
} 

sub list_structure_type($$)
{
	my ($s, $type) = @_;

	if (!defined $s) {
		return ();
	}

	if (exists $s->{multipart}) {
		return map { list_structure_type($_, $type) } @{$s->{multipart}};
	}

	if ($type eq 'text') {
		if (exists $s->{text}) {
			return ($s->{text});
		} elsif (exists $s->{html}) {
			return (html2text($s->{html}));
		} elsif (exists $s->{alternative}) {
			if (defined $s->{alternative}->{text}) {
				return ($s->{alternative}->{text});
			} elsif (defined $s->{alternative}->{html}) {
				return (html2text($s->{alternative}->{html}));
			}
		}
	} elsif ($type eq 'html') {
		if (exists $s->{html}) {
			return ($s->{html});
		} elsif (exists $s->{text}) {
			return (text2html($s->{text}));
		} elsif (exists $s->{alternative}) {
			if (defined $s->{alternative}->{html}) {
				return ($s->{alternative}->{html});
			} elsif (defined $s->{alternative}->{text}) {
				return (text2html($s->{alternative}->{text}));
			}
		}
	} else {
		warning('list_structure_type: invalid type %q', $type);
	};

	warning('unhandled structure element: %{ph}', $s);
	return ();
}

sub list_mime_structure($)
{
	my $mime = shift;
	my $type = $mime->effective_type;

	if ($type eq 'text/html' || $type eq 'text/plain')
	{
		my $content = slurp_mime_part($mime);
		return undef unless defined $content;

		$content =~ s/\r\n$/\n/g;

		if (my $charset = $mime->head->mime_attr('content-type.charset')) {
			if (find_encoding($charset)) {
				$content = decode($charset, $content)
			} else {
				warning("unable to find encoding for character set %q", $charset);
			}
		} elsif ($content =~ /[^\x00-\x7f]/) {
			my $utf8 = decode('UTF-8', (my $scratch = $content), FB_QUIET);
			if ($scratch eq '') {
				warning("non-ascii text without explicit character set decoded as UTF-8");
				$content = $utf8;
			} else {
				warning("non-ascii text without explicit character set left encoded");
			}
		}
		
		return { $type eq 'text/plain' ? 'text' : 'html' => $content };
	} elsif ($type eq 'multipart/alternative') {
		my $part = {};
		if (my ($text) = grep { $_->effective_type eq 'text/plain' } $mime->parts) {
			$part->{text} = (list_mime_structure($text))->{text};
		} elsif (my ($html) = grep { $_->effective_type eq 'text/html' } $mime->parts) {
			$part->{html} = (list_mime_structure($html))->{html};
		} else {
			return undef;
		}

		return { alternative => $part };
	} elsif ($type eq 'multipart/mixed' || $type eq 'multipart/related') {
		my @list = ();
		for my $part ($mime->parts) {
			if (my $t = list_mime_structure($part)) {
				push @list, $t;
			}
		}
		return { multipart => \@list };
	} elsif ($type eq 'multipart/report') {
		if (my ($part) = grep {
			($_->head->get('Content-Description') || '') =~ /Notification/i } $mime->parts)
		{
			my $content = slurp_mime_part($part);
			return undef unless defined $content;
			$content =~ s/\r\n$/\n/g;
			return { text => $content };
		}
	}

	return undef;
}

1;
