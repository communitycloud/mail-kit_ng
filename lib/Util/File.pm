#!/usr/bin/perl

# Copyright (C) 2009-2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Util::File;

use strict;
use warnings;

use App::Message;
use Util::Export qw(fopen fcopy is_handle scalar2io slurp spew
	basename dirname extension file_under_directory
	cleanup_filename absolute_path resolve_path
	find_in_path readable_size
	list_directory concat_path mkhier rmtree path_stat_changed);
use Util::String qw(looks_like_number equndef);

my $path_stat_cache = {};

sub _ferror($$@)
{
	my ($arg, $fmt, @arg) = @_;
	return 0 if defined $arg->{error} && !$arg->{error};
	local $App::Message::caller += ($arg->{depth} || 0) + 2;
	error("$fmt: %s", @arg, $!);
}

sub fopen($%)
{
	my ($in, %arg) = @_;
	return $in if is_handle($in);
	$arg{mode} ||= $in =~ s#^([<>+]+)## ? $1 : '<';
	if ($in eq '-') {
		if ($arg{mode} =~ />/) {
			$arg{mode} = '>&';
			$in = 'STDOUT';
		} else {
			$arg{mode} = '<&';
			$in = 'STDIN';
		}
	}
	my $mode = sprintf('%s:%s',
		$arg{mode}, $arg{layer} || 'bytes');
	if (open my $handle, $mode, $in) {
		return $handle;
	} else {
		$arg{mode} =~ '>'
			? _ferror(\%arg, "cannot open `%s' for writing", $in)
			: _ferror(\%arg, "cannot open `%s'", $in);
	}
}

sub fcopy($$%)
{
	my ($fin, $fout, %arg) = @_;

	my $in = fopen($fin, %arg, depth => 1);
	my $out = fopen($fout, %arg, depth => 1, mode => '>');

	binmode($in);
	binmode($out);

	my $size = $arg{size} || 1024 * 1024 / 4;

	while (1) {
		my $r = sysread($in, my $buff, $size);
		unless (defined $r) {
			return _ferror(\%arg, "error while reading %q", $fin);
		}
		last unless $r;
		for (my ($w, $t) = (0); $w < $r; $w += $t) {
			$t = syswrite($out, $buff, $r - $w, $w);
			unless (defined $t) {
				return _ferror(\%arg, "error while writing %q", $fout);
			}
		}
	}

	close $in or _ferror(\%arg, "failed to close %q", $fin)
		unless $in eq $fin;
	close $out or _ferror(\%arg, "failed to close %q", $fout)
		unless $out eq $fout;

	return 1;
}

sub is_handle($)
{
	ref $_[0] eq "GLOB" && *{$_[0]}{IO} || UNIVERSAL::isa($_[0], "IO::Handle") ||
	ref $_[0] eq '' && &is_handle(\$_[0]);
}

sub scalar2io($)
{
	open my $handle, '+<', \$_[0];
	return $handle;
}

sub slurp($%)
{
	my ($in, %arg) = @_;
	$in = fopen($in, %arg, depth => 1);
	unless ($in) {
		return wantarray ? () : undef;
	}
	if (wantarray) {
		return map { chomp; $_ } readline($in) if $arg{chomp};
		return readline($in);
	}
	my $r = do { local $/ = undef; scalar readline($in) };
	chomp $r if $arg{chomp};
	return $r;
}

sub spew($$%)
{
	my ($out, $buff, %arg) = @_;
	my $handle = fopen($out, %arg, mode => '>', depth => 1) or return;
	print $handle $buff
		or return _ferror(\%arg, "failed to write %q", $out);
	close $handle or return _ferror(\%arg, "failed to close %q", $out)
		unless $handle eq $out;
	1;
}

sub mkhier($%)
{
	my ($path, %arg) = @_;

	my $dir = "";
	for my $p (split /\//, $path) {
		$dir .= $p . "/";
		next if -e $dir || mkdir ($dir, $arg{mode} || 0777);
		return _ferror(\%arg,
			"cannot create path to %s: mkdir %s", $path, $dir);
	}

	return 1;
}

sub rmtree($;$)
{
	my ($path, $keep) = @_;

	if (-d $path) {
		for my $e (list_directory($path)) {
			&rmtree($e)
				or return 0;
		}
		return 1 if $keep;
		rmdir $path;
	} elsif (-e $path) {
		return 1 if $keep;
		unlink $path;
	}
}

sub concat_path($$)
{
	return $_[0] .
		(substr($_[0], -1) ne '/' ? '/' : '') . $_[1];
}

sub list_directory($;$)
{
	my ($dir, $relative) = @_;

	opendir my $handle, $dir
		or return wantarray ? () : undef;

	my @list;

	while (my $entry = readdir $handle) {
		next if $entry eq '.' or $entry eq '..';
		push @list, $relative ? $entry : concat_path($dir, $entry);
	}

	return wantarray ? @list : \@list;
}

sub basename($;$)
{
	my ($path, $ext) = @_;
	return undef unless defined $path;
	substr($path, -1) = "" while substr($path, -1) eq "/";
	substr($path, 0, 1 + (rindex $path, "/")) = "";
	if (defined $ext) {
		if ($ext eq ".*" && (my $i = index $path, ".") != -1) {
			substr($path, $i) = "";
		} elsif (substr($path, - length $ext) eq $ext) {
			substr($path, - length $ext) = "";
		}
	}
	return $path;
}

sub dirname($)
{
	my ($path) = @_;
	return undef unless defined $path;
	substr($path, -1) = "" while substr($path, -1) eq "/";
	if ((my $i = rindex $path, "/") != -1) {
		return "/" if $i eq 0;
		return substr $path, 0, $i;
	} else {
		return ".";
	}
}

sub extension($)
{
	my ($path) = @_;
	return undef unless defined $path;
	my $basename = basename($path);
	if ((my $i = rindex($basename, ".")) != -1) {
		return substr($basename, $i + 1);
	} else {
		return "";
	}
}

sub file_under_directory($$)
{
	my ($file, $dir) = @_;
	return unless substr($file, 0, length $dir) eq $dir;
	return 1 if substr($dir, -1) eq "/";
	my $r = substr($file, length $dir, 1);
	return $r eq "" || $r eq "/";
}

sub cleanup_filename($)
{
	my $file = shift;
	$file =~ s#//+#/#g;
	$file =~ s#[^/]+/\.\./##
		while $file =~ m#[^/]+/\.\./#;
	return $file;
}

sub absolute_path($)
{
	require Cwd;
	my $file = shift;
	if ($file =~ m#^/#) {
		return $file;
	} else {
		return Cwd::getcwd() . '/' . $file;
	}
}

sub resolve_path($)
{
	my $path = absolute_path($_[0]);
	my $r = '/';
	for my $p (split '/', $path) {
		if (my $a = Cwd::realpath("$r/$p")) {
			$r = $a;
		} else {
			$r .= '/' . $p;
		}
	}

	return $r;
}

sub find_in_path($$)
{
	my ($file, $path) = @_;
	return $file if -f $file;
	for my $dir (@{$path}) {
		if (-f "$dir/$file") {
			return "$dir/$file";
		}
	}
	return undef;
}

sub readable_size($)
{
	my $spec = shift;
	my $size;

	if (looks_like_number($spec)) {
		$size = $spec;
	} elsif (my @stat = stat($spec)) {
		$size = $stat[7];
	} else {
		return -1;
	}

	my @units = ('B', 'K', 'M', 'G', 'T');

	my $i = 0;

	while ($size >= 1024 && $i < @units) {
		$size /= 1024;
		$i ++;
	}

	$size = int(10 * $size + .5) / 10;

	return $size . $units[$i];
}

sub path_stat_changed($$;$)
{
	my ($stat, $path, $expire) = @_;

	if (exists $path_stat_cache->{$path} &&
		$path_stat_cache->{$path}->[13] > time)
	{
		return 0;
	}

	my @stat = stat $path;
	$stat[13] = time + (@_ == 3 ? $expire : 1);

	my $r;

	if (exists $path_stat_cache->{$path}) {
		$r = !equndef($path_stat_cache->{$path}->[$stat], $stat[$stat]);
	}

	$path_stat_cache->{$path} = [@stat];

	return $r ? 1 : 0;
}

1;
