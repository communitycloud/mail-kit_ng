#!/usr/bin/perl

package Util::Time;

use strict;
use warnings;

use Util::Export qw(parse_duration make_duration retry_delay $retry_count readable_date);
use Scalar::Util qw(looks_like_number);

our $max_delay = &parse_duration('5m');
our $retry_count = 0;

sub parse_duration($)
{
	my $s = shift;
	return 0 unless defined $s;
	return 0 if $s eq 'now';
	return $1 if $s =~ /^([+-]?\d+)s?$/i;
	return $1 * 60 if $s =~ /^([+-]?\d+)m(?:in)?$/i;
	return $1 * 60 * 60 if $s =~ /^([+-]?\d+)h(?:ours?)?$/i;
	return $1 * 60 * 60 * 24 if $s =~ /^([+-]?\d+)d(?:ays?)?$/i;
	return $1 * 60 * 60 * 24 * 7 if $s =~ /^([+-]?\d+)w(?:eeks?)?$/i;
	return $1 * 60 * 60 * 24 * 365 if $s =~ /^([+-]?\d+)y(?:ears?)?$/i;
	return $s;
}

sub make_duration($)
{
	my $s = shift;
	return 'now' unless $s;
	if ($s < 60) {
		return sprintf("%ds", $s);
	} elsif ($s < 60 * 60) {
		return sprintf("%dm", int($s/60));
	} elsif ($s < 60 * 60 * 24) {
		return sprintf("%dh", int($s/60/60));
	} elsif ($s < 60 * 60 * 24 * 365) {
		return sprintf("%dd", int($s/60/60/24));
	} else {
		return sprintf("%dy", int($s/60/60/24/356));
	}
}

sub retry_delay(;$)
{
	my $a = shift;

	if (defined $a) {
		if ($a eq 'reset' or $a eq 0) {
			$retry_count = 0;
		}
		return 0;
	}

	my $delay = 2 ** $retry_count ++;
	if ($delay > $max_delay) {
		return $max_delay;
	} else {
		return $delay;
	}
}

sub readable_date($)
{
	require POSIX;
	my $spec = shift;
	my $time;

	if (looks_like_number($spec)) {
		$time = $spec;
	} elsif (my @stat = stat($spec)) {
		$time = $stat[9];
	} else {
		return "Unknown";
	}

	my @now = localtime;
	my @time = localtime $time;

	if (POSIX::strftime("%F", @now) eq POSIX::strftime("%F", @time)) {
		return POSIX::strftime("Today %r", @time);
	} elsif (POSIX::strftime("%y", @now) eq POSIX::strftime("%y", @time)) {
		return POSIX::strftime("%e %b %r", @time);
	} else {
		return POSIX::strftime("%D %r", @time);
	}
}

1;
