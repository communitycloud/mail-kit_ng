#!/usr/bin/perl

# Copyright (C) 2009-2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package XML::Light;

use strict;
use warnings;

use XML::Parser;

sub new
{
	my ($proto, $tag) = @_;
	goto $tag ? \&new_element : \&new_text_node;
}

sub new_element
{
	my ($proto, $tag, $attrs, @content) = @_;
	return bless {
		tag => $tag,
		attrs => $attrs || {},
		content => \@content,
	}, ref($proto) || $proto;
}

sub new_text_node
{
	my ($proto, $tag, $content, $attrs) = @_;
	return bless {
		tag => $tag,
		attrs => $attrs || {},
		content => $content,
	}, ref($proto) || $proto;
}

sub parser
{
	return new XML::Parser(Style => __PACKAGE__);
}

sub content
{
	my ($node, $i) = @_;

	if ($node->{tag}) {
		if (defined $i) {
			return $node->{content}->[$i];
		} elsif (wantarray) {
			return @{$node->{content}};
		}
	}

	return $node->{content};
}

sub root
{
	my $node = shift;
	my $root = $node;

	while (my $parent = $root->{parent})
	{
		$root = $parent;
	}

	return $root;
}

sub find
{
	my ($node, $name, $attrs) = @_;
	return () unless $node->{tag};

	my @found;

	CHILD: for my $c (@{$node->{content}})
	{
		if (defined($name) && $c->{tag} ne $name) {
			next CHILD;
		}

		for my $k (keys %$attrs)
		{
			next CHILD unless
				exists $c->{attrs}->{$k} && $c->{attrs}->{$k} eq $attrs->{$k};
		}

		push @found, $c;
	}

	return @found;
}

sub append
{
	my ($node, $child) = @_;
	$child->{parent} = $node;
	push @{$node->{content}}, $child;
}

sub remove
{
	my ($node, $child) = @_;

	for (my $i = 0; $i < @{$node->{content}}; $i++)
	{
		if ($node->{content}->[$i] eq $child)
		{
			splice @{$node->{content}}, $i, 1;
			$child->{parent} = undef;
			return $child;
		}
	}

	return undef;
}

sub empty
{
	my $node = shift;
	while ($node->{content}->[0])
	{
		$node->remove($node->{content}->[0]);
	}
	return $node;
}

sub detach
{
	my $node = shift;
	return unless $node->{parent};
	$node->{parent}->remove($node);
}

sub chomp
{
	my $node = shift;
	return unless $node->{tag};

	for (my $i = 0; $i < @{$node->{content}}; $i++)
	{
		my $c = $node->{content}->[$i];
		if ($c->{tag} eq 0)
		{
			if ($c->{content} =~ /^\s*$/ &&
				($i == 0 || $node->{content}->[$i-1]->{tag}) &&
				($i == $#{$node->{content}} || $node->{content}->[$i+1]->{tag}))
			{
				$c->detach();
				$i --;
			}
		} else {
			$c->chomp();
		}
	}
}

sub select
{
	my ($node, $path) = @_;
	my @path = split('/', $path);

	shift @path while @path && $path[0] eq '';
	return $node unless @path;

	my $selector = shift @path;
	my $attrs = { };

	if (my ($spec) = $selector =~ /\[(.*)\]$/)
	{
		$selector =~ s/\[.*\]$//;
		for my $a (split(',\s*', $spec)) {
			my ($k, $v) = split('=', $a, 2);
			$attrs->{$k} = $v;
		}
	}

	my @found = $node->find($selector, $attrs);
	return undef unless @found;
	return $found[0]->select(join('/', @path));
}

sub text
{
	my $node = shift;
	return $node->{content} unless $node->{tag};

	my $text;
	for my $c (@{$node->{content}}) {
		my $t = $c->text();
		if (defined $t) {
			$text = '' unless defined $text;
			$text .= $t;
		}
	}

	return $text;
}

sub clone
{
	my $node = shift;

	if (!$node->{tag}) {
		return $node->new_text_node($node->{tag}, $node->{content}, $node->{attrs});
	}

	my @content = map $_->clone, @{$node->{content}};
	return $node->new_element($node->{tag}, $node->{attrs}, @content);
}

sub as_string
{
	require XML::Generator;
	my ($node, $x, %options) = @_;
	$x ||= new XML::Generator(escape => 'always', pretty => 2, %options);

	if (!$node->{tag}) {
		if ($node->{attrs}->{cdata}) {
			return $x->xmlcdata($node->{content});
		} else {
			return $node->{content};
		}
	}

	my $name = $node->{tag};
	return $x->$name($node->{attrs}, map $_->as_string($x), @{$node->{content}});
}

sub Start
{
	my ($e, $tag, %attrs) = @_;
	my $node = __PACKAGE__->new($tag, \%attrs);
	$e->{node}->append($node) if $e->{node};
	$e->{node} = $node;
}

sub End
{
	my ($e, $tag) = @_;
	$e->{node} = $e->{node}->{parent}
		if $e->{node}->{parent};
}

sub Char
{
	my ($e, $text) = @_;
	my $c = $e->{node}->{content}->[-1];

	if ($c && !$c->{tag}) {
		$c->{content} .= $text;
	} else {
		my $node = __PACKAGE__->new(0 => $text);
		$e->{node}->append($node);
	}
}

sub CdataEnd
{
	my $e = shift;
	my $t = $e->{node}->{content}->[-1];

	if ($t && !$t->{tag}) {
		$t->{attrs}->{cdata}++;
	}
}

sub Final
{
	my $e = shift;
	$e->{node};
}

1;
