#!/usr/bin/perl

package Push::Dispatcher::MailEv;

use strict;
use warnings;

use Statement::Filter;
use App::Message "mail-ev:";
use App::Proc qw(app_fork set_resource_limit);
use MailEv::Event qw(match_event);
use MailEv::Storage qw(storage_open);
use MailEv::Namecache;
use Push::Loop;
use Push::HTTP::Client;

use base qw(Push::Dispatcher);

sub new
{
	my $class = shift;
	my $d = $class->SUPER::new(@_, subscribers => { }, jobs => { });
	return $d;
}

sub configure
{
	my ($d, $c) = @_;
	if ($c->name eq "storage" ||
		$c->name eq "namecache" ||
		$c->name eq "api_server" ||
		$c->name eq "timeout" ||
		$c->name eq "cpu_limit")
	{
		$d->{$c->name} = $d->{comet}->expand($c->value);
	} else {
		return 0;
	}

	return 1;
}

sub add
{
	my ($d, $sub) = @_;

	my $select = $sub->{query}->{select};
	if (!defined $select ||
		ref $select eq 'HASH' && !keys %{$select} ||
		ref $select eq 'ARRAY' && !scalar @{$select}) {
		return $sub->unregister("empty select");
	}

	unless (ref $select eq '' ||
		ref $select eq 'HASH' ||
		ref $select eq 'ARRAY')
	{
		return $sub->unregister("malformed select");
	}

	note("add %{ph:4dt} select=%{ev:c}",
		subscriber => $sub->{id},
		offset => $sub->{query}->{offset},
		limit => $sub->{query}->{limit},
		count => $sub->{query}->{count},
		$select);

	my $bg = $sub->{query}->{limit} || $sub->{query}->{count};
	my $rt = !(exists $sub->{query}->{offset} || $sub->{query}->{count});

	if ($bg) {
		$sub->poll_cork(1);
		$d->run_search($sub);
		unless ($rt) {
			return;
		}
	}

	$d->{subscribers}->{$sub->{id}}++;
}

sub remove
{
	my ($d, $sub) = @_;

	$d->timeout_cancel($sub->{id});

	if (my $job = delete $d->{jobs}->{$sub->{id}}) {
		if (my $pid = $job->{pid}) {
			kill TERM => $pid;
		}
	}

	delete $d->{subscribers}->{$sub->{id}};
}

sub timeout_search
{
	my ($d, $sid) = @_;
	if (my $sub = $d->{comet}->{subscribers}->{$sid}) {
		warning("search timeout=%d subscriber=%s",
			$d->{timeout},
			$sid);
		$sub->unregister("query timeout");
	}
}

sub timeout_cancel
{
	my ($d, $sid) = @_;
	if (my $job = $d->{jobs}->{$sid}) {
		if (my $t = $job->{timeout}) {
			Push::Loop->cancel($t);
		}
	}
}

sub run_search
{
	my ($d, $sub) = @_;

	my $reap = sub { $d->chld_handler($sub->{id}, @_) };

	if (my $pid = app_fork(reap => $reap, name => "search"))
	{
		my $job = {pid => $pid};
		if (my $t = $d->{timeout}) {
			$job->{timeout} = Push::Loop
				->timeout($t, sub { $d->timeout_search($sub->{id}) });
		}
		$d->{jobs}->{$sub->{id}} = $job;
		return;
	}

	if (my $cpu_limit = $d->{cpu_limit}) {
		set_resource_limit("cpu", $cpu_limit);
		$SIG{XCPU} = sub {
			warning("reached CPU limit=%d subscriber=%s", $cpu_limit, $sub->{id});
			$SIG{$_[0]} = "DEFAULT";
			kill $_[0], $$;
		};
	}

	my $s = storage_open($d->{storage});

	my $events = $sub->{query}->{count}
		? $s->count($sub->{query}->{select})
		: $s->search(@{$sub->{query}}{qw(select limit offset)});

	if (!$sub->{query}->{count} && $d->{namecache}) {
		my $namecache = MailEv::Namecache->new(
			servers => $d->{namecache}, api_server => $d->{api_server});
		my @names = $namecache->get('msg', map $_->{msg_id}, @{$events});
		my @miss;
		for (my $i = 0; $i < @names; $i++) {
			if (defined $names[$i]) {
				$events->[$i]->{extra}->{msg_name} = $names[$i];
			} else {
				push @miss, $events->[$i]->{msg_id};
			}
		}
		if (@miss && $d->{api_server}) {
			$namecache->bgfill('msg', @miss);
		}
	}

	my $message = {
		routing => "mail-ev",
		job => $$,
		subscriber => $sub->{id},
		data => $events,
	};

	my $push = Push::HTTP::Client->new(
		host => $d->{comet}->{server}->{host},
		port => $d->{comet}->{server}->{port},
		base => '/~comet'); # XXX
	$push->publish("mail-ev", $message);
	exit();
}

sub send_result
{
	my ($d, $msg, $sid) = @_;
	my $sub = $d->{comet}->{subscribers}->{$sid};
	$d->send($sid, $msg->{data});
	if (exists $sub->{query}->{offset} || $sub->{query}->{count}) {
		$sub->unregister("query complete");
	} else {
		$sub->poll_cork(0);
	}
	return 1;
}

sub dispatch
{
	my ($d, $msg) = @_;
	return undef unless ref($msg) eq 'HASH';

	if (my $sid = $msg->{subscriber}) {
		return $d->send_result($msg, $sid);
	}

	map $d->send($_, $msg),
	grep match_event($msg,
		$d->{comet}->{subscribers}->{$_}->{query}->{select}),
	keys %{$d->{subscribers}};
}

sub chld_handler
{
	my ($d, $sid, $pid, $status, $signal) = @_;

	unless ($d->{jobs}->{$sid}) {
		return;
	}

	$d->timeout_cancel($sid);

	if ($status != 0) {
		if (my $sub = $d->{comet}->{subscribers}->{$sid}) {
			warning("search process failed subscriber=%s pid=%d status=%d signal=%s",
				$sid, $pid, $status, $signal);
			$sub->unregister("query failed");
		}
	}
}

1;
