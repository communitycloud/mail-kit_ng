#!/usr/bin/perl

# Copyright (C) 2011 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Pickup::Service;

use strict;
use warnings;

use App::Timeout qw(:all);
use App::Message "pickup:";
use App::Yield qw(app_yield);
use Util::Package qw(name2package load_object);
use Util::Time qw(parse_duration);
use Util::File qw(mkhier slurp spew basename);
use Util::List qw(update_hash);
use App::Daemon_ng qw(:all);
use App::Proc qw(app_fork register_signal reap_children set_effective_id);
use Scalar::Util qw(looks_like_number);

use IO::Handle;
use IO::Select;
require POSIX;
use Linux::Inotify2;
use JSON::Events;

sub new
{
	my ($class, $directory, %arg) = @_;

	return bless {
		directory => $directory,
		options => {},
		defaults => {
			concurrency => 5,
			log_level => "note",
			error_implies_failure => 0,
			alias => undef,
			events => undef,
			keep => 0,
			keep_compress => 1,
		},
		other_fds => {},
		njobs => 0,
		queue => [],
		finished => [],
		%arg,
	}, $class;
}

sub bind
{
	my ($p, $service) = @_;

	my $pkg = $service =~ /::/
		? $service : name2package($service, __PACKAGE__);

	$p->{service} = eval { load_object($pkg, undef, $p, $service) }
		or error("failed to bind to %s: %s", $service, error_string);
	$p->{service}->bind();

	my %opts = $p->{service}->options();
	while (my ($key, $value) = each %opts) {
		$p->{defaults}->{$key} = $value;
	}

	return $p->{service};
}

sub service
{
	$_[0]->{service}
}

sub control_fifo
{
	my $p = shift;
	$p->{control} ||= do {
		my $path = $p->path('run', 'control');
		unless (-p $path) {
			POSIX::mkfifo($path, 0777)
				or error("failed to create control fifo %s: %s", $path, $!);
		}

		CORE::open my $handle, '+<', $path
			or error("failed to open control fifo %s: %s", $path, $!);

		$handle->blocking(0);
		$handle;
	};
}

sub inotify
{
	$_[0]->{inotify} ||= Linux::Inotify2->new
		or error("unable to create new inotify object: %s", $!);
}

sub select
{
	$_[0]->{select} ||= IO::Select->new;
}

sub create
{
	unless (ref $_[0]) {
		return $_[0]->new($_[1])->create($_[2]);
	}

	my ($p, $service, $user, $group) = @_;

	set_effective_id($user, $group);

	unless (-d $p->path) {
		mkhier($p->path);
	}

	for my $d (qw(pickup queue active output reject error keep etc tmp run log)) {
		-d $p->path($d) || mkdir($p->path($d))
			or error("create %s: mkdir %s: %s",
				$p->path, $d, $!);
	}

	$p->control_fifo; # vivify

	if ($service) {
		$p->bind($service);
		$p->service->create();
		$p->write_entry("run", "pickup.service", $service);
	}

	$p;
}

sub option
{
	defined $_[0]->{options}->{$_[1]}
		? $_[0]->{options}->{$_[1]}
		: $_[0]->{defaults}->{$_[1]};
}

sub configure
{
	my ($p, $key, $value) = @_;

	unless (exists $p->{defaults}->{$key}) {
		warning("unknown configuration option: %s", $key);
		return 0;
	}

	my $nv = defined $value && length $value
		? $value : $p->{defaults}->{$key};

	if ($key eq "log_level") {
		unless (exists $App::Message::levels{$nv}) {
			warning("invalid %s option: %s", $key, $nv);
			return 0;
		}
		$message_level = $value;
	} elsif ($key eq "concurrency") {
		unless (looks_like_number($nv) && $nv >= 0) {
			warning("invalid %s option: %s", $key, $nv);
			return 0;
		}
	} elsif ($key eq "keep") {
		$value = parse_duration($value)
			if defined $value;
	} elsif ($key eq "error_implies_failure") {
		unless ($nv eq 0 || $nv eq 1) {
			warning("invalid %s option: %s", $key, $nv);
			return 0;
		}
	} elsif ($key eq "alias") {
		$p->service->{alias} = $value;
	} elsif ($key eq "events") {
		$p->events_enable($value);
		# passing this to the service handler as well
		$p->service->configure($key, $value);
	} else {
		$p->service->configure($key, $value);
	}

	if (defined $value && length $value) {
		$p->{options}->{$key} = $value;
	} else {
		delete $p->{options}->{$key};
	}

	return 1;
}

sub write_config
{
	my ($p, %config) = @_;
	for my $key (keys %config) {
		unless ($key =~ /^[a-zA-Z0-9_-]+$/) {
			warning("invalid configuration key=%s", $key);
		} elsif (defined $config{$key} && length $config{$key}) {
			$p->write_entry("etc", $key, $config{$key});
		} else {
			$p->unlink_entry("etc", $key);
		}
	}
}

sub read_config
{
	my $p = shift;

	my $config = {};

	for my $key ($p->list_entry('etc')) {
		my $value = $p->read_entry('etc', $key);
		next unless defined $value;
		chomp $value;
		$config->{$key} = $value;
	}

	return $config;
}

sub load_config
{
	my $p = shift;

	my $config = $p->read_config();
	while (my ($key, $value) = each %{$config}) {
		$p->configure($key, $value);
	}
}

sub open
{
	my ($p, $directory) = @_;

	unless (ref $p) {
		return $p->new($directory)->open();
	}

	my @stat = stat($p->path);
	set_effective_id($stat[4], $stat[5]);

	my $service = $p->read_entry('run', 'pickup.service')
		or error("open %s: open run/pickup.service: %s", $p->path, $!);

	$p->create();

	chdir ($p->path)
		or error("open %s: failed to change directory: %s", $p->path, $!);

	$p->bind($service);

	$p->load_config();

	return $p;
}

sub path
{
	my ($p, @path) = @_;
	return $p->{directory} unless @path;
	join '/', $p->{directory}, @path;
}

sub read_entry
{
	my ($p, @entry) = @_;
	my $rv = scalar slurp($p->path(@entry), error => 0, chomp => 1);
	unless (defined $rv) {
		warning("read entry %s: %s", join('/', @entry), $!);
	}
	return $rv;
}

sub write_entry
{
	my ($p, @entry) = @_;
	my $content = pop @entry;
	my $rv = spew($p->path(@entry), $content, error => 0);
	unless ($rv) {
		warning("write entry %s: %s", join('/', @entry), $!);
	}
	return $rv;
}

sub unlink_entry
{
	my ($p, @entry) = @_;
	my $rv = unlink ($p->path(@entry));
	unless ($rv) {
		warning("unlink entry %s: %s", join('/', @entry), $!);
	}
	return $rv;
}

sub list_entry
{
	my ($p, @entry) = @_;
	my $rv = opendir my $handle, $p->path(@entry);
	unless ($rv) {
		warning("list entry %s: %s", $p->path(@entry), $!);
		return ();
	}

	my @list;
	while (my $e = readdir $handle) {
		next if $e eq "." || $e eq "..";
		push @list, $e;
	}

	closedir $handle;
	return @list;
}

sub rename_entry
{
	my ($p, $from, $to) = @_;

	$from = $p->path(@{$from}) if ref $from;
	$to = $p->path(@{$to}) if ref $to;

	my $rv = rename($from, $to);
	unless ($rv) {
		warning("rename entry %s to %s: %s", $from, $to, $!);
	}
	return $rv;
}

sub pickup_stale
{
	my $p = shift;
	my @stale = $p->list_entry('pickup')
		or return;

	info("stale pickup entries found");
	for my $id (@stale) {
		next if substr($id, 0, 1) eq '.';
		$p->pickup_job($id);
	}
}

sub queue_stale
{
	my $p = shift;
	my @stale = $p->list_entry('queue')
		or return;

	info("stale queue entries found");
	for my $id (@stale) {
		next if substr($id, 0, 1) eq '.';
		$p->queue_job($id);
	}
}

sub open_watchers
{
	my $p = shift;
	$p->inotify->watch($p->path('pickup'), IN_CLOSE_WRITE | IN_MOVED_TO)
		or error("failed to create pickup inotify watcher: %s", $!);
	$p->inotify->watch($p->path('etc'), IN_CLOSE_WRITE | IN_DELETE)
		or error("failed to create pickup inotify watcher: %s", $!);
}

sub read_watchers
{
	my $p = shift;

	for my $ev ($p->inotify->read) {
		if ($ev->w->name eq $p->path('etc')) {
			my $name = $ev->name;
			my $value = $ev->IN_CLOSE_WRITE
				? $p->read_entry('etc', $name) : undef;
			note("configuration changed key=%s value=%s", $name, $value);
			$p->configure($name, $value);
		} elsif ($ev->w->name eq $p->path('pickup')) {
			next if substr($ev->name, 0, 1) eq '.';
			my @mask = ();
			for my $t (qw(IN_CLOSE_WRITE IN_MOVED_TO IN_DELETE)) {
				push @mask, $t if $ev->$t;
			}
			debug('pickup event name=%s mask=%s (%s)',
				$ev->name, $ev->mask, join('|', @mask));
			$p->pickup_job($ev->name);
		}
	}
}

sub read_control
{
	my $p = shift;

	while (my $line = readline $p->control_fifo) {
		chomp $line;
		if ($line =~ /^QUEUE (.+)/) {
			$p->queue_job($1);
		} else {
			warning("invalid control command: %s", $line);
		}
	}
}

sub select_fd
{
	my ($p, $fd, $cb) = @_;
	$p->select->add($fd);
	$p->{other_fds}->{$fd} = $cb;
}

sub cancel_fd
{
	my ($p, $fd) = @_;
	$p->select->remove($fd);
	delete $p->{other_fds}->{$fd};
}

sub update_presence
{
	my $p = shift;
	my $presence = join(' ', time, scalar @{$p->{queue}});
	$p->write_entry('run', 'presence', $presence);
	add_timeout(1, sub { $p->update_presence });
}

sub keep_expire_dir
{
	my $p = shift;

	$p->option('keep') > 0
		or return undef;

	opendir my $dh, $p->path('keep')
		or return undef;

	my $i = 0;

	while (my $e = readdir $dh) {
		next if $e eq "." || $e eq "..";
		$p->keep_expire_file($e) && $i++;
	}

	closedir $dh;

	return $i;
}

sub keep_expire_file
{
	my ($p, $name, $add_timeout) = @_;

	my $dur = $p->option('keep')
		or return undef;

	my ($file, $entry);
	my $time = time;

	for my $a ($name, "$name.gz") {
		my $f = $p->path(keep => $a);
		if (-f $f) {
			$file = $f;
			$entry = $a;
		}
	}

	unless ($file) {
		debug("attempt to expire missing keep file=%s", $name);
		return undef;
	}

	my $mtime = (stat($file))[9];

	unless ($mtime) {
		warning("cannot stat keep file=%s: %s", $entry, error_string);
		return undef;
	}

	if ($mtime + $dur <= $time) {
		if (unlink($file)) {
			note("unlinked keep file=%s mtime=%s time=%s dur=%s",
				$entry, $mtime, $time, $dur);
		} else {
			warning("failed to remove keep file=%s: %s", $file, error_string);
		}
	} elsif ($add_timeout) {
		my $to = $mtime + $dur - $time;
		add_timeout($to, sub { $p->keep_expire_file($name) });
	}
}

sub keep_job
{
	my ($p, $id) = @_;

	my $dur = $p->option('keep');
	my $gz = $p->option('keep_compress');

	$p->rename_entry(['queue', $id], ['keep', $id])
		or return undef;

	if ($gz) {
		my $job = sub { exec('gzip', $p->path('keep' => $id)); exit 1 };
		app_fork(job => $job);
	}

	add_timeout($dur, sub { $p->keep_expire_file($id, 'true') });

	note("job=%s kept for dur=%s%s",
		$id, $dur, $gz ? " (compress)" : "");

	return 1;
}

sub control
{
	my ($p, $action) = @_;
	daemon_control($action, $p->path('run/pickup.dmn'));
}

sub daemonize
{
	my $p = shift;
	daemon_configure(
		action => "start",
		name => $p->service->name,
		control => $p->path('run/pickup.dmn'),
		log_file => $p->path('log/pickup.log'),
		log_watch => 60,
	);
	daemon_apply();
}

sub start
{
	my $p = shift;
	$p->open_watchers();
	$p->daemonize();
	$p->keep_expire_dir();
	$p->loop();
}

sub fg
{
	my $p = shift;
	daemon_configure(foreground => 1);
	$p->start();
}

sub stop
{
	$_[0]->control("stop");
}

sub status
{
	$_[0]->control("status");
}

sub reload
{
	$_[0]->control("reload");
}

sub is_running
{
	$_[0]->control("running");
}

sub loop
{
	my $p = shift;

	info("main loop directory=%s service=%s",
		$p->path, $p->{service}->name);

	register_signal(CHLD => \my $got_sig_chld);

	my $control_fd = fileno($p->control_fifo);
	$p->select->add($p->inotify->fileno);
	$p->select->add($control_fd);

	$p->update_presence();

	$p->queue_stale();
	$p->pickup_stale();

	while (1) {
		while ($got_sig_chld) {
			$got_sig_chld = 0;
			reap_children();
			$p->dequeue();
		}

		app_yield();

		my $timeout = run_timeouts();
		for my $r ($p->select->can_read($timeout)) {
			if ($r == $control_fd) {
				$p->read_control();
			} elsif ($r == $p->inotify->fileno) {
				$p->read_watchers();
			} elsif (my $cb = $p->{other_fds}->{$r}) {
				$cb->($r);
			} else {
				warning("unhandled descriptor in select loop fd=%d", $r);
				$p->select->remove($r);
			}
		}
	}
}

sub pickup_job
{
	my ($p, $id) = @_;

	note("pickup job id=%s", $id);
	$p->rename_entry(['pickup', $id], ['queue', $id])
		or return 0;

	$p->queue_job($id);
}

sub queue_job
{
	my ($p, $id) = @_;

	my $njobs = $p->{njobs};
	my $concurrency = $p->option('concurrency');

	note("queue job id=%s [%d/%d]", $id, $njobs, $concurrency);

	if ($njobs < $concurrency) {
		return $p->run_job($id);
	} else {
		push @{$p->{queue}}, $id;
		return 1;
	}
}

sub run_job
{
	my ($p, $id) = @_;

	$p->{njobs}++;

	my $queue = $p->path('queue', $id);
	my $active = $p->path('active', $id);
	my $error = $p->path('error', $id);

	my $job = sub {
		CORE::open my $input, '+<', $queue
			or error("failed to redirect input to %s: %s", $queue, $!);
		CORE::open STDOUT, '>', $active
			or error("failed to redirect output to %s: %s", $active, $!);
		CORE::open STDERR, '>', $error
			or error("failed to redirect error to %s: %s", $error, $!);

		CORE::open STDIN, '<&', $input;
		my $rv = $p->execute($id, $input);

		CORE::close(STDOUT)
			or error("failed to close standard output: %s", $!);

		exit $rv;
	};

	my $reap = sub {
		my ($pid, $status, $signal) = @_;
		$p->finish_job($id, $pid, $status, $signal);
	};

	my $pid = app_fork(
		job => $job,
		reap => $reap,
	);

	note("run job id=%s pid=%d", $id, $pid);
}

sub execute
{
	my ($p, $id, $input) = @_;

	my $event = JSON::Event->new("pipeline." . $p->service->name)
		if $p->events;

	my $rv = $p->service->execute($id, $input, $event);

	if ($p->{events}) {
		my $status = $p->resolve_status($id, $rv);
		$event->error("Job exited with status %d", $status)
			if $status != 0;

		$p->events_put($event, $id, $event);
	}

	return $rv;
}

sub resolve_status
{
	my ($p, $id, $status) = @_;

	if (!$status &&
		$p->option('error_implies_failure') && -s $p->path('error', $id))
	{
		$status = 1;
	}

	return $status;
}

sub finish_job
{
	my ($p, $id, $pid, $status, $signal) = @_;

	note("job done id=%s pid=%d status=%s signal=%s",
		$id, $pid, $status, $signal);

	$status = $p->resolve_status($id, $status);

	if ($status == 0) {
		if (-s $p->path('active', $id)) {
			$p->rename_entry(['active', $id], ['output', $id])
		} else {
			$p->unlink_entry('active', $id);
		}

		unless ($p->option('keep') > 0 && $p->keep_job($id)) {
			$p->unlink_entry('queue', $id);
		}
		$p->unlink_entry('error', $id);
	} else {
		$p->rename_entry(['queue', $id], ['reject', $id]);
		$p->unlink_entry('active', $id);
		if (! -s $p->path('error', $id)) {
			$p->unlink_entry('error', $id);
		}
	}

	$p->{njobs}--;
}

sub dequeue
{
	my $p = shift;

	my $concurrency = $p->option('concurrency');

	while (@{$p->{queue}} && $p->{njobs} < $concurrency) {
		$p->run_job(shift @{$p->{queue}});
	}
}

sub generate_id
{
	sprintf "%lx", time * rand; # XXX
}

sub pickup_external_file
{
	my ($p, $file) = @_;

	my $id = $p->generate_id();

	my $tmp = $p->path('tmp', $id);

	CORE::open my $in, $file
		or error("couldn't open %s: %s", $file, $!);
	CORE::open my $out, '>', $tmp
		or error("couldn't open temporary %s: %s", $file, $!);

	while (1) {
		use bytes;
		my $nread = sysread($in, my $buffer, 4096);
		error("sysread failed: %s", $!) unless defined $nread;
		last if $nread == 0;
		syswrite($out, $buffer) or error("syswrite failed: %s", $!);
	}

	close($in);
	close($out);

	unless ($p->rename_entry(['tmp', $id], ['pickup', $id])) {
		$p->unlink_entry('tmp', $id);
		error("couldn't rename temporary, file submission failed");
	}

	# info("submitted as id=%s file=%s", $id, $file);
}

sub test
{
	my ($p, $file) = @_;

	my $pid = app_fork();

	if ($pid) {
		waitpid $pid, 0;
		if ($? != 0) {
			error("job failed status=%d", $?);
		}
	} else {
		CORE::open my $input, '+<', $file
			or error("failed to open input file=%s: %s", $file, $!);
		$App::Message::output = \*STDERR;
		CORE::open STDIN, '<&', $input;
		exit $p->execute(basename($file), $input);
	}
}

sub make_multi_name
{
	my ($p, $id, $seq, $size) = @_;

	$size ||= 1;
	sprintf('%s.__MULTI__.%0*d',
		$id, length $size, $seq);
}

sub events
{
	$_[0]->{events};
}

sub events_enable
{
	my ($p, $spec) = @_;
	$spec = 1 if @_ == 1;

	if (! defined $spec || $spec eq '0' || $spec eq 'false') {
		delete $p->{events};
		return undef;
	}

	my $file = ($spec eq '1' || $spec eq 'true')
		? undef : $spec;

	eval { $p->{events} ||= JSON::Events->new($file) };

	if ($@) {
		warning('failed to enable events: %s', error_string);
		return undef;
	}

	1;
}

sub events_put
{
	my ($p, $event, $id) = @_;

	unless ($event && $p->events) {
		return 0;
	}

	if ($id) {
		if ($id =~ /^(.*)\.__MULTI__\.(\d+)$/) {
			$event->push(job_id => $1, seq => $2);
		} else {
			$event->push(job_id => $id);
		}
	}

	$p->events->put($event);
}

1;
