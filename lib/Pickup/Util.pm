#!/usr/bin/perl

# Copyright (C) 2011 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Pickup::Util;

use strict;
use warnings;

use Util::Export qw(is_pickup_directory read_pickup_directory_service pickup_rename);

sub is_pickup_directory($)
{
	return -d $_[0] && -d $_[0] . '/pickup' && -d $_[0] . '/tmp' && -s $_[0] . '/run/pickup.service';
}

sub read_pickup_directory_service($)
{
	return undef unless is_pickup_directory($_[0]);
	open my $handle, $_[0] . '/run/pickup.service'
		or return undef;
	chomp(my $service = readline $handle);
	close $handle;
	return $service;
}

sub pickup_rename($$)
{
	my ($dir, $job) = @_;
	rename(
		$dir . '/tmp/' . $job,
		$dir . '/pickup/' . $job);
}

1;
