#!/usr/bin/perl

package Pickup::Service::MailKit::Store;

use strict;
use warnings;

use App::Message;
use MailKit::Store;
use Util::Email qw(seek_eml_header);

use base qw(Pickup::Service::Common);

sub bind
{
	my $s = shift;
	$s->{store} = MailKit::Store->new();
	1;
}

sub options
{
	my $s = shift;
	(root => undef,
	$s->SUPER::options);
}

sub configure
{
	my ($s, $key, $value) = @_;
	if ($key eq 'root') {
		$s->{store}->{path} = $value;
	} else {
		$s->SUPER::configure($key, $value);
	}
}

sub execute
{
	my $s = shift;
	my ($sender, $recipients) = seek_eml_header(\*STDIN);
	$s->{store}->add(\*STDIN, $recipients, $sender);
	return 0;
}

1;
