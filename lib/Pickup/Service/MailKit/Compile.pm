#!/usr/bin/perl

package Pickup::Service::MailKit::Compile;

use strict;
use warnings;

use App::Message;
use MailKit::Compiler;
use Util::File qw(slurp);
use Util::String qw(un_camel_case);

use base qw(Pickup::Service::Common);

sub bind
{
	my $s = shift;
	$s->{compiler} = MailKit::Compiler->new();
	1;
}

sub execute
{
	my ($s, $id, $input, $event) = @_;

	$s->{compiler}->{context}->add(scalar slurp(*STDIN));

	my $template =
		$s->{compiler}->{context}->get('template');
	($template = un_camel_case($template)) =~ tr/_/-/;

	my $ops = $s->{compiler}->capture();
	print $s->{compiler}->compile($template);

	if (!$event) {
		return 0;
	}

	for my $op (@{$ops}) {
		if ($op->[0] eq '+' && $op->[1] =~ /^(sender|subject|recipient|verp)$/) {
			$event->push($op->[1] => $op->[2]);
		}
	}

	for my $var (qw(community_short_uri default_email_domain recipient_email
		community_display_name site_name community_email))
	{
		my $val = $s->{compiler}->{context}->get($var);
		if (defined $val && length $val) {
			$event->push($var => $val);
		}
	}

	return 0;
}

1;
