#!/usr/bin/perl

package Pickup::Service::MailKit::CompileCustom;

use strict;
use warnings;

use App::Message;
use MailKit::Compiler::Context;
use Util::File qw(slurp mkhier spew);
use Util::String qw(un_camel_case);

use base qw(Pickup::Service::Common);

sub bind
{
	my $s = shift;
	require LWP::UserAgent;
	$s->{ua} = LWP::UserAgent->new;
}


sub options
{
	templates_dir => undef,
	download_base => undef,
}

sub download_template
{
	my ($s, $template, $job_id) = @_;

	my $base = $s->pickup->option('download_base') ||
		error("download_base not defined");
	my $dir = $s->pickup->option('templates_dir') ||
		error("templates_dir not defined");

	# http request
	my $url = sprintf($base, $template);
	my $r = $s->{ua}->get($url);
	unless ($r->is_success) {
		error('failed to retrieve template from %s: %s',
			$url, $r->status_line);
	}

	# copy contents into temporary file, then unzip
	mkhier("$dir/$template");

	my $tmp = $s->pickup->path('tmp', "download_$job_id.zip");
	spew($tmp, $r->content);
	system('unzip', '-qo', $tmp, '-d', "$dir/$template") == 0
		or error('failed to execute unzip: %s', $?);
	unlink($tmp)
		or warning('failed to remove temporary file %s', $tmp);
}

sub execute
{
	my ($s, $id) = @_;

	my $ctx = MailKit::Compiler::Context->new;
	my $input = slurp(*STDIN);
	$ctx->add($input);

	my $template = $ctx->get('template');
	($template = un_camel_case($template)) =~ tr/_/-/;

	my $base = $s->pickup->option('download_base') ||
		error("download_base not defined");

	my $dir = $s->pickup->option('templates_dir') ||
		error("templates_dir not defined");

	unless (-e "$dir/$template") {
		$s->download_template($template, $id);
	}

	print $input;

	return 0;
}

1;
