#!/usr/bin/perl

package Pickup::Service::MailKit::Multi;

use strict;
use warnings;

use base qw(Pickup::Service::Common);
use JSON v2;
use Util::List qw(update_hash);
use App::Message;

sub bind
{
	my $s = shift;
	$s->{json} = JSON->new->latin1;
	1;
}

sub options
{
	splitter => "mkit_compile",
}

sub execute
{
	my ($s, $id) = @_;

	my ($global, $seq);
	my $splitter = $s->pickup->option('splitter');

	while (my $json = <STDIN>) {
		$json =~ s/[\r\n]+$//;
		next unless length $json;
		my $ex = eval { $s->{json}->decode($json) };
		error("invalid JSON expression %q line %d: %s",
			$json, $., error_string(), $@) if $@;
		error("invalid JSON object type %q line %d", $json, $.)
			unless ref($ex) eq 'HASH';
		unless (defined $global) {
			$global = $ex;
		} else {
			update_hash($ex, $global);
			my $json = $s->{json}->encode($ex);
			my $multi_name = $s->pickup->make_multi_name($id, $seq++);
			printf "%d %s/%s\n", length($json), $splitter, $multi_name;
			print $json, "\n";
		}
	}

	return 0;
}

1;
