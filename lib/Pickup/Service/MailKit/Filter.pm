#!/usr/bin/perl

package Pickup::Service::MailKit::Filter;

use strict;
use warnings;

use App::Message;
use MailKit::Filter;

use base qw(Pickup::Service::Common);

sub bind
{
	my $s = shift;

	$s->{filter} = MailKit::Filter->new(
		tmp => $s->pickup->path('tmp'));
	1;
}

sub options
{
	my $s = shift;
	(strip_attachments => undef,
	dmarc_rewrite => undef,
	$s->SUPER::options);
}

sub configure
{
	my ($s, $key, $value) = @_;

	if ($key eq 'strip_attachments' || $key eq 'strip_attachments_legacy' || $key eq 'dmarc_rewrite') {
		if (defined $value) {
			$s->{filter}->{options}->{$key} = $value;
		} else {
			delete $s->{filter}->{options}->{$key};
		}
	} else {
		return $s->SUPER::configure($key, $value);
	}

	return 0;
}

sub execute
{
	my ($s, $id) = @_;

	if ($s->pickup->events) {
		$s->{filter}->{events} = [];
	}

	my $h = $s->{filter}->process(\*STDIN);
	print while <$h>;

	while (my $ev = pop @{$s->{filter}->{events}}) {
		$s->pickup->events_put($ev, $id);
	}

	return 0;
}

1;
