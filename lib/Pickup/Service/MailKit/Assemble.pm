#!/usr/bin/perl

package Pickup::Service::MailKit::Assemble;

use strict;
use warnings;

use App::Message;
use MailKit::Assembler;
use MailKit::CommandParser;

use base qw(Pickup::Service::Common);

sub bind
{
	my $s = shift;
	$s->{assembler} = MailKit::Assembler->new();
	$s->{assembler}->{options}->{format} = 'eml';
	$s->{command_parser} = MailKit::CommandParser->new($s->{assembler});
	1;
}

sub execute
{
	my $s = shift;

	$s->{command_parser}->parse('-', 'utf8');
	print $s->{assembler}->assemble();

	return 0;
}

1;
