#!/usr/bin/perl

# Copyright (C) 2011 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Pickup::Service::Splitter;

use strict;
use warnings;

use App::Message;
use Pickup::Util qw(is_pickup_directory);
use Util::String qw(sysgetc sysreadline);
use base qw(Pickup::Service::Common);

sub options
{
	base => "..",
}

sub execute
{
	my $p = shift;

	while (1) {

		my $offset = sysseek(STDIN, 0, 1);

		my $header = sysreadline();
		last unless defined $header;

		my ($size, $pickup, $job) = $header =~ m#^(\d+) (.+?)(?:/(.+?))?\n$#
			or error("invalid splitter header %q at offset %d", $header, $offset);

		unless (substr ($pickup, 0, 1) eq '/') {
			my $base = $p->pickup->option('base');
			$base = $p->pickup->path . '/' . $base
				unless substr ($base, 0, 1) eq '/';
			$pickup = $base . '/' . $pickup;
		}

		unless (is_pickup_directory($pickup)) {
			error("cannot output to %s: not a pickup directory at offset %d",
				$pickup, $offset);
		}

		unless (defined $job) {
			$job = $p->pickup->generate_id();
		}

		my $tmp = "$pickup/tmp/$job";
		my $submit = "$pickup/pickup/$job";

		open my $handle, '>', $tmp
			or error("failed to open %q for writing: %s", $tmp, $!);

		while ($size > 0) {
			my $n = $size > 4096 ? 4096 : $size;
			my $nread = sysread STDIN, (my $buff), $n;

			unless (defined $nread) {
				error("read error: %s", $!);
			}

			unless ($n == $nread) {
				error("failed to read %d more bytes from input", $n);
			}

			syswrite $handle, $buff
				or error("write error: %s", $!);

			$size -= $n;
		}

		close $handle;

		rename $tmp, $submit
			or error("failed to rename %q to %q: %s",
				$tmp, $submit, $!);

		my $c = sysgetc();

		if (defined $c && $c ne "\n") {
			error("expecting new line separator at offset %d",
				sysseek(STDIN, 0, 1));
		}
	}

	return 0;
}

1;
