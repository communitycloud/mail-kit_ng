#!/usr/bin/perl

package Pickup::Service::ECS::Ng;

use strict;
use warnings;

use App::Message;
use MIME::Parser;
use MIME::Words qw(encode_mimewords);
use JSON v2;
use Util::File qw(slurp);
use Util::String qw(uri_escape to_camel_case compile_glob looks_like_utf8);
use Util::Email qw(make_verp_address);
use Email::Date::Format;
use LWP::UserAgent;
use Storable qw(dclone);

use base qw(Pickup::Service::Common);

sub options
{
}

sub bind
{
	my $s = shift;
	$s->{parser} = MIME::Parser->new();
	$s->{parser}->output_under($s->pickup->path('tmp'));
	$s->{json} = JSON->new;
	$s->{ua} = LWP::UserAgent->new();
}

sub render_template($$)
{
	my ($t, $ctx) = @_;

	require MailKit::Compiler;
	require MailKit::Assembler;

	my $c = MailKit::Compiler->new();
	my $ops = $c->capture();

	$c->context->add($ctx);
	$c->compile($t);

	my $a = MailKit::Assembler->new();

	for my $op (@{$ops}) {
		$a->emit(@{$op});
	}

	return $a->assemble();
}

sub cleanup_entity($)
{
	my $e = shift;
	$e->head->delete('X-Mailer');
	$e->head->delete('Content-Disposition');
	$e->head->set('Content-Transfer-Encoding', $e->suggest_encoding);
}

sub build_mime_part
{
	my ($s, $json, $mime) = @_;

	$mime ||= MIME::Entity->new();

	# resolve headers
	if (my $p = $json->{headers}) {
		$s->do_headers($mime, $p);
	}

	if (my $p = $json->{type}) {
		if (ref $p eq 'ARRAY') {
			$s->do_hdr($mime, 'content-type' => { mime_attr => $p->[0], @$p[-@$p+1 .. -1] });
		} else {
			$s->do_hdr($mime, 'content-type' => $p);
		}
	}

	if (my $p = $json->{attachment} || $json->{inline}) {
		$s->do_hdr($mime, 'content-disposition',
			{ mime_attr => $json->{attachment} ? 'attachment' : 'inline', 'filename' => $p });
		$json->{encoding} ||= 'base64';
		$json->{type} ||= 'application/octet-stream';
	}

	# resolve body
	if (my $p = $json->{body}) {
		my $body = MIME::Body::Scalar->new($p);
		$mime->bodyhandle($body);
	} elsif (my $url = $json->{url}) {
		# XXX: implement caching and callback based download
		my $r = $s->{ua}->get($url, 'Accept-Encoding' => 'gzip');
		if ($r->is_success) {
			my $data = $r->decoded_content;
			my $body = MIME::Body::Scalar->new($data);
			$mime->bodyhandle($body);
		} else {
			warning('MIME part download failed url=%s status=%s', $url, $r->status_line);
		}
	} elsif (my $file = $json->{file}) {
		my $body = MIME::Body::File->new($file)
			or error("failed to open body file=%s: %s", $p, error_string);
		$mime->bodyhandle($body);
	}

	my @multi_types = qw(mixed alternative related);
	if (grep { $json->{$_} } @multi_types) {
		for my $t (@multi_types) {
			if (my $p = $json->{$t}) {
				if (ref $p ne 'ARRAY') {
					error('invalid subpart=%s specification: %q', $t, $p);
				}
				$mime->make_multipart($t);
				for my $j (@{$p}) {
					my $part = $s->build_mime_part($j);
					$mime->add_part($part);
				}
				last;
			}
		}
	} elsif (defined $json->{text} || defined $json->{html}) {
		my @tc = defined $json->{text} && looks_like_utf8($json->{text})
			? (charset => 'utf-8') : ();
		my @hc = defined $json->{html} && looks_like_utf8($json->{html})
			? (charset => 'utf-8') : ();
		if ($json->{text} && $json->{html}) {
			$mime->make_multipart('alternative');
			my $text = $s->build_mime_part({
				type => [ 'text/plain', @tc ],
				body => $json->{text}
			});
			my $html = $s->build_mime_part({
				type => [ 'text/html', @hc ],
				body => $json->{html}
			});
			$mime->parts([$text, $html]);
		} elsif ($json->{text}) {
			$s->build_mime_part({
				type => [ 'text/plain', @tc ],
				body => $json->{text}
			}, $mime);
		} else {
			$s->build_mime_part({
				type => [ 'text/html', @hc ],
				body => $json->{html}
			}, $mime);
		}
	}

	if (my $p = $json->{encoding}) {
		if ($p eq 'suggest' || $p eq 'guess') {
			$mime->head->set('Content-Transfer-Encoding', $mime->suggest_encoding);
		} else {
			$mime->head->set('Content-Transfer-Encoding', $p);
		}
	}

	return $mime;
}

sub build_text_html_part($)
{
	my $json = shift;

	my ($mime, $text, $html);

	if ($json->{text}) {
		$text = MIME::Entity->build(
			Type => 'text/plain',
			Data => $json->{text});
		cleanup_entity($text);
	}

	if ($json->{html}) {
		$html = MIME::Entity->build(
			Type => 'text/html',
			Data => $json->{html});
		cleanup_entity($html);
	}

	if ($text && $html) {
		$mime = MIME::Entity->build(Type => 'multipart/alternative');
		$mime->parts([$text, $html]);
		cleanup_entity($mime);
	} elsif ($html) {
		$mime = $html;
	} else {
		$mime = $text;
	}

	return $mime;
}

sub filter_headers($$$)
{
	my ($mime, $strip, $keep) = @_;

	$strip = [split /\s+/, $strip || '']
		unless ref $strip eq 'ARRAY';
	$keep = [split /\s+/, $keep || '']
		unless ref $keep eq 'ARRAY';;

	my @strip = map { compile_glob($_, 'i') } @{$strip};
	my @keep = map { compile_glob($_, 'i') } @{$keep};

	my $h = $mime->head;

	header: for my $name ($h->tags) {
		for my $s (@strip) {
			if ($name =~ $s) {
				if (grep { $name =~ $_ } @keep) {
					next header;
				}
				$h->delete($name);
				next header;
			}
		}
	}
}

sub get_message
{
	my ($s, $spec) = @_;

	my $data = "";
	my $mime;

	if (defined $spec->{data}) {
		$data = $spec->{data};
	} elsif (my $id = $spec->{mkstore_id}) {
		# XXX: move to options
		my $url = sprintf("http://mkstore.wa-research.ch/%s/%s/data",
			substr($id, 0, 2), substr($id, 2));
		my $r = $s->{ua}->get($url, 'Accept-Encoding' => 'gzip');

		if ($r->is_success) {
			$data = $r->decoded_content;
		} else {
			warning('mkstore download failed url=%s status=%s', $url, $r->status_line);
		}

	} elsif (defined $spec->{s3_ref}) {
		# XXX: also implement file for direct store access
	} elsif (my $t = $spec->{template}) {
		$data = render_template($t->{template}, $t);
	}

	if (length $data) {
		$mime = $s->{parser}->parse_data($data);
	} elsif ($spec->{text} || $spec->{html}) {
		# XXX: this shouldn't be trapped, do it in toplevel instead
		return build_text_html_part({ text => $spec->{text}, html => $spec->{html}});
	} elsif ($spec->{part}) {
		return $s->build_mime_part($spec->{part});
	}


	if ($mime)
	{
		if ($spec->{strip_headers} || $spec->{keep_headers}) {
			filter_headers($mime, $spec->{strip_headers}, $spec->{keep_headers});
		}

		return $mime;
	}

	return MIME::Entity->new();
}

sub do_hdr
{
	my ($s, $mime, $name, $value, $method) = @_;

	$name =~ s/((?:^|[-_]).)/uc($1)/ego;
	$method ||= 'replace';

	if (ref $value eq 'HASH') {
		# mime attributes spec
		if ($value->{mime_attr}) {
			while (my ($a, $v) = each %{$value}) {
				if ($a eq 'mime_attr') {
					$mime->head->mime_attr($name, $v);
				} else {
					$mime->head->mime_attr("$name.$a", $v);
				}
			}
		# mimewords encode
		} elsif (my $v = $value->{mimewords}) {
			$mime->head->$method($name, encode_mimewords($v,
					charset => $value->{charset} || 'utf-8'));
		# email spec
		} elsif (my $addr = $value->{email}) {
			if (my $n = $value->{name} || $value->{display_name}) {
				$n = "\"$n\"" if $n =~ /\s/;
				$n = encode_mimewords($n,
					Charset => $value->{charset} || 'utf-8');
				$addr = "$n <$addr>";
			}

			$mime->head->$method($name, $addr);
		}
	} elsif (ref $value eq 'ARRAY') {
		my @val = @$value;
		if (my $v = pop @val) {
			$s->do_hdr($mime, $name, $v);
		}
		for my $v (@val) {
			$s->do_hdr($mime, $name, $v, 'add');
		}
	} else {
		$mime->head->$method($name, $value);
	}
}

sub do_headers
{
	my ($s, $mime, $hdrs) = @_;

	if (ref $hdrs eq 'ARRAY') {
		for (my $i = 0; $i < @{$hdrs}; $i += 2) {
			$s->do_hdr($mime, $hdrs->[$i], $hdrs->[$i+1]);
		}
	}

	elsif (ref $hdrs eq 'HASH') {
		while (my ($n, $v) = each %{$hdrs}) {
			$s->do_hdr($mime, $n, $v);
		}
	}

	else {
		warning('invalid headers spec: %s', $hdrs);
	}
}

sub merge_headers($$)
{
	my ($h, $r) = @_;

	if (ref $h eq 'ARRAY') {
		if (ref $r eq 'ARRAY') {
			push @{$h}, @{$r};
		} elsif (ref $r eq 'HASH') {
			map { push @$h, $_ => $r->[$_] } keys %$r;
		}
	} elsif (ref $h eq 'HASH') {
		if (ref $r eq 'ARRAY') {
			for (my $i = 0; $i < @{$r}; $i += 2) {
				$h->{$r->[$i]} = $r->[$i+1];
			}
		} elsif (ref $r eq 'HASH') {
			map { $r->{$_} = $r->{$_} } keys %$r;
		}
	}
}

sub do_recipient
{
	my ($s, $mime, $json, $r) = @_;

	unless (ref $r) {
		$r = { recipient_email => $r };
	}

	while (my ($k, $v) = each %$r) {
		if ($k eq 'headers' && ref $json->{headers}) {
			merge_headers($json->{headers}, $v);
		} else {
			$json->{$k} = $v;
		}
	}

	$s->do_toplevel($mime, $json);
}

sub do_templates
{
	my ($s, $mime, $templates) = @_;

	unless (ref $templates eq 'ARRAY') {
		warning('invalid templates spec: %s', $templates);
		return;
	}

	for (my $i = 0; $i < @$templates; $i += 2)
	{
		my $op = $templates->[$i];
		my $tmpl = $templates->[$i+1];

		my $hdr = {};

		if (ref $tmpl eq 'HASH') {
			while (my ($n, $v) = each %{$tmpl}) {
				if ($n eq "template") {
					$hdr->{mime_attr} = $v;
				} else {
					$hdr->{$n} = uri_escape($v);
				}
			}
		} else {
			$hdr = $tmpl;
		}

		$s->do_hdr($mime, "x-$op-template", $hdr);
	}
}

sub do_attachments
{
	my ($s, $mime, $attachments) = @_;

	unless (ref $attachments eq 'ARRAY') {
		warning('invalid attachments spec: %s', $attachments);
		return;
	}

	my @h;
	for my $a (@$attachments) {
		if (ref $a eq 'HASH') {
			$a = join('|', $a->{id}, $a->{filename}, $a->{size}, $a->{url});
		}
		push @h, $a;
	}
	$s->do_hdr($mime, 'x-attachment', \@h);
}

sub do_toplevel
{
	my ($s, $mime, $json) = @_;

	# common headers
	if (defined(my $p = $json->{subject})) {
		$s->do_hdr($mime, subject => {mimewords => $p, charset => 'utf-8'});
	}

	if (defined(my $p = $json->{date})) {
		$s->do_hdr($mime, "date", Email::Date::Format::email_date($p));
	}

	# custom headers
	if (my $p = $json->{headers}) {
		$s->do_headers($mime, $p);
	}

	# list support
	if ($json->{domain} && $json->{list_id} && $json->{message_id} && $json->{recipient_email}) {
		$json->{sender} = make_verp_address($json->{domain}, $json->{list_id}, $json->{message_id}, $json->{recipient_email}, $json->{recipient_id});
		$json->{recipient} = $json->{recipient_email};
	}

	# new verp support
	if ($json->{batch_id} && $json->{recipient_id} && $json->{domain}) {
		$json->{sender} = sprintf('%s%s@%s',
			$json->{batch_id}, $json->{recipient_id}, $json->{domain});
	}

	# templates
	if (my $p = $json->{templates}) {
		$s->do_templates($mime, $p);
	}

	# attachments
	if ($json->{strip_attachments}) {
		$s->do_hdr($mime, 'X-Strip-Attachments', 'true');
	}

	if (my $p = $json->{attachments}) {
		$s->do_attachments($mime, $p);
	}
}

sub do_part
{
	my ($s, $mime, $json) = @_;
}

sub execute
{
	my ($s, $id, $input) = @_;

	my $string = slurp($input);
	my $json = eval { $s->{json}->decode($string) }
		or error('failed to parse input: %s', error_string);

	my $mime = $s->get_message($json->{message});

	if ($json->{recipients}) {
		ref $json->{recipients} eq 'ARRAY'
			or error('invalid recipients property: %s', $json->{recipients});
		for (my $i = 0; $i < @{$json->{recipients}}; $i ++) {
			my $rcp = $json->{recipients}->[$i];
			# recipients get a separate copy of the MIME, not sure if necessary
			my $mime_dup = $mime->dup;
			my $json_dup = dclone($json);
			$s->do_recipient($mime_dup, $json_dup, $rcp);
			$s->output($id, $mime_dup, $i, $json->{sender}, $json->{recipient});
		}
	} else {
		$s->do_toplevel($mime, $json);
		$s->output($id, $mime, undef, $json->{sender}, $json->{recipient});
	}
}

# write mime object, used to set up x-sender, x-recipient headers
sub write_mime($$$$)
{
	my ($mime, $handle, $sender, $recipient) = @_;

	if ($sender && $recipient) {
		printf $handle "x-sender: %s\nx-recipient: %s\n", $sender, $recipient
			or error('write error', error_string);
	} else {
		warning("missing sender/recipient headers");
	}

	$mime->print($handle)
		or error('write error', error_string);
}

sub output
{
	my ($s, $id, $mime, $seq, $sender, $recipient) = @_;

	if (!defined $seq) {
		write_mime($mime, \*STDOUT, $sender, $recipient);
	} else {
		my $filename = sprintf('output/%s_%d', $id, $seq);
		open my $handle, '>', $filename
			or error('failed to open seq file=%s: %s', $filename, $!);
		write_mime($mime, $handle, $sender, $recipient);
		close $handle
			or error('failed to close seq file=%s: %s', $filename, $!);
	}

	return 0;
}

1;
