#!/usr/bin/perl

# Copyright (C) 2011 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Pickup::Service::Null;

use strict;
use warnings;

use base qw(Pickup::Service::Common);

sub execute
{
	return 0;
}

1;
