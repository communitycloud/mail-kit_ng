#!/usr/bin/perl

# Copyright (C) 2011 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Pickup::Service::Common;

use strict;
use warnings;

sub new
{
	my ($class, $pickup, $name) = @_;
	return bless { pickup => $pickup, name => $name }, $class;
}

sub bind
{
}

sub pickup
{
	$_[0]->{pickup};
}

sub flags
{
	{}
}

sub options
{
}

sub name
{
	$_[0]->{alias} || $_[0]->{name};
}

sub configure
{
	my ($s, $key, $value) = @_;
	return 0;
}

sub create
{
	return 1;
}

sub start
{
}

sub stop
{
}

sub execute
{
}


1;
