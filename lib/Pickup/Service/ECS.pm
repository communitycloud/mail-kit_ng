#!/usr/bin/perl

package Pickup::Service::ECS;

use strict;
use warnings;

use App::Message;
use Email::Simple;
use Util::Email qw(make_verp_address read_mail_header);
use Util::String qw(compile_glob looks_like_utf8);
use MIME::Words qw(encode_mimewords);

use base qw(Pickup::Service::Common);

sub options
{
	mode => "single",
	splitter => "sendmail",
}

sub configure
{
	my ($s, $key, $value) = @_;
	if ($key eq "mode") {
		unless ($value eq "single" || $value eq "multi" || $value eq "split") {
			error("invalid mode %q", $value);
		}
	}
}

sub validate_ecs_header($)
{
	my $ecs = shift;

	for my $name (qw(List-ID Message-ID Recipient-Email Recipient-ID Domain)) {
		my $value = $ecs->header('ECS-' . $name);
		unless (defined $value && length $value) {
			error("missing %s ECS header", $name);
		}
	}
}

sub is_ecs_header_name($)
{
	$_[0] =~ /^ECS(?:-|$)/
}

sub format_mail_header($$)
{
	my ($msg_header, $ecs_header) = @_;

	my $h = Email::Simple->new($msg_header->as_string);

	for my $name ($ecs_header->header_names) {
		next if is_ecs_header_name($name);
		my @value = map { looks_like_utf8($_) ? encode_mimewords($_, Charset => 'utf-8') : $_ }
			$ecs_header->header($name);
		$h->header_set($name, @value);
	}

	my ($domain, $list_id, $msg_id, $address, $user_id) =
		map { $ecs_header->header('ECS-' . $_) }
	qw(Domain List-ID Message-ID Recipient-Email Recipient-ID);

	my $verp = make_verp_address($domain, $list_id, $msg_id, $address, $user_id);

	my @strip_headers = map { compile_glob($_) }
		$ecs_header->header('ECS-Strip-Header');
	my @keep_headers = map { compile_glob($_) }
		$ecs_header->header('ECS-Keep-Header');
	push @keep_headers, grep { !is_ecs_header_name($_) }
		$ecs_header->header_names;

	header: for my $name ($h->header_names) {
		for my $strip (@strip_headers) {
			if ($name =~ $strip) {
				for my $keep (@keep_headers) {
					if ($name =~ $keep) {
						next header;
					}
				}
				$h->header_set($name);
				next header;
			}
		}
	}

	$h->header_set('x-sender', $verp);
	$h->header_set('x-receiver', $address);

	return $h;
}

sub split_error
{
	my ($input, $seq, $error, @args) = @_;

	seek($input, 0, 2)
		or error('failed to seek handle: %s', $!);
	printf($input "\nECS: cursor\nCursor: %d\n", $seq)
		or error('failed to write cursor data: %s', $!);
	error($error, @args);
}

sub execute
{
	my ($s, $id, $input, $event) = @_;

	my ($msg_header, $global_header, @ecs_headers, $cursor);

	my $mode = $s->pickup->option('mode');

	while (1) {
		my $h = read_mail_header();
		my $block = $h->header('ECS');
		unless ($block) {
			$msg_header = $h;
			last;
		} elsif ($block eq 'recipient') {
			push @ecs_headers, $h;
		} elsif ($block eq 'global') {
			error("a second global ECS header encountered") if $global_header;
			$global_header = $h;
		} elsif ($block eq 'cursor') {
			$cursor = $h->header('Cursor');
		} else {
			error("invalid ECS block name: %q at offset %d", $block);
		}
	}

	if (@ecs_headers > 1 && $mode eq 'single') {
		error("found multiple ECS headers while in single recipient mode");
	}

	if ($global_header) {
		for my $name ($global_header->header_names) {
			for my $h (@ecs_headers) {
				unless (defined $h->header($name)) {
					$h->header_set($name, ($global_header->header($name)));
				}
			}
		}
	}

	for my $h (@ecs_headers) {
		validate_ecs_header($h);
	}

	my $body_pos = sysseek(STDIN, 0, 1);
	my $body_length = ((stat STDIN)[7]) - $body_pos;

	my $seq = 1;

	for my $ecs_header (@ecs_headers) {
		if ($cursor && $cursor > $seq) {
			next;
		}

		my $h = format_mail_header($msg_header, $ecs_header);

		my $sender = $h->header('x-sender');
		my $receiver = $h->header('x-receiver');

		$h->header_set('x-sender');
		$h->header_set('x-receiver');

		my $header = sprintf "x-sender: %s\nx-receiver: %s\n%s",
			$sender, $receiver, $h->as_string();

		my $out = \*STDOUT;

		my $multi_name = $s->pickup->make_multi_name($id, $seq, scalar @ecs_headers);

		if ($mode eq 'multi') {
			printf $out "%d %s/%s\n", $body_length + length($header),
				$s->pickup->option('splitter'), $multi_name;
		} elsif ($mode eq 'split') {
			my $filename = sprintf('output/%s', $multi_name);
			open my $handle, '>', $filename
				or split_error($input, $seq, 'failed to open split file=%s: %s', $filename, $!);
			$out = $handle;
		}

		print $out $header;
		sysseek STDIN, $body_pos, 0;

		while (sysread STDIN, my $buff, 4096) {
			print $out $buff;
		}

		$seq ++;

		if ($mode eq 'multi') {
			print "\n";
		} elsif ($mode eq 'split') {
			close $out
				or split_error($input, $seq, 'failed to close split handle: %s', $!);
		}
	}

	if ($event)
	{
		unless ($mode eq 'single') {
			$event->push(size => scalar @ecs_headers)
		}

		my $h = $global_header || $ecs_headers[0];

		for my $n (qw(Subject Sender From To CC
			ECS-Domain ECS-List-ID ECS-Message-ID ECS-List-Short-Name))
		{
			my $v = $h->header($n)
				or next;
			my $k = lc $n;
			$k =~ tr/-/_/;

			$event->push($k => $v);
		}

		if (defined (my $v = $h->header('X-ECS-EventName')))
		{
			my ($mkstore_id) = split ('/', $v);
			$event->push(mkstore_id => $mkstore_id);
		}
	}

	return 0;
}

1;
