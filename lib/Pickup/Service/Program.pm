#!/usr/bin/perl

# Copyright (C) 2011 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Pickup::Service::Program;

use base qw(Pickup::Service::Common);

use strict;
use warnings;

sub options
{
	program => "cat",
}

sub execute
{
	system($_[0]->pickup->option('program'));
}

1;
