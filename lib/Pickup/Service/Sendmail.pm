#!/usr/bin/perl

package Pickup::Service::Sendmail;

use strict;
use warnings;

use base qw(Pickup::Service::Common);

use App::Message;
use Util::Email qw(seek_eml_header);
use MailKit::Sendmail qw(sendmail_uri);

sub execute
{
	my ($s, $id, $input, $event) = @_;

	my ($sender, $recipients, $uri) = seek_eml_header(\*STDIN);

	unless ($sender) {
		$event->error('missing x-sender')
			if $event;
		error("missing x-sender");
	}

	unless (@{$recipients}) {
		$event->error('missing x-recipient')
			if $event;
		error("missing x-recipient");
	}

	info("send mail sender=%s recipients=%{pa} uri=%s", $sender, $recipients, $uri);
	sendmail_uri(\*STDIN, $uri, sender => $sender, recipients => $recipients);

	if ($event) {
		$event->push(sender => $sender);
		$event->push(recipients => $recipients);
	}

	return 0;
}

1;
