#!/usr/bin/perl

# Copyright (C) 2011 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package App::Hook;

use strict;
use warnings;

sub new
{
	my $class = shift;
	bless {}, $class;
}

sub add
{
	my ($h, $hook) = @_;
	$h->{$hook} = $hook;
	$h;
}

sub remove
{
	my ($h, $hook) = @_;
	delete $h->{$hook};
	$h;
}

sub trigger
{
	my ($h, @arg) = @_;
	for my $hook (values %{$h}) {
		$hook->(@arg)
			or return 0;
	}
	return 1;
}

sub empty
{
	keys %{$_[0]} == 0;
}

1;
