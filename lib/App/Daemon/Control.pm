#!/usr/bin/perl

package App::Daemon::Control;

use strict;
use warnings;

use Fcntl qw(:DEFAULT :flock);
use Util::Export qw(daemon_control control_lock control_write control_append);
use App::Message qw(daemon:);
use Util::String qw(escape_chars unescape_chars);
use Util::Time qw(make_duration);

my $lock;

sub decode_line($)
{
	my $line = shift;
	return () unless defined $line;
	chomp $line;
	split("\0", unescape_chars($line, 'n'));
}

sub encode_line(@)
{
	return '' unless @_;
	join("\0", map escape_chars($_, 'n'), @_);
}

sub daemon_control($$;@)
{
	my ($action, $control_file, @args) = @_;

	unless ($control_file) {
		error("daemon action `$action' requires --control=FILE specified");
	}

	my $running = control_locked($control_file);

	if ($action eq "running") {
		return $running;
	}

	if ($action eq "create") {
		if ($running) {
			error("create: control file locked");
		}

		my ($name, @command) = @args;
		control_lock($control_file);
		control_write(0, $name, @command);
		control_append();
		return;
	}

	my ($pid, $name, $command, $states) = control_read($control_file);

	if ($action eq "stop") {
		if ($running) {
			kill TERM => $pid
				or error("stop: failed to kill process %s: %s", $pid, $!);
			control_wait($control_file)
				or error("stop: process %s had not released control file", $!);
		}
		return;
	}

	if ($action eq "reload") {
		if ($running) {
			kill HUP => $pid
				or error("reload: failed to kill process %s: %s", $pid, $!);
		} else {
			error("reload: process not running");
		}
		return;
	}

	if ($action eq "status") {
		my ($state) = decode_line($states);
		unless (defined $state) {
			$state = '<undefined>';
		} else {
			(my $time, $state) = split ' ', $state, 2;
			my $age = time - $time;
			$state .= sprintf(" (%s)", $age > 2 ? make_duration($age) . " ago" : "now");
		}
		if ($running) {
			print "$name is running pid=$pid state=$state.\n";
		} else {
			print "$name not running state=$state.\n";
		}
	}

	if ($action eq "start") {
		if ($running) {
			error("start: process already running pid=%s", $pid);
		}
		exec decode_line($command)
			or error("start: execution failed, command was %q", $command);
	}
}

sub control_locked($)
{
	my $control_file = shift;
	open my $lock, $control_file
		or return 0;
	my $locked = !flock($lock, LOCK_EX|LOCK_NB);
	close $lock;
	return $locked;
}

sub control_wait($;$)
{
	my ($control_file, $timeout) = @_;
	open my $lock, $control_file
		or return 0;
	local $SIG{ALRM} = sub { };
	alarm($timeout || 10);
	my $locked = flock($lock, LOCK_EX);
	alarm(0);
	if ($locked) {
		flock($lock, LOCK_UN);
		return 1;
	} else {
		return 0;
	}
}

sub control_read($)
{
	my $control_file = shift;
	open my $lock, $control_file
		or error("couldn't open control file %q: %s", $control_file, $!);

	my ($pid, $name, $command, $states) = readline($lock)
		or return;

	chomp($pid, $name, $command, $states);
	return ($pid, $name, $command, $states);
}

sub control_write($$@)
{
	return unless $lock;
	my ($pid, $name, @command) = @_;
	seek $lock, 0, 0;
	print $lock
		$pid . "\n",
		$name . "\n",
		encode_line(@command) . "\n";
}

sub control_append(@)
{
	return unless $lock;
	my $pos = tell $lock;
	print $lock encode_line(@_) . "\n";
	seek $lock, $pos, 0;
}

sub control_lock($)
{
	require App::Proc;
	my $control_file = shift;
	sysopen $lock, $control_file, O_CREAT|O_RDWR
		or error("couldn't open control file %q: %s", $control_file, $!);
	my $pid = readline($lock) || '??';
	flock($lock, LOCK_EX|LOCK_NB)
		or error("control file locked by process %s", $pid);
	App::Proc::post_fork_hook(\&control_release);
}

sub control_release()
{
	if ($lock) {
		close($lock);
		undef $lock;
	}
}

1;
