#!/usr/bin/perl

package App::Yield;

use strict;
use warnings;

use Util::Export qw(app_yield yield_hook);

our @hooks;

sub app_yield()
{
	for my $sub (@hooks) {
		$sub->();
	}
}

sub yield_hook($)
{
	push @hooks, $_[0];
}

1;
