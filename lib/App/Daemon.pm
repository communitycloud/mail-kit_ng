#!/usr/bin/perl

# Copyright (C) 2009-2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package App::Daemon;

use strict;
use warnings;

use POSIX;
use Fcntl qw(:DEFAULT :flock);
use App::Message "daemon:", qw($program_name report_children usage_error :DEFAULT);
use App::Proc qw(post_fork_hook setup_signal_handler set_effective_id);
use Util::File qw(path_stat_changed);
use FindBin;

sub new
{
	my ($class, %config) = @_;
	return bless {
		options  => { },
		name => $program_name,
		resume_state => scalar time,
		resume_command => undef,
		handlers => {
			reload => delete $config{on_reload},
			terminate => delete $config{on_terminate},
		},
		signals => { },
		active => 0,
		argv => [@ARGV],
		%config,
	}, $class;
}

sub help
{
	my ($class, $long) = @_;

	unless (defined wantarray) {
		print $class->help($long);
		return;
	}

	if ($long) {
		"Daemon options:\n",
		"      --daemon              go into the background (with --pid-file)\n",
		"      --start               start a fresh daemon process\n",
		"      --reload              reload process and configuration\n",
		"      --stop                terminate the process\n",
		"      --status              check the process if it's still running\n",
		"      --config=FILE         read configuration options from FILE\n",
		"      --pid-file=FILE       pid file\n",
		"      --log-file=FILE       log file\n",
		"      --log-level=LEVEL     log level: ", join(', ', @App::Message::levels) , "\n",
		"      --log-watch=INTERVAL  watch the log inode for rotation every INTERVAL seconds\n",
		"      --foreground          emulate daemon mode in the foreground\n",
		"      --user=UID            set the effective user ID\n",
		"      --group=GID           set the effective group ID\n",
		"      --daemon-name=ALIAS   report a different name than $program_name\n",
	} else {
		"This program supports the daemon interface.\n",
		"See $program_name --daemon-help for full options\n",
	}
}

sub options
{
	my $daemon = shift;
	"daemonize"   => \$daemon->{options}->{daemonize},
	"daemon-help" => \$daemon->{options}->{daemon_help},
	"daemon-name=s" => \$daemon->{name},
	"start"       => \$daemon->{options}->{start},
	"reload"      => \$daemon->{options}->{reload},
	"stop"        => \$daemon->{options}->{stop},
	"status"      => \$daemon->{options}->{status},
	"resume=s"    => \$daemon->{options}->{resume},
	"pid-file=s"  => \$daemon->{options}->{pid_file},
	"config|c=s"  => \$daemon->{options}->{config},
	"log-file=s"  => \$daemon->{options}->{log_file},
	"log-level=s" => \$daemon->{options}->{log_level},
	"log-watch:i" => \$daemon->{options}->{log_watch},
	"foreground"  => \$daemon->{options}->{foreground},
	"user:s"      => \$daemon->{options}->{user},
	"group:s"     => \$daemon->{options}->{group},
}

sub handle
{
	my ($daemon, $action) = @_;
	if (my $handler = $daemon->{handlers}->{$action}) {
		$handler->($daemon);
	}
}

sub ctl
{
	my ($daemon, $action) = @_;

	my $pid = $daemon->pid_file_locked() &&
		$daemon->pid_file_get();
	
	unless ($pid) {
		if ($action eq "reload") {
			error("reload: process not running");
		} elsif ($action eq "status") {
			print "$daemon->{name} not running.\n";
		}
		return;
	}

	if ($action eq "reload") {
		kill HUP => $pid
			or error("reload: failed to reload process $pid: $!");
	} elsif ($action eq "stop") {
		kill TERM => $pid
			or error("stop: failed to stop process $pid: $!");
		$daemon->pid_file_wait()
			or error("stop: process $pid did not release pid file");
	} elsif ($action eq "status") {
		print "$daemon->{name} is running pid=$pid.\n";
	}
}

sub daemon_die
{
	return if $App::Message::own_message;
	return unless defined $^S && $^S == 0;
	chomp(my $err = shift);
	warning("(Internal error) $err");
}

sub daemon_warn
{
	return if $App::Message::own_message;
	chomp(my $warn = shift);
	warning("(Internal warning) $warn");
}

sub daemon_info
{
	my $daemon = shift;

	my $info = ["pid", $$];

	if (my $user = $daemon->{options}->{user}) {
		push @{$info}, "user", $user;
	}

	if (my $group = $daemon->{options}->{group}) {
		push @{$info}, "group", $group;
	}

	info("gone in background %{ph:t}", $info);
}

sub daemonize
{
	my $daemon = shift;

	$daemon->drop_privileges();
	$daemon->pid_file_acquire();

	$daemon->background();
	$daemon->app_message_setup();

	$daemon->pid_file_update();
	$daemon->setup_signals();
	$daemon->daemon_info();

	$daemon->{active}++;
}

sub background
{
	my $daemon = shift;
	unless ($daemon->{options}->{foreground}) {
		defined(my $pid = fork)
			or error("couldn't fork: $!");
		POSIX::_exit(0) if $pid;
		POSIX::setsid();
		open STDIN, '</dev/null' if -t STDIN;
		open STDOUT, '>/dev/null';
		open STDERR, '>/dev/null';
	}
}

sub app_message_setup
{
	my $daemon = shift;

	if ($daemon->{options}->{log_file}) {
		message_setup("log",
			file => $daemon->{options}->{log_file},
			level => $daemon->{options}->{log_level});
	}

	if ($daemon->{options}->{log_file} &&
		defined $daemon->{options}->{log_watch})
	{
		$App::Message::output_hook->add(sub {
			if (path_stat_changed(1,
					$daemon->{options}->{log_file},
					$daemon->{options}->{log_watch} || 60))
			{
				message_setup("log",
					file => $daemon->{options}->{log_file},
					level => $daemon->{options}->{log_level});
			}

			1;
		});
	}

	report_children();
}

sub daemon_reload
{
	my ($daemon, $signal) = @_;

	info("received $signal, reloading");

	$daemon->handle('reload');

	exec($daemon->resume_command)
		or error("exec failed: $!");
}

sub resume_command
{
	my $daemon = shift;

	if ($daemon->{resume_command}) {
		return ref $daemon->{resume_command}
			? @{$daemon->{resume_command}}
			: $daemon->{resume_command};
	}

	# remove the --resume option
	my @argv = @{$daemon->{argv}};

	for (my $i = 0; $i < @argv; $i++) {
		if ($argv[$i] eq '--resume') {
			splice @argv, $i, 2;
			last;
		}
	}

	my @me = ("$FindBin::Bin/$FindBin::Script", @argv,
		'--resume', $daemon->{resume_state});
	return @me;

}

sub daemon_terminate
{
	my ($daemon, $signal) = @_;
	info("received $signal, exit process");
	$daemon->handle('terminate');
	exit();
}

sub daemon_resume
{
	my ($daemon, $resume_state) = @_;
	$daemon->app_message_setup();
	info("resuming, state=$daemon->{resume_state}, previous=$resume_state");
	$daemon->setup_signals();
	$daemon->pid_file_acquire();
	$daemon->{active}++;
}

sub drop_privileges
{
	my $daemon = shift;
	set_effective_id(
		$daemon->{options}->{user},
		$daemon->{options}->{group});
}

sub setup_signals
{
	my $daemon = shift;

	$SIG{__DIE__} = \&daemon_die;
	$SIG{__WARN__} = \&daemon_warn;

	my $handler = sub {
		my $sig = shift;
		if ($sig eq "HUP") {
			$daemon->{handlers}->{sig_reload}->($sig)
				if $daemon->{handlers}->{sig_reload};
		} else {
			$daemon->{handlers}->{sig_terminate}->($sig)
				if $daemon->{handlers}->{sig_terminate};
		}
		$daemon->{signals}->{$sig} ++
	};

	setup_signal_handler($handler, "TERM", "INT", "HUP");

	my $restore = sub { setup_signal_handler("DEFAULT", "HUP", "TERM", "INT") };
	post_fork_hook($restore);
}

sub handle_signals
{
	for my $sig (keys %{$_[0]->{signals}}) {
		if ($sig eq 'HUP') {
			$_[0]->daemon_reload($sig);
		} else {
			$_[0]->daemon_terminate($sig);
		}
	}
}

sub parse_options
{
	my $daemon = shift;
	require Getopt::Long;
	Getopt::Long::Configure("pass_through");
	Getopt::Long::GetOptions($daemon->options);
}

sub read_config($$)
{
	my ($config_file, $config) = @_;
	open my $handle, $config_file
		or return undef;

	while (my $line = <$handle>)
	{
		chomp($line);
		$line =~ s/(^\s+|\s+$|#.*)//;
		next unless length $line;
		my ($key, $value) = split ('\s+', $line, 2);

		my $r = $config->{$key}
			or error("invalid configuration key %q", $key);
		$value = 1 unless defined $value;

		if (ref ($r) eq 'ARRAY') {
			push @{$r}, $value;
		} else {
			${$r} = $value;
		}
	}

	return 1;
}

sub init
{
	my $daemon = shift;

	if (my $file = $daemon->{options}->{config}) {
		my $config = {
			pid_file      => \$daemon->{options}->{pid_file},
			daemonize     => \$daemon->{options}->{daemonize},
			log_file      => \$daemon->{options}->{log_file},
			log_level     => \$daemon->{options}->{log_level},
			($daemon->{config} ? %{$daemon->{config}} : ()),
		};

		read_config($file, $config)
			or error("failed to read config file `$file': $!");
	}

	for my $action (qw(start daemonize)) {
		if ($daemon->{options}->{$action}) {
			usage_error("$action requires --pid-file") unless $daemon->{options}->{pid_file};
			last;
		}
	}
}

sub apply
{
	my $daemon = shift;
	$daemon->apply_options();
	$daemon->apply_action();
}

sub apply_options
{
	my $daemon = shift;

	if ($daemon->{options}->{daemon_help}) {
		$daemon->help(1);
		exit();
	}

	for my $action (qw(reload stop status)) {
		if ($daemon->{options}->{$action}) {
			usage_error("$action requires pid file specified")
				unless $daemon->{options}->{pid_file};
			$daemon->ctl($action);
			exit;
		}
	}

	if ($daemon->{options}->{foreground}) {
		$daemon->{options}->{log_file} = '-';
		$daemon->{options}->{daemonize} ++;
	}
}

sub apply_action
{
	my $daemon = shift;

	if (my $state = $daemon->{options}->{resume}) {
		$daemon->daemon_resume($state);
		$daemon->handle('resume');
	} elsif ($daemon->{options}->{start}) {
		$daemon->daemonize();
		$daemon->handle('start');
	} elsif ($daemon->{options}->{daemonize}) {
		$daemon->daemonize();
	} else {
		$daemon->setup_signals();
	}
}

sub pid_file_lock
{
	my $daemon = shift;
	return unless $daemon->{options}->{pid_file};
	sysopen $daemon->{pid_handle}, $daemon->{options}->{pid_file}, O_CREAT|O_RDWR
		or error("couldn't open pid file `$daemon->{options}->{pid_file}': $!");
	my $pid = readline($daemon->{pid_handle}) || '??';
	flock($daemon->{pid_handle}, LOCK_EX|LOCK_NB)
		or error("pid file locked by $pid");
	my $restore = sub { close $daemon->{pid_handle} };
	post_fork_hook($restore);
}

sub pid_file_wait
{
	my ($daemon, $timeout) = @_;
	return unless $daemon->{options}->{pid_file};
	open my $lock, $daemon->{options}->{pid_file}
		or return 0;
	local $SIG{ALRM} = sub { };
	alarm($timeout || 2);
	my $locked = flock($lock, LOCK_EX);
	alarm(0);
	if ($locked) {
		flock($lock, LOCK_UN);
		return 1;
	} else {
		return 0;
	}
}

sub pid_file_acquire
{
	my $daemon = shift;
	$daemon->pid_file_lock();
	$daemon->pid_file_update();
}

sub pid_file_update
{
	my $daemon = shift;
	return unless $daemon->{pid_handle};
	truncate $daemon->{pid_handle}, 0;
	seek $daemon->{pid_handle}, 0, 0;
	syswrite $daemon->{pid_handle}, $$;
}

sub pid_file_release
{
	my $daemon = shift;
	flock($daemon->{pid_handle}, LOCK_UN);
	close($daemon->{pid_handle});
}

sub pid_file_get
{
	my $daemon = shift;
	usage_error("no pid file specified") unless $daemon->{options}->{pid_file};
	open my $lock, $daemon->{options}->{pid_file}
		or error("couldn't open pid file `$daemon->{options}->{pid_file}': $!");
	flock($lock, LOCK_EX|LOCK_NB) == 0
		or error("no process is holding `$daemon->{options}->{pid_file}'");
	return readline($lock);
}

sub pid_file_locked
{
	my $daemon = shift;
	usage_error("no pid file specified") unless $daemon->{options}->{pid_file};
	open my $lock, $daemon->{options}->{pid_file}
		or return 0;
	my $locked = !flock($lock, LOCK_EX|LOCK_NB);
	close $lock;
	return $locked;
}

1;
