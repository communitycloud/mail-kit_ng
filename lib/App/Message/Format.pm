#!/usr/bin/perl

# Copyright (C) 2009-2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package App::Message::Format;

use strict;
use warnings;

use Util::Strfy qw($strfy);
use Util::String qw(custom_sprintf escape_chars ordinate can_strfy);
use Util::Export qw(format_message update_custom_sprintf);

our %map;
update_custom_sprintf(
	s => \&format_string,
	q => \&format_quote,
	e => \&format_escape,
	T => \&format_trace,
	C => \&format_caller,
	o => \&format_ordinate,
	pp => \&format_strfy_depth,
	ps => \&format_strfy_scalar,
	ph => \&format_strfy_hash,
	pa => \&format_strfy_array,
	prg => \&message_program,
	lev => \&message_level, 
	msg => \&message_string,
	time => \&message_time,
);

my $sprintf_message;
sub format_message($;@)
{
	goto $sprintf_message;
}

sub update_custom_sprintf
{
	$sprintf_message = custom_sprintf(%map = (%map, @_));
}

sub non_string($)
{
	return '<undefined>' unless defined $_[0];
	return undef if can_strfy($_[0]);
	return sprintf '<%s>', ref $_[0] if ref $_[0];
	return undef;
}

sub format_string
{
	if (my $ns = non_string($_[2])) {
		return $ns;
	}
	return sprintf "%$_[0]s", $_[2];
}

sub format_escape
{
	if (my $ns = non_string($_[2])) {
		return $ns;
	}
	sprintf "%$_[0]s", escape_chars($_[2], "nrte");
}

sub format_quote
{
	if (my $ns = non_string($_[2])) {
		return $ns;
	}
	return sprintf "%$_[0]s", "`" . escape_chars($_[2], "nrte'\\") . "'";
}

sub format_strfy_scalar
{
	return $strfy->scalar($_[2]);
}

sub format_strfy_hash
{
	if (my ($c) = $_[0] =~ /^(\d+)/) {
		$c *= 2;
		$_[2] = [@{$_[3]}[$_[4]-1 .. $_[4]+$c-2]];
		$_[4] += $c - 1;
	}
	unless (defined $_[2]) {
		return "";
	}

	unless (ref $_[2] eq 'HASH' || ref $_[2] eq 'ARRAY') {
		warn("value is not a hash reference in custom sprintf %$_[1]: ",
			$strfy->scalar($_[2]));
		return "";
	}
	local $strfy->{undefined} = 0
		if $_[0] =~ "d";
	local $strfy->{hash_delim} = "=",
	local $strfy->{array_delim} = " ",
	local $strfy->{unwind} = 0,
		$strfy->{string_quote} = 0
		if $_[0] =~ "t";
	return $strfy->hash($_[2]);
}

sub format_strfy_array
{
	unless (defined $_[2]) {
		return "<undefined>";
	}
	if (my ($c) = $_[0] =~ /^(\d+)/) {
		$_[2] = [@{$_[3]}[$_[4]-1 .. $_[4]+$c-2]];
		$_[4] += $c - 1;
	}
	unless (ref $_[2] eq 'ARRAY') {
		warn("value is not an array reference in custom sprintf %$_[1]: ",
			$strfy->scalar($_[2]));
		return "";
	}
	local $strfy->{undefined} = 0
		if $_[0] =~ "d";
	local $strfy->{string_quote} = 0
		if $_[0] =~ "t";
	return $strfy->array($_[2]);
}

sub format_strfy_depth
{
	local $strfy->{unwind} = 1;
	return $strfy->scalar($_[2])
}

sub format_trace
{
	return $strfy->trace($_[0] || 0 + 2);
}

sub format_caller
{
	return $strfy->caller($_[0] || 0 + 2);
}

sub format_ordinate
{
	return ordinate($_[2]) || format_message("%s", $_[2]);
}

sub message_program
{
	no warnings "once";
	return $App::Message::alias || $App::Message::program_name || "<unknown program>";
}

sub message_level
{
	my $level = $_[3][1];

	for my $f (split "", $_[0]) {
		if ($f eq "u") {
			$level = uc $level;
		} elsif ($f eq "l") {
			$level = uc $level;
		} elsif ($f eq "i") {
			$level = substr $level, 0, 1;
		}
	}

	return $level;
}

sub message_string
{
	return defined $_[3][0] ? $_[3][0] : "<empty message>";
}

sub message_time
{
	if ($_[0] eq "log") {
		require POSIX;
		return POSIX::strftime("%b %d %T", localtime);
	} else {
		return scalar localtime;
	}
}

1;
