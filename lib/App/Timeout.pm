#!/usr/bin/perl

package App::Timeout;

use strict;
use warnings;

use Time::HiRes qw(time sleep);
use Util::Export qw(add_timeout cancel_timeout run_timeouts);

my @timeouts;

sub add_timeout($$)
{
	my ($secs, $sub) = @_;

    my $time = time + $secs;
    my $tout = [$time, $sub];

    if (!@timeouts || $time >= $timeouts[-1][0]) {
        push @timeouts, $tout;
    } else {
		for (my $i = 0; $i < @timeouts; $i++) {
			if ($timeouts[$i][0] > $time) {
				splice(@timeouts, $i, 0, $tout);
				return $tout;
			}
		}
	}

	return $tout;
}

sub cancel_timeout($)
{
	undef $_[0][1];
}

sub run_timeouts()
{
    my $now = time;

    while (@timeouts && $timeouts[0][0] <= $now) {
		my $tout = shift @timeouts;
		$tout->[1]->() if $tout->[1];
    }

	unless (@timeouts) {
		return undef;
	}

	return ($timeouts[0][0] - $now);
}

1;
