#!/usr/bin/perl

# Copyright (C) 2009-2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package App::Message;

use strict;
use warnings;

use Util::Strfy qw($strfy);
use App::Message::Format qw(format_message);
use App::Hook;

use Util::Export qw(
	ok/program: $program_name set_program_name usage_error shift_argv report_children
	default: fatal error warning info note debug trace format_message
	internal_error internal_warning $message_level &message_level
	message_setup error_string program_state);
use IO::Handle;
use Filter::Util::Call;

our ($program_name, $alias, $prefix, %pkg_prefix, $format, $output);

our @levels = qw(none fatal error warning info note debug trace all);
our %levels = map { $levels[$_] => $_ } 0 .. $#levels;
our $level;
*message_level = \$level;

# syslog compatibility map
our %level_to_priority = qw(
	fatal crit
	error err
	warning warning
	info info
	note notice
	debug debug
	trace debug
);

our $caller = 0;
our $own_message = 0;
our $with_backtrace = 0;

our $report_children = 0;
our $output_hook = new App::Hook;

our @program_states;
our $state_changed_hook;
our $program_states_kept = 10;

sub import
{
	my $pkg = shift;
	my @expargs = ();

	my ($caller, $filename) = caller;
	if ($filename eq $0) {
		unshift @_, "-program";
	}

	for (my $i = 0; $i < @_; $i++) {
		if ($_[$i] eq '-program') {
			message_setup('program');
			push @expargs, ':program', ':DEFAULT';
		} elsif ($_[$i] eq '-level') {
			$level = $_[++$i];
		} elsif ($_[$i] eq '-prefix') {
			$pkg_prefix{$caller} = $_[++$i];
		} elsif ($_[$i] =~ /(.+):$/) {
			$pkg_prefix{$caller} = $1;
		} else {
			push @expargs, $_[$i];
		}
	}

	local $Exporter::ExportLevel += 1;
	&Exporter::import($pkg, @expargs);

	if ($ENV{MESSAGE_LEVEL}) {
		$level = $ENV{MESSAGE_LEVEL};
	}

	if (my $spec = $ENV{MESSAGE_FILTER}) {
		return filter_new(split / +/, $spec);
	}
}

sub filter
{
	my $filter = shift;
	my $status = filter_read();

	if ($status > 0) {
		s($filter->{re})($filter->filter_subst($1, $2))eg;
	}

	return $status
}

sub filter_new
{
	my $filter = { };

	for my $a (@_) {
		my $flag = substr($a, 0, 1);
		my $name = substr($a, 1);
		if ($flag eq "+") {
			$filter->{flags}->{$name} = 1;
		} elsif ($flag eq "-") {
			$filter->{flags}->{$name} = 0;
		} else {
			die "Invalid filter -- Usage: [+-]statement, ..\n";
		}
	}

	my $re = "^\\s*(#)?\\s*\\b(\\Q" . join("\\E|\\Q", keys(%{$filter->{flags}})) . "\\E)\\b";
	$filter->{re} = qr/$re/;

	filter_add(bless $filter);
}

sub filter_subst
{
	my ($filter, $comment, $name) = @_;

	if ($filter->{flags}->{$name} && $comment) {
		return $name;
	}

	if (!$filter->{flags}->{$name} && !$comment) {
		return '#' . $name;
	}

	return $name unless $comment;
	return $comment.$name;
}

sub set_program_name($)
{
	($program_name) = $_[0] =~ m#([^/]+)(?:\.p[lm])?$#;
}

sub message_setup($%)
{
	my ($setup, %arg) = @_;

	if ($setup eq "program") {
		set_program_name($0);
		$level = $arg{level} || $ENV{APP_MESSAGE_LEVEL} || "info";
		$output = "standard";
		$format = "%{prg}: %{lev}: %{msg}";
	} elsif ($setup eq "syslog") {
		require Sys::Syslog;
		Sys::Syslog::openlog($arg{identity} || $program_name,
			'ndelay,pid', $arg{facility} || 'local0');
		$level = $arg{level} || $ENV{APP_MESSAGE_LEVEL} || "note";
		$output = "syslog";
		$format = "%{msg}";
	} elsif ($setup eq "log") {
		report_children();
		$level = $arg{level} || "note";
		if ($arg{file} eq "-") {
			open my $handle, '>&STDERR'
				or error("cannot dup stderr: %s", $!);
			$output = $handle;
		} else {
			open my $handle, '>>', $arg{file}
				or error("cannot open log=%s: $!", $arg{file});
			$output = $handle;
		}
		$format = "%{time:log} [%{lev:iu}] %{prg}: %{msg}";
	} elsif ($setup eq "tee") {
		&message_setup("log", %arg);
		$output = [$output, \*STDERR];
	}
}

sub caller_info(;$)
{
	my $step = shift;

	if (wantarray) {
		my @infos = ();
		local $caller = $caller + 1;
		while (my $info = &caller_info($step++)) {
			push @infos, $info;
		}
		return @infos;
	}

	if ($step) {
		return $strfy->trace($caller + $step)
	} else {
		return $strfy->caller($caller);
	}
}

sub internal_error($;@)
{
	my ($message, @arguments) = @_;
	local $with_backtrace = 1;
	local $caller = $caller + 1;
	message("error", "(internal) $message", @arguments);
}

sub internal_warning($;@)
{
	my ($message, @arguments) = @_;
	local $with_backtrace = 1;
	local $caller = $caller + 1;
	message("warning", "(internal) $message", @arguments);
}

sub caller_message($;$)
{
	my ($message, $level) = @_;
	return $message if $message =~ /\n$/;
	local $caller = $caller + ($level || 1) + 1;
	return $message . ' ' . caller_info(0) . ".\n";
}

sub prefix(\$$)
{
	$_[1] .= ': ' unless substr($_[1], -1) eq ' ';
	${$_[0]} = $_[1] . ${$_[0]};
}

sub report_children(;$)
{
	$report_children = @_ ? $_[0] : $$;
}

sub message($$;@)
{
	my ($level, $message, @arguments) = @_;

	if (@arguments) {
		$message = format_message($message, @arguments)
	}

	if ($level eq "error" && $^S) {
		die caller_message($message);
	}

	if (!$prefix && (my $pkg_prefix = $pkg_prefix{caller($caller)})) {
		prefix($message, $pkg_prefix);
	}

	if ($report_children && $report_children != $$) {
		prefix($message, "($$) ");
	}

	if ($prefix) {
		prefix($message, $prefix);
	}

	if ($with_backtrace) {
		local $caller = $caller + 1;
		my ($caller_info, @backtrace) = caller_info();
		my $backtrace = join "", map "\t$_.\n", @backtrace;
		$message = "$message $caller_info.\n $backtrace";
	}

	local $own_message = 1;

	if (defined $output) {
		return unless $output_hook->trigger($message, $level);
		output_message($message, $level);
		if ($level eq "error" || $level eq "fatal") {
			exit 255;
		}
	} elsif ($level eq "error") {
		die caller_message($message);
	} elsif ($level eq "warning") {
		warn caller_message($message);
	} elsif ($level eq "fatal") {
		print STDERR caller_message($message);
		exit 255;
	}
}

sub message_level($)
{
	return $level && $levels{$level} >= $levels{$_[0]};
}

sub output_message($$)
{
	my ($message, $level) = @_;

	my $string = format_message($format, $message, $level) || $message;
	chomp $string;

	if ($output eq "standard") {
		print STDERR ($string, "\n");
	} elsif ($output eq "syslog") {
		Sys::Syslog::syslog($level_to_priority{$level}, $message);
	} elsif (ref($output) eq 'GLOB' ||
		UNIVERSAL::isa($output, 'IO::Handle') ||
		$output eq "STDOUT" || $output eq "STDERR") {
		print {$output} $string, "\n";
		$output->flush;
	} elsif (ref($output) eq 'CODE') {
		$output->($level, $message);
	} elsif (ref($output) eq 'ARRAY') {
		for my $out (@{$output}) {
			local $output = $out;
			&output_message($message, $level);
		}
	}
}

sub error($;@)
{
	unshift(@_, "error");
	goto &message;
}

sub warning($;@)
{
	unshift(@_, "warning");
	goto &message;
}

sub info($;@)
{
	return unless message_level("info");
	unshift(@_, "info");
	goto &message;
}

sub note($;@)
{
	return unless message_level("note");
	unshift(@_, "note");
	goto &message;
}

sub debug($;@)
{
	return unless message_level("debug");
	unshift(@_, "debug");
	goto &message;
}

sub trace($;@)
{
	return unless message_level("trace");
	unshift(@_, "trace");
	goto &message;
}

sub fatal($;@)
{
	unshift(@_, "fatal");
	goto &message;
}

sub usage_error(;$)
{
	print STDERR "Usage error: $_[0]\n" if @_;
	print STDERR "Try `$program_name --help' for more information.\n";
	exit 64;
}

sub shift_argv(;$$$)
{
	my ($min, $max, $argv) = @_;
	$argv ||= \@ARGV;

	if (defined $min && @{$argv} < $min) {
		usage_error("too few arguments");
	}

	unless (defined $max) {
		$max = $min;
	} elsif (!$max) {
		$max = @{$argv};
	} elsif ($max > @{$argv}) {
		$max = @{$argv};
	}

	if (defined $max && @{$argv} > $max) {
		usage_error("too many arguments");
	}

	my @args;

	for (1 .. $max || 1) {
		push @args, shift @{$argv};
	}

	return wantarray ? @args : $args[0];
}

sub error_string(;$)
{
	my $e = @_ == 1 ? $_[0] : $@;
	return "" unless defined $e;
	$e =~ s#\s+at \S+ line \d+\.?\s*$##;
	$e =~ s#\s+$##;
	return $e;
}

sub program_state($;@)
{
	my ($message, @arguments) = @_;

	if (@arguments) {
		$message = format_message($message, @arguments);
	}

	my $entry = time . ' ' . $message;
	my $prev = @program_states ? (split ' ', $program_states[0])[1] : '';

	if ($message eq $prev) {
		$program_states[0] = $entry;
	} else {
		unshift @program_states, $entry;
	}

	if (@program_states > $program_states_kept) {
		pop @program_states;
	}

	if ($state_changed_hook) {
		$state_changed_hook->();
	}
}

1;
