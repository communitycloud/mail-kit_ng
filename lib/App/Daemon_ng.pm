#!/usr/bin/perl

package App::Daemon_ng;

use strict;
use warnings;

use POSIX;
use FindBin;
use Fcntl qw(:DEFAULT :flock);
use App::Message "daemon:", qw($program_name report_children usage_error :DEFAULT);
use App::Proc qw(post_fork_hook setup_signal_handler set_effective_id
	set_proc_title);
use App::Daemon::Control qw(daemon_control control_lock control_write control_append);
use App::Yield qw(yield_hook);
use Util::File qw(path_stat_changed);
use Util::String qw(escape_chars unescape_chars);
use Util::Export qw(daemon_init daemon_getopt daemon_apply daemon_configure
	daemon_control daemon_handler daemon_help);

my %config = init_config();
my ($control, %signals, %handlers);
our $active = 1;

sub init_config
{
	name => $program_name,
	argv => [@ARGV],
	action => '',
	control => undef,
	resume_state => time,
	resume_command => undef,
	daemon_command => undef,
	log_file => undef,
	log_level => undef,
	log_watch => undef,
	user => undef,
	group => undef,
	working_directory => undef,
	foreground => 0,
	syslog => 0,
	syslog_identity => undef,
	syslog_facility => undef,
}

sub daemon_help(;$)
{
	my $long = shift;

	unless (defined wantarray) {
		print &daemon_help($long);
		return;
	}

	if ($long) {
		"Daemon options:\n",
		"      --daemon=ACTION       same as --daemon-ACTION, needs --control-file\n",
		"      --control=FILE        mandatory daemon control file\n",
		"      --daemon-start        daemonize, the default daemon action\n",
		"      --daemon-stop         terminate the daemon\n",
		"      --daemon-status       report daemon process status\n",
		"      --daemon-reload       reload daemon and configuration\n",
		"      --foreground          keep the daemon in the foreground\n",
		"      --log-file=FILE       log file\n",
		"      --log-level=LEVEL     log level: ", join(', ', @App::Message::levels) , "\n",
		"      --log-watch=INTERVAL  watch the log inode for rotation every INTERVAL seconds\n",
		"      --syslog              send the messages to system log\n",
		"      --syslog-identity=N   set the syslog identity (default `$config{name}`)\n",
		"      --syslog-facility=N   set the syslog facility (default `daemon`)\n",
		"      --user=UID            set the effective user ID\n",
		"      --group=GID           set the effective group ID\n",
		"      --daemon-name=ALIAS   report a different name than `$config{name}'\n",
	} else {
		"This program supports the daemon interface.\n",
		"See `$program_name --daemon-help' for full options\n",
	}
}

sub daemon_getopt()
{
	require Getopt::Long;
	my $action = sub {
		my ($name, $value) = @_;
		if ($name =~ /^daemon-(.+)/) {
			$config{action} = $1;
		} else {
			$config{action} = $value || "start";
		}
		die "invalid daemon action: $config{action}\n"
			unless $config{action} =~ /^(start|stop|reload|status|resume)$/;
	};
	my $save = Getopt::Long::Configure("pass_through");
	Getopt::Long::GetOptions(
		"daemon:s"          => $action,
		"daemon-start"      => $action,
		"daemon-stop"       => $action,
		"daemon-reload"     => $action,
		"daemon-status"     => $action,
		"daemon-resume=s"   => \$config{resume},
		"control=s"         => \$config{control},
		"foreground"        => \$config{foreground},
		"daemon-name=s"     => \$config{name},
		"daemon-help"       => \$config{help},
		"log-file=s"        => \$config{log_file},
		"log-level=s"       => \$config{log_level},
		"log-watch:i"       => \$config{log_watch},
		"syslog"            => \$config{syslog},
		"syslog-identity"   => \$config{syslog_identity},
		"syslog-facility"   => \$config{syslog_facility},
		"user=s"            => \$config{user},
		"group=s"           => \$config{group},
		"working-directory=s" => \$config{working_directory},
	) or usage_error();
	Getopt::Long::Configure($save);
}

sub daemon_configure(%)
{
	my %c = @_;
	while (my ($key, $value) = each %c) {
		unless (exists $config{$key}) {
			error("unknown configuration key %q", $key);
		} else {
			$config{$key} = $value;
		}
	}
}

sub daemon_init()
{
	daemon_getopt();

	if ($config{help}) {
		daemon_help(1);
		exit(0)
	}

	if ($config{foreground}) {
		$config{action} ||= 'start';
	}

	unless ($config{control}) {
		if ($config{action} =~ /stop|reload|status/) {
			usage_error("--daemon-$config{action} requires --control=FILE specified");
		}
	}

	if ($config{action} eq "stop") {
		daemon_control("stop", $config{control});
		exit(0);
	} elsif ($config{action} eq "reload") {
		daemon_control("reload", $config{control});
		exit(0);
	} elsif ($config{action} eq "status") {
		daemon_control("status", $config{control});
		exit(0);
	}
}

sub daemon_apply()
{
	if (my $state = $config{resume}) {
		resume($state);
	} elsif ($config{action} eq 'start') {
		start();
	}
}

sub daemon_handler(%)
{
	my %c = @_;
	while (my ($key, $value) = each %c) {
		$handlers{$key} = $value;
	}
}

sub daemonize()
{
	if ($config{foreground}) {
		return;
	}
	# protect process during fork
	$SIG{HUP} = 'IGNORE';

	defined(my $pid = fork)
		or error("couldn't fork: $!");
	POSIX::_exit(0) if $pid;

	open STDIN, '</dev/null' if -t 0;
	open STDOUT, '>/dev/null';
	open STDERR, '>/dev/null';
	POSIX::setsid();

	report_children();
}

sub start()
{
	change_working_directory();
	drop_privileges();
	control_acquire();
	app_message_setup();
	daemonize();
	control_update();
	setup_signals();
	info_message();
}

sub resume($)
{
	my $state = shift;
	app_message_setup();
	info("resuming state=$config{resume_state} previous=$state");
	program_state("daemon resumed");
	control_acquire();
	setup_signals();
}

sub trigger($;@)
{
	my ($event, @args) = @_;
	if (my $cb = $handlers{$event}) {
		$cb->(@args);
	}
}

sub app_message_setup()
{
	if ($config{syslog}) {
		message_setup("syslog",
			identity => $config{syslog_identity} || $config{name},
			facility => $config{syslog_facility} || 'daemon',
			level => $config{log_level});
	} elsif ($config{foreground}) {
		message_setup("log",
			file => '-',
			level => $config{log_level});
	} elsif ($config{log_file}) {
		message_setup("log",
			file => $config{log_file},
			level => $config{log_level});
	}

	if ($config{log_file} && defined $config{log_watch}
		&& $config{log_file} ne '-')
	{
		$App::Message::output_hook->add(sub {
			if (path_stat_changed(1,
					$config{log_file},
					$config{log_watch} || 60))
			{
				message_setup("log",
					file => $config{log_file},
					level => $config{log_level});
			}

			return 1;
		});
	}

	if (my $name = $config{name}) {
		set_proc_title("$program_name ($name daemon)");
		$App::Message::alias = $name;
	} else {
		set_proc_title("$program_name (daemon)");
	}

	$App::Message::state_changed_hook = \&control_update_state;
}

sub info_message()
{
	my $info = ["pid", $$];

	if (my $user = $config{user}) {
		push @{$info}, "user", $user;
	}

	if (my $group = $config{group}) {
		push @{$info}, "group", $group;
	}

	info("gone in background %{ph:t}", $info);
	program_state("daemon started");
}

sub die_trap($)
{
	return if $App::Message::own_message;
	return unless defined $^S && $^S == 0;
	chomp(my $err = shift);
	error("(Trap) $err");
}

sub warn_trap($)
{
	return if $App::Message::own_message;
	chomp(my $warn = shift);
	warning("(Trap) $warn");
}

sub daemon_command()
{
	if ($config{daemon_command}) {
		return @{$config{daemon_command}};
	}

	("$FindBin::Bin/$FindBin::Script", @{$config{argv}});
}

sub resume_command()
{
	if ($config{resume_command}) {
		return @{$config{resume_command}};
	}

	# remove the --resume option
	my @argv = @{$config{argv}};

	for (my $i = 0; $i < @argv; $i++) {
		if ($argv[$i] eq '--daemon-resume') {
			splice @argv, $i, 2;
			last;
		} elsif ($argv[$i] eq '--') {
			last;
		}
	}

	("$FindBin::Bin/$FindBin::Script",
		"--daemon-resume", $config{resume_state}, @argv);
}

sub handle_signals()
{
	for my $sig (keys %signals) {
		if ($sig eq "TERM" || $sig eq "INT") {
			info("received $sig, exit process");
			program_state("daemon terminated");
			trigger("terminate", $sig);
			exit();
		} elsif ($sig eq "HUP") {
			info("received $sig, reloading");
			program_state("daemon reloading");
			trigger("reload", $sig);
			exec(resume_command())
				or error("exec failed: $!");
		}
		delete $signals{$sig};
	}
}

sub setup_signals()
{
	$SIG{__DIE__} = \&die_trap;
	$SIG{__WARN__} = \&warn_trap;

	my $handler = sub {
		my $sig = shift;
		if ($sig eq "TERM" || $sig eq "INT") {
			trigger("sig_terminate", $sig);
		} elsif ($sig eq "HUP") {
			trigger("sig_reload", $sig);
		}
		$signals{$sig} ++;
	};

	setup_signal_handler($handler, "TERM", "INT", "HUP");
	yield_hook(\&handle_signals);

	my $restore = sub { setup_signal_handler("DEFAULT", "HUP", "TERM", "INT") };
	post_fork_hook($restore);
}

sub change_working_directory()
{
	if (my $dir = $config{working_directory}) {
		chdir $dir
			or error("couldn't change working directory: %s", $!);
	}
}

sub drop_privileges()
{
	if ($config{user} || $config{group}) {
		set_effective_id($config{user}, $config{group});
	}
}

sub control_acquire()
{
	if (my $c = $config{control}) {
		control_lock($c);
		control_update();
	}
	$active++;
}

sub control_update()
{
	control_write($$, $config{name}, daemon_command());
	control_update_state();
}

sub control_update_state($)
{
	control_append(@App::Message::program_states);
}

1;
