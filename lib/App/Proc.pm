#!/usr/bin/perl

# Copyright (C) 2009-2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package App::Proc;

use strict;
use warnings;

use App::Message "proc:";
use Util::Export qw(app_fork app_wait afork post_fork_hook reap_children
	setup_signal_handler register_signal block_signal unblock_signal
	set_resource_limit set_effective_id
	set_proc_title set_proc_comment %kids $parent_pipe
	ok/prefork: app_prefork child_proc_state child_proc_busy child_proc_idle);
use Util::List qw(in_list);
use Scalar::Util qw(looks_like_number);
use App::Yield qw(app_yield);
require POSIX;

our $parent = $$;
our %kids;
our @post_fork_hooks;

our %signal_number;
our @signal_name;

our $title;
our $parent_pipe;

sub app_fork(%)
{
	my %c = @_;
	my ($read, $write);

	block_signal('CHLD');

	if ($c{pipe}) {
		pipe($read, $write);
		$read->autoflush();
		$write->autoflush();
		if (ref $c{pipe}) {
			${$c{pipe}} = $read;
		}
	}

	defined (my $pid = fork)
		or error("fork failed: $!");

	if ($pid) {
		# debug("new child %{ph:2dt}", pid => $pid, name => $c{name});
		$kids{$pid} = {
			name => $c{name},
			reap => $c{reap},
			pipe => $read,
			time => time,
			state => 'idle',
		};
		close $write if $write;
		unblock_signal('CHLD');
	} else {
		if (my $name = $c{name}) {
			set_proc_comment($name);
		}
		if ($c{user} or $c{group}) {
			set_effective_id($c{user}, $c{group});
		}
		close $read if $read;
		$parent_pipe = $write;
		while (my $hook = pop @post_fork_hooks) {
			$hook->();
		}
		if ($c{job}) {
			run_proc_job(%c);
		}
	}

	return $pid;
}

sub run_proc_job(%)
{
	my %c = @_;
	my $job = $c{job};

	if ($c{runs} || $c{life}) {
		for (my $run = 0, my $start = time;
			(!$c{runs} || $run < $c{runs}) &&
			(!$c{life} || time - $start < $c{life});
			$run++)
		{
			local $_ = $run;
			$job->();
		}
		exit;
	} else {
		exit $job->();
	}
}

sub app_wait(;$)
{
	my $pid = defined $_[0]
		? $_[0] == -1
			? waitpid(-1, 1)
			: waitpid($_[0], 0)
		: wait;
	reap_child($pid, $?)
		if $pid != -1;
	return $pid;
}

sub app_prefork(%)
{
	my %c = @_;

	internal_error("app_prefork: no job specified")
		unless $c{job};

	my $min_kids = $c{min_kids} || 4;
	my $max_kids = $c{max_kids} || 20;
	my $min_idle = $c{min_idle} || 0;
	my $max_idle = $c{max_idle} || 10;
	my $timeout = $c{timeout} || 10;

	require IO::Select;
	my $s = IO::Select->new();

	my $reap = sub {
		my $e = $_[3];
		$s->remove($e->{pipe});
		$c{reap}->(@_) if $c{reap};
	};

	register_signal(CHLD => \my $got_sig_chld);

	while (1)
	{

		block_signal('CHLD');
		if ($got_sig_chld) {
			$got_sig_chld = 0;
			reap_children();
		}
		unblock_signal('CHLD');

		app_yield();

		my $n_kids = keys %kids;
		my $n_idle = grep $_->{state} eq 'idle', values %kids;

		my $n = 0;

		if ($n_kids < $min_kids) {
			$n += $min_kids - $n_kids;
		}

		if ((my $k = $n_idle - $max_idle) > 0) {
			for my $pid (
				grep $kids{$_}->{state} eq 'idle', keys %kids)
			{
				kill HUP => $pid;
				last unless --$k;
			}
		} elsif ($n_idle + $n < $min_idle) {
			$n = $min_idle - $n_idle;
		}

		if ($n_kids + $n > $max_kids) {
			$n = 0;
		}

		for (1 .. $n)
		{
			my $pid = app_fork(
				name => "worker",
				%c,
				reap => $reap,
				pipe => 1);
			$s->add($kids{$pid}->{pipe});
		}

		if (my $sub = $c{post_fork_hook}) {
			$sub->();
		}

		if ($s->count == 0) {
			sleep $timeout;
			next;
		}

		for my $fh ($s->can_read($timeout)) {
			defined (my $line = readline $fh)
				or $s->remove($fh), next;

			my ($cmd, $pid, $arg) = $line =~ /^(.+?) (\d+)(?: (.+))?$/
				or next;
			next unless exists $kids{$pid};

			if ($cmd eq 'state') {
				$kids{$pid}->{state} = $arg;
			}

			if (my $sub = $kids{$pid}->{read}) {
				$sub->($cmd, $pid, $arg);
			}
		}
	}
}

sub afork(;\&$)
{
	my ($handler, $name) = @_;

	app_fork(
		name => $name,
		reap => $handler,
	);
}

sub set_proc_title($)
{
	$0 = $title = $_[0];
}

sub set_proc_comment($)
{
	return unless defined $title;
	unless ($title =~ s/ \(.*?\)$/ ($_[0])/) {
		$title .= " ($_[0])";
	}
	set_proc_title($title);
}

sub child_proc_state($)
{
	my $state = shift;
	# set_proc_comment($state);
	if ($parent_pipe) {
		print $parent_pipe "state $$ $state\n";
	}
}

sub child_proc_busy()
{
	child_proc_state("busy");
}

sub child_proc_idle()
{
	child_proc_state("idle");
}

sub post_fork_hook($)
{
	unless (ref $_[0] eq "CODE") {
		internal_error("invalid fork hook %s", $_[0]);
	}
	push @post_fork_hooks, $_[0];
}

sub reap_child($$)
{
	my ($pid, $status) = @_;
	if (my $entry = delete $kids{$pid}) {
		# debug("child exit %{ph:3dt}", pid => $pid, status => $status, name => $entry->{name});
		if (my $reap = $entry->{reap}) {
			$reap->($pid, $status >> 8, $signal_name[$status & 127], $entry);
		}
	}
}

sub reap_children
{
	local ($!, $?);
	while ((my $pid = waitpid(-1, 1)) > 0) {
		my $status = $?;
		reap_child($pid, $?);
	}
}

sub register_signal(%)
{
	while (@_)
	{
		my ($signals, $handler) = (shift, shift);

		unless (defined $handler) {
			$handler = 'DEFAULT';
		} elsif (uc $signals eq 'CHLD' && uc $handler eq 'AUTO') {
			$handler = \&reap_children;
		} elsif (!ref $handler) {
			$handler = uc $handler;
		} elsif (ref $handler eq 'HASH') {
			my $ref = $handler;
			$handler = sub { $ref->{$_[0]}++ };
		} elsif (ref $handler eq 'SCALAR') {
			my $ref = $handler;
			$handler = sub { $$ref++ };
		}

		for my $sig(
			ref($signals) ? @{$signals} : split " ", $signals)
		{
			my $const = "SIG" . uc($sig);
			my $sigset = POSIX::SigSet->new(POSIX->$const);
			my $action = POSIX::SigAction->new($handler, $sigset);
			$action->safe(1);
			POSIX::sigaction(POSIX->$const, $action);
		}
	}
}

sub setup_signal_handler($@)
{
	my $handler = shift;

	my $sigset = POSIX::SigSet->new();
	my $action = POSIX::SigAction->new($handler, $sigset);

	$action->safe(1);

	for my $sig (@_) {
		my $const = "SIG" . uc($sig);
		POSIX::sigaction(POSIX->$const, $action);
	}
}

sub set_resource_limit($$;$)
{
	require BSD::Resource;
	my ($resource, $soft, $hard) = @_;
	if ($resource eq "ram") {
		$resource = "rss";
		($soft, $hard) = map { $_ * POSIX::sysconf(&POSIX::_SC_PAGESIZE) }
		($soft, $hard);
	}
	my $const = "RLIMIT_" . uc($resource);
	internal_error("invalid limit %s", $const)
		unless in_list($const, @BSD::Resource::EXPORT);
	my $value = BSD::Resource->$const;
	my ($oldsoft, $oldhard) = BSD::Resource::getrlimit($value);
	BSD::Resource::setrlimit($value,
		defined $soft ? $soft : $oldsoft,
		defined $hard ? $hard : $oldhard)
	? note("limit resource=%s soft=%s hard=%s", $resource, $soft, $hard)
	: warning("limit resource=%s soft=%s hard=%s failed: %s", $resource, $soft, $hard, $?);
}

sub init_signal_names()
{
	require Config;
	for my $sig (
		split ' ', $Config::Config{sig_name})
	{
		$signal_number{$sig} = scalar @signal_name;
		push @signal_name, $sig;
	}
}

sub block_signal($)
{
	my $sig = shift;
	my $const = "SIG" . uc($sig);
	POSIX::sigprocmask(&POSIX::SIG_BLOCK,
		POSIX::SigSet->new(POSIX->$const));
}

sub unblock_signal($)
{
	my $sig = shift;
	my $const = "SIG" . uc($sig);
	POSIX::sigprocmask(&POSIX::SIG_UNBLOCK,
		POSIX::SigSet->new(POSIX->$const));
}

sub set_effective_id($;$)
{
	my ($user, $group) = @_;

	if ($group) {
		my $gid = looks_like_number($group) ? $group : getgrnam($group);
		error("set_effective_id: invalid group %q", $group)
			unless defined $gid;
		$) = $gid;
		error("set_effective_id: couldn't set GID to %d: %s", $gid, $!)
			unless $) == $gid;
	}

	if ($user) {
		my $uid = looks_like_number($user) ? $user : getpwnam($user);
		error("set_effective_id: invalid user %q", $user)
			unless defined $uid;
		$> = $uid;
		error("set_effective_id: couldn't set UID to %d: %s", $uid, $!)
			unless $> == $uid;
	}
}

init_signal_names();

1;
