#!/usr/bin/perl

package Tail::DB;

use strict;
use warnings;

use App::Message "tail_db:";
use Util::Export qw(tail_db_open);
use Util::String qw(parse_uri unescape_chars);
use Util::File qw(dirname);
use POSIX qw(:fcntl_h);
use Cwd qw(abs_path);

sub tail_db_open($;$)
{
	my ($uri, $mode) = @_;

	my $c = parse_uri($uri)
		or error("invalid URI %q", $uri);

	if ($c->{scheme} && $c->{scheme} ne "tail") {
		error("invalid URI scheme %q", $c->{scheme});
	}

	my $sep = unescape_chars($c->{args}->{sep}, "nrt0");

	if ($c->{path} =~ s#^>+##) {
		$mode = "write";
	}

	my $db = __PACKAGE__->new(
		$c->{path},
		sep => $sep,
		mode => $mode,
		type => $c->{scheme});

	if (defined (my $pos = $c->{args}->{pos})) {
		$pos = '-0' if $pos eq 'end';
		$pos = '0' if $pos eq 'start';
		$db->seek_pos($pos);
	}

	if (defined $c->{args}->{cursor}) {
		$db->open_cursor($c->{args}->{cursor});
	}

	return $db;
}

sub new
{
	my ($class, $file, %arg) = @_;

	my $r = bless {
		sep => defined $arg{sep} ? $arg{sep} : "\n",
		mode => $arg{mode} || "read",
		type => $arg{type} || "plain",
		rec => "",
		fs_events => [],
		timeout => $arg{timeout},
		unread => [],
	}, $class;

	$r->open($file);

	if (my $cursor = $arg{cursor}) {
		$r->open_cursor($cursor);
	}

	return $r;
}

sub open
{
	my ($r, $file) = @_;

	note("open file=%s type=%s mode=%s",
		$file, $r->{type}, $r->{mode});

	$r->{file} = $file;

	$r->reopen();
}

sub reopen
{
	my $r = shift;

	if ($r->{mode} eq 'write') {
		CORE::open my $handle, '>>', $r->{file}
			or error("failed to open %q for writing: %s",
			$r->{file}, $!);
		$r->{handle} = $handle;
		$r->{inode} = (stat $handle)[1];
	} elsif (CORE::open my $handle, $r->{file}) {
		$r->{handle} = $handle;
		$r->{pos} = 0;
		$r->{inode} = (stat $handle)[1];
		$r->{size} = (stat $handle)[7];
		$r->{abs_path} = abs_path($r->{file});
		return 1;
	} elsif ($r->{type} eq 'tail' && $!{ENOENT}) {
	} else {
		error("cannot open %q: %s", $r->{file}, $!);
	}

	return 0;
}

sub close
{
	my $r = shift;

	note("close file=%s", $r->{file});

	$r->reset();

	if ($r->{file_watch}) {
		(delete $r->{file_watch})->cancel();
	}

	if ($r->{parent_watch}) {
		(delete $r->{parent_watch})->cancel();
	}

	if ($r->{inotify}) {
		undef $r->{inotify};
	}

	1;
}

sub open_cursor
{
	my ($r, $cursor) = @_;
	sysopen $r->{cursor}, $cursor, O_CREAT | O_RDWR
		or error("failed to open cursor file %q: %s", $cursor, $!);
	$r->{cursor_file} = $cursor;

	my ($inode, $offset) = $r->read_cursor()
		or return undef;

	if ($r->{handle} && $inode == $r->{inode} && $offset <= $r->{size}) {
		$r->seek_pos($offset);
	} else {
		warning("cursor file holds old data");
	}

	return undef;
}

sub close_cursor
{
	my ($r) = @_;
	CORE::close $r->{cursor};
	undef $r->{cursor};
}

sub reload_cursor
{
	my $r = shift;
	my $pos = $r->read_cursor();
	return 0 unless defined $pos;
	if ($pos != $r->{pos}) {
		$r->seek_pos($pos);
		return 1;
	} else {
		return 0;
	}
}

sub read_cursor
{
	my ($r, $cursor) = @_;
	$cursor ||= $r->{cursor};
	seek $cursor, 0, 0;
	if (defined(my $line = readline($cursor))) {
		my ($inode, $offset) = $line =~ /^(\d+) (\d+)$/;
		my ($filename) = readline($cursor);
		chomp($filename) if $filename;
		return wantarray ? ($inode, $offset, $filename) : $offset;
	} else {
		return;
	}
}

sub save_cursor
{
	my $r = shift;
	unless ($r->{cursor} && $r->{inode}) {
		warning("cannot save cursor at this time");
		return 0;
	}
	seek $r->{cursor}, 0, 0;
	syswrite $r->{cursor}, "$r->{inode} $r->{pos}\n$r->{abs_path}\n";
	$r->{cursor}->flush();
}

sub seek_pos
{
	my ($r, $pos) = @_;
	return unless defined $pos;
	return unless $r->{handle};
	if (substr($pos, 0, 1) eq '-') {
		seek $r->{handle}, int($pos), 2;
	} else {
		seek $r->{handle}, int($pos), 0;
	}

	$r->{pos} = tell $r->{handle};
}

sub reset
{
	my ($r) = @_;
	if ($r->{handle}) {
		CORE::close $r->{handle};
	}
	$r->{handle} = $r->{pos} =
		$r->{size} = $r->{inode} = undef;
}

sub check_truncate
{
	my $r = shift;
	return unless $r->{handle};

	my $size = (stat $r->{handle})[7];

	if ($size < $r->{size})
	{
		warning("db truncated");
		seek $r->{handle}, 0, 0;
	}

	$r->{size} = $size;
}

sub read_record
{
	my $r = shift;

	if (@{$r->{unread}}) {
		return pop @{$r->{unread}};
	}

	if ($r->{handle}) {
		local $/ = $r->{sep};
		my $line = readline($r->{handle});
		unless (defined $line) {
			return undef;
		}
		unless (substr ($line, -(length $r->{sep})) eq $r->{sep}) {
			$r->{rec} .= $line;
			return undef;
		} elsif (length $r->{rec}) {
			$r->{rec} .= $line;
			$line = $r->{rec};
			$r->{rec} = "";
		}
		$r->{pos} = tell $r->{handle};
		chomp $line;
		return $line;
	}

	return undef;
}

sub unread_record
{
	my ($r, $rec) = @_;
	push @{$r->{unread}}, $rec;
}

sub read
{
	my $r = shift;

	if ($r->{mode} ne 'read') {
		error("can't read in %s mode",
			$r->{mode});
	}

	if ($r->{cursor}) {
		$r->save_cursor();
	}

	READ: while (1) {

		if (defined (my $line = $r->read_record)) {
			return $line;
		}

		while (my $ev = shift @{$r->{fs_events}}) {
			if ($ev eq "modify") {
				$r->check_truncate();
				next READ;
			} elsif ($ev eq "create") {
				$r->reopen();
				next READ;
			} elsif ($ev eq "delete") {
				$r->reset();
			} elsif ($ev eq "cursor") {
				$r->reload_cursor()
					and next READ;
			}
		}

		$r->fs_wait()
			or return undef;
	}
}

sub read_multi
{
	my ($r, $limit) = @_;

	if ($r->{cursor}) {
		$r->save_cursor();
	}

	my @rec;
	my $size = 0;

	while (defined (my $rec = $r->read_record()))
	{
		$size += length($rec) + length($r->{sep});
		if ($size > $limit) {
			return $rec unless @rec;
			$r->unread_record($rec);
			last;
		}
		push @rec, $rec;
	}

	if (@rec) {
		return wantarray ? @rec : join($r->{sep}, @rec);
	}

	if (defined (my $rec = $r->read())) {
		return $rec;
	} else {
		return wantarray ? () : undef;
	}
}

sub write
{
	my ($r, $rec) = @_;

	if ($r->{mode} ne 'write') {
		error("can't write in %s mode",
			$r->{mode});
	}

	my $inode = (stat $r->{file})[1];

	if ($r->{type} eq 'tail' &&
		(!defined $inode || $inode != $r->{inode})
	)
	{
		$r->reopen();
	}

	return 1 unless defined $rec;

	unless (
		syswrite($r->{handle}, $rec . $r->{sep}) &&
		$r->{handle}->flush())
	{
		return 0;
	}

	1;
}

sub fs_watch_init
{
	my $r = shift;

	require Linux::Inotify2;
	require IO::Select;

	$r->{inotify} = Linux::Inotify2->new
		or error("unable to create new inotify object: $!");

	$r->fs_watch_file();
	$r->fs_watch_parent();
	$r->fs_watch_cursor();

	$r->reopen() unless $r->{handle};

	$r->{select} = IO::Select->new($r->{inotify}->fileno);

	return 1;
}

sub fs_watch_file
{
	my $r = shift;
	$r->{file_watch} = $r->{inotify}->watch($r->{file},
		&Linux::Inotify2::IN_MODIFY |
		&Linux::Inotify2::IN_DELETE_SELF |
		&Linux::Inotify2::IN_MOVE_SELF);
	unless ($r->{file_watch} || $!{ENOENT}) {
		error("couldn't create file watch: $!");
	}
	return $r->{file_watch} ? 1 : 0;
}

sub fs_watch_parent
{
	my $r = shift;
	$r->{parent_watch} = $r->{inotify}->watch(dirname($r->{file}),
		&Linux::Inotify2::IN_CREATE)
		or error("couldn't create parent watch: $!");
	return 1;
}

sub fs_watch_cursor
{
	my $r = shift;
	$r->{cursor_watch} = $r->{inotify}->watch($r->{cursor_file},
		&Linux::Inotify2::IN_MODIFY |
		&Linux::Inotify2::IN_DELETE_SELF |
		&Linux::Inotify2::IN_MOVE_SELF);
	unless ($r->{file_watch} || $!{ENOENT}) {
		error("couldn't create file watch: $!");
	}
	return $r->{cursor_watch} ? 1 : 0;
}

sub fs_add_event
{
	my ($r, $ev) = @_;

	unless (@{$r->{fs_events}} && $r->{fs_events}->[-1] eq $ev) {
		push @{$r->{fs_events}}, $ev;
	}
}

sub fs_wait
{
	my $r = shift;

	if ($r->{type} ne 'tail') {
		return undef;
	}

	$r->{inotify} || $r->fs_watch_init();

	unless ($r->{select}->can_read($r->{timeout})) {
		return undef;
	}

	for my $ev ($r->{inotify}->read) {
		if ($ev->{w}->{name} eq $r->{file}) {
			if ($ev->IN_MODIFY) {
				$r->fs_add_event('modify');
			} elsif ($ev->IN_DELETE_SELF || $ev->IN_MOVE_SELF) {
				$r->{file_watch}->cancel();
				$r->fs_add_event('delete');
			}
		} elsif ($ev->fullname eq $r->{file} ||
			$ev->fullname eq './' . $r->{file})
		{
			if ($ev->IN_CREATE) {
				$r->fs_add_event('create');
				$r->fs_watch_file();
			}
		} elsif ($ev->{w}->{name} eq $r->{cursor_file}) {
			if ($ev->IN_MODIFY) {
				$r->fs_add_event('cursor');
			}
		}
	}

	return 1;
}

1;
