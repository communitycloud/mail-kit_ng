#!/usr/bin/perl

package JSON::Events;

use strict;
use warnings;

use Util::File qw(dirname);
use Tail::DB qw(tail_db_open);
use JSON::Event;

our $DEFAULT_EVENTS_FILE = $ENV{DEFAULT_EVENTS_FILE} ||
	"/var/lib/wa-research/events.json";

sub new
{
	my ($class, $file) = @_;
	$file ||= $DEFAULT_EVENTS_FILE;

	my $taildb_write = tail_db_open("tail:$file", "write");

	return bless {
		file => $file,
		taildb_write => $taildb_write,
	}, $class;
}

sub reader
{
	my ($class, $file, $cursor) = @_;
	$file ||= $DEFAULT_EVENTS_FILE;

	my $taildb_read;

	if ($cursor) {
		$taildb_read = tail_db_open("tail:$file", "read");
		if ($cursor !~ m#/#) {
			$cursor = dirname($file) . '/' . $cursor;
		}
		$taildb_read->open_cursor($cursor);
	} else {
		$taildb_read = tail_db_open($file, "read");
	}

	return bless {
		file => $file,
		taildb_read => $taildb_read,
	}, $class;
}

sub taildb_write
{
	my ($e, $rec) = @_;

	$e->{taildb_write}->write($rec);
}

sub put
{
	my ($e, $source, %event) = @_;

	my $ref;

	if (ref $source eq 'JSON::Event') {
		$ref = $source;
	} else {
		$ref = JSON::Event->new($source, %event);
	}

	my $rec = $ref->to_json();
	$e->taildb_write($rec);
}

sub get
{
	my ($e, $source) = @_;

	while (defined (my $rec = $e->{taildb_read}->read())) {
		my $ev = JSON::Event->parse($rec)
			or next;

		if ($ev->match($source)) {
			return $ev;
		}

		return $ev;
	}
}

sub get_multi
{
	my ($e, $source, $limit) = @_;
	$limit ||= 64 * 1024;

	my @rec = $e->{taildb_read}->read_multi($limit)
		or return undef;

	my @evs = grep { $_ && $_->match($source) }
		map { JSON::Event->parse($_) } @rec;

	return \@evs;
}

1;
