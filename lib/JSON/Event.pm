#!/usr/bin/perl

package JSON::Event;

use strict;
use warnings;

use Sys::Hostname;
require Time::HiRes;
require JSON;
use Util::List qw(update_hash);
use Util::String qw(compile_glob);
use App::Message::Format qw(format_message);
require App::Message;

my $json = JSON->new->utf8;
my $json_bin = JSON->new->ascii;

sub new
{
	my ($class, $source, %event) = @_;

	my $ev = bless {
		_source => $source,
		_timestamp => Time::HiRes::time(),
		_hostname => hostname(),
	}, $class;

	$ev->push(%event);
	return $ev;
}

sub parse
{
	my ($class, $rec) = @_;

	if (length($rec) == 0) {
		App::Message::warning('empty event record encountered');
		return undef;
	}

	if ($rec =~ /\0/) {
		App::Message::warning('stripping null bytes from record=%s', $rec);
		$rec =~ s/\0//g;
	}

	my $ev = eval { $json->decode($rec) };
	if ($@) {
		App::Message::error('failed to parse event record=%s: %s', $rec, $@);
	}

	if (ref $ev ne 'HASH' || !$ev->{_source}) {
		App::Message::warning('invalid event: %s', $ev);
		return undef;
	}

	return JSON::Event->new(undef, %{$ev});
}

sub push
{
	my ($ev, %event) = @_;

	while (my ($key, $value) = each %event)
	{
		if (ref $ev->{$key} eq 'ARRAY') {
			push @{$ev->{$key}},
				ref $value eq 'ARRAY' ? @$value : $value;
		} elsif (ref $ev->{$key} eq 'HASH' && ref $value eq 'HASH') {
			update_hash($ev->{$key}, $value);
		} else {
			$ev->{$key} = $value;
		}
	}
}

sub to_json
{
	my ($ev) = @_;
	return $json->encode({%{$ev}});
}

sub to_json_bin
{
	my ($ev) = @_;
	return $json_bin->encode({%{$ev}});
}

sub match
{
	my ($ev, $source) = @_;
	return 1 unless defined $source;
	return $ev->{_source} =~ compile_glob($source);
}

sub error
{
	my ($ev, $fmt, @args) = @_;

	my $msg = format_message($fmt, @args);
	$ev->push(_error => [$msg]);
}

sub warning
{
	my ($ev, $fmt, @args) = @_;

	my $msg = format_message($fmt, @args);
	$ev->push(_warning => [$msg]);
}

sub info
{
	my ($ev, $fmt, @args) = @_;

	my $msg = format_message($fmt, @args);
	$ev->push(_info => [$msg]);
}

1;
