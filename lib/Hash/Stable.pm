#!/usr/bin/perl

# Copyright (C) 2009-2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Hash::Stable;

use strict;
use warnings;

sub H() { 0 }
sub O() { 1 }
sub A() { 2 }
sub I() { 3 }

sub new
{
	my ($c, @a) = @_;
	tie my (%h), $c;
	if (@a) {
		$h{$a[2 * $_]} = $a[2 * $_ + 1]
			for 0 .. $#a/2;
	}
	return \%h;
}

sub key
{
	my ($s, $k) = @_;
	return wantarray ? () : undef unless defined $k;
	return wantarray ? ($k, $s->[H]->{$k}) : $k;
}

sub sort
{
	my ($s, $sort) = @_;

	my @keys = sort $sort keys %{$s->[H]};

	for (my $i = 0; $i < @keys; $i++) {
		$s->[O]->{$keys[$i]} = $i;
	}
}

sub index_of
{
	my ($s, $key) = @_;
	$s->[O]->{$key};
}

sub TIEHASH
{
	bless [{},{},[],0], shift;
}

sub STORE
{
	my ($s, $k, $v) = @_;
	$s->[O]->{$k} = $s->[I] ++;
	$s->[H]->{$k} = $v;
}

sub FETCH
{
	my ($s, $k) = @_;
	$s->[H]->{$k}
}

sub FIRSTKEY
{
	my ($s) = @_;
	$s->[A] = [ sort { $s->[O]->{$b} <=> $s->[O]->{$a} }
		keys %{$s->[O]} ];
	$s->key(pop @{$s->[A]});
}

sub NEXTKEY
{
	my ($s) = @_;
	$s->key(pop @{$s->[A]});
}

sub EXISTS
{
	my ($s, $k) = @_;
	exists $s->[H]->{$k};
}

sub DELETE
{
	my ($s, $k) = @_;
	delete $s->[O]->{$k};
	delete $s->[H]->{$k};
}

sub CLEAR
{
	my ($s) = @_;
	@{$s->[A]} = ();
	%{$s->[H]} = ();
	%{$s->[O]} = ();
	$s->[I] = 0;
}

1;
