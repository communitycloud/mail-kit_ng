#!/usr/bin/perl

package Hash::Config;

use strict;
use warnings;

use Hash::Stable;
use base qw(Hand::Parser);

sub configure
{
	$_[0]->{word_chars} .= '_.-';
	$_[0]->{quote_chars} .= '/';
	$_[0]->{comment_style} = '#';
}

sub start
{
	goto &parse_entries;
}

sub parse_entries
{
	my $p = shift;

	my $entries = Hash::Stable->new;

	while (my $e = $p->parse_entry) {
		$entries->{$e->[0]} = $e->[1];
	}

	return $entries;
}

sub parse_entry
{
	my $p = shift;
	$p->skip_spaces;

	my $c = $p->peek_char();
	return unless defined $c;

	if ($c eq '}') {
		$p->error('unexpected <}> outside block')
			unless $p->{inside_block};
		$p->{inside_block}--;
		$p->next_token();
		return;
	}

	my $key = $p->parse_key;
	my $value = $p->parse_value;

	return [$key, $value];
}

sub parse_key
{
	my $p = shift;
	$p->expect('key', 'word');
}

sub block_bracket
{
	my $p = shift;

	$p->skip_spaces();
	my $c = $p->peek_char();

	if (defined ($c) && $c eq '{') {
		$p->next_char();
		return 1;
	}

	return 0;
}

sub array_comma
{
	my $p = shift;

	$p->skip_spaces();
	my $c = $p->peek_char();

	if (defined ($c) && $c eq ',') {
		$p->next_char();
		return 1;
	}

	return 0;
}

sub parse_value
{
	my $p = shift;

	my @value = ();

	while (1) {
		if ($p->block_bracket()) {
			$p->{inside_block}++;
			push @value, $p->parse_entries();
		} else {
			push @value, scalar $p->next_token;
		}

		last unless $p->array_comma();
	}

	return @value > 1 ? [@value] : $value[0];
}

1;
