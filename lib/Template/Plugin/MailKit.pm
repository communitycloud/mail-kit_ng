#!/usr/bin/perl

package Template::Plugin::MailKit;

use strict;
use warnings;

use base qw(Template::Plugin::Procedural);

use Util::String qw(html_escape uri_escape uri_unescape
	text2html);

sub html2text
{
	my @args = @_;
	# utf check by default
	$args[3] = 1
		unless defined $args[3];
	&Util::String::html2text(@args);
}

1;
