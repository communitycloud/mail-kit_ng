#!/usr/bin/perl

package WireAPI;

use strict;
use warnings;

use File::Basename qw(dirname basename);
use lib dirname(__FILE__);

use wireapi;
use cutils;

our $VERSION = '0.04';

my @integer_properties = qw(priority delivery_mode timestamp);
my @string_properties = qw(exchange routing_key content_type content_encoding
correlation_id reply_to expiration message_id type user_id app_id sender_id);
my @special_properties = qw(body headers);
my @content_properties = (@special_properties, @string_properties, @integer_properties);

my %maps;
sub initialise_maps();
initialise_maps();

my $did_icl_system_initialise = 0;

sub icl_system_initialise()
{
	my $argv = cutils::new_charp_array(2);
	cutils::charp_array_setitem($argv, 0, "$0");
	cutils::charp_array_setitem($argv, 1, undef);
	my $argc = 1;
	wireapi::icl_system_initialise($argc, $argv);
	wireapi::icl_console_mode($wireapi::ICL_CONSOLE_QUIET, 1);
	$did_icl_system_initialise ++;
}

sub table2hash($)
{
	my $longstr = shift;
	my $hash = { };

	my $list = wireapi::asl_field_list_new($longstr);
	my $field = wireapi::asl_field_list_first($list);
	while ($field) {
		$hash->{ $field->{name} } = wireapi::asl_field_string($field);
		$field = wireapi::asl_field_list_next($field);
	}

	wireapi::asl_field_list_destroy($list);
	return $hash;
}

sub hash2table($)
{
	my $hash = shift;
	my $list = wireapi::asl_field_list_new(undef);
	for my $name (keys %{$hash}) {
		wireapi::asl_field_new_string($list, $name, "$hash->{$name}");
	}

	my $longstr = wireapi::asl_field_list_flatten($list);
	wireapi::asl_field_list_destroy($list);
	return $longstr;
}

sub options
{
	my ($options, $defaults) = @_;
	map { $_ => exists($options->{$_}) ? $options->{$_} : $defaults->{$_} }
		keys %{$defaults};
}

sub member($@)
{
	my $t = shift;
	for my $m (@_) { return 1 if $t eq $m };
	return 0;
}

sub initialise_maps()
{
	$maps{norm_property} = { };
	$maps{property_setter} = { };
	$maps{integer_property} = { };
	$maps{string_property} = { };
	$maps{special_property} = { };

	for my $prop (@content_properties) {
		$maps{norm_property}->{norm_prop_spelling($prop)} = $prop;

		my $symbol = sprintf('wireapi::amq_content_basic_set_%s', $prop);
		my $setter = do { no strict qw(refs); no warnings qw(once); *{$symbol}{CODE} };
		if (defined $setter) {
			$maps{property_setter}->{$prop} = $setter;
		}

		if (member $prop, @integer_properties) {
			$maps{integer_property}->{$prop} ++;
		}

		if (member $prop, @string_properties) {
			$maps{string_property}->{$prop} ++;
		}

		if (member $prop, @special_properties) {
			$maps{special_property}->{$prop} ++;
		}
	}
}

sub norm_prop_spelling($)
{
	my $s = shift;

	if (exists $maps{prop_spelling}->{$s}) {
		return $maps{prop_spelling}->{$s};
	}

	my $t = lc($s);
	$t =~ s#[-_]##g;

	return $maps{prop_spelling}->{$s} = $t;
}

sub connect
{
	my ($class, $server, $username, $password, %options) = @_;

	icl_system_initialise()
		unless $did_icl_system_initialise;

	my $defaults = {
		virtual_host => "/", trace => 0, timeout => 30,
		instance => basename($0),
	};

	if (!defined $username && !defined $password) {
		$username = $password = "guest";
	}

	my %o = options(\%options, $defaults);

	my $auth = wireapi::amq_client_connection_auth_plain($username, $password);
	my $connection = wireapi::amq_client_connection_new($server, $o{virtual_host}, $auth,
		$o{instance}, $o{trace}, int($o{timeout} * 1000)) or return undef;
	my $session = wireapi::amq_client_session_new($connection)
		or return undef;

	return bless {
		connection => $connection,
		session => $session,
	}, $class;
}

sub disconnect
{
	my $wapi = shift;

	if ($wapi->{session} && !$wapi->{in_signal_handler}) {
		# destroying the session in the middle of our own sigal handler will
		# likely result into a segfault due to locking inconsistency
		wireapi::amq_client_session_destroy($wapi->{session});
		delete $wapi->{session};
	}

	if ($wapi->{connection}) {
		wireapi::amq_client_connection_destroy($wapi->{connection});
		delete $wapi->{connection};
	}
}

sub declare_exchange
{
	my ($wapi, $exchange, $type, %options) = @_;

	my $defaults = {
		ticket => 0, passive => 0, durable => 0, auto_delete => 0, internal => 0,
		arguments => undef,
	};

	my %o = options(\%options, $defaults);
	return wireapi::amq_client_session_exchange_declare($wapi->{session}, $o{ticket}, $exchange, $type,
		$o{passive}, $o{durable}, $o{auto_delete}, $o{internal}, $o{arguments}) == 0;
}

sub declare_queue
{
	my ($wapi, $queue, %options) = @_;

	my $defaults = {
		ticket => 0, passive => 0, durable => 0, auto_delete => 0, exclusive => 1,
		arguments => undef
	};

	my %o = options(\%options, $defaults);

	if ($o{arguments}) {
		$o{arguments} = hash2table($o{arguments});
	}

	my $rc = wireapi::amq_client_session_queue_declare($wapi->{session}, $o{ticket}, $queue,
		$o{passive}, $o{durable}, $o{exclusive}, $o{auto_delete}, $o{arguments});

	if ($o{arguments}) {
		wireapi::icl_longstr_destroy($o{arguments});
	}

	return $rc == 0;
}

sub bind
{
	my ($wapi, $exchange, $routing_key, $queue, %options) = @_;

	my $defaults = {
		ticket => 0, arguments => undef,
	};

	my %o = options(\%options, $defaults);

	return wireapi::amq_client_session_queue_bind($wapi->{session}, $o{ticket}, $queue,
		$exchange, $routing_key, $o{arguments}) == 0;
}

sub consume
{
	my ($wapi, $queue, %options) = @_;

	my $defaults = {
		ticket => 0, exclusive => 1, no_ack => 0, no_local => 1,
		consumer_tag => undef, arguments => undef,
	};

	my %o = options(\%options, $defaults);

	return wireapi::amq_client_session_basic_consume($wapi->{session}, $o{ticket}, $queue,
		$o{consumer_tag}, $o{no_local}, $o{no_ack}, $o{exclusive}, $o{arguments}) == 0;
}

sub arrived
{
	my ($wapi) = @_;

	my $content = wireapi::amq_client_session_basic_arrived($wapi->{session})
		or return undef;
	my $c = { };

	for my $prop (@content_properties) {
		next if $maps{special_property}->{$prop};
		if (defined ($content->{$prop}) && length($content->{$prop})) {
			$c->{$prop} = $content->{$prop}
		}
	}

	if ($content->{body_size}) {
		my $body = cutils::malloc_byte($content->{body_size} + 1);
		wireapi::amq_content_basic_get_body($content, $body, $content->{body_size});
		cutils::byte_array_setitem($body, $content->{body_size}, 0);
		my $string = cutils::unsigned2char($body);
		$c->{body} = $string;
		cutils::free_byte($body);
	}

	if ($content->{headers}) {
		$c->{headers} = table2hash($content->{headers});
	}

	wireapi::amq_content_basic_unlink($content);
	return $c;
}

sub wait
{
	my ($wapi, $timeout) = @_;

	$timeout = 0 unless defined $timeout;
	$timeout = int ($timeout * 1000) if $timeout > 0;
	
	return wireapi::amq_client_session_wait($wapi->{session}, $timeout) == 0;
}

sub content
{
	my ($wapi, $content, $headers) = @_;
	goto &arrived if @_ == 1;

	my $c = wireapi::amq_content_basic_new();

	for my $key (keys %{$content})
	{
		next unless defined $content->{$key};
		my $prop = $maps{norm_property}->{norm_prop_spelling($key)};
		next unless defined $prop;

		my $value = $content->{$key};
		my $setter = $maps{property_setter}->{$prop};
		next unless defined $setter;

		if ($prop eq "body") {
			use bytes;
			my $length = length($value);
			&$setter($c, "$value", $length, undef);
		} elsif ($prop eq "headers") {
			$headers = $value;
		} elsif ($maps{integer_property}->{$prop}) {
			&$setter($c, int($value));
		} elsif ($maps{string_property}->{$prop}) {
			&$setter($c, "$value");
		} else {
			# bad property
		}
	}

	if ($headers) {
		my $table = hash2table($headers);
		wireapi::amq_content_basic_set_headers($c, $table);
		wireapi::icl_longstr_destroy($table);
	}

	return $c;
}

sub publish
{
	my ($wapi, $content, $exchange, $routing_key, %options) = @_;

	my $defaults = {
		ticket => 0, mandatory => 0, immediate => 0, free => 1,
	};

	my %o = options(\%options, $defaults);

	if (ref($content) eq 'HASH') {
		$content = $wapi->content($content);
		$o{free} = 1;
	}

	my $rc = wireapi::amq_client_session_basic_publish($wapi->{session}, $content,
		$o{ticket}, $exchange, $routing_key, $o{mandatory}, $o{immediate}) == 0;
	wireapi::amq_content_basic_unlink($content) if $o{free};
	return $rc;
}

sub acknowledge
{
	my ($wapi, $delivery_tag, $multiple) = @_;

	unless (defined $delivery_tag) {
		$delivery_tag = $wapi->{session}->{delivery_tag};
	}

	return wireapi::amq_client_session_basic_ack($wapi->{session}, int($delivery_tag), $multiple ? 1 : 0) == 0;
}

1;
