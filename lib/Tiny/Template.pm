#!/usr/bin/perl

# Copyright (C) 2009-2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Tiny::Template;

use strict;
use warnings;

use Util::String qw(escape_chars strneq);
use Util::Package qw(get_symbol put_symbol delete_package);
use Util::File qw(find_in_path slurp dirname);
use App::Message "template:";

my $seqno = 1;
my $cache = {};

sub new
{
	my ($proto, %arg) = @_;

	my $t = bless {
		code => $arg{code} || '[%,%]',
		echo => $arg{echo} || '[=,=]',
		path => $arg{path} || [],
		interpolate => $arg{interpolate} || 0,
		idx => $seqno ++,
	}, ref $proto || $proto;

	if (defined $arg{file}) {
		$t->compile_file($arg{file});
	} elsif (defined $arg{text}) {
		$t->compile_text($arg{text});
	}

	return $t;
}

sub package
{
	my $t = shift;
	return sprintf "%s::GEN%d", __PACKAGE__, $t->{idx};
}

sub parse_init
{
	my ($t, $text) = @_;

	$t->{pos} = 0;
	$t->{text} = $text;
	$t->{tokens} = [];
	$t->{code} = [split ',', $t->{code}, 2] unless ref $t->{code};
	$t->{echo} = [split ',', $t->{echo}, 2] unless ref $t->{echo};
}

sub parse_interpolate
{
	my $t = shift;
	my $tok = pop @{$t->{tokens}};
	my @toks = split /(\$[\$a-zA-Z0-9_\[\]\{\}]+)/, $tok->{text};

	unless (@toks) {
		push @{$t->{tokens}}, $tok;
		return;
	}

	for (my $i = 0; $i < @toks; $i ++) {
		push @{$t->{tokens}}, {
			type => $i % 2 == 0 ? 'data' : 'echo',
			text => $toks[$i],
		} if defined $toks[$i];
	}
}

sub parse_emit
{
	my ($t, $type, $offset) = @_;
	my $length = ($offset || length $t->{text}) - $t->{pos};
	push @{$t->{tokens}}, { type => $type,
		text => substr($t->{text}, $t->{pos}, $length) };
	$t->parse_interpolate if $t->{interpolate} and $type eq "data";
	$t->{pos} += $length;
}

sub parse_next
{
	my $t = shift;

	my $i1 = index($t->{text}, $t->{code}->[0], $t->{pos});
	my $i2 = index($t->{text}, $t->{echo}->[0], $t->{pos});

	if ($i1 == $t->{pos}) {
		$t->parse_block("code");
	} elsif ($i2 == $t->{pos}) {
		$t->parse_block("echo");
	} elsif ($i1 == -1 && $i2 == -1) {
		$t->parse_emit("data");
	} else {
		my $i = $i1 == -1 ? $i2 : $i2 == -1 ? $i1
			: $i1 < $i2 ? $i1 : $i2;
		$t->parse_emit("data", $i);
	}

	return $t->{pos} < length $t->{text};
}

sub parse_block
{
	my ($t, $block) = @_;
	$t->{pos} += length $t->{$block}->[0];
	my $i = index($t->{text}, $t->{$block}->[1], $t->{pos});
	if ($i < 0) {
		$t->parse_emit($block);
	} else {
		$t->parse_emit($block, $i);
		$t->{pos} += length $t->{$block}->[1];
	}
}

sub parse
{
	my ($t, $text) = @_;
	$t->parse_init($text);
	1 while $t->parse_next;
}

sub include
{
	my ($t, $file, %stash) = @_;
	$t->new(
		code => $t->{code},
		echo => $t->{echo},
		path => [dirname($t->{file} || "."), @{$t->{path}}],
		file => $file,
	)->execute(%stash);
}

sub compile_file
{
	my ($t, $name) = @_;

	my $file = find_in_path($name, $t->{path})
		or error("cannot find template %s in path (%{pa})", $name, $t->{path});

	$t->{file} = $file;

	my @stat = stat($file)
		or error("cannot stat template %s: %s", $file, $!);
	my $mtime = $stat[9];

	if ($cache->{$file} && $cache->{$file}->{mtime} == $mtime) {
		# cache is fresh, simply use the idx
		return $t->{idx} = $cache->{$file}->{idx};
	}

	if ($cache->{$file}) {
		# cache is old, delete template package
		local $t->{idx} = $cache->{$file}->{idx};
		delete_package($t->package);
	}

	my $text = slurp($file);
	$t->compile_text($text);

	# cache it for later use
	$cache->{$file} = { mtime => $mtime, idx => $t->{idx} };
}

sub compile_text
{
	my ($t, $text) = @_;

	$t->parse($text);

	my $perl = sprintf "package %s;\n", $t->package;

	if ($t->{vars}) {
		$perl .= "use vars qw($t->{vars});\n";
	} else {
		$perl .= "no strict qw(vars);\n";
	}

	if ($t->{subs}) {
		$perl .= "use vars qw($t->{subs});\n";
	} else {
		$perl .= "no strict qw(subs);\n";
	}

	$perl .= "sub _template {\n";

	for my $tok (@{$t->{tokens}}) {
		if ($tok->{type} eq "data") {
			$perl .= sprintf("print '%s';",
				escape_chars($tok->{text}, "'"));
		} elsif ($tok->{type} eq "code") {
			$perl .= $tok->{text};
			$perl .= ";";
		} elsif ($tok->{type} eq "echo") {
			$perl .= sprintf('print do { %s };',
				$tok->{text});
		}
		$perl .= "\n";
	}

	$perl .= "};\n;1";

	{
		no warnings;
		eval($perl) or error("compilation failed: %s", $@);
	}
}

sub execute
{
	my ($t, %stash) = @_;

	my $pkg = $t->package;

	put_symbol($pkg, template => $t);
	put_symbol($pkg, include => sub { $t->include(@_, %stash) });

	while (my ($name, $ref) = each %{stash}) {
		put_symbol($pkg, $name, $ref);
	}

	my $entry = "_template";
	my $sub = get_symbol($pkg, $entry, "code")
		or internal_error("cannot find %q in %q for execution", $entry, $pkg);

	eval { $sub->(); 1 };
}

sub capture
{
	my ($t, %stash) = @_;
	my $rv;
	my $capture = Util::String::capture { $rv = $t->execute(%stash) };
	return undef unless defined $rv;
	return defined $capture ? $capture : "";
}

1;
