#!/usr/bin/perl

# Copyright (C) 2009-2010 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package Statement::Filter;

use strict;
use warnings;

use Filter::Util::Call;

sub import
{
	my ($pkg, @args) = @_;

	if (@args == 0 && $ENV{STATEMENT_FILTER}) {
		@args = split(' +', $ENV{STATEMENT_FILTER});
	}

	return unless @args;

	my $filter = { };

	for my $a (@args) {
		my $flag = substr($a, 0, 1);
		my $name = substr($a, 1);
		if ($flag eq "+") {
			$filter->{flags}->{$name} = 1;
		} elsif ($flag eq "-") {
			$filter->{flags}->{$name} = 0;
		} else {
			die "Invalid flag -- Usage: $pkg [+-]statement, ..\n";
		}
	}

	my $re = "^\\s*(#)?\\s*\\b(\Q" . join("\E|\Q", keys(%{$filter->{flags}})) . "\E)\\b";
	$filter->{re} = qr/$re/;

	filter_add(bless $filter);
}

sub filter
{
	my $filter = shift;
	my $status = filter_read();

	if ($status > 0) {
		s($filter->{re})($filter->substitute($1, $2))eg;
	}

	return $status
}

sub substitute
{
	my ($filter, $comment, $name) = @_;

	if ($filter->{flags}->{$name} && $comment) {
		return $name;
	}

	if (!$filter->{flags}->{$name} && !$comment) {
		return '#' . $name;
	}

	return $name unless $comment;
	return $comment.$name;
}

1;
