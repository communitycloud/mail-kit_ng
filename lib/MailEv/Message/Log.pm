#!/usr/bin/perl

package MailEv::Message::Log;

use strict;
use warnings;

use MailEv::Message;
use Exporter qw(import);
use POSIX;

our @EXPORT = qw(log_open);

our $handle;

sub log_open($;$)
{
	my ($filename, $level) = @_;
	if ($filename eq '-') {
		open $handle, '>&STDOUT',
			or error("Failed to dup STDOUT: $!");
	} else {
		open $handle, '>>', $filename
			or error("Failed to open log: $filename: $!");
	}
	add_message_sink(\&log_message, $level || "info");
}

sub log_message($$)
{
	my ($type, $message) = @_;
	printf { $handle } "%s %s: %s\n", POSIX::strftime("%b %d %T", localtime), $type, $message;
	$handle->flush;
}

1;
