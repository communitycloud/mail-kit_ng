#!/usr/bin/perl

package MailEv::Storage::Union;

use strict;
use warnings;

use base qw(MailEv::Storage);
use App::Message "storage (union):";
use Util::List qw(list_remove list_unique);
use Util::File qw(slurp);
use File::Basename qw(dirname);
use MailEv::Util qw(parse_uri);
use MailEv::Storage qw(storage_open);

sub read_union_file($)
{
	my $file = shift;
	my @sources;
	my $dirname = dirname($file);
	for my $line (slurp($file)) {
		chomp $line;
		next if $line =~ /^\s*(#|$)/;
		$line = $dirname . '/' . $line if substr $line, 0, 1 ne "/";
		push @sources, $line;
	}
	return @sources;
}

sub check
{
	my $s = shift;
	unless (@{$s->{handle}}) {
		$s->open_next() or error("cannot open union head");
	}
}

sub OPEN
{
	my ($s, $source) = @_;

	my @sources;

	my $sep = $s->{separator} || ",";
	my @spec = split $sep, $source;

	while (my $spec = shift @spec) {
		$spec =~ /\.union$/
			? unshift @spec, read_union_file($spec)
			: push @sources, reverse glob $spec;
	}

	$s->{uconfig} = { %{$s} };
	$s->{sources} = [ list_unique @sources ];

	return $s->{handle} = [];
}

sub CLOSE
{
	my $s = shift;
	map $_->close, @{$s->{handle}};
}

sub each_source
{
	my $s = shift;
	return 0 .. $#{$s->{sources}};
}

sub handle_of
{
	my ($s, $idx) = @_;
	$s->open_next unless exists $s->{handle}->[$idx];
	return $s->{handle}->[$idx];
}

sub open_next
{
	my $s = shift;

	if (@{$s->{handle}} < @{$s->{sources}}) {
		local $App::Message::level = "warning"
			unless message_level("debug");
		my $source = $s->{sources}->[@{$s->{handle}}];
		my $handle = storage_open($source, %{$s->{uconfig}});
		push @{$s->{handle}}, $handle;
		return $handle;
	}

	return undef;
}

sub put
{
	shift->{handle}->[0]->put(@_);
}

sub get
{
	my ($s, @pk) = @_;

	for my $i ($s->each_source) {
		my $h = $s->handle_of($i);
		if (my $ev = $h->get(@pk)) {
			return $ev;
		}
	}
	return undef;
}

sub ITERINIT
{
	my $s = shift;
	$s->{iter} = 0;
	$s->{handle}->[0]->iterinit();
}

sub ITERNEXT
{
	my $s = shift;

	if (my $ev = $s->{handle}->[$s->{iter}]->iternext()) {
		return $ev;
	}

	if (my $h = $s->handle_of($s->{iter}+1)) {
		$s->{iter}++;
		$h->iterinit();
		return $s->ITERNEXT;
	}

	return undef;
}

sub SEARCH
{
	my ($s, $ev, $limit, $offset) = @_;

	my @r;

	my @sources = $s->each_source();

	while ((!defined $limit || $limit > 0) && @sources) {
		my $h = $s->handle_of(shift @sources);
		my $r = $h->SEARCH($ev, $limit, $offset);
		if (@$r) {
			push @r, @$r;
			$limit -= @$r if defined $limit;
			$offset = 0;
		} elsif ($offset) {
			my $c = $h->COUNT($ev);
			$offset -= $c;
			$offset = 0 if $offset < 0;
		}
	}

	return \@r;
}

sub COUNT
{
	my ($s, $ev, $limit) = @_;

	my $count = 0;

	for my $i ($s->each_source) {
		my $c = $s->handle_of($i)->COUNT($ev, $limit);
		return ".." if $limit && $limit == $c;
		$count += $c;
	}

	return $count;
}

sub DELETE
{
	my ($s, @pk) = @_;

	for my $i ($s->each_source) {
		if ($s->handle_of($i)->DELETE(@pk)) {
			return 1;
		}
	}

	return 0;
}

1;
