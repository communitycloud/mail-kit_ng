#!/usr/bin/perl

package MailEv::Storage::TokyoTyrant;

use strict;
use warnings;

use MailEv::Message;
use base qw(MailEv::Storage::Tokyo);
use TokyoTyrant;
use Cwd qw(abs_path);

sub init
{
	my $s = shift;
	# allow specifying "source" as "server"
	$s->{source} = $s->{server}
		if exists $s->{server};
	
	# autostart by default
	$s->{feat}->{autostart}++
		unless exists $s->{feat}->{autostart};

	# enable path optimisation by default
	$s->{feat}->{path} ++
		unless exists $s->{feat}->{path};
}

sub inform
{
	my ($s, $action) = @_;
	if ($action eq "open") {
		info("Connected to TokyoTyrant server at $s->{source}");
	} elsif ($action eq "close") {
		info("Closed connection to TokyoTyrant server at $s->{source}");
	} else {
		$s->SUPER::inform($action);
	}
}

sub OPEN
{
	my ($s, $source) = @_;

	if ($s->{feat}->{autostart} && $s->{cabinet}) {
		# start a new tyrant server if necessary
		my ($cabinet, $pid_file, $log_file, $params)
			= @{$s}{qw(cabinet pid_file log_file params)};
		unless ($pid_file) {
			error("no tyrant server pid file");
		}

		if (my $tdb = $s->tyrant_open($source)) {
			return $tdb;
		} else {
			$s->tyrant_start($source, $cabinet, $pid_file, $log_file, $params);
		}
	}

	return $s->tyrant_open($source);
}

sub tyrant_open
{
	my ($s, $source) = @_;

	my ($host, $port) = split(':', $source);
	$port ||= 1978;

	my $tdb = TokyoTyrant::RDBTBL->new;
	$tdb->open($host, $port)
		or return undef;

	return $tdb;
}

sub tyrant_start
{
	my ($s, $server, $cabinet, $pid_file, $log_file, $params) = @_;

	my ($host, $port) = split(':', $server);
	unless ($host eq "localhost") {
		error("Cannot start tyrant on a non local host");
	}

	unless (substr($cabinet, -4) eq '.tct') {
		error("Cabinet file ($cabinet) should end in .tct");
	}

	$port ||= 1978;

	my %indices = $s->indices();

	my $params_string = "";
	map $params_string .= "#$_", @{$params} if $params;
	map $params_string .= sprintf("#idx=%s:%s", $_, $indices{$_}), keys %indices;

	my @tyrant = ('ttserver', '-dmn',
		-pid => abs_path($pid_file),
		-ext => abs_path('misc/tyrant-ext.lua'),
		-port => $port,
		($log_file ? (-log => abs_path($log_file)) : ()),
		abs_path($cabinet) . $params_string);
	no warnings qw(exec);
	system (@tyrant) == 0
		or error("Failed to run ttserver: $?");
	info("Started ttserver cabinet=$cabinet, port=$port, pid=$pid_file, params=$params_string");
	sleep(1);
}

sub check
{
	my $s = shift;
	$s->check_extension();
	$s->index("check");
}

sub check_extension
{
	my $s = shift;
	my $extv = $s->tdb->ext('mailev_ext_version');
	# debug("storage - checking mail-ev extension");
	error("Tyrant mail-ev extension not found on $s->{server}") unless $extv;
	error("Tyrant server built without lua support") if $extv =~ /mailev_ext_version::$/;
	error("Tyrant mail-ev extension too old ($extv < 0.02)") if $extv < 0.02;
}

sub queryclass
{
	"TokyoTyrant::RDBQRY";
}

1;
