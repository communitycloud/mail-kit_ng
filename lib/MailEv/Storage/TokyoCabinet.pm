#!/usr/bin/perl

package MailEv::Storage::TokyoCabinet;

use strict;
use warnings;

use MailEv::Message;
use base qw(MailEv::Storage::Tokyo);
use TokyoCabinet;

sub init
{
	my $s = shift;
	# allow specifying "source" as "cabinet"
	$s->{source} = $s->{cabinet}
		if exists $s->{cabinet};
	# enable path optimisation by default
	$s->{feat}->{path} ++
		unless exists $s->{feat}->{path};
}

sub inform
{
	my ($s, $action) = @_;
	if ($action eq "open") {
		info("Using TokyoCabinet database from $s->{source}");
	} elsif ($action eq "close") {
		info("Closing TokyoCabinet database at $s->{source}");
	} else {
		$s->SUPER::inform($action);
	}
}

sub LOCK
{
	my ($s, $level) = @_;

	unless ($level) {
		$s->tdb->close();
	} elsif ($level == 1) {
		$s->tdb->open($s->{source}, $s->tdb->OREADER)
			or $s->tdb_error("failed to open $s->{source} for reading");
	} elsif ($level == 2) {
		$s->tdb->open($s->{source}, $s->tdb->OWRITER)
			or $s->tdb_error("failed to open $s->{source} for writing");
	}
}

sub index
{
	my ($s, $op) = @_;
	$s->lock(2);
	$s->SUPER::index($op);
	$s->lock(0);
}

sub OPEN
{
	my ($s, $source) = @_;
	my $tdb = TokyoCabinet::TDB->new;
	$tdb->open($source, $tdb->OWRITER | $tdb->OCREAT)
		or return undef; 
	return $tdb;
}

sub check
{
	my $s = shift;
	$s->index("check");
}

sub queryclass
{
	"TokyoCabinet::TDBQRY";
}

1;
