#!/usr/bin/perl

package MailEv::Storage::SQLite;

use strict;
use warnings;

use DBI;
use DBD::SQLite;
use App::Message "storage (sqlite):";

use base qw(MailEv::Storage);

my %column_type = qw(
msg_id char list_id char address char server char
timestamp int status char comment char domain char);
my @columns = grep exists $column_type{$_}, @MailEv::Event::fields;

sub OPEN
{
	my ($s, $dbfile) = @_;

	my $dbh = DBI->connect("dbi:SQLite:dbname=$dbfile", "", "",
		{ PrintError => 0, RaiseError => 0 })
		or return undef;
	$dbh->do("pragma synchronous = $s->{sync}")
		if defined $s->{sync};
	return $dbh;
}

sub dbh
{
	$_[0]->{handle};
}

sub dbi_error
{
	my ($s, $prefix) = @_;
	error("db error: %s: %s", $prefix, $s->dbh->errstr);
}

sub create_structure
{
	my $s = shift;

	my $pk = join (',', @MailEv::Event::pk);

	my $columns = join ", ", map "$_ $column_type{$_}", @columns;

	my $sql = "create table if not exists ev ($columns, PRIMARY KEY ($pk));";

	$s->dbh->do($sql)
		or $s->dbi_error("failed to execute sqlite structure");
}

sub query_indices
{
	my $s = shift;
	my $sql = "select name, sql from sqlite_master
	where tbl_name = 'ev' and type = 'index'";
	my $sth = $s->dbh->prepare($sql);
	unless ($sth && $sth->execute) {
		$s->dbi_error("get indices failed");
	}

	my $indices = {};

	while (my $r = $sth->fetchrow_hashref) {
		next unless defined $r->{sql};
		$indices->{$r->{name}} = lc $r->{sql};
	}

	$sth->finish;
	return $indices;
}

sub prepare_statement
{
	$_[0]->{feat}->{stcache}
		? $_[0]->dbh->prepare_cached($_[1])
		: $_[0]->dbh->prepare($_[1]);
}

sub finish_statement
{
	$_[1]->finish unless $_[0]->{feat}->{stcache};
}

sub check
{
	my $s = shift;
	unless (grep $_ eq "ev", $s->dbh->tables) {
		$s->create_structure();
	}
	if ($s->{feat}->{strict}) {
		$s->index("check");
	}
}

sub event_build_clause($)
{
	my $ev = shift;

	my @clauses;
	my @params;

	while (my ($k, $v) = each %{$ev}) {
		my $clause = "";

		$v =~ s#^\+##; # used by TokyoCabinet, no index
		$clause = "NOT " if $v =~ s#^!##;

		my $op;

		if (ref $v eq 'ARRAY') {
			$op = "IN";
		} elsif ($v =~ s#^=##) {
			$op = "=";
		} elsif ($v =~ s#^~##) {
			$op = "REGEXP";
		} elsif ($v =~ s#^>##) {
			$op = ">";
		} elsif ($v =~ s#^>=##) {
			$op = ">=";
		} elsif ($v =~ s#^<##) {
			$op = "<";
		} elsif ($v =~ s#^<=##) {
			$op = "<=";
		} else {
			$op = "=";
		}

		if (ref $v eq 'ARRAY') {
			$clause .= sprintf("%s %s (%s)", $k, $op,
				join(', ', ('?') x scalar @{$v}));
			push @params, @{$v};
		} else {
			$clause .= "$k $op ?";
			push @params, $v;
		}

		push @clauses, $clause;
	}

	@clauses = (1) unless @clauses;

	my $clause = join(' AND ', @clauses);
	return $clause, @params;
}

sub PUT
{
	my ($s, $ev) = @_;

	my $columns = join (',', @columns);
	my $values = join (',', ('?') x @columns);
	my @params = @{$ev}{@MailEv::Event::fields};

	my $sql = "insert or $s->{update} into ev ($columns) values ($values)";
	my $sth = $s->prepare_statement($sql);

	unless ($sth && $sth->execute(@params)) {
		$s->dbi_error("PUT failed");
	}

	$s->finish_statement($sth);
}

sub GET
{
	my ($s, @pk) = @_;

	my $ev = {};

	for (my $i = 0; $i < @MailEv::Event::pk; $i++) {
		$ev->{$MailEv::Event::pk[$i]} = $pk[$i];
	}

	my $r = $s->SEARCH($ev, 1);
	return $r->[0];
}

sub DELETE
{
	my ($s, @pk) = @_;

	my @clause;
	my @params;

	for (my $i = 0; $i < @MailEv::Event::pk; $i++) {
		my ($key, $value) = ($MailEv::Event::pk[$i], $pk[$i]);
		push @clause, "$key = ?";
		push @params, $value;
	}

	my $clause = join ' AND ', @clause;
	my $sql = "delete from ev where $clause";
	my $sth = $s->prepare_statement($sql);

	unless ($sth && $sth->execute(@params)) {
		$s->dbi_error("delete failed");
	}

	my $r = $sth->rows;

	$s->finish_statement($sth);

	return $r;
}

sub ITERINIT
{
	my $s = shift;

	my $sql = "select * from ev order by timestamp desc";
	my $sth = $s->dbh->prepare($sql);

	unless ($sth && $sth->execute) {
		$s->dbi_error("ITERINIT failed");
	}

	$s->{iter} = $sth;
}

sub ITERNEXT
{
	my $s = shift;
	return $s->{iter}->fetchrow_hashref();
}

sub SEARCH
{
	my ($s, $ev, $limit, $offset) = @_;

	my ($clause, @params) = event_build_clause($ev);

	my $sql = "select * from ev where $clause order by timestamp desc";

	if ($limit && $offset) {
		$sql .= " limit $offset, $limit";
	} elsif ($limit) {
		$sql .= " limit $limit";
	}

	my $sth = $s->prepare_statement($sql);

	unless ($sth && $sth->execute(@params)) {
		$s->dbi_error("search failed");
	}

	my $r = [];

	while (my $ev = $sth->fetchrow_hashref) {
		push @$r, $ev;
	}

	$s->finish_statement($sth);

	return $r;
}

sub COUNT
{
	my ($s, $ev, $limit) = @_;

	my ($clause, @params) = event_build_clause($ev);

	my $sql = "select count(*) from ev where $clause";

	if ($limit) {
		$sql .= " limit $limit";
	}

	my $sth = $s->prepare_statement($sql);

	unless ($sth && $sth->execute(@params)) {
		$s->dbi_error("search failed");
	}

	my $r = $sth->fetchrow_arrayref;
	$s->finish_statement($sth);

	return $r->[0];
}

sub INDEX
{
	my ($s, $op_name) = @_;

	my %struct = (
		user => "list_id, address, timestamp desc",
		message => "msg_id, timestamp desc",
		status => "status, msg_id, timestamp desc",
		timestamp => "timestamp desc",
	);

	my $indices = $s->query_indices();

	if ($op_name eq "create" || $op_name eq "check") {
		while (my ($index, $columns) = each %struct) {
			my $sql = sprintf("create index $index on ev ($columns)");
			unless (exists $indices->{$index}) {
				info("create missing index `%s'", $index);
			} elsif ($sql ne $indices->{$index}) {
				info("upgrade old index `%s'", $index);
				$s->dbh->do("drop index $index")
					or $s->dbi_error("failed to drop index `$index'");
			} else {
				next;
			}
			$s->dbh->do($sql) or $s->dbi_error("failed to create index `$index'");
		}
	} elsif ($op_name eq "drop") {
		while (my ($index, $columns) = each %struct) {
			next unless exists $indices->{$index};
			$s->dbh->do("drop index $index") or $s->dbi_error("failed to drop index `$index'");
		}
	}
}

sub transaction
{
	my ($s, $op) = @_;

	if ($op eq "begin") {
		$s->dbh->begin_work;
	} elsif ($op eq "commit") {
		$s->dbh->commit;
	} elsif ($op eq "abort") {
		$s->dbh->rollback;
	}
}

sub CLOSE
{
	my $s = shift;
	$s->dbh->disconnect;
}

1;
