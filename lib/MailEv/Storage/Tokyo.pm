#!/usr/bin/perl

package MailEv::Storage::Tokyo;

use strict;
use warnings;

use MailEv::Message;
use MailEv::Event;
use MailEv::Statement::Filter;
use base qw(MailEv::Storage);

sub indices
{
	my $s = shift;

	my %indices = (
		msg_id  => "lexical",
		list_id => "lexical",
		address => "lexical",
		server  => "lexical",
		timestamp => "decimal",
		status  => "lexical",
	);

	if ($s->{feat}->{path}) {
		$indices{path} = "lexical";
	}

	return %indices;
}

sub check
{
	my $s = shift;
	if ($s->{feat}->{strict}) {
		$s->index("check");
	}
}

sub tdb
{
	$_[0]->{handle};
}

sub tdb_error
{
	my ($s, $prefix) = @_;
	error("tdb error: %s: %s", $prefix, $s->tdb_errstr);
}

sub tdb_errstr
{
	my $s = shift;
	my $ecode = $s->tdb->ecode();
	return $s->tdb->errmsg($ecode);
}

sub INDEX
{
	my ($s, $op_name) = @_;

	my $tdb = $s->tdb;
	my $op = 0;

	if ($op_name eq "check") {
		$op = $tdb->ITKEEP;
	} elsif ($op_name eq "drop") {
		$op = $tdb->ITVOID;
	} elsif ($op_name eq "optimize") {
		$op = $tdb->ITOPT;
	} elsif ($op_name eq "create") {
		$op = 0;
	} else {
		error("invalid tokyo index op: $op_name");
	}

	my %indices = $s->indices();
	# debug("storage - $op_name indices ...");
	while (my ($column, $type) = each %indices) {
		my $ctype = uc("IT$type");
		# debug("storage -    %s (%s)", $column, $type);
		unless ($tdb->setindex($column, $tdb->$ctype | $op)) {
			$s->tdb_error("failed to build index ($column, $type)")
				unless $tdb->ecode == $tdb->EKEEP || $tdb->ecode == $tdb->EMISC;
		}
	}
	# debug("storage - done.");
}

sub PUT
{
	my ($s, $ev) = @_;

	my $key = "@{$ev}{@MailEv::Event::pk}";

	$s->{update} eq "ignore"
		? $s->tdb->putkeep($key, $ev)
		: $s->tdb->put($key, $ev)
	or $s->tdb_error("failed to put record: $key");
}

sub GET
{
	my ($s, @pk) = @_;
	my $ev = $s->tdb->get("@pk")
		or error("failed to retrieve record (@pk)");
	MailEv::Event::crop($ev);
	return $ev;
}

sub ITERINIT
{
	my $s = shift;
	$s->tdb->iterinit;
}

sub ITERNEXT
{
	my $s = shift;
	my $pk = $s->tdb->iternext;
	return undef unless defined $pk;
	return $s->ITERNEXT if substr($pk, 0, 1) eq ".";
	return $s->GET($pk);
}

sub SEARCH
{
	my ($s, $ev, $limit, $offset) = @_;

	my $tdb = $s->tdb;
	my $result;

	my $qry = $s->query($ev, $limit, $offset);
	$qry->setorder("timestamp", $qry->QONUMDESC);
	return [ map $s->tdb->get($_), @{$qry->search} ];

}

sub queryclass
{
	internal_error "$_[0]: Virtual queryclass";
}

sub query
{
	my ($s, $ev, $limit, $offset) = @_;

	my $qry = $s->queryclass->new($s->tdb);

	if ($s->{feat}->{path} && exists $ev->{list_id})
	{
		if (exists $ev->{address} && exists $ev->{msg_id}) {
			$ev->{path} = sprintf("/%s/%s/%s",
				delete $ev->{list_id},
				delete $ev->{address},
				delete $ev->{msg_id});
		} elsif (exists $ev->{address}) {
			$ev->{path} = sprintf("^/%s/%s/",
				delete $ev->{list_id},
				delete $ev->{address});
		}
	}

	while (my ($k, $v) = each %{$ev}) {
		my $op = 0;

		$op |= $qry->QCNOIDX if ($v =~ s#^\+##);
		$op |= $qry->QCNEGATE if ($v =~ s#^!##);

		if ($v =~ s#^=##) {
			$op |= $qry->QCSTREQ;
		} elsif ($v =~ s#^~##) {
			$op |= $qry->QCSTRRX;
		} elsif ($v =~ s#^>##) {
			$op |= $qry->QCNUMGT;
		} elsif ($v =~ s#^>=##) {
			$op |= $qry->QCNUMGE;
		} elsif ($v =~ s#^<##) {
			$op |= $qry->QCNUMLT;
		} elsif ($v =~ s#^<=##) {
			$op |= $qry->QCNUMLE;
		} elsif ($v =~ s#^\^##) {
			$op |= $qry->QCSTRBW;
		} elsif ($v =~ s#^\$##) {
			$op |= $qry->QCSTREW;
		} else {
			$op |= $qry->QCSTREQ;
		}

		$qry->addcond($k, $op, $v);
	}

	$qry->setlimit($limit, $offset);

	return $qry;
}

sub CLOSE
{
	my $s = shift;
	$s->tdb->close();
}

1;
