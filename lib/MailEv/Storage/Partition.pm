#!/usr/bin/perl

package MailEv::Storage::Partition;

use strict;
use warnings;

use base qw(MailEv::Storage);
use App::Message "storage (part):";
use Util::File qw(slurp);
use Util::String qw(str_replace);
use File::Basename qw(basename dirname);
use MailEv::Storage qw(storage_open);
use MailEv::Event qw(mailev_is_final);
use Hash::Stable;

sub open_partition
{
	my ($s, $part) = @_;

	my $p = $s->{partitions}->{$part}
		= storage_open($s->{directory} . '/' . $part, %{$s->{config}});

	# Sort partitions in reverse chronological order
	(tied %{$s->{partitions}})->sort(sub ($$) { $_[1] cmp $_[0] });

	$p;
}

sub get_partition
{
	$_[0]->{partitions}->{$_[1]} || $_[0]->open_partition($_[1]);
}

sub format_pattern($$)
{
	my @time = gmtime($_[1]);
	return str_replace($_[0],
		{ '%y' => $time[5] + 1900,
		  '%m' => sprintf '%02d', $time[4] + 1,
	  	  '%d' => sprintf '%02d', $time[3] });
}

sub previous_partition($$)
{
	my @keys = keys %{$_[0]};
	return undef unless @keys > 1;
	my $i = (tied %{$_[0]})->index_of($_[1]);
	if ($i < $#keys) {
		return $keys[$i+1];
	} else {
		return undef;
	}
}

sub OPEN
{
	my ($s, $source) = @_;

	# Keep partitions in an ordered hash
	$s->{partitions} = Hash::Stable->new();

	# Copy self for subsequent storage_open()
	$s->{config} = { %{$s} };

	# Determite partition directory and pattern
	$s->{directory} = dirname($source);
	if ($source =~ /\.part$/) {
		($s->{pattern}) = slurp($source);
		chomp $s->{pattern};
	} else {
		$s->{pattern} = basename($source);
	}

	# Glob the pattern and open partitions
	(my $glob = $s->{pattern}) =~ s/%./\*/g;
	for my $p (glob ("$s->{directory}/$glob")) {
		$s->open_partition(basename($p));
	}

	1;
}

sub CLOSE
{
	my $s = shift;
	map $_->close, values %{$s->{partitions}};
}

sub put
{
	my ($s, $ev) = @_;

	my $partition = format_pattern($s->{pattern}, $ev->{timestamp});
	$s->get_partition($partition)->put($ev);

	if (mailev_is_final($ev)) {
		if (my $prev = previous_partition($s->{partitions}, $partition)) {
			$s->get_partition($prev)->delete($ev);
		}
	}
}

sub get
{
	my ($s, @pk) = @_;

	for my $h (values %{$s->{partitions}}) {
		if (my $ev = $h->get(@pk)) {
			return $ev;
		}
	}
	return undef;
}

sub SEARCH
{
	my ($s, $ev, $limit, $offset) = @_;

	my @r;

	my @handles = values %{$s->{partitions}};

	while ((!defined $limit || $limit > 0) && @handles) {
		my $h = shift @handles;
		my $r = $h->SEARCH($ev, $limit, $offset);
		if (@$r) {
			push @r, @$r;
			$limit -= @$r if defined $limit;
			$offset = 0;
		} elsif ($offset) {
			my $c = $h->COUNT($ev);
			$offset -= $c;
			$offset = 0 if $offset < 0;
		}
	}

	return \@r;
}

sub ITERINIT
{
	my $s = shift;
	if (@{$s->{iter} = [values %{$s->{partitions}}]}) {
		$s->{iter}->[0]->iterinit();
	}
}

sub ITERNEXT
{
	my $s = shift;

	if (my $ev = $s->{iter}->[0]->iternext()) {
		return $ev;
	}

	shift @{$s->{iter}};

	if (@{$s->{iter}}) {
		$s->{iter}->[0]->iterinit();
		return $s->ITERNEXT;
	}

	return undef;
}

sub COUNT
{
	my ($s, $ev, $limit) = @_;

	my $count = 0;

	for my $h (values %{$s->{partitions}}) {
		my $c = $h->COUNT($ev, $limit);
		return ".." if $limit && $limit == $c;
		$count += $c;
	}

	return $count;
}

sub DELETE
{
	my ($s, @pk) = @_;

	for my $h (values %{$s->{partitions}}) {
		if ($h->DELETE(@pk)) {
			return 1;
		}
	}

	return 0;
}

1;
