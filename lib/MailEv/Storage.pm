#!/usr/bin/perl

package MailEv::Storage;

use strict;
use warnings;

use App::Message "storage:";
use MailEv::Event qw(mailev_pk);
use MailEv::Util qw(parse_uri);

use Exporter qw(import);
our @EXPORT_OK = qw(storage_open);

sub storage_open($;%)
{
	my ($uri, %extra) = @_;

	my $s = parse_uri($uri)
		or error("invalid URI %s", $uri);

	my $type = get_storage_class($s)
		or error("cannot determine storage class from %s", $uri);

	my %config = (%extra, source => $s->{path});
	for my $k (keys %{$s->{args}}) {
		$config{$k} = $s->{args}->{$k};
	}

	if ($s->{fragment}) {
		for my $feat (split /\s+/, $s->{fragment}) {
			if ($feat =~ s#^!##) {
				$config{feat}->{$feat} = 0
			} else {
				$config{feat}->{$feat} = 1;
			}
		}
	}

	return MailEv::Storage->new($type, %config);
}

sub get_storage_class($)
{
	my $s = shift;

	my $aliases = {
		cabinet => "TokyoCabinet",
		tyrant  => "TokyoTyrant",
		sqlite  => "SQLite",
		union   => "Union",
		part    => "Partition",
	};

	my $extensions = {
		sqlite => "SQLite",
		tdb => "TokyoCabinet",
		union => "Union",
		part => "Partition",
	};

	if ($s->{scheme}) {
		return $aliases->{$s->{scheme}} || $s->{scheme};
	} elsif ($s->{path} && $s->{path} =~ /\.([^.]+)$/) {
		return $extensions->{$1};
	} else {
		return undef;
	}
}

sub new
{
	my ($class, $type, %config) = @_;

	internal_error("undefined class") unless $type;
	my $pkg = $type !~ /::/ ? __PACKAGE__ . '::' . $type : $type;

	eval "require $pkg"
		or internal_error("invalid class: %s", $type);

	my $s = bless \%config, $pkg;

	unless (exists $s->{feat}->{strict}) {
		$s->{feat}->{strict}++;
	}

	if ($s->{transize}) {
		$s->{writecount} = 0;
	}

	$s->init();

	$s->{type} = $type;

	$s->{update} ||= "replace";
	unless ($s->{update} eq "replace" ||
		$s->{update} eq "ignore" ||
		$s->{update} eq "old") {
		error("invalid update mode: %s", $s->{update});
	}

	if (my $source = $s->{source}) {
		$s->open($source);
	}

	return $s;
}

sub init
{
}

sub inform
{
	my ($s, $action) = @_;
	info("$action $s->{type} source=$s->{source}");
}

sub open
{
	my ($s, $source) = @_;
	$s->{handle} = $s->OPEN($source)
		or error("failed to open $s->{type} from \"$source\"");

	$s->{source} = $source;
	$s->lock(0);

	$s->inform("open");
	$s->check();

	if ($s->{transize}) {
		$s->transaction("begin");
	}
}

sub OPEN
{
	internal_error "$_[0]: Virtual OPEN";
}

sub check
{
}

sub lock
{
	my ($s, $level) = @_;

	if (defined $s->{lock} && $s->{lock} == $level) {
		return;
	}

	$s->LOCK($level);
	$s->{lock} = $level;
}

sub LOCK
{
}

sub put
{
	my ($s, $ev) = @_;

	# be strict about the event format (default)
	if ($s->{feat}->{strict}) {
		unless (ref($ev) eq 'HASH') {
			error("bad event data: %s",
				defined $ev ? $ev : "<undef>");
		}

		for my $k (@MailEv::Event::fields) {
			unless (defined $ev->{$k}) {
				error("bad event: missing $k");
			}
		}

		for my $k (keys %{$ev}) {
			unless ($k eq "extra" || $MailEv::Event::field_exists{$k}) {
				error("bad event: extraneous $k");
			}
		}
	}

	if ($s->{feat}->{path}) {
		$ev->{path} = sprintf("/%s/%s/%s",
			$ev->{list_id}, $ev->{address}, $ev->{msg_id});
	}

	if ($s->{update} eq "old") {
		my $t = $s->get($ev);
		if ($t && $t->{timestamp} >= $ev->{timestamp}) {
			return;
		}
	}

	$s->lock(2);
	$s->PUT($ev);
	$s->lock(0);

	if ($s->{transize} && ++$s->{writecount} % $s->{transize} == 0) {
		$s->transaction("commit");
		$s->transaction("begin");
	}
}

sub PUT
{
	internal_error "$_[0]: Virtual PUT";
}

sub get
{
	my ($s, @pk) = @_;
	@pk = mailev_pk($pk[0])
		if @pk == 1 && ref $pk[0];
	$s->lock(1);
	my $ev = $s->GET(@pk);
	$s->lock(0);
	return $ev;
}

sub GET
{
	internal_error "$_[0]: Virtual GET";
}

sub delete
{
	my ($s, @pk) = @_;
	@pk = mailev_pk($pk[0])
		if @pk == 1 && ref $pk[0];
	$s->lock(2);
	my $r = $s->DELETE(@pk);
	$s->lock(0);
	return $r;
}

sub DELETE
{
	internal_error "$_[0]: Virtual DELETE";
}

sub iterinit
{
	my $s = shift;
	$s->lock(1);
	$s->ITERINIT();
}

sub ITERINIT
{
	internal_error "$_[0]: Virtual ITERINIT";
}

sub iternext
{
	my $s = shift;
	return $s->ITERNEXT();
}

sub ITERNEXT
{
	internal_error "$_[0]: Virtual ITERNEXT";
}

sub iterend
{
	my $s = shift;
	$s->lock(0);
}

sub search
{
	my ($s, $ev, $limit, $offset) = @_;

	for my $l (grep defined, $s->{search_limit}, $s->{safety_limit}) {
		if (defined $limit) {
			$limit = $l if $limit > $l;
		} else {
			$limit = $l;
		}
	}

	if (!$ev || $ev eq '*') {
		$ev = {};
	}

	if (ref($ev) eq 'HASH') {
		$s->lock(1);
		my ($count, $r);

		if (delete $ev->{count}) {
			$count = $s->count($ev);
		}

		unless (defined $limit && $limit == 0) {
			$r = $s->SEARCH($ev, $limit, $offset);
		}

		if (defined $count) {
			$r->[0]->{count} = $count;
		}

		$s->lock(0);
		return $r || [];
	}

	if (ref($ev) eq 'ARRAY') {
		return $s->msearch($ev, $limit, $offset);
	}

	error("invalid query (%s)", $ev);
}

sub msearch
{
	my ($s, $mev, $limit, $offset) = @_;

	my $r = [];

	for my $ev (@{$mev}) {
		push @{$r}, @{$s->search($ev, $limit, $offset)};
	}

	return $r;
}

sub SEARCH
{
	internal_error "$_[0]: Virtual SEARCH";
}

sub count
{
	my ($s, $ev) = @_;
	
	return [ map $s->count($_), @{$ev} ]
		if ref $ev eq "ARRAY";

	my $limit = $s->{safety_limit};
	my $count = $s->COUNT($ev, $limit);
	if ($count && $limit && $count == $limit) {
		return "..";
	} else {
		return $count;
	}
}

sub COUNT
{
	internal_error "$_[0]: Virtual COUNT";
}

sub close
{
	my $s = shift;
	$s->inform("close");
	if ($s->{transize}) {
		$s->transaction("commit");
	}
	$s->CLOSE();
	delete $s->{handle};
}

sub CLOSE
{
	internal_error "$_[0]: Virtual CLOSE";
}

sub index
{
	my ($s, $op) = @_;

	if (exists $s->{feat}->{index} && !$s->{feat}->{index}) {
		return;
	}

	$s->INDEX($op);
}

sub INDEX
{
}

sub transaction
{
}

sub DESTROY
{
	my $s = shift;
	if ($s->{transize} && !$s->{writecount}) {
		$s->transaction("abort");
	}
}

1;
