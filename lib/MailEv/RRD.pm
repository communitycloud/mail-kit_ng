#!/usr/bin/perl

# Copyright (C) 2011 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package MailEv::RRD;

use strict;
use warnings;

use RRDs;
use App::Message qw(rrd:);
use Util::List qw(update_hash);
use Tail::DB qw(tail_db_open);
use MailEv::Event;
require POSIX;

my @statuses = @MailEv::Event::statuses;

sub check_error()
{
	if (my $error = RRDs::error()) {
		error($error);
	}
}

sub templates()
{
	return {
		defaults => {
			-title => 'Mail Events',
			-vertical_label => 'per minute',
			-width => 932,
			-height => 346,
			-watermark => '__timestamp__',
			-dynamic_labels => undef,
			-alt_y_grid => undef,
			mult => 60,
			cf => 'AVERAGE',
		},
		clean => {
			-only_graph => undef,
			-width => '',
			-height => '',
			-color => 'shadea#ffffff,shadeb#ffffff,back#ffffff,arrow#ffffff,axis#ffffff',
		},
		day => {
			-vertical_label => 'per hour',
			-start => '-1day',
			mult => 3600,
			update => 300,
		},
		week => {
			-vertical_label => 'per hour',
			-start => '-1week',
			mult => 3600,
			update => 3600,
		},
		month => {
			-vertical_label => 'per day',
			-start => '-1month',
			mult => 24 * 3600,
			update => 12 * 3600,
		},
		year => {
			-vertical_label => 'per day',
			-start => '-1year',
			mult => 24 * 3600,
			update => 24 * 3600,
		},
	}
}

sub new
{
	my ($class, %arg) = @_;
	bless {
		x_points => 932,
		y_points => 346,
		point_per_sample => 2,
		step => 300,
		counters => {},
		templates => templates(),
		%arg,
	}, $class;
}

sub create
{
	my ($r, $rrd, %arg) = @_;

	return $r->new->create($rrd, %arg)
		unless ref $r;

	my $step = $arg{step} || $r->{step};
	my $start = $arg{start} || '-1year';
	# Thanks mailgraph (http://mailgraph.schweikert.ch/) for code below
	my $rows = $r->{x_points} / $r->{point_per_sample};
	my $day_steps = 3600*24 / ($step * $rows);
	my $week_steps = $day_steps*7;
	my $month_steps = $week_steps*5;
	my $year_steps = $month_steps*12;

	$rows = int($rows*1.1);

	RRDs::create
	(
		$rrd, '--start', $start, '--step', $step,
		(map { "DS:$_:COUNTER:".($step*2).":0:U" }
			@statuses),
		(map { my $step = int($_) || 1;
			("RRA:AVERAGE:0.5:$step:$rows", "RRA:MAX:0.5:$step:$rows") }
			$day_steps, $week_steps, $month_steps, $year_steps),
	);

	check_error();

	note("create %s step=%d", $r->{rrd}, $r->{step});

	$r->{rrd} = $rrd;
	$r->{step} = $step;
	$r;
}

sub open
{
	my ($r, $rrd) = @_;

	return $r->new->open($rrd)
		unless ref $r;

	my $info = RRDs::info($rrd);
	check_error();

	my $updated;
	for my $status (@statuses) {
		if ($info->{"ds[$status].last_ds"} ne "U") {
			$r->{counters}->{$status} = $info->{"ds[$status].last_ds"};
			$updated++;
		}
	}

	$r->{rrd} = $rrd;
	$r->{step} = $info->{step};
	$r->{last} = $updated ? $info->{last_update} : 0;
	$r;
}

sub update
{
	my ($r, $ev) = @_;

	my $t = $ev->{timestamp};
	my $time = $t - $t % $r->{step};

	return 0 if $time + $r->{step} < $r->{last};
	$r->{counters}->{$ev->{status}}++;

	return 0 if $time <= $r->{last};

	my @fill;
	if ($r->{last} && $r->{last} < $time - $r->{step}) {
		for (my $t = $r->{last} + $r->{step}; $t < $time; $t+=$r->{step}) {
			push @fill, $t;
		}
	}

	my $data = join ':', map { $r->{counters}->{$_} || 'U' } @statuses;

	RRDs::update($r->{rrd}, map { "$_:$data" } @fill, $time);
	check_error();

	return $r->{last} = $time;
}

sub map_graph_argument
{
	my ($r, $arg, $val, $args) = @_;

	$arg =~ s/^-+//;
	$arg =~ s/_/-/g;

	if (!defined $val || $val eq '<undef>') {
		return "--$arg";
	} elsif (!length $val) {
		return undef;
	} elsif ($arg eq 'color') {
		return map { ("--$arg", uc($_)) }
			split ',', $val;
	}

	if ($arg eq 'watermark') {
		if ($val eq '__timestamp__') {
			$val = POSIX::strftime("%c",
				localtime($args->{timestamp} || $r->{last}));
		}
	}

	("--$arg", $val);
}

sub graph
{
	my ($r, $filename, %arg) = @_;

	if (my $ts = $arg{template}) {
		for my $t (reverse(split(/:/, $ts)), 'defaults') {
			update_hash(\%arg, $r->{templates}->{$t});
		}
	}
	update_hash(\%arg, $r->{templates}->{defaults});

	if ($arg{update} && $arg{timestamp}) {
		if (my $mtime = (stat $filename)[9]) {
			return unless $arg{timestamp} - $mtime >= $arg{update};
		}
	}

	if (my $tz = $arg{tz}) {
		$arg{otz} = $ENV{TZ};
		$ENV{TZ} = $tz;
		POSIX::tzset();
	}

	my @rrd_graph_args = grep defined, map
		$r->map_graph_argument($_, $arg{$_}, \%arg), grep /^-/, keys %arg;

	RRDs::graph
	($filename,
		'--imgformat' => 'PNG',
		'--slope-mode',
		@rrd_graph_args,
		(map { "DEF:d_$_=$r->{rrd}:$_:$arg{cf}" } @statuses),
		(map { "CDEF:c_$_=d_$_,$arg{mult},*" } @statuses),
		"CDEF:c_failed=c_bounced,c_expired,+",
		"LINE2:c_sent#3366cc:Sent",
		"LINE2:c_failed#dc3912:Failed",
		"LINE2:c_deferred#ff9900:Deferred\\n",
	);

	check_error();

	if (my $tz = $arg{otz}) {
		$ENV{TZ} = $tz;
		POSIX::tzset();
	}

	unless ($arg{end}) {
		$arg{timestamp} ||= $r->{last};
	}

	if (my $t = $arg{timestamp}) {
		utime($t, $t, $filename);
	}
}

1;
