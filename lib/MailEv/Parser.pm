#!/usr/bin/perl

package MailEv::Parser;

use strict;
use warnings;

use Time::Local;
use Util::Export qw(parse_new parse_syslog parse_journal parse_syslog_time
	parse_postfix parse_postfix_comment parse_postfix_attrs parse_postfix_address
	parse_postfix_queue_id parse_postfix_command parse_postfix_relay
	parse_postfix_delays);

my %syslog_months = (
	'Jan' => 0, 'Feb' =>  1, 'Mar' =>  2,
	'Apr' => 3, 'May' =>  4, 'Jun' =>  5,
	'Jul' => 6, 'Aug' =>  7, 'Sep' =>  8,
	'Oct' => 9, 'Nov' => 10, 'Dec' => 11,
);

our $verp_delimiter = '=';
our $current_year;
our $previous_time;
our $json;

sub parse_new($)
{
	chomp(my $text = shift);
	+{ -text => $text };
}

sub parse_syslog($)
{
	my $tok = shift;

	my ($date, $hostname, $text) =
	$tok->{-text} =~ /^(\S{3}\s+\d+\s\d+:\d+:\d+)\s([-\w\.\@:]+)\s+(.+)/
		or return 0;

	$tok->{-type} = "syslog";
	$tok->{-date} = $date;
	$tok->{-text} = $text;
	$tok->{-hostname} = $hostname;

	return 1;
}

sub parse_journal($)
{
	my $tok = shift;

	$json ||= eval "use JSON v2; JSON->new->latin1"
		or die('could not load JSON object: %s', $@);

	# journal reboot lines are not json encoded
	if ($tok->{-text} =~ /^-- Reboot --/) {
		return 0;
	}

	my $r = $json->decode($tok->{-text})
		or return 0;

	my ($command) = ($r->{SYSLOG_IDENTIFIER} || '') =~ m#^postfix/(.*)#;
		#or return 0;

	$tok->{-type} = $command ? "postfix" : undef;
	$tok->{-text} = $r->{MESSAGE};
	$tok->{-cursor} = $r->{__CURSOR};
	$tok->{-time} = int(($r->{_SOURCE_REALTIME_TIMESTAMP} || 0) / (1000 * 1000));
	$tok->{-hostname} = $r->{_HOSTNAME};
	$tok->{pid} = $r->{SYSLOG_PID};
	$tok->{command} = $command;

	return 1;
}

sub parse_syslog_time($)
{
	my $tok = shift;
	return 1 if $tok->{-time};
	my ($month, $day, $hh, $mm, $ss) =
	$tok->{-date} =~ /^(\S{3})\s+(\d+)\s(\d+):(\d+):(\d+)$/
		or return 0;
	my $time = timelocal($ss, $mm, $hh, $day, $syslog_months{$month}, $current_year);
	if ($previous_time && $previous_time > $time + 3600) {
		# Happy new year!
		$time = timelocal($ss, $mm, $hh, $day, $syslog_months{$month}, ++$current_year);
	}
	$previous_time = $time;
	$tok->{-time} = $time;
	return 1;
}

sub parse_postfix($)
{
	my $tok = shift;
	my ($command, $pid, $text) =
	$tok->{-text} =~ m#^postfix/(\S+)\[(\d+)\]:\s(.+)#
		or return 0;
	$tok->{command} = $command;
	$tok->{pid} = $pid;
	$tok->{-type} = "postfix";
	$tok->{-text} = $text;
	return 1;
}

sub parse_postfix_comment($)
{
	my $tok = shift;
	return 1 if exists $tok->{comment};

	if ($tok->{-text} =~ s/\s\((.*?)\)?\s*$//) {
		$tok->{comment} = $1;
		return 1;
	}

	return 0;
}

sub parse_postfix_attrs($)
{
	my $tok = shift;
	return 1 unless exists $tok->{-text};

	return 0 unless $tok->{-text} =~ /^[a-z_-]+=/;
	parse_postfix_comment($tok);

	while ($tok->{-text} =~ s/^([a-z_-]+)=([^,]+)(?:,\s+|,|$)//)
	{
		$tok->{$1} = $2;
	}

	delete $tok->{-text} unless length($tok->{-text});
	return 1;
}

sub parse_postfix_address($)
{
	my $tok = shift;
	return 1 if exists $tok->{address};

	if ($tok->{from}) {
		$tok->{direction} = 'in';
		$tok->{address} = $tok->{from};
	} elsif ($tok->{to}) {
		$tok->{direction} = 'out';
		$tok->{address} = $tok->{to};
	} else {
		return 0;
	}

	$tok->{address} =~ s/^<(.*)>$/$1/;
	return 1 unless length $tok->{address};

	my ($user, $domain) = split('@', $tok->{address}, 2);
	$domain =~ s#/$##; # remove buggy trailing slash

	$tok->{user} = $user;
	$tok->{domain} = $domain;

	return 1;
}

sub parse_postfix_queue_id($)
{
	my $tok = shift;
	return 1 if exists $tok->{qid};
	my ($qid, $text) = $tok->{-text} =~ /^([0-9A-F]{8,}):\s(.+)/
		or return 0;
	$tok->{qid} = $qid;
	$tok->{-text} = $text;
	return 1;
}

sub parse_postfix_command($)
{
	my $tok = shift;
	parse_postfix_queue_id($tok)
		or return 0;
	parse_postfix_comment($tok);

	if (parse_postfix_attrs($tok)) {
		parse_postfix_address($tok)
			or return 0;
	}

	return 1;
}

sub parse_postfix_relay($)
{
	my $tok = shift;
	return 1 if exists $tok->{relay_ip};
	return 0 unless exists $tok->{relay};
	my ($host, $ip, $port) =
	$tok->{relay} =~ /^(.+)\[(.+)\]:(\d+)$/
		or return 0;
	$tok->{relay_ip} = $ip;
	$tok->{relay_host} = $host;
	$tok->{relay_port} = $port;
}

sub parse_postfix_delays($)
{
	my $tok = shift;
	return 1 if exists $tok->{delay_1};
	return 0 unless exists $tok->{delay};
}

sub set_current_year(;$)
{
	my $time = shift || time;
	$previous_time = undef;
	if ($time > 10000) {
		$current_year = (localtime $time)[5] + 1900;
	} else {
		$time += 1900 if $time < 1900;
		$current_year = $time;
	}
}

set_current_year();

1;
