#!/usr/bin/perl

package MailEv::Namecache;

use strict;
use warnings;

use App::Message "namecache:";
use Util::List qw(list_mesh);

sub new
{
	my ($class, %opt) = @_;

	my $nc = bless \%opt, $class;

	$nc->{fast} = eval { require Cache::Memcached::Fast } ?
		1 : eval { require Cache::Memcached } ?
		0 : error("neither Cache::Memcached::Fast nor Cache::Memcached is installed");
	
	if ($nc->{servers}) {
		$nc->open($nc->{servers});
	}

	if ($nc->{api_server}) {
		require LWP::UserAgent;
		$nc->{ua} = LWP::UserAgent->new;
		$nc->{ua}->timeout(
			$nc->{api_timeout} ||= 900);
	}

	return $nc;
}

sub open
{
	my ($nc, $servers) = @_;
	my @servers = ref $servers ? @{$servers} : split /\s+/, $servers;
	my $class = $nc->{fast} ? "Cache::Memcached::Fast" : "Cache::Memcached";
	$nc->{memcache} = $class->new({servers => \@servers});
	info("using servers from %s", join ', ', @servers);
}

sub mkey($$)
{
	return $_[1] unless defined $_[0];
	return $_[0] . ':' . $_[1];
}

sub flush
{
	my $nc = shift;
	$nc->{memcache}->set_multi(@{$nc->{buffer}});
	@{$nc->{buffer}} = ();
}

sub get
{
	my ($nc, $type, @ids) = @_;

	my @values;

	if (@ids > 1 && $nc->{fast}) {
		my $r = $nc->{memcache}->get_multi(map mkey($type, $_), @ids);
		@values = map $r->{mkey($type, $_)}, @ids;
	} else {
		@values = map $nc->{memcache}->get(mkey($type, $_)), @ids;
	}

	if (wantarray) {
		return @values;
	} else {
		return { list_mesh(@ids, @values) };
	}
}

sub check
{
	my ($nc, $type, @ids) = @_;
	my @add = map [mkey($type, $_), "", $nc->{api_timeout}], @ids;
	my @check = $nc->{fast}
		? $nc->{memcache}->add_multi(@add)
		: map $nc->{memcache}->add(@$_), @add;
	return map $ids[$_], grep $check[$_], 0 .. $#ids;
}

sub fill
{
	my ($nc, $type, @ids) = @_;
	return unless @ids;

	if ($type eq "msg") {
		return $nc->fill_msg(@ids);
	} else {
		warning("invalid entry type %s", $type);
		return undef;
	}
}

sub fill_msg
{
	my ($nc, @ids) = @_;

	# debug("message name lookup %s", join(', ', @ids));
	my $uri = sprintf('http://%s/ssapi.axd?command=lookup_name&shorturi=%s',
		$nc->{api_server}, join ('|', @ids));
	my $r = $nc->{ua}->get($uri);

	my %miss = map { $_ => 1 } @ids;

	if ($r->is_success) {
		my $content = $r->content;
		my %map;
		for my $line (grep length, split "\n", $content) {
			my ($id, $name) = split('\|', $line, 2);
			next if exists $map{$id} or !$name;
			$map{$id} = $name;
			delete $miss{$id};
		}
		$nc->put("msg", %map);
	} else {
		warning("message name lookup request failed: %s", $r->status_line);
	}

	if (my @miss = keys %miss) {
		# debug("message name lookup miss %s", join(', ', @ids));
		@miss == 1
			? $nc->{memcache}->delete($miss[0])
			: $nc->{memcache}->delete_multi(@miss);
	}
}

sub bgfill
{
	my ($nc, $type, @ids) = @_;
	my @check = $nc->check($type, @ids);
	return unless @check;
	defined (my $pid = fork)
		or error("fork failed: $!");
	if ($pid) {
		info("lookup names in the background ids=%s pid=%s",
			join(', ', @ids), $pid);
		return $pid;
	}
	$nc->{memcache}->disconnect_all();
	$nc->open($nc->{servers});
	$0 = sprintf('namecache fill %s %s',
		$type, join (', ', @ids));
	$nc->fill($type, @check);
	exit;
}

sub put
{
	my ($nc, $type, %map) = @_;

	while (my ($id, $value) = each %map) {
		my $key = mkey($type, $id);
		if ($nc->{fast} && $nc->{buffer_size}) {
			push @{$nc->{buffer}}, [$key, $value];
			$nc->flush() if @{$nc->{buffer}} == $nc->{buffer_size};
		} else {
			$nc->{memcache}->set($key, $value);
		}
	}
}

1;
