#!/usr/bin/perl

package MailEv::Util;

use strict;
use warnings;

use Util::Export qw(generate_id read_config parse_uri parse_amqp_uri);

sub generate_id(;$$)
{
	my $len = shift || 8;
	my $charset = shift || ['a'..'z', 'A'..'Z', '0'..'9'];

	my $id = "";
	while ($len -- > 0) {
		$id .= $charset->[int rand(@{$charset})]
	}
	return $id;
}

sub read_config($$)
{
	my ($config_file, $config) = @_;
	open my $handle, $config_file
		or return undef;

	while (my $line = <$handle>)
	{
		chomp($line);
		$line =~ s/(^\s+|\s+$|#.*)//;
		next unless length $line;
		my ($key, $value) = split ('\s+', $line, 2);

		my $r = $config->{$key} or next;
		$value = 1 unless defined $value;

		if (ref ($r) eq 'ARRAY') {
			push @{$r}, $value;
		} else {
			${$r} = $value;
		}
	}

	return 1;
}

sub parse_uri($)
{
	my $u = shift;
	return undef unless defined $u;

	my $s = { uri => $u };

	@{$s}{qw(scheme opaque query_string fragment)} = $u =~ m{
		^
		(?:(\w+):)? # optional scheme
		(.+?) # opaque part
		(?:\?(.*?))? # query
		(?:\#(.*))? # fragment
		$
	}xo or return undef;

	if (defined $s->{opaque} && $s->{opaque} =~ m#^//#) {
		@{$s}{qw(authority path)} = $s->{opaque} =~ m{
			^
			//
			(.+?) # authority
			(?:/(.*))? # path
			$
		}xo or return undef;
	} else {
		$s->{path} = $s->{opaque};
	}

	if (defined $s->{authority}) {
		@{$s}{qw(user pass address)} = $s->{authority} =~ m{
			^
			(?:
				(\w+?) # username
				(?::(\w+))? # password
				@
			)?
			([\w.-]+(?::\d+)?) # address
			$
		}xo or return undef;

		@{$s}{qw(host port)} = split(':', $s->{address}, 2);
	}

	while (defined $s->{path} && $s->{path} =~ s#;(\w+)=([^;]*)$##) {
		$s->{args}->{$1} = $2;
	}

	if (defined $s->{query_string}) {
		for my $arg (split /[;&]/, $s->{query_string}) {
			my ($n, $v) = split /=/, $arg, 2;
			$s->{query}->{$n} = $v;
		}
	}

	return $s;
}

sub parse_amqp_uri($)
{
	my $u = shift;
	return undef unless defined $u;
	return $u if ref $u; # already parsed, most likely

	my $c = parse_uri($u)
		or return undef;

	if ($c->{scheme} && $c->{scheme} ne "amqp") {
		return undef;
	}

	unless (defined $c->{host}) {
		return undef;
	}

	if ($c->{path}) {
		@{$c}{qw(type exchange key)} = split('/', $c->{path});
	}

	if (my $key = $c->{args}->{key}) {
		$c->{key} = $key;
	}

	return $c;
}

1;
