#!/usr/bin/perl

package MailEv::Event;

use strict;
use warnings;

use Util::String qw(define deprefix);
use Util::Strfy qw($strfy);
use App::Message::Format qw(update_custom_sprintf);
use Util::Export qw(
	mailev_to_text mailev_from_text format_event
	match_value match_event mailev_is_final mailev_pk);

our @pk = qw(list_id msg_id address);
our @compact = qw(list_id msg_id address status timestamp domain);
our @fields = qw(list_id msg_id address status timestamp domain server comment);
our %field_exists = map { $_ => 1 } @fields;
our @statuses = qw(sent bounced deferred expired queued);

sub format_event($;$);
sub match_event($$);

update_custom_sprintf(ev => sub { format_event($_[2], $_[0]) });

sub format_event($;$)
{
	my ($ev, $flag) = ($_[0], define $_[1]); 
	return '<undefined event>' unless defined $ev;
	return '<any event>' if $ev eq "*";
	return join ' | ', map { format_event($_, $flag) } @{$ev}
		if ref $ev eq "ARRAY";
	return '<malformed event>'
		unless ref $ev eq 'HASH';
	my @keys = $flag =~ "C" ? @fields
		: $flag =~ "c" ? @compact : @pk;
	local $strfy->{hash_delim} = ":";
	local $strfy->{undefined} = 0;
	local $strfy->{string_limit} = 0
		if $flag =~ "u";
	return sprintf "%s", $strfy->hash([ map { $_ => $ev->{$_} } @keys ]);
}

sub match_value($$)
{
	my ($match, $value) = @_;

	deprefix($match, "+");

	my $negate = deprefix($match, "!");

	my $ret;

	if (deprefix($match, "=")) {
		$ret = $match eq $value;
	} elsif (deprefix($match, "~")) {
		$ret = eval { $match =~ $value };
	} elsif (deprefix($match, ">")) {
		$ret = $match > $value;
	} elsif (deprefix($match, ">=")) {
		$ret = $match >= $value;
	} elsif (deprefix($match, "<")) {
		$ret = $match < $value;
	} elsif (deprefix($match, "<=")) {
		$ret = $match <= $value;
	} else {
		$ret = $match eq $value;
	}

	return $negate ? !$ret : $ret;
}

sub match_event($$)
{
	my ($ev, $select) = @_;

	if (!ref($select)) {
		return 1 if defined($select) && $select eq '*';
		return 0;
	}

	if (ref ($select) eq 'ARRAY') {
		map { return 1 if match_event($ev, $_) } @{$select};
		return 0;
	}

	if (ref ($select) eq 'HASH') {
		for my $key (keys %{$select}) {
			return 0 unless exists $ev->{$key};
			return 0 unless match_value($select->{$key}, $ev->{$key});
		}

		return 1;
	}

	return 0;
}

sub mailev_to_text($)
{
	my $ev = shift || return undef;
	join "\t", map { defined $ev->{$_} ? $ev->{$_} : "" } @fields;
}

sub mailev_from_text($)
{
	my $text = shift;
	return undef unless defined $text;

	my @text = split "\t", $text;
	return undef unless @text == @fields;

	my $ev = {};

	@$ev{@fields} = @text;

	return $ev;
}

sub mailev_is_final($)
{
	return $_[0]->{status} ne 'deferred';
}

sub mailev_pk($)
{
	return @{$_[0]}{@pk};
}

1;
