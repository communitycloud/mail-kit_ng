#!/usr/bin/perl

package S3::Lite;

use strict;
use warnings;

use LWP::UserAgent;
use HTTP::Date;
use Util::File qw(fopen);
use Digest::HMAC_SHA1;
use MIME::Base64;

use App::Message "s3:";

sub new
{
	my ($class, %args) = @_;

	bless {
		host => 's3.amazonaws.com',
		ssl => 0,
		%args,
	}, $class;
}

sub ua
{
	$_[0]->{ua} ||= LWP::UserAgent->new();
}

# parse awc-cli compatible credentials

sub parse_credentials
{
	my ($s3, $file) = @_;

	my $h = fopen($file);
	my ($id, $secret) = @_;

	while (defined(my $line = readline $h)) {
		if ($line =~ /^\s*aws_access_key_id\s*=\s*(.*?)\s*$/) {
			$id = $1;
		} elsif ($line =~ /^\s*aws_secret_access_key\s*=\s*(.*?)\s*$/) {
			$secret = $1;
		}
	}

	unless ($id && $secret) {
		error('no valid credentials found file=%s', $file);
	}

	$s3->{id} = $id;
	$s3->{secret} = $secret;
}

# common S3 request wrapper

sub request
{
	my ($s3, $method, $key, $headers, $content) = @_;

	my ($bucket, $trail) = $key =~ m#^/?([^/]*)(/.*)?#;
	my $proto = $s3->{ssl} ? 'https' : 'http';
	my $uri = sprintf('%s://%s.%s%s',
		$proto, $bucket, $s3->{host}, $trail || '');

	my $req = HTTP::Request->new($method => $uri);

	while (my ($key, $value) = each %$headers) {
		$req->header($key => $value);
	}

	if (!$req->header('Date')) {
		$req->header(Date => HTTP::Date::time2str(time));
	}

	if (defined $content) {
		$req->header('Content-Length' => length $content);
		$req->content($content);
	}

	$s3->sign($req, $key);

	return $req;
}

# add the S3 authorization header to the request

sub sign
{
	my ($s3, $req, $key) = @_;

	my $data = "";

	$data .= $req->method . "\n";

	my @aws_headers = qw(content-md5 content-type date);
	for my $name (sort @aws_headers) {
		$data .= ($req->header($name) || '') . "\n";
	}

	$data .= $key;

	my $hmac = Digest::HMAC_SHA1->new($s3->{secret});
	$hmac->add($data);

	my $digest = MIME::Base64::encode_base64($hmac->digest, '');
	$req->header(Authorization => "AWS $s3->{id}:$digest");
}

# extract the message tag from server response to improve log readability

sub content_message($)
{
	my $c = shift;

	if ($c =~ m'<Message>(.*)</Message>'m) {
		return $1;
	} else {
		return $c;
	}
}

sub put
{
	my ($s3, $bucket, $key, $data) = @_;

	my $req = $s3->request('PUT', "/$bucket/$key", {}, $data);
	my $r = $s3->ua->request($req);

	if ($r->is_success) {
		return 1;
	} else {
		warning('put request failed bucket=%s key=%s response=%s',
			$bucket, $key, content_message($r->content));
		return 0;
	}
}

sub get
{
	my ($s3, $bucket, $key) = @_;

	my $req = $s3->request('GET', "/$bucket/$key");
	my $r = $s3->ua->request($req);

	if ($r->is_success) {
		return $r->content;
	} else {
		warning('get request failed bucket=%s key=%s response=%s',
			$bucket, $key, content_message($r->content));
		return undef;
	}
}

sub delete
{
	my ($s3, $bucket, $key, $data) = @_;

	my $req = $s3->request('DELETE', "/$bucket/$key");
	my $r = $s3->ua->request($req);

	if ($r->is_success) {
		return 1;
	} else {
		warning('delete request failed bucket=%s key=%s response=%s',
			$bucket, $key, content_message($r->content));
		return 0;
	}
}

1;
