#!/usr/bin/perl

package ECS::DB::jq;

use strict;
use warnings;

use base qw(ECS::DB);

use JSON v2;
my $JSON = JSON->new->utf8;
use App::Message;

sub new
{
	my ($class, @args) = @_;

	my $ref = $class->SUPER::new(@args);
	$ref->{database} ||= 'cc__job_queue';

	return $ref;
}

sub connect
{
	my $db = shift;
	my $dbh = $db->SUPER::connect(@_);
	$dbh->{pg_enable_utf8} = 0;
	$dbh;
}

# XXX
sub parse_table($)
{
	my $table = shift;
	my ($id, $q, $json) = $table =~ /^\((.+?),("?)(.*)\g2\)$/;

	if (!defined $id) {
		warning("failed to parse table: %s", $table);
		return undef;
	}

	$json =~ s/""/"/g;
	return ($id, $json);
}

sub enqueue_job
{
	my ($db, $type, $json) = @_;
	my $sql = "select jq.enqueue_job(?, ?)";

	my $sth = $db->dbh->prepare($sql)
		or return undef;

	if (ref $json) {
		$json = $JSON->encode($json);
	}

	$sth->execute($type, $json)
		or return undef;

	my $a = $sth->fetchrow_arrayref();
	$sth->finish;

	return $a->[0];
}

sub start_next_job
{
	my ($db, $type, $meta) = @_;

	my $sql = $meta ? "SELECT id, payload FROM jq.start_next_job(?,?)"
		: "SELECT id, payload FROM jq.start_next_job(?)";

	if (ref $meta) {
		$meta = $JSON->encode($meta);
	}

	my $sth = $db->dbh->prepare($sql)
		or return undef;

	$sth->execute($type, $meta)
		or return undef;

	my $r = $sth->fetchrow_hashref();
	$sth->finish;

	return ($r->{id} || 0, $r->{payload});
}

sub start_job
{
	my ($db, $id, $json) = @_;
	my $sql = "SELECT id, payload FROM jq.start_job(?, ?)";

	my $sth = $db->dbh->prepare($sql)
		or return undef;

	if (ref $json) {
		$json = $JSON->encode($json);
	}

	$sth->execute($id, $json)
		or return undef;

	my $r = $sth->fetchrow_hashref();
	$sth->finish;

	if (!$r) {
		return (0);
	}

	return ($r->{id} || 0, $r->{payload});
}

sub complete_job
{
	my ($db, $id, $json) = @_;
	my $sql = "select jq.complete_job(?, ?)";

	my $sth = $db->dbh->prepare($sql)
		or return undef;

	if (ref $json) {
		$json = $JSON->encode($json);
	}

	$sth->execute($id, $json)
		or return undef;

	my $a = $sth->fetchrow_arrayref();
	$sth->finish;

	my $job_id = $a->[0];

	unless ($job_id) {
		warning("complete_job() nil return id=%s", $id);
		$job_id = 0;
	}

	return $job_id;
}

sub fail_job
{
	my ($db, $id) = @_;
	my $sql = "select jq.fail_job(?)";

	my $sth = $db->dbh->prepare($sql)
		or return undef;

	$sth->execute($id)
		or return undef;

	my $a = $sth->fetchrow_arrayref();
	$sth->finish;

	my $job_id = $a->[0];

	unless ($job_id) {
		warning("fail_job() nil return id=%s", $id);
		$job_id = 0;
	}

	return $job_id;
}

sub get_job_archive
{
	my ($db, $id) = @_;

	my $sql = "select data from jq.job_archive where id = ?";

	my $sth = $db->dbh->prepare($sql)
		or return undef;

	$sth->execute($id)
		or return undef;

	my $a = $sth->fetchrow_arrayref();
	$sth->finish;

	return $a->[0];
}

1;
