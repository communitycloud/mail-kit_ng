#!/usr/bin/perl

# Copyright (C) 2011 WA Research
# 
# This library is free software; you can redistribute it and/or
# modify it under the same terms as Perl itself.

package ECS::API;

use strict;
use warnings;

use App::Message qw(ecs:);
use Digest::SHA qw(sha1_hex);
use Util::List qw(update_hash);
use Util::String qw(uri_escape html2text);
use Util::File qw(dirname slurp);
use LWP::UserAgent;
use Hash::Config;
require POSIX;

sub new
{
	my ($class, $spec) = @_;

	my $api = bless { map => {} }, $class;

	if (ref $spec eq 'HASH') {
		$api->{map} = $spec;
	} else {
		$api->parse($spec);
	}

	return $api;
}

sub parse
{
	my ($api, $cfg_file) = @_;

	$api->{cfg_file} ||= $cfg_file || $ENV{ECS_API_CFG} || $ENV{ecs_api_cfg}
		|| dirname(__FILE__) . '/../../config/ecs-api.cfg';

	my $p = Hash::Config->new();
	$p->{word_chars} .= '()*'; # for readfile() and * entry

	my $cfg = $p->parse($api->{cfg_file})
		or error('failed to parse %s: %s', $api->{cfg_file}, error_string);

	my $map = {};

	my $domains = delete $cfg->{domains};
	my $defaults = delete $cfg->{defaults};

	$api->{ua_timeout} = delete $cfg->{ua_timeout};

	if (my ($file) = $domains =~ /^readfile\((.*)\)$/) {
		$file = dirname($api->{cfg_file}) . '/' . $file
			unless $file =~ /^\//;
		$domains = [grep { $_ !~ '^#' } slurp($file, chomp => 1)];
		note("loaded map from %s domains from %s",
			$api->{cfg_file}, $file);
	} else {
		note("loaded map from %s", $api->{cfg_file});
	}

	for my $e (keys %{$cfg}) {
		$map->{$e} = ref $cfg->{$e} ? $cfg->{$e} : {url => $cfg->{$e}};
	}

	if (ref($domains) eq 'ARRAY') {
		for my $e (@{$domains}) {
			$map->{$e} ||= {};
		}
	}

	for my $e (keys %{$map}) {
		update_hash($map->{$e}, $defaults);
		if (my $fmt = delete $map->{$e}->{url_fmt}) {
			$map->{$e}->{url} ||= sprintf($fmt, $e);
		}
	}

	$api->{map} = $map;
}

sub ua
{
	$_[0]->{ua} ||= do {
		my $ua = LWP::UserAgent->new();
		$ua->ssl_opts(verify_hostname => 0);
		$ua->ssl_opts(SSL_verify_mode => 'SSL_VERIFY_NONE');
		$ua->requests_redirectable([qw(GET HEAD POST)]);
		$ua->timeout($_[0]->{ua_timeout} || 60);
		$ua;
	};
}

sub request
{
	my ($api, $operation, $domain, @param) = @_;

	my $entry = $api->{map}->{lc $domain} || $api->{map}->{'*'}
		or error("domain %q has no entry", $domain);

	my $time = POSIX::strftime("%FT%T", gmtime);
	my $signature = sha1_hex($operation . $entry->{user} . $entry->{pass} . $time);

	my $url = sprintf "%s/api.axd?operation=%s&site-id=%s&request-time=%s&signature=%s",
		$entry->{url}, $operation, $entry->{user}, $time, $signature;

	my $r = $api->ua->post($url, @param);

	if ($r->is_success) {
		return $r->decoded_content();
	} else {
		# my $content = html2text($r->decoded_content());
		error("request failed: %s [domain=%s url=%s]",
			$r->status_line, $domain, $url);
	}
}

1;
