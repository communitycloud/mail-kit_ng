#!/usr/bin/perl

package ECS::DB;

use strict;
use warnings;

use DBI;
use DBD::Pg;

use App::Message "db:";

sub new
{
	my ($class, %config) = @_;

	bless {
		hostname => $config{hostname} || "localhost",
		username => $config{username},
		password => $config{password},
		database => $config{database},
		port => $config{port} || 5432,
	}, $class;
}

sub connect
{
	my ($db, $database) = @_;

	$database ||= $db->{database}
		or error("no database specified");

	my $uri = $db->{uri} = sprintf "dbi:Pg:dbname=%s;host=%s;port=%s",
		$database, $db->{hostname}, $db->{port};

	$db->{dbh} = DBI->connect($uri, $db->{username}, $db->{password},
		{ AutoCommit => 1, RaiseError => 0, PrintError => 0 })
		or error('failed to connect to %s: %s', $uri, $db->errstr);

	note("connected to %s", $uri);

	$db->{dbh};
}

sub dbh
{
	$_[0]->{dbh} ||= $_[0]->connect();
}

sub errstr
{
	defined $_[0]->{dbh} ? $_[0]->{dbh}->errstr : $DBI::errstr;
}

sub assure_connection
{
	my $db = shift;

	if ((my $ping = $db->dbh->pg_ping) < 0) {
		warning("dbh ping=%s, attempting to reconnect", $ping);
		$db->dbh->disconnect;
		eval { $db->connect };

		if ($@) {
			warning("failed to reconnect to %s: %s", $db->{uri}, $db->errstr);
			return 0;
		}
	}

	return 1;
}

1;
