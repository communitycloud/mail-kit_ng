#!/usr/bin/perl

package AMQ::Helper;

use strict;
use warnings;

use IO::Handle;
use POSIX ":sys_wait_h";

sub new
{
	my ($class, %amq) = @_;
	return bless \%amq, $class;
}

sub init
{
	my $amq = shift;

	$amq->{helper} ||= $amq->find_amp_helper();
	return $amq->error("cannot find the helper binary!")
		unless ($amq->{helper} && -x $amq->{helper});

	$amq->get_ident() && $amq->get_ecodes();
}

sub error
{
	my ($amq, $error, $ecode) = @_;
	return $amq->{error} if (@_ == 1);
	$amq->{error} = $error;
	$amq->{ecode} = $ecode;
	return defined($error) ? 0 : 1;
}

sub check_exit_error
{
	my ($amq, $status) = @_;
	$status = $? if @_ == 1;
	return $amq->error(undef) if $status == 0;

	my $code = $status >> 8;
	my $signal = $status & 127;

	my $error = "exit status was $code";
	$error .= "; killed by $signal" if $signal;
	if (exists ($amq->{ecodes}->{$code})) {
		return $amq->error(
			$amq->{ecodes}->{$code}->[1],
			$amq->{ecodes}->{$code}->[0]);
	}

	return $amq->error($error);
}

sub find_amp_helper
{
	use Cwd qw(abs_path);
	use FindBin;
	use File::Basename;

	my @search_path = (
		dirname(__FILE__) . '/../../src',
		$FindBin::Bin . '/src',
		split(':', $ENV{PATH}));

	for my $path (@search_path) {
		if (-x $path . '/amq_helper') {
			return abs_path($path . '/amq_helper');
		}
	}

	return undef;
}

sub get_ident
{
	my $amq = shift;
	open (my $out, '-|', $amq->{helper}, '.identify')
		or return $amq->error("piping the .identify command failed: $!");

	$amq->{ident} = {};
	while (defined (my $line = readline($out)))
	{
		chomp($line);
		my ($key, $value) = split(' ', $line, 2);
		$amq->{ident}->{lc($key)} = $value;
	}

	wait;

	unless ($amq->check_exit_error()) {
		return $amq->error(".identify failed: $amq->{error}");
	}
}

sub get_ecodes
{
	my $amq = shift;
	open my $out, '-|', $amq->{helper}, '.ecodes'
		or return $amq->error("piping the .ecodes command failed: $!");

	$amq->{ecodes} = {};
	while (defined (my $line = readline($out))) {
		chomp($line);
		my ($code, $name, $text) = split(' ', $line, 3);
		$amq->{ecodes}->{$code} = [$name, $text];
	}

	wait;

	unless ($amq->check_exit_error()) {
		return $amq->error(".ecodes failed: $amq->{error}");
	}
}

sub switches
{
	my ($amq, %config) = @_;

	for my $prop (
		qw(server username password exchange
		exchange_type routing_key))
	{
		$config{$prop} = $amq->{$prop}
			unless exists $config{$prop};
	}

	my @switches = ();

	if ($config{server}) {
		push @switches, -s => $config{server};
	}

	if ($config{username}) {
		push @switches, -u => $config{username};
	}

	if ($config{password}) {
		push @switches, -p => $config{password};
	}

	if ($config{exchange}) {
		push @switches, -e => $config{exchange};
	}

	if ($config{exchange_type}) {
		push @switches, -t => $config{exchange_type};
	}

	if ($config{routing_key}) {
		push @switches, -k => $config{routing_key};
	}

	return @switches;
}

sub execute
{
	my ($amq, @args) = @_;
	my @amq_helper = ($amq->{helper}, $amq->switches, @args);
	system(@amq_helper);
	$amq->check_exit_error();
}

sub test
{
	my $amq = shift;
	$amq->execute("test");
}

sub create_exchange
{
	my ($amq, $exchange, $type) = @_;
	$amq->execute("test",
		"-t", $type || $amq->{exchange_type},
		"-E", $exchange || $amq->{exchange});
}

sub pipe
{
	my ($amq, %config) = @_;
	$amq->{pipe_pid} = open $amq->{pipe}, '|-', ($amq->{helper}, $amq->switches(%config), "pipe")
		or return $amq->error("piping the pipe command failed: $!");
}

sub listen
{
	my ($amq, %config) = @_;
	$amq->{dump_pid} = open $amq->{dump}, '-|', ($amq->{helper}, $amq->switches(%config), "dump")
		or return $amq->error("piping the dump command failed: $!");
}

sub sysreadline($)
{
	my $handle = shift;
	my $line = "";;
	while (sysread($handle, my $c, 1))
	{
		$line .= $c;
		return $line if $c eq $/;
	}
	return undef;
}

sub next
{
	my $amq = shift;

	my $ev = { };
	while (defined (my $line = sysreadline($amq->{dump})))
	{
		chomp($line);
		if ($line eq "") {
			if (my $len = $ev->{'content-length'}) {
				read($amq->{dump}, my $buffer, $len);
				read($amq->{dump}, my $tmp, 2);
				$ev->{body} = $buffer;
			}
			return $ev;
		}

		my ($header, $value) = split(': ', $line, 2);
		$ev->{lc($header)} = $value;
	}

	waitpid $amq->{dump_pid}, 0;
	return $amq->check_exit_error();
}

sub send
{
	my $amq = shift;
	my $body = @_ % 2 ? shift : undef;
	my %message = @_;
	$body ||= delete $message{body};

	my $out = '';

	for my $k (sort keys %message) {
		my $v = $message{$k};
		$v =~ s#\n#\\n#g;
		$v =~ s#\r#\\r#g;
		$v =~ s#\t#\\t#g;
		$v =~ s#([\\"])#\\$1#g;
		$out .= sprintf("%s: %s\n", $k, $v);
	}

	if ($body) {
		$out .= sprintf("%s: %s\n", "Content-Length", length($body));
		$out .= "\n";
		$out .= $body;
		$out .= "\n";
	}

	$out .= "\n";

	if ((waitpid $amq->{pipe_pid}, WNOHANG) > 0) {
		return $amq->check_exit_error();
	}

	print {$amq->{pipe}} $out;
	$amq->{pipe}->flush();
	$amq->{pipe}->sync();

	return 1;
}

sub close
{
	my $amq = shift;
	kill 1, $amq->{dump_pid} if $amq->{dump_pid};
	close $amq->{pipe} if $amq->{pipe};
	close $amq->{dump} if $amq->{dump};
}

1;
