#!/usr/bin/perl

package AMQ::Relay;

use strict;
use warnings;

use WireAPI;
use App::Message;
use MailEv::Util qw(parse_amqp_uri);

our $VERSION = '0.04';

my ($src, $dst);

sub amq_connect($)
{
	my $c = shift;

	my $wapi;
	my $retries = 0;

	until ($wapi = WireAPI->connect($c->{address}, $c->{user}, $c->{pass}, instance => __PACKAGE__)) {
		warning("failed to connect AMQ server at $c->{address}, retrying (%d)", ++$retries);
		sleep($retries > 3 ? 10 : 1);
	}

	info("connected to AMQ server at $c->{address}");

	return $wapi;
}

sub amq_cleanup($)
{
	my $wapi = shift;

	if ($wapi)
	{
		if ($wapi->{connection}->{alive}) {
			warning("closing alive connection to $wapi->{connection}->{host}");
		} else {
			warning("connection to $wapi->{connection}->{host} lost: $wapi->{connection}->{reply_text}");
		}

		$wapi->disconnect();
	}
}

sub connect_src($)
{
	my $c = shift;

	if ($src) {
		amq_cleanup($src);
	}

	$src = amq_connect($c);

	unless ($src->declare_exchange($c->{exchange}, $c->{type}))
	{
		warning("failed to declare exchange=$c->{exchange} type=$c->{type}: $src->{session}->{reply_text}");
		return;
	}

	my $queue = 'relay:/' . $c->{exchange} . '/' . $c->{key};
	my $arguments = $c->{args}->{profile} ? { profile => $c->{args}->{profile} } : undef;

	unless ($src->declare_queue($queue, exclusive => 0, auto_delete => 0, arguments => $arguments))
	{
		warning("failed to declare the queue");
		return;
	}

	unless ($src->bind($c->{exchange}, $c->{key}, $queue)) {
		warning("failed to bind the queue: $src->{session}->{reply_text}");
		return;
	}

	unless ($src->consume($queue, exclusive => 1, no_ack => 1)) {
		warning("failed to consume the queue ($queue): $src->{session}->{reply_text}");
		return;
	}

	info("relaying exchange=$c->{exchange} type=$c->{type} routing_key=$c->{key}");

	return 1;
}

sub connect_dst($)
{
	my $c = shift;

	if ($dst) {
		amq_cleanup($dst);
	}

	$dst = amq_connect($c);

	unless ($dst->declare_exchange($c->{exchange}, $c->{type}))
	{
		warning("failed to declare exchange=$c->{exchange} type=$c->{type}: $src->{session}->{reply_text}");
		return;
	}
}

sub relay($$)
{
	my ($s, $d) = @_;

	until ($src->wait) {
		connect_src($s);
	}

	while (my $content = $src->arrived) {

		my $exchange = $content->{exchange};
		my $routing_key = $content->{routing_key};

		unless ($exchange) {
			warning("content has no exchange");
			next;
		}

		until ($dst->publish($content, $exchange, $routing_key)) {
			if ($dst->{session}->{alive})
			{
				warning("publish failed exchange=$exchange: $dst->{session}->{reply_text}");
				next;
			}
			connect_dst($d);
		}
	}
}

sub get_src_connection($)
{
	my $uri = shift;
	my $c = parse_amqp_uri($uri)
		or error("Invalid relay source: $uri");
	for my $field (qw(exchange type key)) {
		error("Invalid relay source: missing `$field'")
			unless defined $c->{$field};
	}
	return $c;
}

sub get_dst_connection($)
{
	my $uri = shift;
	my $c = parse_amqp_uri($uri)
		or error("Invalid relay destination: $uri");
	return $c;
}

sub run($$)
{
	my $s = get_src_connection($_[0]);
	my $d = get_dst_connection($_[1]);

	$d->{exchange} ||= $s->{exchange};
	$d->{type} ||= $s->{type};

	connect_src($s);
	connect_dst($d);

	for (;;) {
		relay($s, $d);
	}
}

1;
