#!/usr/bin/perl

use strict;
use warnings;

use vars qw($c);
use MailEv::Storage qw(storage_open);

my $p = $c->{request}->{param};

unless (defined $p->{msg_id} || defined $p->{list_id}) {
	return $c->{response}->status(500, "Usage error");
}

my $q = {};
for my $k (qw(msg_id list_id address status count)) {
	$q->{$k} = $p->{$k} if defined $p->{$k};
}

my $s = storage_open($c->{server}->{ext}->{'mail-ev.storage'});
my $r = $s->search($q, defined $p->{limit} ? $p->{limit} : 10, $p->{offset});

$c->{response}->status(200, "OK");
$c->{response}->json($r);
