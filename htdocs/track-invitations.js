var TrackInvitations = {
	client: null,

	handleMessage: function (ev, msg) {
		var self = TrackInvitations;
		if (typeof msg.data.length  == "number") {
			for (var i = 0; i < msg.data.length; i ++) {
				self.handleEvent(msg.data[i]);
			}
		} else {
			self.handleEvent(msg.data);
		}
	},

	handleEvent: function (ev) {
		var fields = ev.list_id + '|' + ev.msg_id + '|' + ev.address;
		$('input[value="' + fields + '"]').parents('.invitationInfo').addClass(ev.status);
	},

	init: function (server) {
		var self = TrackInvitations;

		var query = { select: [], limit: 1 };

		$('input.trackInfo').each(function() {
			var fields = this.value.split('|');
			query.select.push({ list_id: fields[0], msg_id: fields[1], address: fields[2] });
		});

		if (query.select.length == 0)
			return;

		self.client = new Push.Client(server);
		$(self.client).bind('message', self.handleMessage);
		self.client.connect('mail-ev', query);
	}
};
