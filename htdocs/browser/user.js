function set_info_text(text)
{
	$('#info').text(text);
}

function update()
{
	var props = ['list_id', 'address', 'status'];

	for (var i = 0, prop; prop = props[i++];) {
		var val = $('#' + prop).val();
		if (val.length == 0)
			$('#browser').removeAttr(prop);
		else
			$('#browser').attr(prop, val);
		if (prop == "list_id" && val.length == 0) {
			set_info_text("err: list id is mandatory");
			return;
		}
		if (prop == "address" && val.length == 0) {
			set_info_text("err: address is mandatory");
			return;
		}
	}

	var offset = $.url.param('offset');
	$('#browser').attr('offset', offset);

	try {
		MailEv.Browser.attach($('#browser'));
		set_info_text("Ok");
	} catch (e) {
		set_info_text("err: " + e);
	}
}

function init()
{
	if ($.url.param('list_id') == null || $.url.param('address') == null) {
		$('.interactive').show();
		$('#update').click(update);
	} else {
		var props = ['list_id', 'address', 'status'];
		for (var i = 0, prop; prop = props[i++];)
			$('#' + prop).val($.url.param(prop));
		update();
	}
}

$(init);
