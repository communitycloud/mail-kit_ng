var status_color_map = {
	sent: '109618',
	bounced: 'DC3912',
	deferred: 'FF9900',
	expired: '003971'
};

function set_info_text(text)
{
	$('#info').text(text);
}

function chart(ev)
{
	var query = {routing: 'mail-ev', select: [], count: 1};

	for (var status in status_color_map) {
		var select = {};
		for (var k in ev)
			if (ev[k] != null && ev[k].length)
				select[k] = ev[k];
		select.status = status;
		query.select.push(select);
	}

	var msghandler = function (msg) {

		set_info_text("Done.");
		var counts = [];
		var max = 1;
		for (var i = 0; i < msg.data.length; i++) {
			var count = msg.data[i];
			if (count == "..") count = 9999;
			if (count > max) max = count;
			counts.push(count);
		}

		var columns = [];
		var colors = [];

		for (var status in status_color_map) {
			columns.push(status);
			colors.push(status_color_map[status]);
		}

		var data = counts.join(',');
		var src = 'http://chart.apis.google.com/chart?cht=bvs&' +
		'chd=t:' + data + '&chdl=' + columns.join('|') + '&chco=' + colors.join('|') +
		'&chm=N,,0,,10&chtt=Status&chs=300x150&chds=0,' + max;
		$('#chart').attr('src', src);
	};

	set_info_text("Loading ...");
	var client = new Push.Client();
	client.msghandler = msghandler;
	client.connect(query);
}

function update()
{
	var ev = {msg_id: null, list_id: null, address: null};
	for (var key in ev) {
		ev[key] = $('#' + key).val();
	}

	if (ev.list_id == null && ev.msg_id == null) {
		set_info_text("err: must set list_id OR msg_id");
		return;
	}

	chart(ev);
}

function init()
{
	var ev = {msg_id: null, list_id: null, address: null};
	var do_update = false;

	for (var key in ev) {
		var val = ev[key] = $.url.param(key);
		$('#' + key).val(val);
		if (val != null && val.length) {
			do_update = true;
		}
	}

	if (do_update)
		update();

	$('#count').click(update);
}

$(init);
