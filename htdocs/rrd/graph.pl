#!/usr/bin/perl

use strict;
use warnings;

use vars qw($c);
require File::Temp;
require MailEv::RRD;

my $p = $c->{request}->{param};
my $db = $c->{server}->{vars}->{mev_rrd}
	or error("mev_rrd not set");
my $ct = {
	svg => 'image/svg+xml',
	png => 'image/png',
	pdf => 'application/pdf',
	eps => 'application/eps',
};
my $ext = lc($p->{-imgformat} || 'png');

return if $c->fork_socket();

my $rrd = MailEv::RRD->open($db);
my $tmp = File::Temp->new(UNLINK => 1);
$rrd->graph($tmp->filename, %{$p});

$c->{response}->status(200, 'OK');
$c->{response}->header('content_disposition', 'filename=graph.'.$ext);
$c->{response}->header('content_type', $ct->{$ext});
$c->write_response();
$c->write_handle($tmp);
