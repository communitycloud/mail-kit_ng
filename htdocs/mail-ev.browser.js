MailEv.Browser = {

	/* Initialise the .mail-ev-browser elements.
	 * These must to be marked with the "autostart" class */
	init: function () {
		$('.mail-ev-browser.autostart').each(function() {
				MailEv.Browser.attach($(this));
			});
	},

	/* Construct the browser object */
	getBrowser: function (elem) {
		/* The "server" attribute is mandatory */
		var server = elem.attr('server');
		if (!server)
			throw "No server";
		if (server == "/")
			server = "";
		/* The "template" element is mandatory.  Its name is either specified
		 * by the "template" attribute or by the second class name of the element. */
		var tname = elem.attr('template');
		if (!tname)
			tname = elem.attr('class').split(' ')[1];
		if (!tname)
			throw "No template name";
		var telem = $('.mail-ev-template[name='+tname+']');
		if (telem.length == 0)
			telem = $('.mail-ev-template.' + tname);
		if (telem.length != 1)
			throw "No template element";
		/* Search the template parts:
		 * event - used to layout the event; mandatory
		 * empty - used to display empty results
		 * loading - used to display the loading status
		 */
		var template = { };
		var tparts = ['event', 'empty', 'loading', 'next', 'prev', 'info', 'pagination'];
		for (var i = 0, name; name = tparts[i ++];) {
			var part = $('.' + name, telem);
			if (part.length)
				template[name] = part;
		}
		if (!template.event)
			throw "No template event part";

		/* Search the content targets */
		var target = { };
		for (var i = 0, name; name = tparts[i ++];) {
			var part = $('.' + name + '-target', elem);
			if (part.length)
				target[name] = part;
		}

		/* Look for the mandatory query attributes:
		 * select - must be either the literal "*" or an appropriate JSON expression
		 *   OR
		 * domain, list_id, msg_id, address, status - used for simple conditions
		 */
		var select = elem.attr('select');
		var domain = elem.attr('domain');
		var list_id = elem.attr('list_id');
		var msg_id = elem.attr('msg_id');
		var address = elem.attr('address');
		var status = elem.attr('status');

		if (select) {
			if (select != "*")
				try { select = eval("("+select+")") }
				catch (e) { throw "Bad select expression" };
		} else if (list_id || msg_id || address || status || domain) {
			select = { };
			if (list_id)
				select.list_id = list_id;
			if (msg_id)
				select.msg_id = msg_id;
			if (address)
				select.address = address;
			if (status)
				select.status = status.split(',');
			if (domain)
				select.domain = domain;
		} else {
			throw "No query";
		}

		/* Get the optional query limit and offset */
		var limit = elem.attr('limit');
		var offset = elem.attr('offset');

		if (limit != undefined)
			limit = parseInt(limit);
		if (offset != undefined)
			offset = parseInt(offset);

		/* The "fold" flag tells the server to return distinct
		 * combinations of list-address-message-status */
		var fold = elem.attr('fold');

		/* Enable count for pagination */
		var pagination = elem.hasClass('pagination');

		/* Keep track of loaded parts */
		var part = { };

		return {
			server: server,
			template: template,
			target: target,
			part: part,
			select: select,
			limit: limit,
			offset: offset,
			pagination: pagination
		};
	},

	/* Attach a new browser to the given jQuery element */
	attach: function (elem) {

		/* Detach old browser */
		if (elem.data('browser'))
			MailEv.Browser.detach(elem.data('browser'));

		/* Get a new browser */
		var browser = MailEv.Browser.getBrowser(elem);

		elem.data('browser', browser);
		browser.elem = elem;

		/* Show the loading element, if available */
		if (browser.limit && browser.template.loading) {
			MailEv.Browser.loadPart(browser, 'loading');
			browser.loading = 1;
		}

		/* Set up a new MailEv Client */
		var msghandler = function (ev, msg) { MailEv.Browser.handleMessage(browser, msg) };
		var errhandler = function (ev, err) { MailEv.Browser.handleError(browser, err) };

		var query = { };
		query.select = browser.select;
		query.limit = browser.limit + 1; // check next
		query.offset = browser.offset;

		if (browser.fold)
			query.fold = 1;

		if (browser.pagination)
			query.select.count = 1;

		var client = new Push.Client(browser.server);
		$(client).bind("message", msghandler);
		$(client).bind("error", errhandler);
		client.connect('mail-ev', query);

		browser.client = client;
	},

	/* Detach the browser, stop the client */
	detach: function (browser) {
		browser.client.disconnect();
		browser.target.event.empty();
		for (var name in browser.part)
			MailEv.Browser.removePart(browser, name);
	},

	/* Load a template part */
	loadPart: function (browser, name) {
		if (browser.template[name] && browser.target[name]) {
			var part = browser.template[name].clone();
			browser.target[name].append(part);
			browser.part[name] = part;
			return part;
		}

		return null;
	},

	/* Remove a previously loaded part */
	removePart: function (browser, name) {
		if (browser.part[name]) {
			browser.part[name].remove();
		}
	},

	/* Attach the navigation controls */
	attachControls: function (browser) {

		var next = MailEv.Browser.loadPart(browser, 'next');
		if (next) {
			if (!browser.last || browser.offset === undefined)
				next.removeClass('disabled');
			else
				next.addClass('disabled');
			next.click(function () { MailEv.Browser.handleNext(browser) });
		}

		var prev = MailEv.Browser.loadPart(browser, 'prev');
		if (prev) {
			if (browser.offset !== undefined)
				prev.removeClass('disabled');
			else
				prev.addClass('disabled');
			prev.click(function () { MailEv.Browser.handlePrev(browser) });
		}

		if (browser.pagination) {
			var pagination = MailEv.Browser.loadPart(browser, 'pagination');
			if (pagination)
				MailEv.Browser.renderPagination(browser, pagination);
		}

		var info = MailEv.Browser.loadPart(browser, 'info');
		if (info) {
			MailEv.Browser.renderInfo(browser, info);
		}
	},

	/* Render an event field's data as specified by its element */
	renderField: function (elem, name, data, ev) {

		/* Format data */
		var html;

		if (name == "timestamp") {
			var format = elem.attr("format");
			if (format) {
				var format_today = elem.attr("format_today");
				var date = new Date(data * 1000);
				if (format_today) {
					var now = new Date();
					if (now.format("isoDate") == date.format("isoDate")) {
						format = format_today;
					}
				}
				data = date.format(format);
			}
		}

		if (name == "address") {
			var format = elem.attr("format");
			if (format == "mailto") {
				html = '<a href="mailto:' + data + '">' + data + '</a>';
			}
		}

		/* Figure data target */
		var target = $('.target', elem);

		if (elem.hasClass('target') || target.length == 0)
			target = elem;

		/* target an attribute */
		var target_attr = target.attr('target');

		if (target_attr) {
			var attrs = target_attr.split(',');
			var content = false;
			for (var i = 0; i < attrs.length; i ++) {
				var attr = attrs[i];
				if (attr == "class")
					target.addClass(data);
				else if (attr == "content")
					content = true;
				else
					target.attr(attr, data);
			}

			target.removeAttr("target");

			if (!content)
				return;
		}

		/* target an element */
		if (html)
			target.html(html);
		else
			target.text(data);
	},

	/* Render static browser information */
	renderInfo: function (browser, elem) {
		var bindings = {};
		for (var k in browser.select) {
			$('.query-' + k, elem).text(browser.select[k]);
		}
	},

	/* Render pagination controls */
	renderPagination: function (browser, elem) {
		var current_page = browser.offset === undefined
			? "0 (realtime)"
			: Math.floor(browser.offset / browser.limit) + (browser.offset % browser.limit == 0 ? 1 : 0);
		var total_pages = browser.count == ".."
			? "many more"
			: Math.floor(browser.count / browser.limit) + (browser.count % browser.limit == 0 ? 0 : 1);
		MailEv.Browser.renderField($('.current-page', elem), 'current_page', current_page);
		MailEv.Browser.renderField($('.total-pages', elem), 'total_pages', total_pages);
	},

	/* Push Client message handler */
	handleMessage: function (browser, msg) {
		/* Remove the loading element */
		if (browser.loading) {
			MailEv.Browser.removePart(browser, 'loading');
			browser.loading = 0;
		}

		if (typeof msg.data.length == "number") {
			/* This is the query result read from the storage */

			/* Check whether this is the last event */
			browser.last = msg.data.length < browser.limit;

			/* Remove the extra event */
			if (!browser.last)
				msg.data.pop();

			if (browser.pagination &&
				(msg.data[0].count == 0 || browser.offset > msg.data[0].count) ||
				msg.data.length == 0)
			{
				/* The query was empty */
				MailEv.Browser.handleEmpty(browser);
				return;
			}

			if (browser.pagination) {
				browser.count = msg.data[0].count;
			}

			/* Show the navigation elements */
			MailEv.Browser.attachControls(browser);
			/* Unpack the events and handle them one by one */
			for (var i = msg.data.length; i > 0; i--) {
				MailEv.Browser.handleEvent(browser, msg.data[i - 1]);
			}
		} else {
			/* This is the a realtime event */
			MailEv.Browser.handleEvent(browser, msg.data);
			/* No longer empty */
			MailEv.Browser.removePart(browser, 'empty');
		}
	},

	/* MailEv Client error handler */
	handleError: function (browser, err) {
		/* TODO */
	},

	/* Handle a single mail event */
	handleEvent: function (browser, ev) {
		/* Render every field of the event template */
		var tevent = browser.template.event.clone();

		for (var i = 0, field; field = MailEv.fields[i++];) {
			if (typeof ev[field] != "undefined") {
				var tfield = $('.' + field, tevent);
				if (tfield.length)
					MailEv.Browser.renderField(tfield, field, ev[field], ev);
			}
		}

		/* Render "extra" fields */
		if (typeof ev.extra == "object") {
			for (var field in ev.extra) {
				if (typeof ev.extra[field] != "undefined" && ev.extra[field].length) {
					var tfield = $('.extra.' + field, tevent);
					if (tfield.length)
						MailEv.Browser.renderField(tfield, field, ev.extra[field], ev);
				}
			}
		}

		/* Prepend the event to browser element target */
		var target = browser.target.event;
		var children = target.children();
		var length = children.length;

		/* Don't overflow the limit */
		if (length == browser.limit)
			$(children[length - 1]).remove();
	
		target.prepend(tevent);
	},

	/* Handle an empty result */
	handleEmpty: function (browser) {
		MailEv.Browser.loadPart(browser, 'empty');
	},

	/* Handle next click */
	handleNext: function (browser) {
		var offset = browser.offset;

		if (browser.last && offset !== undefined)
			return false;

		if (offset === undefined)
			offset = 0;
		else
			offset += browser.limit;

		browser.elem.attr('offset', offset);
		MailEv.Browser.attach(browser.elem);
	},

	/* Handle prev click */
	handlePrev: function (browser) {
		var offset = browser.offset;

		if (offset === undefined)
			return false;
		else if (offset == 0)
			offset = undefined;
		else
			offset = Math.max(0, offset - browser.limit);

		if (offset === undefined)
			browser.elem.removeAttr('offset');
		else
			browser.elem.attr('offset', offset);
		MailEv.Browser.attach(browser.elem);
	},
	
	done: true
};

$(MailEv.Browser.init);
