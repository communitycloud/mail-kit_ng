var TrackResetPasswordEmail = {
	server: null,

	status: function (message, type, tooltip) {
		var status = $('#track-status');

		if (status.length == 0)
		{
			status = $(document.body).append(
				'<div id="track-status" style="       \
				position: absolute;                   \
				background-color: infobackground;     \
				color: infotext;                      \
				top: 0;                               \
				width: 300px;                         \
				left: 50%;                            \
				margin-left: -150px;                  \
				padding: 3px;                         \
				"><span class="message"></span>       \
				<span class="tooltip" style="         \
				cursor: help;                         \
				">(<b>?</b>)</span></div>').children(':last-child');
		}

		var msg = $('.message', status);
		var tip = $('.tooltip', status);

		if (message) {
			msg.attr('class', 'message ' + type);
			msg.text(message);
			if (tooltip) {
				tip.attr('title', tooltip);
				tip.show();
			} else {
				tip.hide();
			}
			status.show();
		}
		else
			status.hide();
	},

	submit: function () {
		var self = TrackResetPasswordEmail;
		var url = 'http://' + location.hostname + '/api.axd';
		var address = $('#email').val();

		// by Scott Gonzalez: http://projects.scottsplayground.com/email_address_validation/
		var valid = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i;
		if (!valid.test(address)) {
			self.status("Email address is invalid", "error");
			return false;
		}

		var request = {
			type: 'POST',
			url: url,
			data: { email: address, operation: "reset_pwd" },
			dataType: "json",
			success: function (r) {
				self.status("Waiting for mail events", "loading");

				var client = new Push.Client(self.server);

				var query = { routing: "mail-ev", select: {} };
				query.select.list_id = r["namespace"];
				query.select.msg_id = r["token-id"];
				query.select.address = address;

				client.msghandler = self.process;
				client.connect(query);
			},
			error: function () {
				self.status("Request failed", "error");
			}
		};

		self.status("Sending the HTTP request", "loading");
		$.ajax(request);

		return false;
	},

	process: function (msg) {
		var self = TrackResetPasswordEmail;
		var ev = msg.data;
		if (ev.status)
		{
			var tooltip = ev.status == "sent" ? null : ev.comment;
			self.status("Password recovery email "+ev.status+"!", ev.status, tooltip);
			this.disconnect();
		}
	},

	init: function (server) {
		var send_button = $('input[value=Send]');
		TrackResetPasswordEmail.server = server;
		send_button.click(TrackResetPasswordEmail.submit);
	}
};
