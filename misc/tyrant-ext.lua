function mailev_ext_version()
	return 0.02
end

function mailev_increment_stat(key, stat)
	local stream = _get(key)
	local counters = {}
	local found = false

	if stream then
		counters = _split(stream)
	end

	for k, v in pairs(counters) do
		if v == stat then
			local c = tonumber(counters[k+1]) or 0
			counters[k+1] = tostring(c+1)
			found = true
			break
		end
	end

	if not found then
		counters[#counters+1] = tostring(stat)
		counters[#counters+1] = tostring(1)
	end

	return _put(key, table.concat(counters, '\0'))
end
