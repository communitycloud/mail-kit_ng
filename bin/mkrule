#!/usr/bin/perl

use strict;
use warnings;

use App::Message;
use MailKit::Rule qw(mk_match_rule mk_list_rules);
use Util::Email::Render qw(render_mime_message);
use Util::File qw(find_in_path);
use Util::String qw(utf8_encode);
use MIME::Parser;
use Getopt::Long;

sub parse($)
{
	require MIME::Parser;
	my $parser = MIME::Parser->new;
	$parser->extract_nested_messages(0);
	$parser->output_to_core(1);
	$parser->parse_open($_[0] || '-');
}

sub find_rule($)
{
	my $rule = shift;
	my $path = \@MailKit::Rule::path;
	find_in_path($rule, $path)
		or error("couldn't find %q in path (%s)",
			$rule, join(', ', @{$path}));
}

sub main()
{
	my ($help);

	Getopt::Long::Configure("pass_through");
	GetOptions(
		"h|help" => \$help,
	) or usage_error();

	if ($help) {
		help();
		exit();
	}

	my $action = shift_argv()
		or usage_error("no action specified");

	if ($action eq "render") {
		my $mime = parse(shift_argv());
		print utf8_encode(render_mime_message($mime));
	} elsif ($action eq "match") {
		my ($rule, $filename) = shift_argv(1, 2);
		exit !mk_match_rule($rule, parse($filename));
	} elsif ($action eq "count") {
		my ($rule, $filename) = shift_argv(1, 2);
		my $count = grep { mk_match_rule($rule, $_) }
			parse($filename)->parts;
		print "$count\n";
	} elsif ($action eq "grep") {
		my $negate = @ARGV && $ARGV[0] eq '-v'
			? shift @ARGV : 0;
		my ($rule, @files) = shift_argv(1, 0);
		for my $file (@files) {
			my $match = mk_match_rule($rule, parse($file));
			print "$file\n"
				if $negate ? !$match : $match;
		}
	} elsif ($action eq "dump") {
		MailKit::Rule->parse(find_rule(shift_argv(1)))->dump;
	} elsif ($action eq "list") {
		print "$_\n" for mk_list_rules();
	} elsif ($action eq "lint") {
		my $errors;
		for my $rule (@ARGV ? @ARGV : mk_list_rules()) {
			eval { MailKit::Rule->parse(find_rule($rule)) };
			$errors ++ if $@;
			printf "%-25s: %s\n",
				$rule, $@ ? error_string() : 'ok';
		}
		exit($errors ? 1 : 0);
	} else {
		usage_error("invalid action: $action");
	}
}

sub help()
{
	print "Usage: $program_name [options] <action> [arguments..]\n";
	print "\n";
	print "Options\n";
	print "  -h, --help         Display this help\n";
	print "\n";
	print "Test a MailKit rule against input:\n";
	print "  $program_name match <rule> [input]\n";
	print "Count a MailKit rule against each input's parts:\n";
	print "  $program_name count <rule> [input]\n";
	print "Print every matching file against rule:\n";
	print "  $program_name grep [-v] <rule> <files..>\n";
	print "Render a MIME message to standard output:\n";
	print "  $program_name render [filename]\n";
	print "Dump rule definition to standard output:\n";
	print "  $program_name dump <rule>\n";
	print "Check rule or all rules for errors:\n";
	print "  $program_name lint [rule..]\n";
	print "\n";
}

main();
