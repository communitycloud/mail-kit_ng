#!/usr/bin/perl

use strict;
use warnings;

use App::Message;
use App::Daemon_ng qw(:all);
use App::Yield qw(app_yield);
use Getopt::Long qw(:config no_ignore_case bundling);
use IO::Handle;
use Sys::Hostname;
use Tail::DB qw(tail_db_open);
use MailEv::Event qw(mailev_to_text);
use MailEv::Parser qw(:all);
use MailEv::Storage qw(storage_open);
use Util::List qw(list_intersect);
use Util::String qw(parse_uri);
use Fcntl qw(:flock);
use JSON::Events;

my $version = "0.20";

my ($output, $output_json, $input_file, $input_lineno, $input_year);
my ($cursor);
my (@ignored_relays, @domains, %in_ignored_relays, %in_domains, $queue_lifetime);
my ($replay_mode, $replay_delay, $replay_max_delay);
my ($skip_lines, $max_lines, $max_events);
my ($verbose, $show_progress);

my $outgoing = {};
my $stats = { map { $_ => 0 }
	qw(outgoing sent expired deferred bounced acknowledged) };

my $persist_file;
my $storage;

sub replay_delay($)
{
	my $time = shift;

	unless (defined $replay_delay) {
		$replay_delay = $time - time;
		return;
	}

	my $delay = ($time - time) - $replay_delay;


	if ($replay_max_delay && $delay > $replay_max_delay) {
		$delay = $replay_max_delay;
	}

    select(undef, undef, undef, $delay * $replay_mode) if $delay > 0;
}

sub normalize_event($$)
{
	my ($smtp, $qmgr) = @_;
	return {
		domain  => $qmgr->{domain},
		address => $smtp->{address},
		sender => $qmgr->{address},
		status  => $smtp->{status},
		comment => $smtp->{comment},
		timestamp => $smtp->{-time},
		server => $qmgr->{-hostname},
	}
}

sub handle_outgoing($)
{
	my ($tok) = @_;

	parse_syslog_time($tok);

	my $qmgr = delete $outgoing->{$tok->{qid}};
	my $ev = normalize_event($tok, $qmgr);

	if ($output || $output_json || $storage) {
		output_outgoing_event($ev) if $output;
		store_outgoing_event($ev) if $storage;
		output_outgoing_event_json($ev) if $output_json;
	} else {
		print_outgoing_event($ev);
	}

	$stats->{outgoing} ++;
	$stats->{$tok->{status}} ++;
}
 
sub print_outgoing_event($)
{
	my $ev = shift;
	clear_progress_line() if $show_progress;

	if ($ev->{batch_id}) {
		printf("%-20s | %-8s | %-8s -> %s: %s\n",
			$ev->{domain}, $ev->{batch_id}, $ev->{member_id}, $ev->{address}, $ev->{status});
	} elsif ($ev->{list_id}) {
		printf("%-20s | %-8s | %-8s -> %s: %s\n",
			$ev->{domain}, $ev->{list_id}, $ev->{msg_id}, $ev->{address}, $ev->{status});
	} else {
		printf("%-20s | %-8s -> %s: %s\n",
			$ev->{domain}, $ev->{sender}, $ev->{address}, $ev->{status});
	}

	if ($verbose) {
		require Text::Wrap;
		print Text::Wrap::wrap("\t# ", "\t# ",
			"Timestamp: ", scalar localtime $ev->{timestamp}, "\n",
			"Comment:   ", $ev->{comment}, "\n");
	}
}

sub output_outgoing_event($)
{
	my $ev = shift;

	my $text = mailev_to_text($ev);
	$output->write($text);
}

sub output_outgoing_event_json($)
{
	my $ev = shift;

	my %json_ev = map { $_ => $ev->{$_} } grep { defined $ev->{$_} }
		qw(list_id msg_id batch_id member_id sender address status domain comment);

	$output_json->put("outbound.postfix.logs", _timestamp => $ev->{timestamp}, %json_ev);
}

sub store_outgoing_event($)
{
	my $ev = shift;
	$storage->put($ev);
}

sub norm_queue_lifetime($)
{
	my $s = shift;

	if (!defined $s && -x "/usr/sbin/postconf") {
		$s = qx"/usr/sbin/postconf -h maximal_queue_lifetime";
		chomp($s);
	}

	return undef unless defined $s;
	my ($n, $units) = $s =~ /^(\d+)([smhdw])?$/
		or return undef;
	$units ||= 'd';

	return $n * {
		s => 1,
		m => 60,
		h => 60 * 60,
		d => 24 * 3600,
		w => 24 * 3600 * 7,
	}->{$units};
}

sub open_progress(;$)
{
	my $filename = shift;
	unless ($filename) {
		open PROGRESS, '>&', STDERR
			or error("Cannot dup STDOUT: $!");
		PROGRESS->autoflush(1);
		return;
	}

	unless (-p $filename) {
		POSIX::mkfifo($filename, 0640)
			or error("Cannot create progress FIFO: $!");
	}

	open PROGRESS, '+>', $filename
		or error("Cannot open progress FIFO: $!");
	PROGRESS->blocking(0);
}

sub progress_line()
{
	print PROGRESS "\r" if -t PROGRESS;
	printf PROGRESS "%s:%7d [qsize:%d ev:%d sent:%d dfr:%d bnc:%d exp:%d]",
		$input_file, $input_lineno, scalar keys %{$outgoing},
			$stats->{outgoing},
			$stats->{sent},
			$stats->{deferred},
			$stats->{bounced},
			$stats->{expired};
	unless (-t PROGRESS) {
		print PROGRESS "\n";
		PROGRESS->flush();
	}
}

sub clear_progress_line()
{
	print PROGRESS "\r\e[K" if -t PROGRESS;
}

sub is_new_in_queue($)
{
	my $tok = shift;

	unless (
		$tok->{command} eq 'qmgr' && parse_postfix_command($tok) &&
		$tok->{comment} && $tok->{comment} eq 'queue active' &&
		$tok->{direction} eq 'in')
	{
		return 0;
	}

	if (@domains) {
		return 0 unless exists $in_domains{$tok->{domain}};
	}

	return 1;
}

sub is_outgoing($)
{
	my $tok = shift;
	return unless $tok->{command} eq 'smtp';
	parse_postfix_queue_id($tok) && exists $outgoing->{$tok->{qid}}
		or return 0;
	parse_postfix_attrs($tok) && parse_postfix_address($tok)
		or return 0;
	if (@ignored_relays && parse_postfix_relay($tok) &&
		exists($in_ignored_relays{$tok->{relay_ip}}))
	{
		delete $outgoing->{$tok->{qid}};
		return 0;
	}
	return 1;
}

sub is_expired($)
{
	my $tok = shift;
	return unless $queue_lifetime;
	return unless $tok->{command} eq 'smtp' || $tok->{command} eq 'qmgr';
	parse_postfix_queue_id($tok) && exists $outgoing->{$tok->{qid}}
		or return 0;
	parse_postfix_command($tok) && $tok->{status} && $tok->{status} eq 'deferred'
		or return 0;
	parse_postfix_delays($tok)
		or return 0;
	return $tok->{delay} >= $queue_lifetime;
}

sub process($)
{
	$input_file = shift;

	info("processing input from `$input_file' (%s)",
		$input_file =~ m#^tail:# ? 'tail db' :
		$input_file eq '-' ? 'standard input' :
		-p $input_file ? 'named FIFO' :
		$input_file =~ /\.gz$/ ? 'compressed file' :
		$input_file =~ /^journal:/ ? 'system journal' :
		'regular file');

	my ($tail_db, @journal, $journal_poll);

	if ($input_file =~ /^tail:/) {
		$tail_db = tail_db_open($input_file);
		$input_file = $tail_db->{file}; # for current year
	} elsif ($input_file =~ /^journal:/) {
		my $c = parse_uri($input_file)
			or error('failed to parse journal uri: %s', $input_file);
		$journal_poll = $c->{args}->{poll};
		@journal = qw(journalctl -o json);
		unless ($journal_poll) {
			push @journal, qw(--follow --no-tail);
			push @journal, ('--after-cursor', $cursor)
				if defined $cursor;
		}
		push @journal, ('--directory', $c->{path}) if defined $c->{path};
	} elsif ($input_file eq '-') {
		open (INPUT, '<&', *STDIN)
			or error("couldn't dup STDIN: $!");
	} elsif ($input_file =~ /\.gz$/) {
		open INPUT, '-|', 'gzip', '-d', '-c', '--', $input_file
			or error("couldn't pipe gzip: $!");
	} else {
		my $mode = -p $input_file ? '+<' : '<';
		open INPUT, $mode, $input_file
			or error("couldn't open input `$input_file': $!");
	}

	my $plain_file = !($tail_db || @journal);

	if ($plain_file && -p INPUT && !flock (INPUT, LOCK_EX|LOCK_NB)) {
		error("input pipe locked: $input_file");
	}

	MailEv::Parser::set_current_year($input_year ||
		$plain_file && -p INPUT ? undef : (stat($input_file))[9]);

	$replay_delay = undef;
	$input_lineno = 0;

	if ($tail_db) {
		while (defined (my $rec = $tail_db->read)) {
			process_line($rec);
			app_yield();
		}

		$tail_db->close();
	} elsif ($journal_poll) {
		while (1) {
			app_yield();

			debug('read from cursor %s', $cursor);
			my $pid = open my $journal, '-|', @journal, ($cursor ? ('--after-cursor', $cursor) : ())
				or error('failed to execute journalctl: %s', $!);
			while (my $line = <$journal>) {
				process_line($line, 't');
				app_yield();
			}
			close($journal)
				or warning('failed to close journalctl pipe: %s', $!);

			sleep 1;
		}
	} elsif (@journal) {
		my $pid = open my $journal, '-|', @journal
			or error('failed to execute journalctl: %s', $!);

		my $kill = sub { kill INT => $pid };

		daemon_handler(
			sig_terminate => $kill,
			sig_reload => $kill
		);

		while (my $line = <$journal>) {
			process_line($line, 't');
			app_yield();
		}

		close($journal);

	} else {
		while (my $line = <INPUT>) {
			process_line($line);
			app_yield();
		}

		close(INPUT);
	}
}

sub process_line($;$)
{
	my ($line, $is_journal) = @_;

	$input_lineno++;

	if (defined($skip_lines) && --$skip_lines)
	{
		return 0;
	}

	undef $skip_lines;

	if (defined($max_lines) && --$max_lines == 0)
	{
		finish();
	}

	my $tok = parse_new($line);

	if (!$is_journal) {
		parse_syslog($tok)
			or return 0;

		parse_postfix($tok)
			or return 0;
	} else {
		parse_journal($tok)
			or return 0;
		$cursor = $tok->{-cursor};

		return 0 unless $tok->{-type};
	}

	if ($replay_mode && parse_syslog_time($tok))
	{
		replay_delay($tok->{-time});
	}

	if (is_new_in_queue($tok))
	{
		$outgoing->{$tok->{qid}} = $tok;
	}
	elsif (is_expired($tok))
	{
		$tok->{status} = "expired";
		handle_outgoing($tok);
	}
	elsif (is_outgoing($tok))
	{
		handle_outgoing($tok);
	}

	progress_line() if $show_progress;

	if (defined($max_events) && defined($stats->{outgoing}) && $stats->{outgoing} >= $max_events)
	{
		finish(0);
	}

	return 1;
}

sub persist_store()
{
	require Storable;
	my $state = {
		outgoing => $outgoing,
		stats    => $stats,
		version  => $version,
		$cursor ? (cursor => $cursor) : (),
	};
	unless (Storable::store($state, $persist_file))
	{
		warning("failed to store data in $persist_file: $!");
	}
}

sub persist_restore()
{
	require Storable;
	return unless -f $persist_file;
	my $state = eval { Storable::retrieve($persist_file) } || {};

	if ($@) {
		warning("error while retrieving persist data from `$persist_file': $@");
		return;
	}

	my $version_info = $state->{version} eq $version ? "same version"
		: "version $state->{version} -> $version";

	$outgoing = $state->{outgoing} if $state->{outgoing};
	$stats = $state->{stats} if $state->{stats};
	$cursor = $state->{cursor} if $state->{cursor};

	info("loaded persist data from `%s' queue_size=%d ($version_info)",
		$persist_file, scalar keys %{$outgoing});
}

sub finish(;$)
{
	my $ecode = shift || 0;

	if ($show_progress) {
		print PROGRESS "\n";
	}

	if ($persist_file) {
		persist_store();
	}

	info("Bye!");
	exit($ecode);
}

sub on_reload
{
	if ($persist_file) {
		persist_store();
	}

	if (defined (my $fd = fileno(INPUT))) {
		my $mode = -p INPUT ? '+<&=' : '<&=';
		open (STDIN, $mode, $fd)
			or error("failed to redirect STDIN to fd=$fd: $!");
	}
}

sub on_terminate
{
	finish();
}

sub main()
{
	my ($help, $output_file, $json_events, $progress_fifo,
		$storage_uri, @input, $print_cursor);

	daemon_handler(
		terminate => \&on_terminate,
		reload => \&on_reload,
	);

	daemon_init();

	GetOptions(
		"output|o=s"        => \$output_file,
		"json-events|j=s"   => \$json_events,
		"replay|r:f"        => \$replay_mode,
		"max-delay=f"       => \$replay_max_delay,
		"skip-lines|l=i"    => \$skip_lines,
		"max-lines|n=i"     => \$max_lines,
		"max-events|M=i"    => \$max_events,
		"verp-delimiter=s"  => \$MailEv::Parser::verp_delimiter,
		"ignore-relay|R=s"  => \@ignored_relays,
		"queue-lifetime=s"  => \$queue_lifetime,
		"persist-file=s"    => \$persist_file,
		"print-cursor"      => \$print_cursor,
		"domains|d=s"       => \@domains,
		"progress|P"        => \$show_progress,
		"progress-fifo=s"   => \$progress_fifo,
		"year=i"            => \$input_year,
		"storage=s"         => \$storage_uri,
		"verbose|v"         => \$verbose,
		"help|h"            => \$help,
	) or usage_error();

	if ($help) {
		help();
		exit();
	}

	if ($print_cursor) {
		persist_restore();
		print $cursor;
		exit();
	}

	%in_ignored_relays = map { $_ => 1 } @ignored_relays;
	%in_domains = map { $_ => 1 } @domains;
	$queue_lifetime = norm_queue_lifetime($queue_lifetime);

	if (defined $replay_mode && $replay_mode == 0) {
		$replay_mode = 1;
	}

	if ($show_progress || $progress_fifo) {
		$show_progress ++;
		open_progress($progress_fifo);
	}

	if ($persist_file) {
		persist_restore();
	}

	if ($storage_uri) {
		$storage = storage_open($storage_uri);
	}

	unless (@ARGV) {
		usage_error("no arguments given and standard input is not redirected.")
			if -t STDIN;
		@input = ('-');
	} else {
		@input = @ARGV;
	}

	usage_error('postfix queue lifetime not defined')
		unless $queue_lifetime;

	daemon_apply();

	if ($output_file) {
		$output = tail_db_open($output_file, "write");
	}

	if ($json_events) {
		$output_json = JSON::Events->new(($json_events eq 1 || $json_events eq 'true')
			? undef : $json_events);
	}

	for my $file (@input)
	{
		process($file);
		app_yield();
	}

	app_yield();
	finish();
}

sub help()
{
	print "Usage: $program_name [OPTION]... [FILE]...\n";
	print "\n";
	print "Parse Posfix logs and translate them into mail events. Either\n";
	print "file arguments may be specified, or standard input redirected,\n";
	print "but will refuse to read input from the terminal.\n";
	print "\n";
	print "Parser options:\n";
	print "  -o, --output=FILE         append the text representation of events to FILE\n";
	print "  -j, --json-events[=FILE]  write events in new JSON format to FILE or default path\n";
	print "  -r, --replay[=RATIO]      replay the log delays (at optional ratio)\n";
	print "      --max-delay=NUM       don't delay more than NUM seconds\n";
	print "  -l, --skip-lines=NUM      skip the first NUM lines\n";
	print "  -n, --max-lines=NUM       don't parse more that NUM lines\n";
	print "  -M, --max-events=NUM      stop after sending NUM events\n";
	print "  -R, --ignore-relay=IP     exclude messages relayed through IP\n";
	print "  -d, --domain=NAME         domain name to consider; may repeat\n";
	print "      --verp-delimiter=CHAR specify the VERP delimiter\n";
	print "      --queue-lifetime=NUM  maximal queue lifetime\n";
	print "      --persist-file=FILE   store session data across program runs\n";
	print "      --print-cursor        print journal cursor store in state file\n";
	print "  -P, --progress            show progress in status line\n";
	print "      --year=NUM            assume input is from year NUM\n";
	print "      --progress-fifo=FIFO  write the progress to FIFO instead of STDERR\n";
	print "  -v, --verbose             increase report verbosity\n";
	print "\n";
	print "Miscellaneous options:\n";
	print "      --storage=URI         store the events to specified URI\n";
	print "  -h, --help                display this help\n";
	print "\n";
	daemon_help();
}

main();
